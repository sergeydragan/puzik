package
{

import com.kekkoneko.core.config.CoreInitializer;
import com.kekkoneko.core.loader.LocalizationLoader;
import com.kekkoneko.puzik.config.PuzikInitializer;

import flash.display.Sprite;
import flash.events.Event;

import org.swiftsuspenders.Injector;

public class Main extends Sprite
{
    public static const injector:Injector = new Injector();

    public function Main()
    {
        if (stage)
        {
            init();
        }
        else
        {
            addEventListener(Event.ADDED_TO_STAGE, init);
        }
    }

    private function init(event:Event = null):void
    {
        removeEventListener(Event.ADDED_TO_STAGE, init);

        //var debugBuildNumberLoader:BuildNumberLoader = new BuildNumberLoader("version.txt", loadLocalization);
        loadLocalization();
    }

    private function loadLocalization():void
    {
        var localizationLoader:LocalizationLoader = new LocalizationLoader("ru", continueInit);
    }

    private function continueInit():void
    {
        var coreInitializer:CoreInitializer = new CoreInitializer(injector, stage);
        coreInitializer.initCore();
        var puzikInitializer:PuzikInitializer = new PuzikInitializer(injector, this);
        puzikInitializer.initPuzik();
    }
}
}
