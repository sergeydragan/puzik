/**
 * Created by SeeD on 26/10/2014.
 */
package com.kekkoneko.puzik.gameplay.quests.vos
{
public class QuestVO
{
    public var id:int;
    public var name:String;
    public var description:String;
    [Transient]
    public var subtasks:Vector.<QuestSubtaskVO>;
    public var minLevel:int;
    public var requiredQuestId:int;

    // что касается вознаграждения
    /** XP, даваемые за выполнение*/
    public var rewardXP:int = 0;
    /** Деньги, даваемые за выполнение*/
    public var rewardMoney:int = 0;
    /** Донат, даваемый за выполнение*/
    public var rewardHC:int = 0;
    /** Объект, даваемый за выполнение*/
    public var rewardGameObject:int = 0;

    public function QuestVO()
    {
        subtasks = new <QuestSubtaskVO>[];
    }

    public function get isComplete():Boolean
    {
        if (!subtasks || subtasks.length <= 0) return true;

        for (var i:int = 0; i < subtasks.length; i++)
        {
            if (subtasks[i].countCompleted < subtasks[i].countTotal)
            {
                return false;
            }
        }
        return true;
    }
}
}
