/**
 * Created by SeeD on 26/10/2014.
 */
package com.kekkoneko.puzik.gameplay.quests.vos
{
public class QuestSubtaskVO
{
    public var id:int;
    /** к какому квесту относится */
    public var questId:int;

    public var description:String;
    /** сколько уже набрано */
    public var countCompleted:int = 0;

    /** сколько нужно набрать */
    public var countTotal:int;
    /** Например, купить предмет, собрать дроп, пойти в гости, покормить малыша */
    public var actionType:int;
    /** Например, id предмета или дропа; в некоторых случаях вообще не требуется */
    public var actionTarget:String;
    /** Стоимость пропуска подзадания в HC */
    public var skipCostHC:int = 0;

    public function QuestSubtaskVO()
    {
    }

    [Transient]
    public function get isComplete():Boolean
    {
        return countCompleted >= countTotal;
    }
}
}
