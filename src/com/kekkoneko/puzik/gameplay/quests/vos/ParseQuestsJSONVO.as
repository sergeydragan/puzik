/**
 * Created by sdragan on 27.01.2015.
 */
package com.kekkoneko.puzik.gameplay.quests.vos
{
public class ParseQuestsJSONVO
{
    public var json:String;
    public var callback:Function;

    public function ParseQuestsJSONVO(questsJson:String, callback:Function = null)
    {
        this.json = questsJson;
        this.callback = callback;
    }
}
}
