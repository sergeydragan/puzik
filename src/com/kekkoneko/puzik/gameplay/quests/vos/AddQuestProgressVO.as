/**
 * Created by sdragan on 29.01.2015.
 */
package com.kekkoneko.puzik.gameplay.quests.vos
{
public class AddQuestProgressVO
{
    public var subtaskType:int;
    public var subtaskTarget:String;
    public var count:int;

    public function AddQuestProgressVO(subtaskType:int, subtaskTarget:String, count:int = 1)
    {
        this.subtaskType = subtaskType;
        this.subtaskTarget = subtaskTarget;
        this.count = count;
    }
}
}
