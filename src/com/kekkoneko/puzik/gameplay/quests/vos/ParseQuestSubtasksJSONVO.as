/**
 * Created by sdragan on 27.01.2015.
 */
package com.kekkoneko.puzik.gameplay.quests.vos
{
public class ParseQuestSubtasksJSONVO
{
    public var json:String;
    public var callback:Function;

    public function ParseQuestSubtasksJSONVO(subtasksJson:String, callback:Function = null)
    {
        this.json = subtasksJson;
        this.callback = callback;
    }
}
}
