/**
 * Created by SeeD on 26/10/2014.
 */
package com.kekkoneko.puzik.gameplay.quests.model
{
import com.kekkoneko.puzik.gameplay.quests.vos.QuestSubtaskVO;
import com.kekkoneko.puzik.gameplay.quests.vos.QuestVO;

import flash.utils.Dictionary;

public class QuestsModel
{
    private var _quests:Dictionary;
    private var _activeQuestIds:Vector.<int>;

    public function QuestsModel()
    {
        _activeQuestIds = new <int>[];
        _quests = new Dictionary();
    }

    public function addQuests(quests:Vector.<QuestVO>):void
    {
        for each (var quest:QuestVO in quests)
        {
            addQuest(quest);
        }
    }

    public function addQuest(quest:QuestVO):void
    {
        _quests[quest.id] = quest;
    }

    public function getQuestById(id:int):QuestVO
    {
        return (_quests[id]);
    }

    public function getActiveQuests(playerLevel:int):Vector.<QuestVO>
    {
        var vectorToReturn:Vector.<QuestVO> = new Vector.<QuestVO>();
        for each (var currentQuest:QuestVO in _quests)
        {
            if ((currentQuest.minLevel > playerLevel) || (currentQuest.requiredQuestId > 0 && !(_quests[currentQuest.requiredQuestId] as QuestVO).isComplete))
            {
                continue;
            }
            vectorToReturn.push(currentQuest);
            addQuestToActiveQuests(currentQuest.id);
        }
        return vectorToReturn;
    }

    public function addSubtaskToQuest(subtask:QuestSubtaskVO):void
    {
        var quest:QuestVO = getQuestById(subtask.questId);
        if (quest.subtasks.indexOf(subtask) < 0)
        {
            quest.subtasks.push(subtask);
        }
    }

    public function getActiveQuestIds():Vector.<int>
    {
        return _activeQuestIds;
    }

    public function removeQuestFromActiveQuests(questId:int):void
    {
        if (_activeQuestIds.indexOf(questId) >= 0)
        {
            _activeQuestIds.splice(_activeQuestIds.indexOf(questId), 1);
        }
    }

    public function addQuestToActiveQuests(questId:int):void
    {
        if (_activeQuestIds.indexOf(questId) < 0)
        {
            _activeQuestIds.push(questId);
        }
    }
}
}
