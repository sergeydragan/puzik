/**
 * Created by sdragan on 29.01.2015.
 */
package com.kekkoneko.puzik.gameplay.quests.constants
{
public class QuestActionTypes
{
    public static const REACH_LEVEL:int = 0;
    public static const GATHER_MONEY:int = 1;
    public static const SPEND_MONEY:int = 2;
    public static const BUY_GAME_OBJECT:int = 3;
    public static const SELL_GAME_OBJECT:int = 4;

    public function QuestActionTypes()
    {
    }
}
}
