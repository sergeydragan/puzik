/**
 * Created by sdragan on 29.01.2015.
 */
package com.kekkoneko.puzik.gameplay.quests.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.core.debug.Debug;
import com.kekkoneko.puzik.gameplay.quests.model.QuestsModel;
import com.kekkoneko.puzik.gameplay.quests.vos.AddQuestProgressVO;
import com.kekkoneko.puzik.gameplay.quests.vos.QuestSubtaskVO;
import com.kekkoneko.puzik.gameplay.quests.vos.QuestVO;

public class AddQuestProgressCommand extends Command
{
    [Inject]
    public var questsModel:QuestsModel;

    public function AddQuestProgressCommand()
    {
        super();
    }

    override public function execute():void
    {
        var vo:AddQuestProgressVO = _payload as AddQuestProgressVO;
        var activeQuestIds:Vector.<int> = questsModel.getActiveQuestIds();
        var quest:QuestVO;
        var atLeastOneSubtaskIsCompleted:Boolean;
        var questWasUpdated:Boolean;

        for (var i:int = 0; i < activeQuestIds.length; i++)
        {
            quest = questsModel.getQuestById(activeQuestIds[i]);

            if (quest.isComplete)
            {
                Debug.error("This shouldn't happen here. Completed quest aren't supposed to be in activeQuests vector");
            }

            atLeastOneSubtaskIsCompleted = false;
            questWasUpdated = false;

            for each (var subtask:QuestSubtaskVO in quest.subtasks)
            {
                if (vo.subtaskType == subtask.actionType && vo.subtaskTarget == subtask.actionTarget && subtask.countCompleted < subtask.countTotal)
                {
                    subtask.countCompleted += vo.count;
                    if (subtask.countCompleted >= subtask.countTotal)
                    {
                        subtask.countCompleted = subtask.countTotal;
                        atLeastOneSubtaskIsCompleted = true;
                    }
                    questWasUpdated = true;
                }
            }

            if (questWasUpdated)
            {
                if (quest.isComplete)
                {
                    //todo: send "quest finished" message to server
                }
                else
                {
                    //todo: do "well, at least this quest was updated" stuff (maybe send message to server?)

                    if (atLeastOneSubtaskIsCompleted)
                    {
                        //todo: show notification on quest panel or something similar
                    }
                }
            }

        }
    }
}
}
