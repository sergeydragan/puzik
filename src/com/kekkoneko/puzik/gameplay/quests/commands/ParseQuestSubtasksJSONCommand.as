/**
 * Created by sdragan on 27.01.2015.
 */
package com.kekkoneko.puzik.gameplay.quests.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.puzik.gameplay.quests.model.QuestsModel;
import com.kekkoneko.puzik.gameplay.quests.vos.ParseQuestSubtasksJSONVO;
import com.kekkoneko.puzik.gameplay.quests.vos.QuestSubtaskVO;
import com.kekkoneko.puzik.gameplay.quests.vos.QuestVO;

import org.osflash.vanilla.extract;

public class ParseQuestSubtasksJSONCommand extends Command
{
    [Inject]
    public var questsModel:QuestsModel;

    public function ParseQuestSubtasksJSONCommand()
    {
    }

    override public function execute():void
    {
        var vo:ParseQuestSubtasksJSONVO = _payload as ParseQuestSubtasksJSONVO;
        var json:String = vo.json;

        var subtasks:Vector.<QuestSubtaskVO> = extract(JSON.parse(json), Vector.<QuestSubtaskVO> as Class);

        for (var i:int = 0; i < subtasks.length; i++)
        {
            var questVO:QuestVO = questsModel.getQuestById(subtasks[i].questId);
            if (!questVO.subtasks)
            {
                questVO.subtasks = new <QuestSubtaskVO>[];
            }
            questVO.subtasks.push(subtasks[i]);
        }

        if (vo.callback)
        {
            vo.callback();
        }
    }
}
}
