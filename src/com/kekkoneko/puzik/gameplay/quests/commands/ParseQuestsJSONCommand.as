/**
 * Created by sdragan on 27.01.2015.
 */
package com.kekkoneko.puzik.gameplay.quests.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.puzik.gameplay.quests.model.QuestsModel;
import com.kekkoneko.puzik.gameplay.quests.vos.ParseQuestsJSONVO;
import com.kekkoneko.puzik.gameplay.quests.vos.QuestVO;

import org.osflash.vanilla.extract;

public class ParseQuestsJSONCommand extends Command
{
    [Inject]
    public var questsModel:QuestsModel;

    public function ParseQuestsJSONCommand()
    {
    }

    override public function execute():void
    {
        var vo:ParseQuestsJSONVO = _payload as ParseQuestsJSONVO;
        var json:String = vo.json;

        var quests:Vector.<QuestVO> = extract(JSON.parse(json), Vector.<QuestVO> as Class);
        questsModel.addQuests(quests);

        if (vo.callback)
        {
            vo.callback();
        }
    }
}
}
