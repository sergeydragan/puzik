/**
 * Created by SeeD on 15/03/2015.
 */
package com.kekkoneko.puzik.gameplay.room.model
{
import com.kekkoneko.puzik.gameplay.player.constants.PlayerIDs;
import com.kekkoneko.puzik.gameplay.room.vos.RoomVO;

import flash.utils.Dictionary;

public class RoomsModel
{
    private var _rooms:Dictionary;

    public function RoomsModel()
    {
        _rooms = new Dictionary();
    }

    public function addRoom(room:RoomVO, playerId:int = PlayerIDs.OWN_PLAYER_ID):void
    {
        if (!_rooms[playerId])
        {
            _rooms[playerId] = new Dictionary();
        }
        _rooms[playerId][room.id] = room;
    }

    public function getRoomById(roomId:int, playerId:int = PlayerIDs.OWN_PLAYER_ID):RoomVO
    {
        return _rooms[playerId][roomId];
    }
}
}
