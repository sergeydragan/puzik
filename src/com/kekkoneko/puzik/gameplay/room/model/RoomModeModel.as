/**
 * Created by SeeD on 04/10/2014.
 */
package com.kekkoneko.puzik.gameplay.room.model
{
import com.kekkoneko.core.eventdispatcher.CoreEvent;
import com.kekkoneko.puzik.gameplay.characters.model.CharacterModel;
import com.kekkoneko.puzik.gameplay.objects.constants.GameObjectClasses;
import com.kekkoneko.puzik.gameplay.objects.model.InventoryModel;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;
import com.kekkoneko.puzik.gameplay.room.vos.CharacterAddedVO;
import com.kekkoneko.puzik.gameplay.room.vos.RoomVO;

import flash.events.EventDispatcher;

public class RoomModeModel extends EventDispatcher
{
    [Inject]
    public var inventoryModel:InventoryModel;

    public var roomWidth:int = 10;

    private var _roomVO:RoomVO;
    private var _characterModels:Vector.<CharacterModel>;

    public static const CHARACTER_ADDED:String = "CharacterAdded";

    public function RoomModeModel()
    {
        _characterModels = new <CharacterModel>[];
    }

    public function getWallpapers():InventoryObjectVO
    {
        return getFloorOrWallpaper(GameObjectClasses.WALLPAPERS);
    }

    public function getFloors():InventoryObjectVO
    {
        return getFloorOrWallpaper(GameObjectClasses.FLOORS);
    }

    private function getFloorOrWallpaper(classId:int):InventoryObjectVO
    {
        var inventoryObjects:Vector.<InventoryObjectVO> = inventoryModel.getInventoryObjectsByClassId(classId);
        for (var i:int = 0; i < inventoryObjects.length; i++)
        {
            if (inventoryObjects[i].roomId == _roomVO.id && inventoryObjects[i].isInWorld)
            {
                return inventoryObjects[i];
            }
        }
        return null;
    }

    public function getPlaceableObjects():Vector.<InventoryObjectVO>
    {
        return inventoryModel.getPlaceableObjects();
    }

    public function addCharacterModel(charModel:CharacterModel):void
    {
        _characterModels.push(charModel);
        dispatchEvent(new CoreEvent(CHARACTER_ADDED, new CharacterAddedVO(charModel)));
    }

    public function update():void
    {
        for each (var currentCharacter:CharacterModel in _characterModels)
        {
            currentCharacter.update();
        }
    }

    public function cleanUp():void
    {
        for each (var currentCharacter:CharacterModel in _characterModels)
        {
            currentCharacter.cleanUp();
        }
    }

    public function get roomVO():RoomVO
    {
        return _roomVO;
    }

    public function set roomVO(value:RoomVO):void
    {
        _roomVO = value;
    }
}
}
