/**
 * Created by SeeD on 23/11/2014.
 */
package com.kekkoneko.puzik.gameplay.room.constants
{
public class Directions
{
    public static const NONE:int = 0;
    public static const LEFT:int = 1;
    public static const RIGHT:int = 2;
    public static const UP:int = 3;
    public static const DOWN:int = 4;

    public function Directions()
    {
    }
}
}
