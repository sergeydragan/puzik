/**
 * Created by SeeD on 15/03/2015.
 */
package com.kekkoneko.puzik.gameplay.room.helpers
{
public class RoomsHelper
{
    public function RoomsHelper()
    {
    }

    public static function getMaxAmountOfRoomsByPlayerLevel(playerLevel:int):int
    {
        if (playerLevel <= 9)
        {
            return 3;
        }
        else if (playerLevel <= 15)
        {
            return 6;
        }
        else
        {
            return 12;
        }
    }
}
}
