/**
 * Created by SeeD on 15/03/2015.
 */
package com.kekkoneko.puzik.gameplay.room.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.panel.vos.ClosePanelVO;
import com.kekkoneko.core.screen.DisplayLayersModel;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.display.constants.PuzikLayers;
import com.kekkoneko.puzik.gameplay.characters.constants.CharacterTypes;
import com.kekkoneko.puzik.gameplay.characters.model.CharactersModel;
import com.kekkoneko.puzik.gameplay.characters.vos.AddCharacterVO;
import com.kekkoneko.puzik.gameplay.locations.models.LocationsModel;
import com.kekkoneko.puzik.gameplay.room.model.RoomModeModel;
import com.kekkoneko.puzik.gameplay.room.model.RoomsModel;
import com.kekkoneko.puzik.gameplay.room.view.RoomModeView;
import com.kekkoneko.puzik.gameplay.room.vos.RoomVO;
import com.kekkoneko.puzik.gameplay.room.vos.SwitchCurrentRoomVO;
import com.kekkoneko.puzik.panel.constants.PuzikPanelNames;

public class SwitchCurrentRoomCommand extends Command
{
    [Inject]
    public var locationsModel:LocationsModel;

    [Inject]
    public var roomsModel:RoomsModel;

    [Inject]
    public var displayLayersModel:DisplayLayersModel;

    [Inject]
    public var charactersModel:CharactersModel;

    public function SwitchCurrentRoomCommand()
    {
    }

    override public function execute():void
    {
        var vo:SwitchCurrentRoomVO = _payload as SwitchCurrentRoomVO;

        coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.ACTION_SELECTION_PANEL));
        coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.CHARACTER_ACTION_SELECTION_PANEL));

        locationsModel.destroyCurrentRoom();
        charactersModel.clearCharacters();

        var roomModeModel:RoomModeModel = new RoomModeModel();
        var roomVO:RoomVO = roomsModel.getRoomById(vo.roomId);
        injector.injectInto(roomModeModel);
        roomModeModel.roomVO = roomVO;
        var roomView:RoomModeView = new RoomModeView(roomModeModel);
        injector.injectInto(roomView);
        locationsModel.registerRoom(roomModeModel, roomView);
        displayLayersModel.getLayer(PuzikLayers.GAMEPLAY).addChild(roomView);

        // todo: придумать как-то получше, куда будут деваться персонажи при переходах между комнатами
        coreEventDispatcher.broadcastMessage(PuzikCommands.ADD_CHARACTER, new AddCharacterVO(CharacterTypes.MOM, 1, true));
        coreEventDispatcher.broadcastMessage(PuzikCommands.ADD_CHARACTER, new AddCharacterVO(CharacterTypes.BABY, 3));
        coreEventDispatcher.broadcastMessage(PuzikCommands.ADD_CHARACTER, new AddCharacterVO(CharacterTypes.DAD, 5));
    }
}
}
