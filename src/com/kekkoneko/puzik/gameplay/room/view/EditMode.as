/**
 * Created by SeeD on 09/11/2014.
 */
package com.kekkoneko.puzik.gameplay.room.view
{
import com.greensock.TweenLite;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.eventdispatcher.CoreEventDispatcher;
import com.kekkoneko.core.panel.vos.ClosePanelVO;
import com.kekkoneko.core.panel.vos.ShowPanelPropertiesVO;
import com.kekkoneko.core.screen.ScreenModel;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.gameplay.objects.constants.Rows;
import com.kekkoneko.puzik.gameplay.objects.view.InventoryObjectView;
import com.kekkoneko.puzik.gameplay.objects.vos.SetInventoryObjectVO;
import com.kekkoneko.puzik.gameplay.room.model.RoomModeModel;
import com.kekkoneko.puzik.panel.constants.PuzikPanelNames;
import com.kekkoneko.puzik.panel.panels.inventoryObjectEdit.vos.InventoryObjectEditPanelPropertiesVO;

import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.filters.GlowFilter;

public class EditMode
{
    public var isActive:Boolean;

    private var _roomView:RoomModeView;
    private var _roomModel:RoomModeModel;
    private var _mainLayer:Sprite;
    private var _objectViews:Vector.<InventoryObjectView>;

    private var _draggedObject:InventoryObjectView;
    private var _selectedObject:InventoryObjectView;

    private var _objectOffsetX:int;
    private var _objectOffsetY:int;

    private var _currentDraggedObjectCellX:int;
    private var _currentDraggedObjectCellY:int;

    [Inject]
    public var coreEventDispatcher:CoreEventDispatcher;

    [Inject]
    public var screenModel:ScreenModel;

    public function EditMode(roomView:RoomModeView, roomModel:RoomModeModel, mainLayer:Sprite, objectViews:Vector.<InventoryObjectView>)
    {
        _roomView = roomView;
        _roomModel = roomModel;
        _mainLayer = mainLayer;
        _objectViews = objectViews;

        isActive = false;
    }

    public function activate(inventoryObjectView:InventoryObjectView = null):void
    {
        trace("Activating edit mode");
        isActive = true;

        for (var i:int = 0; i < _objectViews.length; i++)
        {
            _objectViews[i].addEventListener(MouseEvent.CLICK, onObjectMouseClick);
            _objectViews[i].addEventListener(MouseEvent.MOUSE_DOWN, onObjectMouseDown);
        }
        _mainLayer.stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);

        if (inventoryObjectView)
        {
            selectInventoryObjectView(inventoryObjectView);
        }
    }

    private function onObjectMouseClick(event:MouseEvent):void
    {
        var invObj:InventoryObjectView = event.currentTarget as InventoryObjectView;

        if (invObj != _selectedObject)
        {
            selectInventoryObjectView(invObj);
        }
    }

    private function selectInventoryObjectView(inventoryObjectView:InventoryObjectView):void
    {
        coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.INVENTORY_OBJECT_EDIT_PANEL));
        _selectedObject = inventoryObjectView;
        coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_PANEL, new ShowPanelPropertiesVO(PuzikPanelNames.INVENTORY_OBJECT_EDIT_PANEL, "", screenModel.getMouseCoords(), new InventoryObjectEditPanelPropertiesVO(_selectedObject.inventoryObjectVO)));
    }

    private function onObjectMouseDown(event:MouseEvent):void
    {
        if (event.currentTarget != _selectedObject)
        {
            return;
        }

        _objectOffsetX = (event.currentTarget as InventoryObjectView).mouseX;
        _objectOffsetY = (event.currentTarget as InventoryObjectView).mouseY;

        (event.currentTarget as InventoryObjectView).filters = [];
        coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.INVENTORY_OBJECT_EDIT_PANEL));
        startDrag(event.currentTarget as InventoryObjectView);
    }

    private function onMouseMove(event:MouseEvent):void
    {
        var calculatedX:int = Math.floor((_mainLayer.mouseX - _objectOffsetX) / RoomModeView.CELL_WIDTH);
        var calculatedY:int = Math.floor((_mainLayer.mouseY - _objectOffsetY) / RoomModeView.CELL_HEIGHT);

        if (calculatedX != _currentDraggedObjectCellX || calculatedY != _currentDraggedObjectCellY)
        {
            _currentDraggedObjectCellX = calculatedX;
            _currentDraggedObjectCellY = getObjectCellY(_draggedObject, calculatedY);

            _draggedObject.x = _currentDraggedObjectCellX * RoomModeView.CELL_WIDTH;
            _draggedObject.y = _currentDraggedObjectCellY * RoomModeView.CELL_HEIGHT;

            var canBePlaced:Boolean = objectCanBePlaced(_draggedObject, _currentDraggedObjectCellX, _currentDraggedObjectCellY);
            if (canBePlaced)
            {
                _draggedObject.filters = [new GlowFilter(0x00FF00)];
            }
            else
            {
                _draggedObject.filters = [new GlowFilter(0xFF0000)];
            }
        }
    }

    private function objectCanBePlaced(object:InventoryObjectView, cellX:int, cellY:int):Boolean
    {
        var leftX:int = _currentDraggedObjectCellX;
        var rightX:int = _currentDraggedObjectCellX + object.inventoryObjectVO.gameObjectVO.width;
        var topY:int = _currentDraggedObjectCellY - object.inventoryObjectVO.gameObjectVO.height;
        var bottomY:int = _currentDraggedObjectCellY;

        // check for overlap with other inventory objects
        for each (var invObj:InventoryObjectView in _objectViews)
        {
            if (invObj == _draggedObject)
            {
                continue;
            }

            var otherLeftX:int = invObj.inventoryObjectVO.x;
            var otherRightX:int = invObj.inventoryObjectVO.x + invObj.inventoryObjectVO.gameObjectVO.width;
            var otherTopY:int = invObj.inventoryObjectVO.y - invObj.inventoryObjectVO.gameObjectVO.height;
            var otherBottomY:int = invObj.inventoryObjectVO.y;

            if ((otherLeftX >= leftX && otherLeftX < rightX) || (otherRightX > leftX && otherRightX <= rightX))
            {
                if ((otherBottomY <= bottomY && otherBottomY > topY) || (otherTopY < bottomY && otherTopY >= topY))
                {
                    return false;
                }
            }
        }
        return true;
    }

    private function getObjectCellY(object:InventoryObjectView, calculatedY:int):int
    {
        if (object.inventoryObjectVO.gameObjectVO.row == Rows.ON_THE_FLOOR)
        {
            return 4;
        }
        else
        {
            return calculatedY;
        }
    }

    private function returnObjectToItsPlace(inventoryObjectView:InventoryObjectView):void
    {
        TweenLite.to(inventoryObjectView, 0.3, {
            x:inventoryObjectView.inventoryObjectVO.x * RoomModeView.CELL_WIDTH,
            y:inventoryObjectView.inventoryObjectVO.y * RoomModeView.CELL_HEIGHT
        });
    }

    public function onMouseUp(event:MouseEvent):void
    {
        if (_draggedObject)
        {
            stopDrag();
        }
    }

    private function startDrag(objectToDrag:InventoryObjectView):void
    {
        _draggedObject = objectToDrag;

        _currentDraggedObjectCellX = _draggedObject.inventoryObjectVO.x;
        _currentDraggedObjectCellY = _draggedObject.inventoryObjectVO.y;

        if (_mainLayer.stage)
        {
            _mainLayer.stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
            _mainLayer.stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
        }
    }

    private function stopDrag():void
    {
        if (_mainLayer.stage)
        {
            _mainLayer.stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
            _mainLayer.stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
        }

        if (objectCanBePlaced(_draggedObject, _currentDraggedObjectCellX, _currentDraggedObjectCellY))
        {
            coreEventDispatcher.broadcastMessage(PuzikCommands.SET_INVENTORY_OBJECT, new SetInventoryObjectVO(_draggedObject.inventoryObjectVO, _currentDraggedObjectCellX, _currentDraggedObjectCellY, true, _roomModel.roomVO.id));
            coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_PANEL, new ShowPanelPropertiesVO(PuzikPanelNames.INVENTORY_OBJECT_EDIT_PANEL, "", screenModel.getMouseCoords(), new InventoryObjectEditPanelPropertiesVO(_draggedObject.inventoryObjectVO)));
        }
        else
        {
            returnObjectToItsPlace(_draggedObject);
            _selectedObject = null;
        }
        _draggedObject.filters = [];
        _draggedObject = null;
    }

    public function exit():void
    {
//        trace("Exiting edit mode");
        isActive = false;

        for (var i:int = 0; i < _objectViews.length; i++)
        {
            _objectViews[i].removeEventListener(MouseEvent.CLICK, onObjectMouseClick);
            _objectViews[i].removeEventListener(MouseEvent.MOUSE_DOWN, onObjectMouseDown);
        }
        if (_mainLayer.stage)
        {
            _mainLayer.stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
            _mainLayer.stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseUp);
        }
        _selectedObject = null;
        _draggedObject = null;
        coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.INVENTORY_OBJECT_EDIT_PANEL));
    }
}
}
