/**
 * Created by SeeD on 05/10/2014.
 */
package com.kekkoneko.puzik.gameplay.room.view
{
import com.greensock.TweenLite;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.debug.Debug;
import com.kekkoneko.core.eventdispatcher.CoreEvent;
import com.kekkoneko.core.eventdispatcher.CoreEventDispatcher;
import com.kekkoneko.core.panel.vos.ClosePanelVO;
import com.kekkoneko.core.screen.Screen;
import com.kekkoneko.core.screen.ScreenModel;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.controller.notifications.EditInventoryObjectNotification;
import com.kekkoneko.puzik.controller.notifications.EndInventoryObjectEditNotification;
import com.kekkoneko.puzik.controller.notifications.InventoryObjectUpdatedNotification;
import com.kekkoneko.puzik.controller.notifications.RoomAppearanceUpdatedNotification;
import com.kekkoneko.puzik.gameplay.characters.model.CharacterModel;
import com.kekkoneko.puzik.gameplay.characters.orders.constants.InteractionTypes;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.AddOrderToActiveCharacterVO;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.OrderVO;
import com.kekkoneko.puzik.gameplay.characters.view.CharacterView;
import com.kekkoneko.puzik.gameplay.characters.vos.ProcessCharacterSelectionVO;
import com.kekkoneko.puzik.gameplay.objects.constants.GameObjectClasses;
import com.kekkoneko.puzik.gameplay.objects.constants.Rows;
import com.kekkoneko.puzik.gameplay.objects.helpers.ObjectAppearanceHelper;
import com.kekkoneko.puzik.gameplay.objects.view.InventoryObjectView;
import com.kekkoneko.puzik.gameplay.objects.vos.GameObjectVO;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;
import com.kekkoneko.puzik.gameplay.objects.vos.ProcessInventoryObjectSelectionVO;
import com.kekkoneko.puzik.gameplay.room.constants.Directions;
import com.kekkoneko.puzik.gameplay.room.model.RoomModeModel;
import com.kekkoneko.puzik.gameplay.room.vos.CharacterAddedVO;
import com.kekkoneko.puzik.gameplay.room.vos.SwitchCurrentRoomVO;
import com.kekkoneko.puzik.gameplay.shop.vos.InitiateSellObjectVO;
import com.kekkoneko.puzik.panel.constants.PuzikPanelNames;

import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.filters.GlowFilter;
import flash.ui.Keyboard;

public class RoomModeView extends Sprite
{
    [Inject]
    public var coreEventDispatcher:CoreEventDispatcher;

    [Inject]
    public var screenModel:ScreenModel;

    private var _model:RoomModeModel;
    private var _wallpapersVector:Vector.<DisplayObject>;
    private var _floorsVector:Vector.<DisplayObject>;
    private var _objectViews:Vector.<InventoryObjectView>;

    private var _characterViews:Vector.<CharacterView>;

    private var _floorAndWallsLayer:Sprite;
    private var _objectsLayer:Sprite;
    private var _charactersLayer:Sprite;
    private var _mainLayer:Sprite;

    private var _editMode:EditMode;

    private var _slidingDirection:int = Directions.NONE;

    private var _scrollButtonLeft:Sprite;
    private var _scrollButtonRight:Sprite;

    public static const CELL_WIDTH:int = 100;
    public static const CELL_HEIGHT:int = 120;
    public static const CHARACTERS_DEFAULT_Y:int = 540;
    public static const SLIDE_STEP:int = 10;

    public function RoomModeView(roomModel:RoomModeModel)
    {
        _model = roomModel;

        _characterViews = new <CharacterView>[];
        _wallpapersVector = new <DisplayObject>[];
        _floorsVector = new <DisplayObject>[];
        _objectViews = new <InventoryObjectView>[];

        _mainLayer = new Sprite();
        _floorAndWallsLayer = new Sprite();
        _objectsLayer = new Sprite();
        _charactersLayer = new Sprite();

        _scrollButtonLeft = new Btn_RoomScroll();
        _scrollButtonRight = new Btn_RoomScroll();

        _scrollButtonRight.x = Screen.width - _scrollButtonRight.width * 0.5;
        _scrollButtonRight.y = Screen.height - _scrollButtonRight.height * 0.5;

        _scrollButtonLeft.scaleX = -1;
        _scrollButtonLeft.x = _scrollButtonLeft.width * 0.5;
        _scrollButtonLeft.y = Screen.height - _scrollButtonLeft.height * 0.5;

        _scrollButtonLeft.buttonMode = _scrollButtonRight.buttonMode = true;
        _scrollButtonLeft.useHandCursor = _scrollButtonRight.useHandCursor = true;

        addChild(_mainLayer);
        _mainLayer.addChild(_floorAndWallsLayer);
        _mainLayer.addChild(_objectsLayer);
        _mainLayer.addChild(_charactersLayer);

        addChild(_scrollButtonLeft);
        addChild(_scrollButtonRight);

        redrawFloorsAndWallpapers();
        initObjects();

        _editMode = new EditMode(this, _model, _mainLayer, _objectViews);
        Main.injector.injectInto(_editMode);
        // yes, it's ugly, but the deadline clock is ticking

        addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
        addEventListener(Event.ENTER_FRAME, onEnterFrame);

        _model.addEventListener(RoomModeModel.CHARACTER_ADDED, onCharacterAdded);

        addObjectListeners();
        _floorAndWallsLayer.addEventListener(MouseEvent.CLICK, onFloorClick);

        _scrollButtonLeft.addEventListener(MouseEvent.MOUSE_DOWN, onScrollButtonMouseDown);
        _scrollButtonRight.addEventListener(MouseEvent.MOUSE_DOWN, onScrollButtonMouseDown);

        validateScrollButtonsVisibility();
    }

    private function onScrollButtonMouseDown(event:MouseEvent):void
    {
        const target:DisplayObject = event.currentTarget as DisplayObject;
        const direction:int = (target == _scrollButtonLeft) ? Directions.LEFT : Directions.RIGHT;

        _slidingDirection = direction;

        addScrollCompleteListeners(target);
    }

    private function onFloorClick(event:MouseEvent):void
    {
        coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.ACTION_SELECTION_PANEL));
        coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.CHARACTER_ACTION_SELECTION_PANEL));

        if (event.stageY >= CELL_HEIGHT * 4)
        {
            trace("Clicked on floor");
            var orderVO:OrderVO = new OrderVO();
            orderVO.interactionType = InteractionTypes.MOVE_TO_X;
            orderVO.coordsX = _floorAndWallsLayer.mouseX / CELL_WIDTH;
            orderVO.isIdle = true;
            coreEventDispatcher.broadcastMessage(PuzikCommands.ADD_ORDER_TO_ACTIVE_CHARACTER, new AddOrderToActiveCharacterVO(orderVO));
        }
    }

    private function onEditInventoryObjectNotification(event:CoreEvent):void
    {
        var inventoryObjectVO:InventoryObjectVO = (event.body as EditInventoryObjectNotification).inventoryObjectVO;
        var inventoryObjectView:InventoryObjectView;
        for (var i:int = 0; i < _objectViews.length; i++)
        {
            if (_objectViews[i].inventoryObjectVO.id == inventoryObjectVO.id)
            {
                inventoryObjectView = _objectViews[i];
                break;
            }
        }
        if (!inventoryObjectView)
        {
            Debug.error("Selected inventoryObjectView is null. That shouldn't happen.");
        }
        // coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_PANEL, new ShowPanelPropertiesVO(PuzikPanelNames.INVENTORY_OBJECT_EDIT_PANEL, "", screenModel.getMouseCoords(), new InventoryObjectEditPanelPropertiesVO(inventoryObjectVO)));
        removeObjectListeners();
        _editMode.activate(inventoryObjectView);
    }

    private function onInventoryObjectUpdatedNotification(event:CoreEvent):void
    {
        var inventoryObjectVO:InventoryObjectVO = (event.body as InventoryObjectUpdatedNotification).inventoryObjectVO;

        var classId:int = inventoryObjectVO.gameObjectVO.classId;
        if (classId == GameObjectClasses.FLOORS || classId == GameObjectClasses.WALLPAPERS) return;

        var inventoryObjectView:InventoryObjectView = getInventoryObjectViewByInventoryObjectVO(inventoryObjectVO);

        if (!inventoryObjectView)
        {
            inventoryObjectView = new InventoryObjectView(inventoryObjectVO);
            placeObject(inventoryObjectView);
            addListenersToObject(inventoryObjectView);
            _objectViews.push(inventoryObjectView);
        }
        else
        {
            if ((event.body as InventoryObjectUpdatedNotification).inWorldHasChanged)
            {
                removeInventoryObjectViewFromRoom(inventoryObjectView);
            }
            else
            {
                inventoryObjectView.x = inventoryObjectView.inventoryObjectVO.x * CELL_WIDTH;
                inventoryObjectView.y = inventoryObjectView.inventoryObjectVO.y * CELL_HEIGHT;
            }
        }
        rearrangeObjects();
    }

    private function onEndInventoryObjectEditNotification(event:CoreEvent):void
    {
        _editMode.exit();
        addObjectListeners();
    }

    private function onSellInventoryObjectInitiated(event:CoreEvent):void
    {
        var inventoryObjectVO:InventoryObjectVO = (event.body as InitiateSellObjectVO).inventoryObjectVO;
        var inventoryObjectView:InventoryObjectView = getInventoryObjectViewByInventoryObjectVO(inventoryObjectVO);

        if (inventoryObjectView)
        {
            removeInventoryObjectViewFromRoom(inventoryObjectView);
        }
    }

    private function onRoomAppearanceUpdatedNotification(event:CoreEvent):void
    {
        redrawFloorsAndWallpapers();
    }

    private function getInventoryObjectViewByInventoryObjectVO(inventoryObjectVO:InventoryObjectVO):InventoryObjectView
    {
        var inventoryObjectView:InventoryObjectView;
        for (var i:int = 0; i < _objectViews.length; i++)
        {
            if (_objectViews[i].inventoryObjectVO.id == inventoryObjectVO.id)
            {
                inventoryObjectView = _objectViews[i];
                break;
            }
        }
        return inventoryObjectView;
    }

    private function removeInventoryObjectViewFromRoom(inventoryObjectView:InventoryObjectView):void
    {
        if (inventoryObjectView.parent)
        {
            inventoryObjectView.parent.removeChild(inventoryObjectView);
        }
        inventoryObjectView.removeEventListener(MouseEvent.CLICK, onObjectMouseClick);

        _objectViews.splice(_objectViews.indexOf(inventoryObjectView), 1);

        if (_objectViews.length == 0 && _editMode.isActive)
        {
            _editMode.exit();
            addObjectListeners();
        }
    }

    private function onObjectMouseClick(event:MouseEvent):void
    {
        coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.ACTION_SELECTION_PANEL));
        coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.CHARACTER_ACTION_SELECTION_PANEL));
        var inventoryObjectView:InventoryObjectView = event.currentTarget as InventoryObjectView;
        coreEventDispatcher.broadcastMessage(PuzikCommands.PROCESS_INVENTORY_OBJECT_SELECTION, new ProcessInventoryObjectSelectionVO(inventoryObjectView));
    }

    private function onCharacterMouseClick(event:MouseEvent):void
    {
        var characterView:CharacterView = event.currentTarget as CharacterView;
        coreEventDispatcher.broadcastMessage(PuzikCommands.PROCESS_CHARACTER_SELECTION, new ProcessCharacterSelectionVO(characterView));
    }

    private function onCharacterAdded(e:CoreEvent):void
    {
        var characterModel:CharacterModel = (e.body as CharacterAddedVO).characterModel;
        var characterView:CharacterView = new CharacterView(characterModel);
        characterView.initHelpers();
        _charactersLayer.addChild(characterView);
        characterView.x = CELL_WIDTH * characterModel.cellX;
        characterView.y = CHARACTERS_DEFAULT_Y;
        _characterViews.push(characterView);

        characterView.addEventListener(MouseEvent.CLICK, onCharacterMouseClick);
    }

    private function onAddedToStage(event:Event):void
    {
        removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
        stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
        stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);

        coreEventDispatcher.addEventListener(InventoryObjectUpdatedNotification.INVENTORY_OBJECT_UPDATED, onInventoryObjectUpdatedNotification);
        coreEventDispatcher.addEventListener(EditInventoryObjectNotification.EDIT_INVENTORY_OBJECT, onEditInventoryObjectNotification);
        coreEventDispatcher.addEventListener(EndInventoryObjectEditNotification.END_INVENTORY_OBJECT_EDIT, onEndInventoryObjectEditNotification);
        coreEventDispatcher.addEventListener(PuzikCommands.INITIATE_SELL_OBJECT, onSellInventoryObjectInitiated);
        coreEventDispatcher.addEventListener(RoomAppearanceUpdatedNotification.ROOM_APPEARANCE_UPDATED, onRoomAppearanceUpdatedNotification);
    }

    private function onKeyDown(e:KeyboardEvent):void
    {
        if (e.keyCode == Keyboard.LEFT)
        {
            _slidingDirection = Directions.LEFT;
        }
        else if (e.keyCode == Keyboard.RIGHT)
        {
            _slidingDirection = Directions.RIGHT;
        }
        else if (e.keyCode == Keyboard.NUMBER_1)
        {
            coreEventDispatcher.broadcastMessage(PuzikCommands.SWITCH_CURRENT_ROOM, new SwitchCurrentRoomVO(1));
        }
        else if (e.keyCode == Keyboard.NUMBER_2)
        {
            coreEventDispatcher.broadcastMessage(PuzikCommands.SWITCH_CURRENT_ROOM, new SwitchCurrentRoomVO(2));
        }
        else if (e.keyCode == Keyboard.NUMBER_3)
        {
            coreEventDispatcher.broadcastMessage(PuzikCommands.SWITCH_CURRENT_ROOM, new SwitchCurrentRoomVO(3));
        }
    }

    private function onKeyUp(e:KeyboardEvent):void
    {
        _slidingDirection = Directions.NONE;
    }

    private function addScrollCompleteListeners(target:DisplayObject):void
    {
        target.addEventListener(MouseEvent.MOUSE_UP, onScrollComplete);
        target.addEventListener(MouseEvent.ROLL_OUT, onScrollComplete);
    }

    private function removeScrollCompleteListeners(target:DisplayObject):void
    {
        target.removeEventListener(MouseEvent.MOUSE_UP, onScrollComplete);
        target.removeEventListener(MouseEvent.ROLL_OUT, onScrollComplete);
    }

    private function removeScrollButtonsListeners():void
    {
        removeScrollCompleteListeners(_scrollButtonLeft);
        removeScrollCompleteListeners(_scrollButtonRight);

        _scrollButtonRight.removeEventListener(MouseEvent.MOUSE_DOWN, onScrollButtonMouseDown);
        _scrollButtonLeft.removeEventListener(MouseEvent.MOUSE_DOWN, onScrollButtonMouseDown);
    }

    private function onScrollComplete(event:MouseEvent):void
    {
        removeScrollCompleteListeners(event.target as DisplayObject);
        _slidingDirection = Directions.NONE;
    }

    private function onEnterFrame(e:Event):void
    {
        _model.update();

        if (_slidingDirection != Directions.NONE)
        {
            scrollRoom(_slidingDirection);
            validateScrollButtonsVisibility(true);
        }

        updateCharacters();
    }

    private function validateScrollButtonsVisibility(useTween:Boolean = false):void
    {
        if (_mainLayer.x == Screen.width - _model.roomWidth * CELL_WIDTH)
            hideScrollButton(_scrollButtonRight, useTween);
        else
            showScrollButton(_scrollButtonRight, useTween);

        if (_mainLayer.x == 0)
            hideScrollButton(_scrollButtonLeft, useTween);
        else
            showScrollButton(_scrollButtonLeft, useTween);
    }

    private function showScrollButton(scrollButton:Sprite, useTween:Boolean):void
    {
        if (scrollButton.visible) return;
        if (!useTween) {
            scrollButton.alpha = 1;
            scrollButton.visible = true;
        } else {
            const durationInSec : Number = 0.5 * (1 - scrollButton.alpha);
            scrollButton.visible = true;
            TweenLite.to(scrollButton, durationInSec, {alpha:1});
        }
    }

    private function hideScrollButton(scrollButton:Sprite, useTween:Boolean):void
    {
        if (!scrollButton.visible) return;
        if (!useTween) {
            scrollButton.alpha = 0;
            scrollButton.visible = false;
        } else {
            const durationInSec : Number = 0.5 * (scrollButton.alpha);
            TweenLite.to(scrollButton, durationInSec, {alpha:0, visible: false});
        }
    }

    private function updateCharacters():void
    {
        for each (var currentCharacter:CharacterView in _characterViews)
        {
            currentCharacter.x = CELL_WIDTH * currentCharacter.characterModel.cellX;
        }
    }

    private function scrollRoom(direction:int):void
    {
        var displacement:int = 0;
        if (direction == Directions.LEFT)
        {
            displacement = SLIDE_STEP;
        }
        else if (direction == Directions.RIGHT)
        {
            displacement = -SLIDE_STEP;
        }

        _mainLayer.x += displacement;
        if (_mainLayer.x > 0)
        {
            _mainLayer.x = 0;
        }
        else if (_mainLayer.x < Screen.width - _model.roomWidth * CELL_WIDTH)
        {
            _mainLayer.x = Screen.width - _model.roomWidth * CELL_WIDTH;
        }
    }

    private function redrawFloorsAndWallpapers():void
    {
        _floorAndWallsLayer.removeChildren();

        var floorInventoryObject:GameObjectVO = _model.getFloors().gameObjectVO;
        var wallpaperInventoryObject:GameObjectVO = _model.getWallpapers().gameObjectVO;

        fillRoomWithTiles(floorInventoryObject, CELL_HEIGHT * 4);
        fillRoomWithTiles(wallpaperInventoryObject, 0);

        function fillRoomWithTiles(gameObjectVO:GameObjectVO, tileY:int):void
        {
            if (!gameObjectVO)
            {
                return;
            }
            var appearance:DisplayObject = ObjectAppearanceHelper.getObjectSprite(gameObjectVO);
            var tileWidth:int = appearance.width;
            if (gameObjectVO.classId == GameObjectClasses.FLOORS) {
                tileWidth = 800;
            }
            var roomWidth:int = _model.roomWidth * CELL_WIDTH;
            var tilesCount:int = Math.ceil(roomWidth / tileWidth);
            var horizontalOffset:int = ((tileWidth * tilesCount) - roomWidth) / 2;
            for (var i:int = 0; i < tilesCount; i++)
            {
                if (appearance.parent)
                {
                    appearance = ObjectAppearanceHelper.getObjectSprite(gameObjectVO);
                }
                _floorAndWallsLayer.addChild(appearance);
                appearance.x = -horizontalOffset + i * tileWidth;
                appearance.y = tileY;
            }
        }
    }

    private function initObjects():void
    {
        var objects:Vector.<InventoryObjectVO> = _model.getPlaceableObjects();
        var object:InventoryObjectView;
        for (var i:int = 0; i < objects.length; i++)
        {
            if (!objects[i].isInWorld || objects[i].roomId != _model.roomVO.id)
            {
                continue;
            }
            object = new InventoryObjectView(objects[i]);
            _objectViews.push(object);
            placeObject(object);
        }
    }

    private function placeObject(object:InventoryObjectView):void
    {
        _objectsLayer.addChild(object);
        object.x = object.inventoryObjectVO.x * CELL_WIDTH;
        object.y = object.inventoryObjectVO.y * CELL_HEIGHT;
    }

    private function addObjectListeners():void
    {
        for (var i:int = 0; i < _objectViews.length; i++)
        {
            addListenersToObject(_objectViews[i]);
        }
    }

    private function addListenersToObject(inventoryObjectView:InventoryObjectView):void
    {
        inventoryObjectView.addEventListener(MouseEvent.CLICK, onObjectMouseClick);
        inventoryObjectView.addEventListener(MouseEvent.MOUSE_OVER, onObjectMouseOver);
        inventoryObjectView.addEventListener(MouseEvent.MOUSE_OUT, onObjectMouseOut);
        inventoryObjectView.buttonMode = true;
    }

    private function onObjectMouseOver(event:MouseEvent):void
    {
        var inventoryObjectView:InventoryObjectView = event.currentTarget as InventoryObjectView;
        inventoryObjectView.filters = [new GlowFilter(0xFFFFFF)];
    }

    private function onObjectMouseOut(event:MouseEvent):void
    {
        var inventoryObjectView:InventoryObjectView = event.currentTarget as InventoryObjectView;
        inventoryObjectView.filters = [];
    }

    private function removeObjectListeners():void
    {
        for (var i:int = 0; i < _objectViews.length; i++)
        {
            _objectViews[i].removeEventListener(MouseEvent.CLICK, onObjectMouseClick);
            _objectViews[i].removeEventListener(MouseEvent.MOUSE_OVER, onObjectMouseOver);
            _objectViews[i].removeEventListener(MouseEvent.MOUSE_OUT, onObjectMouseOut);
            _objectViews[i].buttonMode = false;
        }
    }

    private function rearrangeObjects():void
    {
        performRearrange(Rows.ON_THE_FLOOR);

        function performRearrange(rowToSort:int):void
        {
            var objectsInRow:int = 0;
            var objectsArray:Array = [];
            var i:int = 0;
            var u:int = 0;
            var lowestX:int = -1000;
            var lowestObjApp:InventoryObjectView;
            var lastLowestObjApp:InventoryObjectView;

            // определяем количество объектов с таким row
            for (i = 0; i < _objectViews.length; i++)
            {
                if (_objectViews[i].inventoryObjectVO.gameObjectVO.row == rowToSort)
                {
                    objectsArray.push(_objectViews[i]);
                    objectsInRow++;
                }
            }

            objectsArray.sortOn("x", Array.NUMERIC);
            for (i = 0; i < objectsArray.length; i++)
            {
                (objectsArray[i] as InventoryObjectView).parent.addChild(objectsArray[i] as InventoryObjectView);
            }
        }
    }

    public function cleanUp():void
    {
        _editMode.exit();

        coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.ACTION_SELECTION_PANEL));
        coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.CHARACTER_ACTION_SELECTION_PANEL));

        for each (var currentCharacter:CharacterView in _characterViews)
        {
            currentCharacter.cleanUp();
        }

        coreEventDispatcher.removeEventListener(InventoryObjectUpdatedNotification.INVENTORY_OBJECT_UPDATED, onInventoryObjectUpdatedNotification);
        coreEventDispatcher.removeEventListener(EditInventoryObjectNotification.EDIT_INVENTORY_OBJECT, onEditInventoryObjectNotification);
        coreEventDispatcher.removeEventListener(EndInventoryObjectEditNotification.END_INVENTORY_OBJECT_EDIT, onEndInventoryObjectEditNotification);

        stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
        stage.removeEventListener(KeyboardEvent.KEY_UP, onKeyUp);

        removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
        removeEventListener(Event.ENTER_FRAME, onEnterFrame);
        removeObjectListeners();
        removeScrollButtonsListeners();

        _model.removeEventListener(RoomModeModel.CHARACTER_ADDED, onCharacterAdded);
    }
}
}
