/**
 * Created by SeeD on 21/12/2014.
 */
package com.kekkoneko.puzik.gameplay.room.vos
{
import com.kekkoneko.puzik.gameplay.characters.model.CharacterModel;

public class CharacterAddedVO
{
    public var characterModel:CharacterModel;

    public function CharacterAddedVO(characterModel:CharacterModel)
    {
        this.characterModel = characterModel;
    }
}
}
