/**
 * Created by SeeD on 15/03/2015.
 */
package com.kekkoneko.puzik.gameplay.room.vos
{
public class SwitchCurrentRoomVO
{
    public var roomId:int;

    public function SwitchCurrentRoomVO(roomId:int)
    {
        this.roomId = roomId;
    }
}
}
