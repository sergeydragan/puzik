/**
 * Created by SeeD on 25/01/2015.
 */
package com.kekkoneko.puzik.gameplay.shop.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.dialog.vos.ShowDialogPropertiesVO;
import com.kekkoneko.puzik.dialog.constants.PuzikDialogNames;
import com.kekkoneko.puzik.dialog.dialogs.sellObject.SellObjectDialogPropertiesVO;
import com.kekkoneko.puzik.gameplay.objects.model.InventoryModel;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;
import com.kekkoneko.puzik.gameplay.shop.vos.SuggestSellObjectVO;

public class SuggestSellObjectCommand extends Command
{
    [Inject]
    public var inventoryModel:InventoryModel;

    public function SuggestSellObjectCommand()
    {
    }

    override public function execute():void
    {
        var payload:SuggestSellObjectVO = _payload as SuggestSellObjectVO;
        var inventoryObjectVO:InventoryObjectVO = inventoryModel.getInventoryObjectById(payload.inventoryObjectId);

        coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_DIALOG, new ShowDialogPropertiesVO(PuzikDialogNames.SELL_OBJECT, new SellObjectDialogPropertiesVO(inventoryObjectVO)));
    }
}
}
