/**
 * Created by SeeD on 25/01/2015.
 */
package com.kekkoneko.puzik.gameplay.shop.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.core.debug.Debug;
import com.kekkoneko.core.utils.MathUtils;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.display.vos.RemoveOverlayVO;
import com.kekkoneko.puzik.display.vos.ShowOverlayVO;
import com.kekkoneko.puzik.gameplay.objects.model.GameObjectsModel;
import com.kekkoneko.puzik.gameplay.objects.model.InventoryModel;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;
import com.kekkoneko.puzik.gameplay.player.model.PlayersModel;
import com.kekkoneko.puzik.gameplay.player.vos.UpdatePlayerMoneyVO;
import com.kekkoneko.puzik.gameplay.shop.vos.InitiateBuyObjectVO;

public class InitiateBuyObjectCommand extends Command
{
    [Inject]
    public var gameObjectsModel:GameObjectsModel;

    [Inject]
    public var playersModel:PlayersModel;

    [Inject]
    public var inventoryModel:InventoryModel;

    public function InitiateBuyObjectCommand()
    {
    }

    override public function execute():void
    {
        var payload:InitiateBuyObjectVO = _payload as InitiateBuyObjectVO;

        coreEventDispatcher.broadcastMessage(PuzikCommands.SHOW_OVERLAY, new ShowOverlayVO());

        // todo: send server command here
        Debug.info("Sending buy command for object with ID: " + payload.gameObjectVO.id);

        // ONLY FOR TESTING!
        var price:int = payload.gameObjectVO.price;
        var priceHC:int = payload.gameObjectVO.pricePremium;

        var inventoryObjectVO:InventoryObjectVO = new InventoryObjectVO(payload.gameObjectVO);
        inventoryObjectVO.id = MathUtils.randomInt(100, 10000);
        inventoryObjectVO.isInWorld = false;
        inventoryObjectVO.x = 0;
        inventoryObjectVO.y = 0;
        inventoryObjectVO.state = "";
        inventoryModel.addInventoryObjectToInventory(inventoryObjectVO);

        coreEventDispatcher.broadcastMessage(PuzikCommands.REMOVE_OVERLAY, new RemoveOverlayVO());
        coreEventDispatcher.broadcastMessage(PuzikCommands.UPDATE_PLAYER_MONEY, new UpdatePlayerMoneyVO(playersModel.getPlayer().money - price, playersModel.getPlayer().hc - priceHC));
        // =================
    }
}
}
