/**
 * Created by SeeD on 25/01/2015.
 */
package com.kekkoneko.puzik.gameplay.shop.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.core.debug.Debug;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.controller.notifications.InventoryObjectSoldNotification;
import com.kekkoneko.puzik.display.vos.RemoveOverlayVO;
import com.kekkoneko.puzik.display.vos.ShowOverlayVO;
import com.kekkoneko.puzik.gameplay.objects.helpers.ObjectsPriceHelper;
import com.kekkoneko.puzik.gameplay.objects.model.InventoryModel;
import com.kekkoneko.puzik.gameplay.player.model.PlayersModel;
import com.kekkoneko.puzik.gameplay.player.vos.UpdatePlayerMoneyVO;
import com.kekkoneko.puzik.gameplay.shop.vos.InitiateSellObjectVO;

public class InitiateSellObjectCommand extends Command
{
    [Inject]
    public var inventoryModel:InventoryModel;

    [Inject]
    public var playersModel:PlayersModel;

    public function InitiateSellObjectCommand()
    {
    }

    override public function execute():void
    {
        var payload:InitiateSellObjectVO = _payload as InitiateSellObjectVO;

        coreEventDispatcher.broadcastMessage(PuzikCommands.SHOW_OVERLAY, new ShowOverlayVO());

        //todo: send server command here
        Debug.info("Sending sell command for inventoryObject with ID: " + payload.inventoryObjectVO.id);

        // ONLY FOR TESTING!
        var price:int = ObjectsPriceHelper.getObjectSellPrice(payload.inventoryObjectVO.gameObjectVO);
        inventoryModel.deleteInventoryObjectById(payload.inventoryObjectVO.id);
        coreEventDispatcher.broadcastMessage(InventoryObjectSoldNotification.INVENTORY_OBJECT_SOLD, new InventoryObjectSoldNotification(price));
        coreEventDispatcher.broadcastMessage(PuzikCommands.REMOVE_OVERLAY, new RemoveOverlayVO());
        coreEventDispatcher.broadcastMessage(PuzikCommands.UPDATE_PLAYER_MONEY, new UpdatePlayerMoneyVO(playersModel.getPlayer().money + price, playersModel.getPlayer().hc));
        // =================
    }
}
}
