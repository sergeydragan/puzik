/**
 * Created by SeeD on 25/01/2015.
 */
package com.kekkoneko.puzik.gameplay.shop.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.dialog.constants.DialogPriorities;
import com.kekkoneko.core.dialog.vos.ShowDialogPropertiesVO;
import com.kekkoneko.puzik.dialog.constants.PuzikDialogNames;
import com.kekkoneko.puzik.dialog.dialogs.buyObject.BuyObjectDialogPropertiesVO;
import com.kekkoneko.puzik.gameplay.objects.model.GameObjectsModel;
import com.kekkoneko.puzik.gameplay.objects.vos.GameObjectVO;
import com.kekkoneko.puzik.gameplay.shop.vos.SuggestBuyObjectVO;

public class SuggestBuyObjectCommand extends Command
{
    [Inject]
    public var gameObjectsModel:GameObjectsModel;

    public function SuggestBuyObjectCommand()
    {
    }

    override public function execute():void
    {
        var payload:SuggestBuyObjectVO = _payload as SuggestBuyObjectVO;
        var gameObjectVO:GameObjectVO = gameObjectsModel.getObjectById(payload.gameObjectId);

        coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_DIALOG, new ShowDialogPropertiesVO(PuzikDialogNames.BUY_OBJECT, new BuyObjectDialogPropertiesVO(gameObjectVO), DialogPriorities.IMMEDIATE));
    }
}
}
