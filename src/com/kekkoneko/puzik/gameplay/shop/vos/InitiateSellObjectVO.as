/**
 * Created by SeeD on 25/01/2015.
 */
package com.kekkoneko.puzik.gameplay.shop.vos
{
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;

public class InitiateSellObjectVO
{
    public var inventoryObjectVO:InventoryObjectVO;

    public function InitiateSellObjectVO(inventoryObjectVO:InventoryObjectVO)
    {
        this.inventoryObjectVO = inventoryObjectVO;
    }
}
}
