/**
 * Created by SeeD on 25/01/2015.
 */
package com.kekkoneko.puzik.gameplay.shop.vos
{
import com.kekkoneko.puzik.gameplay.objects.vos.GameObjectVO;

public class InitiateBuyObjectVO
{
    public var gameObjectVO:GameObjectVO;

    public function InitiateBuyObjectVO(gameObjectVO:GameObjectVO)
    {
        this.gameObjectVO = gameObjectVO;
    }
}
}
