/**
 * Created by SeeD on 25/01/2015.
 */
package com.kekkoneko.puzik.gameplay.shop.vos
{
public class SuggestBuyObjectVO
{
    public var gameObjectId:int;

    public function SuggestBuyObjectVO(gameObjectId:int)
    {
        this.gameObjectId = gameObjectId;
    }
}
}
