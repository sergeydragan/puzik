/**
 * Created by SeeD on 25/01/2015.
 */
package com.kekkoneko.puzik.gameplay.shop.vos
{
public class SuggestSellObjectVO
{
    public var inventoryObjectId:int;

    public function SuggestSellObjectVO(inventoryObjectId:int)
    {
        this.inventoryObjectId = inventoryObjectId;
    }
}
}
