/**
 * Created by SeeD on 08/02/2015.
 */
package com.kekkoneko.puzik.gameplay.objects.constants
{
public class MoneyConstants
{
    public static const HC_TO_SC_COURSE:int = 20;
    public static const SELL_PRICE_RATIO:Number = 0.5;

    public function MoneyConstants()
    {
    }
}
}
