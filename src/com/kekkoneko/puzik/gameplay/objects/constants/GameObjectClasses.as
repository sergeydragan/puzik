/**
 * Created by SeeD on 05/10/2014.
 */
package com.kekkoneko.puzik.gameplay.objects.constants
{
public class GameObjectClasses
{
    // мебель
    public static const SOFA:int = 1;
    public static const WARDROBE:int = 2;
    public static const CHILD_BED:int = 3;
    public static const PLAYPEN:int = 4;
    public static const CHILD_CHAIR:int = 5;
    public static const CHAIR:int = 6;
    public static const NIGHTSTAND:int = 7;
    public static const KITCHEN_CUPBOARD:int = 8;
    public static const FIREPLACES:int = 9;
    public static const COFFE_TABLES:int = 10;
    public static const CARPETS:int = 11;
    public static const WALL_LIGHTS:int = 12;
    public static const EXTRACTOR_HOOD:int = 13;
    public static const CRADLE:int = 14;
    public static const DIAPER_TABLE:int = 15;		// так себе перевод слова "пеленатор", конечно
    public static const MIRROR:int = 16;

    // гигиена
    public static const BATH:int = 21;
    public static const SINK:int = 22;
    public static const TOILET:int = 23;
    public static const KITCHEN_SINK:int = 24;
    public static const BATHROOM_SHELF:int = 26;

    // всякое
    public static const CARRIAGE:int = 31;

    // подгузники
    public static const DIAPERS:int = 41;

    // мыльные принадлежности
    public static const SOAP:int = 42;

    // горшки
    public static const POTTIES:int = 43;

    // игрушки
    public static const TOYS_BIG:int = 51;
    public static const TOYS_SMALL:int = 52;
    public static const TOYS_BOX:int = 53;
    public static const TOYS_BATH:int = 54;
    public static const TOYS_BIGGEST:int = 55;

    // еда - готовая
    public static const FOOD_0_12_READY:int = 60;

    // еда - ингредиенты
    public static const BOTTLE:int = 65;
    public static const CUTLERY:int = 66;		// это так по-английски "столовые приборы"
    public static const FRUITS_VEGETABLES:int = 67;

    // стиральные машинки
    public static const WASH_MACHINE:int = 71;

    // холодильники
    public static const FRIDGES:int = 72;

    // увлажнители воздуха
    public static const HUMIDIFIERS:int = 73;

    // подогреватели бутылочек
    public static const BOTTLE_HEATERS:int = 74;

    // духовки
    public static const OVEN:int = 75;

    // телефоны
    public static const TELEPHONES:int = 76;

    // кухонные комбайны
    public static const BABYPAPPAS:int = 77;

    // вазы
    public static const VASES:int = 81;

    // картины
    public static const PAINTINGS:int = 82;

    // строительство
    public static const WINDOW:int = 101;
    public static const DOOR:int = 102;
    public static const PARTITIONS:int = 103;	// не, ну а как это правильно называется по-английски?
    public static const STAIRS:int = 104;

    // оборудование магазинов
    public static const SHOP_SHELF:int = 151;
    public static const SHOP_MANNEQUIN:int = 152;
    public static const SHOP_BUILD_ITEM_PREVIEW:int = 153;

    // грязь в домах юзеров
    public static const FLOOR_DIRT:int = 161;
    public static const WALL_DIRT:int = 162;

    // оборудование детской площадки
    public static const YARD_BENCH:int = 171;
    public static const YARD_SANDBOX:int = 172;
    public static const YARD_SLIDE:int = 173;
    public static const YARD_SWING:int = 174;

    // разное оборудование
    public static const VIDEOGAME_MACHINE_SAME:int = 181;
    public static const VIDEOGAME_MACHINE_BEJEWELED:int = 182;

    // одежда - мамин верх
    public static const MOM_CLOTH_TOP:int = 201;
    // одежда - мамин низ
    public static const MOM_CLOTH_BOTTOM:int = 202;
    // одежда - мамины юбки
    public static const MOM_CLOTH_SKIRT:int = 203;
    // одежда - мамина обувь
    public static const MOM_CLOTH_SHOES:int = 204;
    // одежда - мамины носки
    public static const MOM_CLOTH_SOCKS:int = 205;
    // одежда - мамины очки
    public static const MOM_CLOTH_GLASSES:int = 206;
    // одежда - мамины шапки
    public static const MOM_CLOTH_HAT:int = 207;

    public static const MOM_CLOTH_EYES:int = 251;
    public static const MOM_CLOTH_LIPS:int = 252;
    public static const MOM_CLOTH_NOSE:int = 253;
    public static const MOM_CLOTH_EYEBROWS:int = 254;
    public static const MOM_CLOTH_FACE_FORM:int = 255;
    public static const MOM_CLOTH_HAIR_STYLE:int = 256;
    public static const MOM_CLOTH_EYES_COLOR:int = 257;
    public static const MOM_CLOTH_EYES_VISAGE:int = 258;
    public static const MOM_CLOTH_LIPS_VISAGE:int = 259;


    // одежда - папин верх
    public static const DAD_CLOTH_TOP:int = 211;
    // одежда - папин низ
    public static const DAD_CLOTH_BOTTOM:int = 212;
    // одежда - папина обувь
    public static const DAD_CLOTH_SHOES:int = 213;

    public static const DAD_CLOTH_EYES:int = 271;
    public static const DAD_CLOTH_LIPS:int = 272;
    public static const DAD_CLOTH_NOSE:int = 273;
    public static const DAD_CLOTH_EYEBROWS:int = 274;
    public static const DAD_CLOTH_FACE_FORM:int = 275;
    public static const DAD_CLOTH_HAIR_STYLE:int = 276;
    public static const DAD_CLOTH_EYES_COLOR:int = 277;


    // одежда - ребенков верх
    public static const BABY_CLOTH_TOP:int = 221;
    // одежда - ребенков низ
    public static const BABY_CLOTH_BOTTOM:int = 222;
    // одежда - ребенкова обувь
    public static const BABY_CLOTH_SHOES:int = 223;
    // одежда - ребенковы шапки
    public static const BABY_CLOTH_HAT:int = 224;

    public static const BABY_CLOTH_EYES:int = 291;
    public static const BABY_CLOTH_LIPS:int = 292;
    public static const BABY_CLOTH_NOSE:int = 293;
    public static const BABY_CLOTH_FACE_FORM:int = 294;
    public static const BABY_CLOTH_HAIR_STYLE:int = 295;
    public static const BABY_CLOTH_EYES_COLOR:int = 296;


    // === совсем уж разное ===
    // пополнение социальности
    public static const SOCIALITY_REPLENISH:int = 301;
    // пополнение всего
    public static const ALL_STATS_REPLENISH:int = 302;
    // пополнение энергии
    public static const ENERGY_REPLENISH:int = 308;

    public static const WALLPAPERS:int = 401;
    public static const FLOORS:int = 402;

    public function GameObjectClasses()
    {
    }
}
}
