/**
 * Created by SeeD on 10/01/2015.
 */
package com.kekkoneko.puzik.gameplay.objects.constants
{
public class GameObjectCollections
{
    public static const FURNITURE:String = "ObjectsCollection_Furniture";
    public static const FOOD:String = "ObjectsCollection_Food";
    public static const ELECTRONICS:String = "ObjectsCollection_Electronics";
    public static const HYGIENE:String = "ObjectsCollection_Hygiene";
    public static const TOYS:String = "ObjectsCollection_Toys";
    public static const DECO:String = "ObjectsCollection_Deco";

    public static const WALLPAPERS:String = "ObjectsCollection_Wallpapers";
    public static const FLOORS:String = "ObjectsCollection_Floors";

    public static const MOM_CLOTH:String = "ObjectsCollection_MomCloth";
    public static const MOM_APPEARANCE:String = "ObjectsCollection_MomAppearance";
    public static const MOM_MAKEUP:String = "ObjectsCollection_MomMakeUp";

    public static const DAD_CLOTH:String = "ObjectsCollection_DadCloth";
    public static const DAD_APPEARANCE:String = "ObjectsCollection_DadAppearance";

    public static const BABY_CLOTH:String = "ObjectsCollection_BabyCloth";
    public static const BABY_APPEARANCE:String = "ObjectsCollection_BabyAppearance";


    public function GameObjectCollections()
    {
    }
}
}
