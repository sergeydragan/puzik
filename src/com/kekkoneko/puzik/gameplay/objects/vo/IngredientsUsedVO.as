/**
 * Created by SeeD on 08/02/2015.
 */
package com.kekkoneko.puzik.gameplay.objects.vo
{
import com.kekkoneko.puzik.gameplay.characters.model.CharacterModel;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;

public class IngredientsUsedVO
{
    public var ingredients:Vector.<InventoryObjectVO>;
    public var subject:CharacterModel;

    public function IngredientsUsedVO(ingredients:Vector.<InventoryObjectVO>, subject:CharacterModel = null)
    {
        this.ingredients = ingredients;
        this.subject = subject;
    }
}
}
