/**
 * Created by SeeD on 27/12/2014.
 */
package com.kekkoneko.puzik.gameplay.objects.smth
{
import com.kekkoneko.puzik.gameplay.characters.model.CharacterModel;
import com.kekkoneko.puzik.gameplay.characters.model.CharactersModel;
import com.kekkoneko.puzik.gameplay.objects.view.InventoryObjectView;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectActionVO;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;

public class InventoryObjectMediator
{
    [Inject]
    public var charactersModel:CharactersModel;

    public function InventoryObjectMediator()
    {
    }

    // щелкаем по предмету
    // его view и inventoryObjectVO отправляются в нотификейшне
    // команда обращается к objectsModel: "Обработай клик по такому-то vo"
    // в модели есть заранее созданные медиаторы для каждого класса предметов

    // персонаж выполнил действие с предметом, диспетчится нотификейшн
    // команда обращается к objectsModel: "Обработай действие такое-то с таким-то vo"
    // модель вновь обращается к своим заранее созданным медиаторам

    public function initInventoryObject(inventoryObjectVO:InventoryObjectVO, inventoryObjectView:InventoryObjectView):void
    {

    }

    public function getObjectActions(inventoryObjectVO:InventoryObjectVO):Vector.<InventoryObjectActionVO>
    {
        return null;
    }

    public function processFinishedInteraction(interactionType:String, characterModel:CharacterModel, ingredients:Vector.<InventoryObjectVO>):void
    {

    }

    public function isUsedByCharacter(inventoryObjectVO:InventoryObjectVO):CharacterModel
    {
        var characters:Vector.<CharacterModel> = charactersModel.getAllCharacters();
        for (var i:int = 0; i < characters.length; i++)
        {
            if (
                    characters[i].interactingWithObject == inventoryObjectVO ||
                    characters[i].sittingOnObject == inventoryObjectVO ||
                    (characters[i].babyInHands && (characters[i].babyInHands.sittingOnObject == inventoryObjectVO || characters[i].babyInHands.interactingWithObject == inventoryObjectVO)))
            {
                return characters[i];
            }
        }
        return null;
    }
}
}
