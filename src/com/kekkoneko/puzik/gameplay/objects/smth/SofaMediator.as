/**
 * Created by SeeD on 28/12/2014.
 */
package com.kekkoneko.puzik.gameplay.objects.smth
{
import com.kekkoneko.puzik.gameplay.characters.orders.constants.InteractionTypes;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectActionVO;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;

public class SofaMediator extends InventoryObjectMediator
{
    public function SofaMediator()
    {
    }

    override public function getObjectActions(inventoryObjectVO:InventoryObjectVO):Vector.<InventoryObjectActionVO>
    {
        var actions:Vector.<InventoryObjectActionVO> = new <InventoryObjectActionVO>[];

        if (!isUsedByCharacter(inventoryObjectVO))
        {
            actions.push(new InventoryObjectActionVO(InteractionTypes.SIT_ON, inventoryObjectVO));
        }

        return actions;
    }
}
}
