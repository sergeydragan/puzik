/**
 * Created by SeeD on 10/12/14.
 */
package com.kekkoneko.puzik.gameplay.objects.view
{
import com.kekkoneko.puzik.gameplay.objects.helpers.ObjectAppearanceHelper;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;

import flash.display.DisplayObject;
import flash.display.Sprite;

public class InventoryObjectView extends Sprite
{
    public var inventoryObjectVO:InventoryObjectVO;
    private var _appearance:DisplayObject;

    public function InventoryObjectView(inventoryObjectVO:InventoryObjectVO)
    {
        this.inventoryObjectVO = inventoryObjectVO;
        _appearance = ObjectAppearanceHelper.getObjectSprite(inventoryObjectVO.gameObjectVO);
        if (_appearance)
        {
            addChild(_appearance);
        }
    }
}
}
