/**
 * Created by SeeD on 28/01/2015.
 */
package com.kekkoneko.puzik.gameplay.objects.helpers
{
import com.kekkoneko.puzik.gameplay.objects.constants.GameObjectClasses;

import flash.display.DisplayObject;
import flash.display.Sprite;

public class ClothCollectionsHelper
{
    public function ClothCollectionsHelper()
    {
    }

    public static function getIconForClothCollection(collectionName:String):DisplayObject
    {
        return new Sprite();
    }

    public static function getIconForClothClass(clothClassId:int):DisplayObject
    {
        return new Sprite();
    }

    public static function getIconNameForClothCollection(collectionName:String):String
    {
        return collectionName;
    }

    public static function getIconNameForClothClass(classId:int):String
    {
        switch (classId)
        {
            case GameObjectClasses.MOM_CLOTH_TOP:
                return "momClothTop";
            case GameObjectClasses.MOM_CLOTH_BOTTOM:
                return "momClothBottom";
            case GameObjectClasses.MOM_CLOTH_SKIRT:
                return "momClothSkirt";
            case GameObjectClasses.MOM_CLOTH_SHOES:
                return "momClothShoes";
            case GameObjectClasses.MOM_CLOTH_SOCKS:
                return "momClothSocks";
            case GameObjectClasses.MOM_CLOTH_HAT:
                return "momClothHat";
            case GameObjectClasses.MOM_CLOTH_EYEBROWS:
                return "momBrows";
            case GameObjectClasses.MOM_CLOTH_EYES:
                return "momEyes";
            case GameObjectClasses.MOM_CLOTH_EYES_COLOR:
                return "momEyesColor";
            case GameObjectClasses.MOM_CLOTH_EYES_VISAGE:
                return "momEyesVisage";
            case GameObjectClasses.MOM_CLOTH_FACE_FORM:
                return "momFaceform";
            case GameObjectClasses.MOM_CLOTH_GLASSES:
                return "momGlasses";
            case GameObjectClasses.MOM_CLOTH_HAIR_STYLE:
                return "momHair";
            case GameObjectClasses.MOM_CLOTH_LIPS:
                return "momLips";
            case GameObjectClasses.MOM_CLOTH_LIPS_VISAGE:
                return "momLipsVisage";
            case GameObjectClasses.MOM_CLOTH_NOSE:
                return "momNose";

            case GameObjectClasses.DAD_CLOTH_TOP:
                return "dadClothTop";
            case GameObjectClasses.DAD_CLOTH_BOTTOM:
                return "dadClothBottom";
            case GameObjectClasses.DAD_CLOTH_SHOES:
                return "dadClothShoes";
            case GameObjectClasses.DAD_CLOTH_EYEBROWS:
                return "dadBrows";
            case GameObjectClasses.DAD_CLOTH_EYES:
                return "dadEyes";
            case GameObjectClasses.DAD_CLOTH_EYES_COLOR:
                return "dadEyesColor";
            case GameObjectClasses.DAD_CLOTH_FACE_FORM:
                return "dadFaceform";
            case GameObjectClasses.DAD_CLOTH_HAIR_STYLE:
                return "dadHair";
            case GameObjectClasses.DAD_CLOTH_LIPS:
                return "dadLips";
            case GameObjectClasses.DAD_CLOTH_NOSE:
                return "dadNose";

            case GameObjectClasses.BABY_CLOTH_TOP:
                return "babyClothTop";
            case GameObjectClasses.BABY_CLOTH_BOTTOM:
                return "babyClothBottom";
            case GameObjectClasses.BABY_CLOTH_SHOES:
                return "babyClothShoes";
            case GameObjectClasses.BABY_CLOTH_HAT:
                return "babyClothHat";
            case GameObjectClasses.BABY_CLOTH_EYES_COLOR:
                return "babyEyesColor";
            case GameObjectClasses.BABY_CLOTH_NOSE:
                return "babyNose";
            case GameObjectClasses.BABY_CLOTH_EYES:
                return "babyEyes";
            case GameObjectClasses.BABY_CLOTH_FACE_FORM:
                return "babyFaceform";
            case GameObjectClasses.BABY_CLOTH_HAIR_STYLE:
                return "babyHair";
            case GameObjectClasses.BABY_CLOTH_LIPS:
                return "babyLips";

            default:
                return "momClothTop";
        }
    }
}
}
