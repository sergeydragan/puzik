/**
 * Created by SeeD on 08/02/2015.
 */
package com.kekkoneko.puzik.gameplay.objects.helpers
{
import com.kekkoneko.puzik.gameplay.objects.constants.MoneyConstants;
import com.kekkoneko.puzik.gameplay.objects.vos.GameObjectVO;

public class ObjectsPriceHelper
{
    public function ObjectsPriceHelper()
    {
    }

    public static function getObjectSellPrice(gameObjectVO:GameObjectVO):int
    {
        if (gameObjectVO.pricePremium > 0)
        {
            return MoneyConstants.HC_TO_SC_COURSE * gameObjectVO.pricePremium * MoneyConstants.SELL_PRICE_RATIO;
        }
        else
        {
            return gameObjectVO.price * MoneyConstants.SELL_PRICE_RATIO;
        }
    }
}
}
