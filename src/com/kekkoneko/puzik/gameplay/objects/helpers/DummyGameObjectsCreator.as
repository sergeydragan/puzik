/**
 * Created by SeeD on 05/10/2014.
 */
package com.kekkoneko.puzik.gameplay.objects.helpers
{
import com.kekkoneko.puzik.gameplay.objects.constants.GameObjectClasses;
import com.kekkoneko.puzik.gameplay.objects.constants.Rows;
import com.kekkoneko.puzik.gameplay.objects.vos.GameObjectVO;

public class DummyGameObjectsCreator
{
    public function DummyGameObjectsCreator()
    {

    }

    public static function generateDummyObjectsJSON():String
    {
        var objects:Array = [];
        var obj:GameObjectVO;

        obj = new GameObjectVO();
        obj.classId = GameObjectClasses.WALLPAPERS;
        obj.id = 401001;
        objects.push(obj);

        obj = new GameObjectVO();
        obj.classId = GameObjectClasses.WALLPAPERS;
        obj.id = 401002;
        obj.row = -1;
        objects.push(obj);

        obj = new GameObjectVO();
        obj.classId = GameObjectClasses.FLOORS;
        obj.id = 402001;
        obj.row = -1;
        objects.push(obj);

        obj = new GameObjectVO();
        obj.classId = GameObjectClasses.SOFA;
        obj.id = 1001;
        obj.price = 250;
        obj.row = Rows.ON_THE_FLOOR;
        obj.name = "Диван в китайском стиле";
        obj.description = "Любопытный (хотя и не слишком) диван в довольно ординарном китайском стиле.";
        obj.width = 3;
        obj.height = 2;
        objects.push(obj);


        objects.push({
            classId:GameObjectClasses.SOFA,
            id:     1002,
            price:  200,
            row:    Rows.ON_THE_FLOOR,
            name:  "Винтажный диван",
            width: 3,
            height:2
        });

        objects.push({
            classId:GameObjectClasses.SOFA,
            id:     1003,
            price:  420,
            row:    Rows.ON_THE_FLOOR,
            name:  "Современный диван",
            width: 3,
            height:2
        });

        // Mom cloth
        objects.push({
            classId:GameObjectClasses.MOM_CLOTH_TOP,
            id:     201001,
            price:  50,
            name:     "Мамин верх",
            thumbnail:"flash.display.Sprite"
        });

        objects.push({
            classId:GameObjectClasses.MOM_CLOTH_TOP,
            id:     201002,
            price:  60,
            name:     "Мамин верх",
            thumbnail:"flash.display.Sprite"
        });

        objects.push({
            classId:GameObjectClasses.MOM_CLOTH_TOP,
            id:     201003,
            price:  70,
            name:     "Мамин верх",
            thumbnail:"flash.display.Sprite"
        });

        objects.push({
            classId:GameObjectClasses.MOM_CLOTH_TOP,
            id:     201004,
            price:  80,
            name:     "Мамин верх",
            thumbnail:"flash.display.Sprite"
        });

        objects.push({
            classId:GameObjectClasses.MOM_CLOTH_BOTTOM,
            id:     202001,
            price:  50,
            name:     "Мамин низ",
            thumbnail:"flash.display.Sprite"
        });

        objects.push({
            classId:GameObjectClasses.MOM_CLOTH_BOTTOM,
            id:     202002,
            price:  60,
            name:     "Мамин низ",
            thumbnail:"flash.display.Sprite"
        });

        objects.push({
            classId:GameObjectClasses.MOM_CLOTH_SKIRT,
            id:     203001,
            price:  70,
            name:     "Мамина юбка",
            thumbnail:"flash.display.Sprite"
        });

        objects.push({
            classId:GameObjectClasses.MOM_CLOTH_SKIRT,
            id:     203002,
            price:  80,
            name:     "Мамина юбка",
            thumbnail:"flash.display.Sprite"
        });

        return JSON.stringify(objects);
    }
}
}
