/**
 * Created by sdragan on 27.01.2015.
 */
package com.kekkoneko.puzik.gameplay.objects.helpers
{
import com.kekkoneko.puzik.gameplay.objects.model.GameObjectsModel;

public class DummyInventoryCreator
{
    public function DummyInventoryCreator()
    {

    }

    public static function generateDummyInventoryJSON(gameObjectsModel:GameObjectsModel):String
    {
        var inventoryJSONObject:Object = {};
        var inventoryArray:Array = [];

        inventoryArray.push({
            gameObjectId:40101,
            x:        0,
            y:        0,
            roomId:   1,
            isInWorld:true,
            id:       1
        });
        inventoryArray.push({
            gameObjectId:40201,
            x:           0,
            y:           0,
            roomId:   1,
            isInWorld:true,
            id:       2
        });
        inventoryArray.push({
            gameObjectId:40106,
            x:           0,
            y:           0,
            roomId:      2,
            isInWorld:   true,
            id:          5
        });
        inventoryArray.push({
            gameObjectId:40214,
            x:           0,
            y:           0,
            roomId:      2,
            isInWorld:   true,
            id:          6
        });
        inventoryArray.push({
            gameObjectId:40104,
            x:           0,
            y:           0,
            roomId:      3,
            isInWorld:   true,
            id:          7
        });
        inventoryArray.push({
            gameObjectId:40208,
            x:           0,
            y:           0,
            roomId:      3,
            isInWorld:   true,
            id:          8
        });
        inventoryArray.push({
            gameObjectId:101,
            x:        3,
            y:        4,
            isInWorld:true,
            roomId:   1,
            id:       3
        });
        inventoryArray.push({
            gameObjectId:103,
            x:           5,
            y:           4,
            isInWorld:   true,
            roomId:      2,
            id:          4
        });

        inventoryJSONObject.inventory = inventoryArray;
        inventoryJSONObject.playerId = -1;

        return JSON.stringify(inventoryJSONObject);
    }
}
}
