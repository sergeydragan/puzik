/**
 * Created by SeeD on 05/10/2014.
 */
package com.kekkoneko.puzik.gameplay.objects.helpers
{
import com.kekkoneko.puzik.gameplay.objects.vos.GameObjectVO;

import flash.display.DisplayObject;
import flash.display.MovieClip;
import flash.utils.getDefinitionByName;

public class ObjectAppearanceHelper
{
    public function ObjectAppearanceHelper()
    {
    }

    static public function getObjectSprite(gameObjectVO:GameObjectVO, isThumbnail:Boolean = false):DisplayObject
    {
        try
        {
            var appearanceName:String;

            if (isThumbnail)
            {
                appearanceName = gameObjectVO.thumbnail;
            }
            else
            {
                appearanceName = gameObjectVO.appearance;
            }

            var appearanceToReturn:DisplayObject = new (getDefinitionByName(appearanceName) as Class) as MovieClip;
        }
        catch (err:Error)
        {
            trace("Error in ObjectAppearanceHelper.getObjectSprite. Object " + gameObjectVO.name + ", sprite: " + gameObjectVO.appearance);
        }

        return appearanceToReturn;
    }
}
}
