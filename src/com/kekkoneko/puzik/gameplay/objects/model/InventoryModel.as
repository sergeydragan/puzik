/**
 * Created by SeeD on 04/10/2014.
 */
package com.kekkoneko.puzik.gameplay.objects.model
{
import com.kekkoneko.core.debug.Debug;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;
import com.kekkoneko.puzik.gameplay.player.constants.PlayerIDs;

import flash.utils.Dictionary;

public class InventoryModel
{
    private var _inventories:Dictionary;

    public function InventoryModel()
    {
        _inventories = new Dictionary();
    }

    public function addInventory(inventory:Dictionary, playerId:int = PlayerIDs.OWN_PLAYER_ID):void
    {
        _inventories[playerId] = inventory;
    }

    public function addInventoryObjectToInventory(inventoryObjectVO:InventoryObjectVO, playerId:int = PlayerIDs.OWN_PLAYER_ID):void
    {
        if (!_inventories[playerId])
        {
            Debug.warning("Somehow there's no inventory for player " + playerId + ", creating...");
            addInventory(new Dictionary(), playerId);
        }
        _inventories[playerId][inventoryObjectVO.id] = inventoryObjectVO;
    }

    public function getInventoryObjectById(inventoryObjectId:int, playerId:int = PlayerIDs.OWN_PLAYER_ID):InventoryObjectVO
    {
        return _inventories[playerId][inventoryObjectId];
    }

    public function getInventoryObjectsByObjectId(objectId:int, playerId:int = PlayerIDs.OWN_PLAYER_ID):Vector.<InventoryObjectVO>
    {
        var objectsVector:Vector.<InventoryObjectVO> = new Vector.<InventoryObjectVO>();
        for each (var inventoryObject:InventoryObjectVO in _inventories[playerId])
        {
            if (inventoryObject.gameObjectVO.id == objectId)
            {
                objectsVector.push(inventoryObject);
            }
        }
        if (objectsVector.length == 0)
        {
            Debug.info("Can't find any inventory objects with objectId " + objectId);
        }
        return objectsVector;
    }

    public function getInventoryObjectsByClassId(classId:int, playerId:int = PlayerIDs.OWN_PLAYER_ID):Vector.<InventoryObjectVO>
    {
        var objectsVector:Vector.<InventoryObjectVO> = new Vector.<InventoryObjectVO>();
        for each (var inventoryObject:InventoryObjectVO in _inventories[playerId])
        {
            if (inventoryObject.gameObjectVO && inventoryObject.gameObjectVO.classId == classId)
            {
                objectsVector.push(inventoryObject);
            }
        }
        if (objectsVector.length == 0)
        {
            Debug.info("Can't find any inventory objects with classId " + classId);
        }
        return objectsVector;
    }

    public function deleteInventoryObjectById(inventoryObjectId:int, playerId:int = PlayerIDs.OWN_PLAYER_ID):void
    {
        delete _inventories[playerId][inventoryObjectId];
    }

    public function getPlaceableObjects(playerId:int = PlayerIDs.OWN_PLAYER_ID):Vector.<InventoryObjectVO>
    {
        var objectsVector:Vector.<InventoryObjectVO> = new Vector.<InventoryObjectVO>();
        for each (var inventoryObject:InventoryObjectVO in _inventories[playerId])
        {
            if (inventoryObject.gameObjectVO && inventoryObject.gameObjectVO.row >= 0)
            {
                objectsVector.push(inventoryObject);
            }
        }
        if (objectsVector.length == 0)
        {
            Debug.info("Can't find any placeable inventory objects");
        }
        return objectsVector;
    }
}
}
