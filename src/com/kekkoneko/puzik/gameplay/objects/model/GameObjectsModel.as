/**
 * Created by SeeD on 05/10/2014.
 */
package com.kekkoneko.puzik.gameplay.objects.model
{
import com.kekkoneko.core.debug.Debug;
import com.kekkoneko.puzik.gameplay.objects.vos.GameObjectVO;

import flash.utils.Dictionary;

public class GameObjectsModel
{
    private var _gameObjects:Dictionary;
    private var _gameObjectCollections:Dictionary;

    public function GameObjectsModel()
    {
        _gameObjects = new Dictionary();
        _gameObjectCollections = new Dictionary();
    }

    public function addObjectsCollection(collectionName:String, ...objectClassIds):void
    {
        if (_gameObjectCollections[collectionName])
        {
            Debug.warning("GameObjectCollection with name " + collectionName + " is already registered");
        }

        var objectClasses:Vector.<int> = new <int>[];
        for (var i:int = 0; i < objectClassIds.length; i++)
        {
            objectClasses.push(objectClassIds[i]);
        }

        _gameObjectCollections[collectionName] = objectClasses;
    }

    public function getObjectsCollection(collectionName:String):Vector.<int>
    {
        if (!_gameObjectCollections[collectionName])
        {
            Debug.warning("GameObjectCollection with name " + collectionName + " is not registered");
        }

        return (_gameObjectCollections[collectionName]);
    }

    public function addObject(gameObjectVO:GameObjectVO):void
    {
        _gameObjects[gameObjectVO.id] = gameObjectVO;
    }

    public function getObjectById(gameObjectId:int):GameObjectVO
    {
        return _gameObjects[gameObjectId];
    }

    public function getObjectsByClassId(classId:int):Vector.<GameObjectVO>
    {
        var objectsVector:Vector.<GameObjectVO> = new Vector.<GameObjectVO>();
        for each (var gameObject:GameObjectVO in _gameObjects)
        {
            if (gameObject.classId == classId)
            {
                objectsVector.push(gameObject);
            }
        }

        if (objectsVector.length == 0)
        {
            Debug.info("Can't find any game objects with classId " + classId);
        }
        return objectsVector;
    }
}
}
