/**
 * Created by SeeD on 27/12/2014.
 */
package com.kekkoneko.puzik.gameplay.objects.model
{
import com.kekkoneko.core.debug.Debug;
import com.kekkoneko.puzik.gameplay.objects.smth.InventoryObjectMediator;
import com.kekkoneko.puzik.gameplay.objects.view.InventoryObjectView;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectActionVO;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;

import flash.utils.Dictionary;

public class InventoryObjectsModel
{
    private var _objectMediators:Dictionary;

    public function InventoryObjectsModel()
    {
        _objectMediators = new Dictionary();
    }

    public function addObjectMediator(objectCategory:int, objectMediatorClass:Class):void
    {
        if (_objectMediators[objectCategory])
        {
            Debug.warning("ObjectMediator for category " + objectCategory + " is already registered");
        }
        var objectMediator:InventoryObjectMediator = new objectMediatorClass as InventoryObjectMediator;
        Main.injector.injectInto(objectMediator);
        _objectMediators[objectCategory] = objectMediator;
    }

    public function initInventoryObjectVOView(inventoryObjectVO:InventoryObjectVO, inventoryObjectView:InventoryObjectView):void
    {
        var inventoryObjectMediator:InventoryObjectMediator = getInventoryObjectMediator(inventoryObjectVO);
        if (inventoryObjectMediator)
        {
            inventoryObjectMediator.initInventoryObject(inventoryObjectVO, inventoryObjectView);
        }
    }

    public function getPossibleOrdersForInventoryObjectVO(inventoryObjectVO:InventoryObjectVO):Vector.<InventoryObjectActionVO>
    {
        var inventoryObjectMediator:InventoryObjectMediator = getInventoryObjectMediator(inventoryObjectVO);
        if (inventoryObjectMediator)
        {
            return inventoryObjectMediator.getObjectActions(inventoryObjectVO);
        }
        else
        {
            return null;
        }
    }

    private function getInventoryObjectMediator(inventoryObjectVO:InventoryObjectVO):InventoryObjectMediator
    {
        var inventoryObjectMediator:InventoryObjectMediator = _objectMediators[inventoryObjectVO.gameObjectVO.classId];
        if (inventoryObjectMediator)
        {
            return inventoryObjectMediator;
        }
        else
        {
            Debug.warning("InventoryObjectMediator for classID " + inventoryObjectVO.gameObjectVO.classId + " not found");
            return null;
        }
    }
}
}
