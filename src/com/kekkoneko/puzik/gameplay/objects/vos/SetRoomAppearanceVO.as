package com.kekkoneko.puzik.gameplay.objects.vos
{
/**
 * @author Ilya Malanin (flashdeveloper[at]pastila.org)
 */
public class SetRoomAppearanceVO
{
    public var inventoryObject:InventoryObjectVO;

    public function SetRoomAppearanceVO(inventoryObject:InventoryObjectVO)
    {
        this.inventoryObject = inventoryObject;
    }
}
}
