/**
 * Created by SeeD on 02/01/2015.
 */
package com.kekkoneko.puzik.gameplay.objects.vos
{
public class ProcessInventoryObjectActionSelectedVO
{
    public var inventoryObjectVO:InventoryObjectVO;
    public var inventoryObjectActionVO:InventoryObjectActionVO;

    public function ProcessInventoryObjectActionSelectedVO()
    {
    }

}
}
