/**
 * Created by SeeD on 28/09/2014.
 */
package com.kekkoneko.puzik.gameplay.objects.vos
{
public class InventoryObjectVO
{
    public var gameObjectVO:GameObjectVO;
    public var id:int;
    public var isInWorld:Boolean;
    public var x:int;
    public var y:int;
    public var roomId:int;
    public var state:String;
    public var count:int;

    public function InventoryObjectVO(gameObjectVO:GameObjectVO)
    {
        this.gameObjectVO = gameObjectVO;
    }
}
}
