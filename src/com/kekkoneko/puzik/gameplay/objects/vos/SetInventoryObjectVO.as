/**
 * Created by SeeD on 05/02/2015.
 */
package com.kekkoneko.puzik.gameplay.objects.vos
{
public class SetInventoryObjectVO
{
    public var newX:int;
    public var newY:int;
    public var newIsInWorld:Boolean;
    public var newRoomId:int;
    public var newState:String;
    public var inventoryObjectVO:InventoryObjectVO;

    public function SetInventoryObjectVO(inventoryObjectVO:InventoryObjectVO, newX:int, newY:int, newIsInWorld:Boolean, newRoomId:int, newState:String = "")
    {
        this.newX = newX;
        this.newY = newY;
        this.newIsInWorld = newIsInWorld;
        this.newRoomId = newRoomId;
        this.newState = newState;
        this.inventoryObjectVO = inventoryObjectVO;
    }
}
}
