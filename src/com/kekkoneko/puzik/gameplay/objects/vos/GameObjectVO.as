/**
 * Created by SeeD on 28/09/2014.
 */
package com.kekkoneko.puzik.gameplay.objects.vos
{
public class GameObjectVO
{
    [Marshall(field="id")]
    /**
     * Идентификатор предмета в базе данных
     */
    public var dbId:int;
    [Marshall(field="idP")]
    /**
     * Идентификатор объекта
     */
    public var id:int;
    [Marshall(field="idClass")]
    /**
     * <p>Идентификатор класса объекта.</p>
     * Класс представляет собой группу однотипных объектов (например верхняя одежда).
     *
     * @see com.kekkoneko.puzik.gameplay.objects.constants.GameObjectClasses
     */
    public var classId:int;

    /**
     * Название объекта
     */
    public var name:String;
    /**
     * Описание объекта
     */
    public var description:String;
    /**
     * <p>URL предмета.</p>
     * Для перехода на сайт, где можно просмотреть и купить реальный прототип.
     */
    public var url:String;

    /**
     * Высота объекта
     */
    public var height:int;
    /**
     * Ширина объекта
     */
    public var width:int;

    public var row:int = -1;

    [Marshall(field="shopIcon")]
    /**
     * Изображение для магазина
     */
    public var thumbnail:String;
    /**
     * Имя класса ассета для отображения
     */
    public var appearance:String;

    /**
     * Стоимость
     */
    public var price:int;
    [Marshall(field="donate")]
    /**
     * Премиум-стоимость
     */
    public var pricePremium:int;

    /**
     * Участвует ли предмет в распродаже
     */
    public var isInSale:Boolean;
    /**
     * Стоимость по распродаже
     */
    public var priceSale:int;
    [Marshall(field="saleDonate")]
    /**
     * Премиум-стоимость по распродаже
     */
    public var pricePremiumSale:int;
    /**
     * Время окончания распродажи
     */
    public var saleEndTime:int;

    /**
     * Может ли предмет быть сгруппированым в кучку
     */
    public var isStackable:Boolean;

    /**
     * Уровень игрока, необходимый для приобретения данного предмета
     */
    public var minLevel:int;

    public var rating:int;
    /**
     * Может ли данный предмет быть подарен
     */
    public var canBeGifted:Boolean;

    [Marshall(field="health")]
    /**
     * Эффект оказываемый на здоровье персонажа
     */
    public var effectHealth:int = 0;
    [Marshall(field="food")]
    /**
     * Эффект оказываемый на голод персонажа
     */
    public var effectFood:int = 0;
    [Marshall(field="sleep")]
    /**
     * Эффект оказываемый на сонливость персонажа
     */
    public var effectSleep:int = 0;
    [Marshall(field="wash")]
    /**
     * Эффект оказываемый на чистоту персонажа
     */
    public var effectHygiene:int = 0;
    [Marshall(field="toilet")]
    /**
     * Эффект оказываемый на желание сходить в туалет
     */
    public var effectToilet:int = 0;
    [Marshall(field="happiness")]
    /**
     * Эффект оказываемый на счастье персонажа
     */
    public var effectHappiness:int = 0;

    public function GameObjectVO()
    {
    }

}
}