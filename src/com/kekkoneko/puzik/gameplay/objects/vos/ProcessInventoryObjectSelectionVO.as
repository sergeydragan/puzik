/**
 * Created by SeeD on 07/02/2015.
 */
package com.kekkoneko.puzik.gameplay.objects.vos
{
import com.kekkoneko.puzik.gameplay.objects.view.InventoryObjectView;

public class ProcessInventoryObjectSelectionVO
{
    public var inventoryObjectView:InventoryObjectView;

    public function ProcessInventoryObjectSelectionVO(inventoryObjectView:InventoryObjectView)
    {
        this.inventoryObjectView = inventoryObjectView;
    }
}
}
