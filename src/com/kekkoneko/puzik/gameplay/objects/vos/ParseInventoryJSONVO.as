/**
 * Created by sdragan on 27.01.2015.
 */
package com.kekkoneko.puzik.gameplay.objects.vos
{
public class ParseInventoryJSONVO
{
    public var json:String;
    public var callback:Function;

    public function ParseInventoryJSONVO(json:String, callback:Function = null)
    {
        this.json = json;
        this.callback = callback;
    }
}
}
