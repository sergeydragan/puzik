/**
 * Created by SeeD on 08/02/2015.
 */
package com.kekkoneko.puzik.gameplay.objects.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.controller.notifications.NeedsChangedNotification;
import com.kekkoneko.puzik.gameplay.cycles.SingleBabyNeedsModel;
import com.kekkoneko.puzik.gameplay.objects.vo.IngredientsUsedVO;
import com.kekkoneko.puzik.gameplay.objects.vos.GameObjectVO;
import com.kekkoneko.puzik.gameplay.player.helpers.PlayerXPHelper;
import com.kekkoneko.puzik.gameplay.player.vos.AddXPVO;

public class IngredientsUsedCommand extends Command
{
    [Inject]
    public var singleBabyNeedsModel:SingleBabyNeedsModel;

    public function IngredientsUsedCommand()
    {
    }

    override public function execute():void
    {
        var vo:IngredientsUsedVO = _payload as IngredientsUsedVO;
        var sumOfEffects:int = 0;
        for (var i:int = 0; i < vo.ingredients.length; i++)
        {
            addObjectEffectToNeeds(vo.ingredients[i].gameObjectVO);
            sumOfEffects += getSumOfGameObjectEffects(vo.ingredients[i].gameObjectVO);
        }

        var averageNeeds:Number = singleBabyNeedsModel.getAverageNeeds();
        coreEventDispatcher.broadcastMessage(NeedsChangedNotification.NEEDS_CHANGED, new NeedsChangedNotification());
        coreEventDispatcher.broadcastMessage(PuzikCommands.ADD_XP, new AddXPVO(PlayerXPHelper.getXPBySumOfObjectEffects(sumOfEffects, averageNeeds)));
    }

    private function addObjectEffectToNeeds(gameObjectVO:GameObjectVO):void
    {
        singleBabyNeedsModel.getNeedsVO().foodCurrent += gameObjectVO.effectFood;
        singleBabyNeedsModel.getNeedsVO().sleepCurrent += gameObjectVO.effectSleep;
        singleBabyNeedsModel.getNeedsVO().toiletCurrent += gameObjectVO.effectToilet;
        singleBabyNeedsModel.getNeedsVO().washCurrent += gameObjectVO.effectHygiene;
        singleBabyNeedsModel.getNeedsVO().happinessCurrent += gameObjectVO.effectHappiness;

        if (singleBabyNeedsModel.getNeedsVO().foodCurrent > singleBabyNeedsModel.getNeedsVO().foodMax)
        {
            singleBabyNeedsModel.getNeedsVO().foodCurrent = singleBabyNeedsModel.getNeedsVO().foodMax;
        }

        if (singleBabyNeedsModel.getNeedsVO().sleepCurrent > singleBabyNeedsModel.getNeedsVO().sleepMax)
        {
            singleBabyNeedsModel.getNeedsVO().sleepCurrent = singleBabyNeedsModel.getNeedsVO().sleepMax;
        }

        if (singleBabyNeedsModel.getNeedsVO().toiletCurrent > singleBabyNeedsModel.getNeedsVO().toiletMax)
        {
            singleBabyNeedsModel.getNeedsVO().toiletCurrent = singleBabyNeedsModel.getNeedsVO().toiletMax;
        }

        if (singleBabyNeedsModel.getNeedsVO().washCurrent > singleBabyNeedsModel.getNeedsVO().washMax)
        {
            singleBabyNeedsModel.getNeedsVO().washCurrent = singleBabyNeedsModel.getNeedsVO().washMax;
        }

        if (singleBabyNeedsModel.getNeedsVO().happinessCurrent > singleBabyNeedsModel.getNeedsVO().happinessMax)
        {
            singleBabyNeedsModel.getNeedsVO().happinessCurrent = singleBabyNeedsModel.getNeedsVO().happinessMax;
        }
    }

    private function getSumOfGameObjectEffects(gameObjectVO:GameObjectVO):int
    {
        var sum:int = 0;
        if (gameObjectVO.effectFood > 0)
        {
            sum += gameObjectVO.effectFood;
        }

        if (gameObjectVO.effectSleep > 0)
        {
            sum += gameObjectVO.effectSleep;
        }

        if (gameObjectVO.effectToilet > 0)
        {
            sum += gameObjectVO.effectToilet;
        }

        if (gameObjectVO.effectHygiene > 0)
        {
            sum += gameObjectVO.effectHygiene;
        }

        if (gameObjectVO.effectHappiness > 0)
        {
            sum += gameObjectVO.effectHappiness;
        }

        return sum;
    }
}
}
