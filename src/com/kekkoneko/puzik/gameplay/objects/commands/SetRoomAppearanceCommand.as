package com.kekkoneko.puzik.gameplay.objects.commands
{
import com.kekkoneko.puzik.gameplay.locations.models.LocationsModel;
import com.kekkoneko.puzik.controller.notifications.RoomAppearanceUpdatedNotification;
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.puzik.controller.notifications.InventoryObjectUpdatedNotification;
import com.kekkoneko.puzik.gameplay.objects.constants.GameObjectClasses;
import com.kekkoneko.puzik.gameplay.objects.model.InventoryModel;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;
import com.kekkoneko.puzik.gameplay.objects.vos.SetRoomAppearanceVO;
import com.kekkoneko.puzik.gameplay.room.model.RoomModeModel;

public class SetRoomAppearanceCommand extends Command
{
    [Inject]
    public var inventoryModel:InventoryModel;
    [Inject]
    public var locationsModel:LocationsModel;

    public function SetRoomAppearanceCommand()
    {
        super();
    }

    override public function execute():void
    {
        const roomModel:RoomModeModel = locationsModel.getCurrentRoomModel();
        const vo:SetRoomAppearanceVO = _payload as SetRoomAppearanceVO;
        const newObject:InventoryObjectVO = vo.inventoryObject;

        const classId:int = newObject.gameObjectVO.classId;

        var currentObject:InventoryObjectVO;

        if (classId == GameObjectClasses.FLOORS)
        {
            currentObject = roomModel.getFloors();
        }
        else if (classId == GameObjectClasses.WALLPAPERS)
        {
            currentObject = roomModel.getWallpapers();
        }

        if (currentObject)
        {
            currentObject.isInWorld = false;
            currentObject.roomId = 0;
        }

        newObject.isInWorld = true;
        newObject.roomId = roomModel.roomVO.id;

        /**
         * Необходимо вызвать для обновления окна инвентаря, при этом 
         * RoomModeView не среагирует на данное событие из-за classId объекта, так как в нём намеренно
         * пропускаются обои и полы, ввиду того, что они рендерятся особым способом.
         * 
         * TODO: в идеале - событие обновления инвентаря и обновления объекта инвентаря необходимо разделить 
         */
        coreEventDispatcher.broadcastMessage(InventoryObjectUpdatedNotification.INVENTORY_OBJECT_UPDATED, new InventoryObjectUpdatedNotification(newObject, false, false, false));
        coreEventDispatcher.broadcastMessage(RoomAppearanceUpdatedNotification.ROOM_APPEARANCE_UPDATED);
    }

}
}
