/**
 * Created by SeeD on 28/12/2014.
 */
package com.kekkoneko.puzik.gameplay.objects.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.dialog.vos.ShowDialogPropertiesVO;
import com.kekkoneko.core.panel.vos.ShowPanelPropertiesVO;
import com.kekkoneko.core.screen.ScreenModel;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.dialog.constants.PuzikDialogNames;
import com.kekkoneko.puzik.dialog.dialogs.noIngredients.NoIngredientsDialogPropertiesVO;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.AddOrderToActiveCharacterVO;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.OrderVO;
import com.kekkoneko.puzik.gameplay.objects.model.InventoryModel;
import com.kekkoneko.puzik.gameplay.objects.model.InventoryObjectsModel;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;
import com.kekkoneko.puzik.gameplay.objects.vos.ProcessInventoryObjectActionSelectedVO;
import com.kekkoneko.puzik.panel.constants.PuzikPanelNames;
import com.kekkoneko.puzik.panel.panels.ingredientSelection.vos.IngredientSelectionPanelPropertiesVO;

public class ProcessInventoryObjectActionSelectedCommand extends Command
{
    [Inject]
    public var inventoryObjectsModel:InventoryObjectsModel;

    [Inject]
    public var screenModel:ScreenModel;

    [Inject]
    public var inventoryModel:InventoryModel;

    public function ProcessInventoryObjectActionSelectedCommand()
    {
    }

    override public function execute():void
    {
        var vo:ProcessInventoryObjectActionSelectedVO = _payload as ProcessInventoryObjectActionSelectedVO;

        var orderVO:OrderVO = new OrderVO();
        orderVO.coordsX = vo.inventoryObjectVO.x;
        orderVO.interactionType = vo.inventoryObjectActionVO.interactionType;
        orderVO.targetInventoryObjectVO = vo.inventoryObjectVO;

        if (vo.inventoryObjectActionVO.ingredientsNeeded)
        {
            var usableInventoryObjectsVO:Vector.<InventoryObjectVO> = inventoryModel.getInventoryObjectsByClassId(vo.inventoryObjectActionVO.ingredientsType);
            if (usableInventoryObjectsVO.length > 0)
            {
                // если нужны ингредиенты - показать панельку про выбор ингредиентов
                var ingredientSelectionPanelPropertiesVO:IngredientSelectionPanelPropertiesVO = new IngredientSelectionPanelPropertiesVO();
                // ей скармливается класс ингредиентов и сам ордер, чтобы она в него запихнула выбранные ингредиенты и отправила дальше
                ingredientSelectionPanelPropertiesVO.ingredientsClass = vo.inventoryObjectActionVO.ingredientsType;
                ingredientSelectionPanelPropertiesVO.orderVO = orderVO;

                var showIngredientsPanelPropertiesVO:ShowPanelPropertiesVO = new ShowPanelPropertiesVO(PuzikPanelNames.INGREDIENT_SELECTION_PANEL, "", screenModel.getMouseCoords(), ingredientSelectionPanelPropertiesVO);
                coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_PANEL, showIngredientsPanelPropertiesVO);
            }
            else
            {
                var noIngredientsDialogPropertiesVO:NoIngredientsDialogPropertiesVO = new NoIngredientsDialogPropertiesVO(vo.inventoryObjectActionVO.ingredientsType);
                coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_DIALOG, new ShowDialogPropertiesVO(PuzikDialogNames.NO_INGREDIENTS, noIngredientsDialogPropertiesVO));
            }
        }
        else
        {
            // если не нужны ингредиенты - выполнить сразу это действие
            coreEventDispatcher.broadcastMessage(PuzikCommands.ADD_ORDER_TO_ACTIVE_CHARACTER, new AddOrderToActiveCharacterVO(orderVO));
        }

    }
}
}
