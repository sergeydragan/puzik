/**
 * Created by sdragan on 27.01.2015.
 */
package com.kekkoneko.puzik.gameplay.objects.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.puzik.gameplay.objects.model.GameObjectsModel;
import com.kekkoneko.puzik.gameplay.objects.vos.GameObjectVO;
import com.kekkoneko.puzik.gameplay.objects.vos.ParseGameObjectsJSONVO;

import flash.utils.getDefinitionByName;

import org.osflash.vanilla.extract;

public class ParseGameObjectsJSONCommand extends Command
{
    [Inject]
    public var gameObjectsModel:GameObjectsModel;

    public function ParseGameObjectsJSONCommand()
    {
    }

    override public function execute():void
    {
        var vo:ParseGameObjectsJSONVO = _payload as ParseGameObjectsJSONVO;
        var json:String = vo.json;

        var gameObjects:Vector.<GameObjectVO> = extract(JSON.parse(json), Vector.<GameObjectVO> as Class);

        for (var i:int = 0; i < gameObjects.length; i++)
        {
            var gameObject:GameObjectVO = gameObjects[i];

            /**
             *  Если не определен appearance - генерируем его из идентификатора
             */
            if (!gameObject.appearance || gameObject.appearance == "")
            {
                gameObject.appearance = "Obj_" + String(gameObject.id) + "_appearance";
            }

            /**
             * Если не определена иконка - вначале проверяем, есть ли класс с id предмета и суффиксом thumbnail,
             * в противном случае используем appearance
             */
            if (!gameObject.thumbnail || gameObject.thumbnail == "")
            {
                try
                {
                    if (getDefinitionByName("Obj_" + String(gameObject.id) + "_thumbnail") as Class)
                    {
                        gameObject.thumbnail = "Obj_" + String(gameObject.id) + "_thumbnail";
                    }
                }
                catch (error:Error)
                {
                    gameObject.thumbnail = gameObject.appearance;
                }
            }

            gameObjectsModel.addObject(gameObjects[i]);
        }

        if (vo.callback)
        {
            vo.callback();
        }
    }
}
}
