/**
 * Created by SeeD on 05/02/2015.
 */
package com.kekkoneko.puzik.gameplay.objects.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.core.debug.Debug;
import com.kekkoneko.puzik.controller.notifications.InventoryObjectUpdatedNotification;
import com.kekkoneko.puzik.gameplay.objects.model.InventoryModel;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;
import com.kekkoneko.puzik.gameplay.objects.vos.SetInventoryObjectVO;

public class SetInventoryObjectCommand extends Command
{
    [Inject]
    public var inventoryModel:InventoryModel;

    public function SetInventoryObjectCommand()
    {
        super();
    }

    override public function execute():void
    {
        var vo:SetInventoryObjectVO = _payload as SetInventoryObjectVO;
        var invObj:InventoryObjectVO = vo.inventoryObjectVO;

        var positionHasChanged:Boolean = invObj.x != vo.newX || invObj.y != vo.newY;
        var inWorldHasChanged:Boolean = invObj.isInWorld != vo.newIsInWorld;
        var stateHasChanged:Boolean = invObj.state != vo.newState;

        invObj.x = vo.newX;
        invObj.y = vo.newY;
        invObj.isInWorld = vo.newIsInWorld;
        invObj.roomId = vo.newRoomId;
        invObj.state = vo.newState;
        Debug.info("Changing inventory object with id " + invObj.id);
        //todo: send message to server here
        coreEventDispatcher.broadcastMessage(InventoryObjectUpdatedNotification.INVENTORY_OBJECT_UPDATED, new InventoryObjectUpdatedNotification(invObj, positionHasChanged, inWorldHasChanged, stateHasChanged));
    }

}
}
