/**
 * Created by SeeD on 28/12/2014.
 */
package com.kekkoneko.puzik.gameplay.objects.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.panel.vos.ShowPanelPropertiesVO;
import com.kekkoneko.core.screen.ScreenModel;
import com.kekkoneko.puzik.gameplay.objects.model.InventoryObjectsModel;
import com.kekkoneko.puzik.gameplay.objects.view.InventoryObjectView;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectActionVO;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;
import com.kekkoneko.puzik.gameplay.objects.vos.ProcessInventoryObjectSelectionVO;
import com.kekkoneko.puzik.panel.constants.PuzikPanelNames;
import com.kekkoneko.puzik.panel.panels.actionSelection.vos.ActionSelectionPanelPropertiesVO;

public class ProcessInventoryObjectSelectionCommand extends Command
{
    [Inject]
    public var inventoryObjectsModel:InventoryObjectsModel;

    [Inject]
    public var screenModel:ScreenModel;

    public function ProcessInventoryObjectSelectionCommand()
    {
    }

    override public function execute():void
    {
        var payload:ProcessInventoryObjectSelectionVO = _payload as ProcessInventoryObjectSelectionVO;
        var inventoryObjectView:InventoryObjectView = payload.inventoryObjectView;
        var inventoryObjectVO:InventoryObjectVO = inventoryObjectView.inventoryObjectVO;

        var actions:Vector.<InventoryObjectActionVO> = inventoryObjectsModel.getPossibleOrdersForInventoryObjectVO(inventoryObjectVO);

        trace("Here are possible actions for " + inventoryObjectVO.gameObjectVO.name + ":");
        if (actions && actions.length > 0)
        {
            for (var i:int = 0; i < actions.length; i++)
            {
                trace(actions[i].interactionType);
            }

            /*
            if (!actions || actions.length == 0)
            {
            return;
            }
            else if (actions.length == 1)
            {
            var processInventoryObjectActionSelectedVO:ProcessInventoryObjectActionSelectedVO = new ProcessInventoryObjectActionSelectedVO();
            processInventoryObjectActionSelectedVO.inventoryObjectVO = inventoryObjectVO;
            processInventoryObjectActionSelectedVO.inventoryObjectActionVO = actions[0];
            coreEventDispatcher.broadcastMessage(PuzikCommands.PROCESS_INVENTORY_OBJECT_ACTION_SELECTED, processInventoryObjectActionSelectedVO);
            }
            else
            {
             */
            // показать панельку про выбор действия
            var actionSelectionPanelPropertiesVO:ActionSelectionPanelPropertiesVO = new ActionSelectionPanelPropertiesVO();
            actionSelectionPanelPropertiesVO.actions = actions;
            actionSelectionPanelPropertiesVO.inventoryObjectVO = inventoryObjectVO;

            var showActionsPanelPropertiesVO:ShowPanelPropertiesVO = new ShowPanelPropertiesVO(PuzikPanelNames.ACTION_SELECTION_PANEL, "", screenModel.getMouseCoords(), actionSelectionPanelPropertiesVO);
            coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_PANEL, showActionsPanelPropertiesVO);
        }
        // }
    }
}
}
