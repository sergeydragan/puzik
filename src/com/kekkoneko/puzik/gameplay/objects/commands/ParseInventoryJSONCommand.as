/**
 * Created by sdragan on 27.01.2015.
 */
package com.kekkoneko.puzik.gameplay.objects.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.puzik.gameplay.objects.model.GameObjectsModel;
import com.kekkoneko.puzik.gameplay.objects.model.InventoryModel;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;
import com.kekkoneko.puzik.gameplay.objects.vos.ParseInventoryJSONVO;

import flash.utils.Dictionary;

public class ParseInventoryJSONCommand extends Command
{
    [Inject]
    public var gameObjectsModel:GameObjectsModel;

    [Inject]
    public var inventoryModel:InventoryModel;

    public function ParseInventoryJSONCommand()
    {
    }

    override public function execute():void
    {
        var vo:ParseInventoryJSONVO = _payload as ParseInventoryJSONVO;
        var json:String = vo.json;

        // {playerId:1234,inventory:[{},{},{},{}]}

        var inventoryJSONObject:Object = JSON.parse(json);
        var inventoryObjectsArray:Array = inventoryJSONObject.inventory as Array;
        var playerId:int = inventoryJSONObject.playerId;
        var inventoryObjectVO:InventoryObjectVO;
        var inventory:Dictionary = new Dictionary();

        for (var i:int = 0; i < inventoryObjectsArray.length; i++)
        {
            inventoryObjectVO = new InventoryObjectVO(gameObjectsModel.getObjectById(inventoryObjectsArray[i].gameObjectId));
            inventoryObjectVO.id = inventoryObjectsArray[i].id;
            inventoryObjectVO.isInWorld = inventoryObjectsArray[i].isInWorld;
            inventoryObjectVO.count = inventoryObjectsArray[i].level;
            inventoryObjectVO.state = inventoryObjectsArray[i].state;
            inventoryObjectVO.x = inventoryObjectsArray[i].x;
            inventoryObjectVO.y = inventoryObjectsArray[i].y;
            inventoryObjectVO.roomId = inventoryObjectsArray[i].roomId;
            inventory[inventoryObjectVO.id] = inventoryObjectVO;
        }

        inventoryModel.addInventory(inventory, playerId);

        if (vo.callback)
        {
            vo.callback();
        }
    }
}
}
