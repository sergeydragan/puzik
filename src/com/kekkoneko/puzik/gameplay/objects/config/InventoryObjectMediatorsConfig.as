/**
 * Created by SeeD on 28/12/2014.
 */
package com.kekkoneko.puzik.gameplay.objects.config
{
import com.kekkoneko.puzik.gameplay.characters.model.CharactersModel;
import com.kekkoneko.puzik.gameplay.objects.constants.GameObjectClasses;
import com.kekkoneko.puzik.gameplay.objects.model.InventoryObjectsModel;
import com.kekkoneko.puzik.gameplay.objects.smth.SofaMediator;

public class InventoryObjectMediatorsConfig
{
    [Inject]
    public var inventoryObjectsModel:InventoryObjectsModel;

    [Inject]
    public var charactersModel:CharactersModel;

    public function InventoryObjectMediatorsConfig()
    {
    }

    public function init():void
    {
        inventoryObjectsModel.addObjectMediator(GameObjectClasses.SOFA, SofaMediator);
    }
}
}
