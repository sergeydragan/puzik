/**
 * Created by SeeD on 10/01/2015.
 */
package com.kekkoneko.puzik.gameplay.objects.config
{
import com.kekkoneko.puzik.gameplay.objects.constants.GameObjectClasses;
import com.kekkoneko.puzik.gameplay.objects.constants.GameObjectCollections;
import com.kekkoneko.puzik.gameplay.objects.model.GameObjectsModel;

public class GameObjectsConfig
{
    [Inject]
    public var gameObjectModel:GameObjectsModel;

    public function GameObjectsConfig()
    {
    }

    public function init():void
    {
        gameObjectModel.addObjectsCollection(GameObjectCollections.FURNITURE,
                GameObjectClasses.SOFA,
                GameObjectClasses.WARDROBE,
                GameObjectClasses.CHILD_BED,
                GameObjectClasses.PLAYPEN,
                GameObjectClasses.CHILD_CHAIR,
                GameObjectClasses.CHAIR,
                GameObjectClasses.NIGHTSTAND,
                GameObjectClasses.KITCHEN_CUPBOARD,
                GameObjectClasses.COFFE_TABLES,
                GameObjectClasses.EXTRACTOR_HOOD,
                GameObjectClasses.KITCHEN_SINK,
                GameObjectClasses.TOYS_BOX,
                GameObjectClasses.CRADLE,
                GameObjectClasses.SINK,
//                GameObjectClasses.TOILET,
                GameObjectClasses.MIRROR,
                GameObjectClasses.BATHROOM_SHELF,
                GameObjectClasses.DIAPER_TABLE);

        gameObjectModel.addObjectsCollection(GameObjectCollections.FOOD,
                GameObjectClasses.FOOD_0_12_READY);

        gameObjectModel.addObjectsCollection(GameObjectCollections.HYGIENE,
                GameObjectClasses.SOAP,
                GameObjectClasses.BATH,
                GameObjectClasses.DIAPERS,
                GameObjectClasses.POTTIES,
                GameObjectClasses.SINK);

        gameObjectModel.addObjectsCollection(GameObjectCollections.ELECTRONICS,
                GameObjectClasses.WASH_MACHINE,
                GameObjectClasses.FRIDGES,
                GameObjectClasses.HUMIDIFIERS,
                GameObjectClasses.BOTTLE_HEATERS,
                GameObjectClasses.OVEN,
                GameObjectClasses.TELEPHONES,
                GameObjectClasses.BABYPAPPAS,
                GameObjectClasses.EXTRACTOR_HOOD);

        gameObjectModel.addObjectsCollection(GameObjectCollections.DECO,
                GameObjectClasses.FIREPLACES,
                GameObjectClasses.CARPETS,
                GameObjectClasses.WALL_LIGHTS,
                GameObjectClasses.PAINTINGS,
                GameObjectClasses.WINDOW,
                GameObjectClasses.MIRROR,
                GameObjectClasses.TELEPHONES,
                GameObjectClasses.VASES);

        gameObjectModel.addObjectsCollection(GameObjectCollections.TOYS,
                GameObjectClasses.TOYS_BIG,
                GameObjectClasses.TOYS_BIGGEST,
                GameObjectClasses.TOYS_BATH,
                GameObjectClasses.TOYS_SMALL);

        gameObjectModel.addObjectsCollection(GameObjectCollections.WALLPAPERS,
                GameObjectClasses.WALLPAPERS);

        gameObjectModel.addObjectsCollection(GameObjectCollections.FLOORS,
                GameObjectClasses.FLOORS);

        gameObjectModel.addObjectsCollection(GameObjectCollections.MOM_CLOTH,
                GameObjectClasses.MOM_CLOTH_TOP,
                GameObjectClasses.MOM_CLOTH_BOTTOM,
                GameObjectClasses.MOM_CLOTH_SKIRT,
                GameObjectClasses.MOM_CLOTH_SHOES,
                GameObjectClasses.MOM_CLOTH_SOCKS);

        gameObjectModel.addObjectsCollection(GameObjectCollections.MOM_APPEARANCE,
                GameObjectClasses.MOM_CLOTH_EYES,
                GameObjectClasses.MOM_CLOTH_LIPS,
                GameObjectClasses.MOM_CLOTH_NOSE,
                GameObjectClasses.MOM_CLOTH_EYEBROWS,
                GameObjectClasses.MOM_CLOTH_FACE_FORM,
                GameObjectClasses.MOM_CLOTH_HAIR_STYLE,
                GameObjectClasses.MOM_CLOTH_EYES_COLOR);

        gameObjectModel.addObjectsCollection(GameObjectCollections.DAD_CLOTH,
                GameObjectClasses.DAD_CLOTH_TOP,
                GameObjectClasses.DAD_CLOTH_BOTTOM,
                GameObjectClasses.DAD_CLOTH_SHOES);

        gameObjectModel.addObjectsCollection(GameObjectCollections.DAD_APPEARANCE,
                GameObjectClasses.DAD_CLOTH_EYES,
                GameObjectClasses.DAD_CLOTH_LIPS,
                GameObjectClasses.DAD_CLOTH_NOSE,
                GameObjectClasses.DAD_CLOTH_EYEBROWS,
                GameObjectClasses.DAD_CLOTH_FACE_FORM,
                GameObjectClasses.DAD_CLOTH_HAIR_STYLE,
                GameObjectClasses.DAD_CLOTH_EYES_COLOR);

        gameObjectModel.addObjectsCollection(GameObjectCollections.BABY_CLOTH,
                GameObjectClasses.BABY_CLOTH_TOP,
                GameObjectClasses.BABY_CLOTH_BOTTOM,
                GameObjectClasses.BABY_CLOTH_SHOES);

        gameObjectModel.addObjectsCollection(GameObjectCollections.BABY_APPEARANCE,
                GameObjectClasses.BABY_CLOTH_EYES,
                GameObjectClasses.BABY_CLOTH_LIPS,
                GameObjectClasses.BABY_CLOTH_NOSE,
                GameObjectClasses.BABY_CLOTH_FACE_FORM,
                GameObjectClasses.BABY_CLOTH_HAIR_STYLE,
                GameObjectClasses.BABY_CLOTH_EYES_COLOR);
    }
}
}
