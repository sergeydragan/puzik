/**
 * Created by SeeD on 04/10/2014.
 */
package com.kekkoneko.puzik.gameplay.cycles
{
import com.kekkoneko.core.eventdispatcher.CoreEvent;
import com.kekkoneko.core.eventdispatcher.CoreEventDispatcher;
import com.kekkoneko.core.timer.TimerModel;
import com.kekkoneko.puzik.controller.notifications.NeedsChangedNotification;
import com.kekkoneko.puzik.controller.notifications.PlayerEnergyUpdatedNotification;
import com.kekkoneko.puzik.gameplay.cycles.vos.NeedsVO;
import com.kekkoneko.puzik.gameplay.player.helpers.PlayerEnergyHelper;
import com.kekkoneko.puzik.gameplay.player.model.PlayersModel;

public class SingleBabyNeedsModel
{
    [Inject]
    public var coreEventDispatcher:CoreEventDispatcher;

    [Inject]
    public var playersModel:PlayersModel;

    private var _babyNeeds:NeedsVO;
    private var _secondsToUpdateNeeds:int = NEEDS_UPDATING_PERIOD;
    private var _secondsToAddEnergy:int = ENERGY_ADD_PERIOD;

    private static const NEEDS_UPDATING_PERIOD:int = 3;
    private static const ENERGY_ADD_PERIOD:int = 60;

    public function SingleBabyNeedsModel()
    {
        _babyNeeds = new NeedsVO();
    }

    public function init():void
    {
        coreEventDispatcher.addEventListener(TimerModel.ONE_SECOND_TIMER, onTimer);
    }

    public function setNeedsVO(needsVO:NeedsVO):void
    {
        _babyNeeds = needsVO;
    }

    public function getNeedsVO():NeedsVO
    {
        return _babyNeeds;
    }

    private function onTimer(e:CoreEvent):void
    {
        updateNeeds();
        updateEnergy();
    }

    private function updateNeeds():void
    {
        _secondsToUpdateNeeds--;
        if (_secondsToUpdateNeeds <= 0)
        {
            _secondsToUpdateNeeds = NEEDS_UPDATING_PERIOD;

            _babyNeeds.foodCurrent -= NEEDS_UPDATING_PERIOD;
            _babyNeeds.washCurrent -= NEEDS_UPDATING_PERIOD;
            _babyNeeds.toiletCurrent -= NEEDS_UPDATING_PERIOD;
            _babyNeeds.happinessCurrent -= NEEDS_UPDATING_PERIOD;

            if (_babyNeeds.foodCurrent < 0)
            {
                _babyNeeds.foodCurrent = 0;
            }

            if (_babyNeeds.washCurrent < 0)
            {
                _babyNeeds.washCurrent = 0;
            }

            if (_babyNeeds.toiletCurrent < 0)
            {
                _babyNeeds.toiletCurrent = 0;
            }

            if (_babyNeeds.happinessCurrent < 0)
            {
                _babyNeeds.happinessCurrent = 0;
            }

            coreEventDispatcher.broadcastMessage(NeedsChangedNotification.NEEDS_CHANGED);
        }
    }

    private function updateEnergy():void
    {
        _secondsToAddEnergy--;
        if (_secondsToAddEnergy <= 0)
        {
            _secondsToAddEnergy = ENERGY_ADD_PERIOD;

            if (playersModel.getPlayer().energy < PlayerEnergyHelper.getMaxEnergyByLevel(playersModel.getPlayer().level))
            {
                playersModel.getPlayer().energy++;
                coreEventDispatcher.broadcastMessage(PlayerEnergyUpdatedNotification.PLAYER_ENERGY_UPDATED);
            }
        }
    }

    public function getAverageNeeds():Number
    {
        var food:Number = _babyNeeds.foodCurrent / _babyNeeds.foodMax;
        var sleep:Number = _babyNeeds.sleepCurrent / _babyNeeds.sleepMax;
        var toilet:Number = _babyNeeds.toiletCurrent / _babyNeeds.toiletMax;
        var wash:Number = _babyNeeds.washCurrent / _babyNeeds.washMax;
        var happiness:Number = _babyNeeds.happinessCurrent / _babyNeeds.happinessMax;

        return (food + sleep + toilet + wash + happiness) / 5;
    }
}
}
