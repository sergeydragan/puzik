/**
 * Created by SeeD on 04/10/2014.
 */
package com.kekkoneko.puzik.gameplay.cycles
{
import com.kekkoneko.core.eventdispatcher.CoreEvent;
import com.kekkoneko.core.eventdispatcher.CoreEventDispatcher;
import com.kekkoneko.core.timer.TimerModel;
import com.kekkoneko.puzik.controller.notifications.NeedsChangedNotification;
import com.kekkoneko.puzik.gameplay.cycles.vos.NeedsVO;
import com.kekkoneko.puzik.gameplay.player.vos.CharacterVO;

import flash.utils.Dictionary;

public class NeedsModel
{
    [Inject]
    public var coreEventDispatcher:CoreEventDispatcher;

    private var _needs:Dictionary;
    private var _secondsToUpdateNeeds:int = NEEDS_UPDATING_PERIOD;

    private static const NEEDS_UPDATING_PERIOD:int = 3;

    public function NeedsModel()
    {
        _needs = new Dictionary();
    }

    public function init():void
    {
        coreEventDispatcher.addEventListener(TimerModel.ONE_SECOND_TIMER, onTimer);
    }

    public function registerNeedsVO(characterVO:CharacterVO, needsVO:NeedsVO):void
    {
        _needs[characterVO] = needsVO;
    }

    public function removeNeedsVO(characterVO:CharacterVO):void
    {
        delete _needs[characterVO];
    }

    private function onTimer(e:CoreEvent):void
    {
        _secondsToUpdateNeeds--;
        if (_secondsToUpdateNeeds <= 0)
        {
            _secondsToUpdateNeeds = NEEDS_UPDATING_PERIOD;

            for each (var currentNeedsVO:NeedsVO in _needs)
            {
                currentNeedsVO.foodCurrent -= NEEDS_UPDATING_PERIOD;
                currentNeedsVO.washCurrent -= NEEDS_UPDATING_PERIOD;
                currentNeedsVO.toiletCurrent -= NEEDS_UPDATING_PERIOD;
                currentNeedsVO.happinessCurrent -= NEEDS_UPDATING_PERIOD;
            }

            coreEventDispatcher.broadcastMessage(NeedsChangedNotification.NEEDS_CHANGED, new NeedsChangedNotification());
        }
    }

}
}
