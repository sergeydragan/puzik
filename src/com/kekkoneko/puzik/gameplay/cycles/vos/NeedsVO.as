/**
 * Created by SeeD on 04/10/2014.
 */
package com.kekkoneko.puzik.gameplay.cycles.vos
{
public class NeedsVO
{
    public var foodCurrent:int;
    public var foodMax:int;

    public var toiletCurrent:int;
    public var toiletMax:int;

    public var washCurrent:int;
    public var washMax:int;

    public var happinessCurrent:int;
    public var happinessMax:int;

    public var sleepCurrent:int;
    public var sleepMax:int;

    public var healthCurrent:int;
    public var healthMax:int;

    public function NeedsVO()
    {
    }
}
}
