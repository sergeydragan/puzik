/**
 * Created by SeeD on 21/01/2015.
 */
package com.kekkoneko.puzik.gameplay.locations.models
{
import flash.geom.Point;

public class LocationVO
{
    public var locationType:String;

    // for friend's house: ID of friend
    public var playerId:String;

    // for own house: coords of room
    public var roomCoords:Point;

    public function LocationVO(locationType:String)
    {
        this.locationType = locationType;
    }
}
}
