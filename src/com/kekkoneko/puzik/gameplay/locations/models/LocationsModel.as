/**
 * Created by sdragan on 20.01.2015.
 */
package com.kekkoneko.puzik.gameplay.locations.models
{
import com.kekkoneko.puzik.gameplay.room.model.RoomModeModel;
import com.kekkoneko.puzik.gameplay.room.view.RoomModeView;

public class LocationsModel
{
    private var _currentLocation:LocationVO;

    private var _currentRoomModel:RoomModeModel;
    private var _currentRoomView:RoomModeView;

    public function LocationsModel()
    {

    }

    public function registerRoom(roomModel:RoomModeModel, roomView:RoomModeView):void
    {
        _currentRoomModel = roomModel;
        _currentRoomView = roomView;
    }

    public function getCurrentRoomModel():RoomModeModel
    {
        return _currentRoomModel;
    }

    public function getCurrentRoomView():RoomModeView
    {
        return _currentRoomView;
    }

    public function destroyCurrentRoom():void
    {
        if (_currentRoomView)
        {
            _currentRoomView.cleanUp();
            if (_currentRoomView.parent)
            {
                _currentRoomView.parent.removeChild(_currentRoomView);
            }
        }

        if (_currentRoomModel)
        {
            _currentRoomModel.cleanUp();
        }

        _currentRoomView = null;
        _currentRoomModel = null;
    }

    public function get currentLocation():LocationVO
    {
        return _currentLocation;
    }

    public function set currentLocation(value:LocationVO):void
    {
        _currentLocation = value;
    }
}
}
