/**
 * Created by SeeD on 21/01/2015.
 */
package com.kekkoneko.puzik.gameplay.locations.vos
{
import com.kekkoneko.puzik.gameplay.locations.models.LocationVO;

public class SwitchLocationVO
{
    public var locationVO:LocationVO;

    public function SwitchLocationVO(locationVO:LocationVO)
    {
        this.locationVO = locationVO;
    }
}
}
