/**
 * Created by SeeD on 21/01/2015.
 */
package com.kekkoneko.puzik.gameplay.locations.constants
{
public class LocationTypes
{
    public static const HOME:String = "Home";
    public static const FRIENDS_HOME:String = "FriendsHome";
    public static const HOSPITAL:String = "Hospital";
    public static const SHOP:String = "Shop";
    public static const CLOTH_SHOP:String = "ClothShop";

    public function LocationTypes()
    {
    }
}
}
