/**
 * Created by SeeD on 21/01/2015.
 */
package com.kekkoneko.puzik.gameplay.locations.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.debug.Debug;
import com.kekkoneko.core.dialog.vos.CloseDialogVO;
import com.kekkoneko.core.dialog.vos.ShowDialogPropertiesVO;
import com.kekkoneko.core.panel.constants.PanelPositions;
import com.kekkoneko.core.panel.vos.ClosePanelVO;
import com.kekkoneko.core.panel.vos.ShowPanelPropertiesVO;
import com.kekkoneko.core.screen.DisplayLayersModel;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.dialog.constants.PuzikDialogNames;
import com.kekkoneko.puzik.gameplay.locations.constants.LocationTypes;
import com.kekkoneko.puzik.gameplay.locations.models.LocationsModel;
import com.kekkoneko.puzik.gameplay.locations.vos.SwitchLocationVO;
import com.kekkoneko.puzik.gameplay.room.model.RoomsModel;
import com.kekkoneko.puzik.gameplay.room.vos.SwitchCurrentRoomVO;
import com.kekkoneko.puzik.panel.constants.PuzikPanelNames;

public class SwitchLocationCommand extends Command
{
    [Inject]
    public var locationsModel:LocationsModel;

    [Inject]
    public var displayLayersModel:DisplayLayersModel;

    [Inject]
    public var roomsModel:RoomsModel;

    public function SwitchLocationCommand()
    {
    }

    override public function execute():void
    {
        var switchLocationVO:SwitchLocationVO = _payload as SwitchLocationVO;
        var newLocationType:String = switchLocationVO.locationVO.locationType;

        if (locationsModel.currentLocation && locationsModel.currentLocation.locationType == newLocationType)
        {
            Debug.info("SwitchLocationCommand was called but newLocationType is same as current one");
            return;
        }

        clearCurrentLocation();
        locationsModel.currentLocation = switchLocationVO.locationVO;

        /* Normally this shouldn't be in one command and in one "switch..case" because of lack of expandability
         But for now we can use it since we are not planning to add A LOT of locations any time soon.
         */
        switch (newLocationType)
        {
            case LocationTypes.HOME:
                coreEventDispatcher.broadcastMessage(PuzikCommands.SWITCH_CURRENT_ROOM, new SwitchCurrentRoomVO(1));

                coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_PANEL, new ShowPanelPropertiesVO(PuzikPanelNames.BOTTOM_MAIN_PANEL, PanelPositions.BOTTOM_CENTER));
                coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_PANEL, new ShowPanelPropertiesVO(PuzikPanelNames.ORDERS_QUEUE_PANEL, PanelPositions.TOP_LEFT));
                coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_PANEL, new ShowPanelPropertiesVO(PuzikPanelNames.NEEDS_PANEL, PanelPositions.CENTER_RIGHT));
                coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_PANEL, new ShowPanelPropertiesVO(PuzikPanelNames.ACTIVE_CHARACTER_PANEL, PanelPositions.TOP_CENTER));
                break;

            case LocationTypes.FRIENDS_HOME:
                Debug.error("Friend's home is not implemented yet");
                break;

            case LocationTypes.HOSPITAL:
                coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_DIALOG, new ShowDialogPropertiesVO(PuzikDialogNames.HOSPITAL));
                break;

            case LocationTypes.SHOP:
                coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_DIALOG, new ShowDialogPropertiesVO(PuzikDialogNames.SHOP));
                break;

            case LocationTypes.CLOTH_SHOP:
                coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_DIALOG, new ShowDialogPropertiesVO(PuzikDialogNames.CLOTH_SHOP));
                break;
        }
    }

    private function clearCurrentLocation():void
    {
        if (!locationsModel.currentLocation)
        {
            return;
        }

        switch (locationsModel.currentLocation.locationType)
        {
            case LocationTypes.HOME:
            case LocationTypes.FRIENDS_HOME:
                locationsModel.destroyCurrentRoom();
                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.BOTTOM_MAIN_PANEL));
                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.ORDERS_QUEUE_PANEL));
                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.NEEDS_PANEL));
                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.ACTIVE_CHARACTER_PANEL));
                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.ACTION_SELECTION_PANEL));
                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.CHARACTER_ACTION_SELECTION_PANEL));
                break;

            case LocationTypes.SHOP:
                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_DIALOG, new CloseDialogVO(PuzikDialogNames.SHOP));
                break;

            case LocationTypes.CLOTH_SHOP:
                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_DIALOG, new CloseDialogVO(PuzikDialogNames.CLOTH_SHOP));
                break;

            case LocationTypes.HOSPITAL:
                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_DIALOG, new CloseDialogVO(PuzikDialogNames.HOSPITAL));
                break;
        }
    }
}
}