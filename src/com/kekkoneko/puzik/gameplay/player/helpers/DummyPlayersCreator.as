/**
 * Created by sdragan on 27.01.2015.
 */
package com.kekkoneko.puzik.gameplay.player.helpers
{
import com.kekkoneko.puzik.gameplay.player.constants.PlayerIDs;
import com.kekkoneko.puzik.gameplay.player.vos.PlayerVO;

public class DummyPlayersCreator
{
    public function DummyPlayersCreator()
    {

    }

    public static function generateDummyPlayersJSON():String
    {
        var players:Array = [];
        var playerVO:PlayerVO;

        playerVO = new PlayerVO();
        playerVO.id = PlayerIDs.OWN_PLAYER_ID;
        playerVO.name = "SeeD";
        playerVO.level = 7;
        playerVO.energy = 5;
        playerVO.money = 800;
        playerVO.hc = 20;
        playerVO.xp = 2;
        playerVO.roomsCount = 3;
        playerVO.dailyVisitBonus = false;
        playerVO.dailyVisitDay = 2;
        players.push(playerVO);

        playerVO = new PlayerVO();
        playerVO.id = 15535;
        playerVO.name = "Oldfag_228";
        playerVO.level = 14;
        playerVO.energy = 8;
        playerVO.money = 2000;
        playerVO.hc = 30;
        playerVO.xp = 0;
        playerVO.roomsCount = 3;
        playerVO.dailyVisitBonus = false;
        playerVO.dailyVisitDay = 1;
        players.push(playerVO);

        return JSON.stringify(players);
    }
}
}
