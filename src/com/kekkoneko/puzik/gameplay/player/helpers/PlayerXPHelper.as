/**
 * Created by SeeD on 06/03/2015.
 */
package com.kekkoneko.puzik.gameplay.player.helpers
{
public class PlayerXPHelper
{
    private static const XP_TO_NEXT_LEVEL:Array =
            [3, 20, 40, 50, 60, 20, 30, 45, 65, 90, 110, 140, 180, 220, 250, 270, 290, 320,
                340, 360, 380, 410, 430, 460, 480, 500, 520, 550, 580, 610, 635, 680, 740, 780, 830, 5000];


    public function PlayerXPHelper()
    {
    }

    public static function getMaxXPByLevel(playerLevel:int):int
    {
        if (playerLevel <= XP_TO_NEXT_LEVEL.length)
        {
            return XP_TO_NEXT_LEVEL[playerLevel - 1];
        }
        else
        {
            return XP_TO_NEXT_LEVEL[XP_TO_NEXT_LEVEL.length - 1];
        }
    }

    public static function getXPBySumOfObjectEffects(sumOfEffects:int, averageNeeds:Number):int
    {
        // todo: посчитать добавляемый XP в зависимости от эффективности использованных объектов
        return sumOfEffects * averageNeeds * 0.01;
    }
}
}
