/**
 * Created by SeeD on 06/03/2015.
 */
package com.kekkoneko.puzik.gameplay.player.helpers
{
public class PlayerEnergyHelper
{
    public function PlayerEnergyHelper()
    {
    }

    public static function getMaxEnergyByLevel(playerLevel:int):int
    {
        if (playerLevel <= 12)
        {
            return 8;
        }
        else
        {
            return 8 + Math.floor((playerLevel - 12) / 2);
        }
    }
}
}
