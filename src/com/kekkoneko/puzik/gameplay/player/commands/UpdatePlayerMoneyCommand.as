/**
 * Created by SeeD on 28/12/2014.
 */
package com.kekkoneko.puzik.gameplay.player.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.puzik.controller.notifications.PlayerMoneyUpdatedNotification;
import com.kekkoneko.puzik.gameplay.player.model.PlayersModel;
import com.kekkoneko.puzik.gameplay.player.vos.UpdatePlayerMoneyVO;

public class UpdatePlayerMoneyCommand extends Command
{
    [Inject]
    public var playersModel:PlayersModel;

    public function UpdatePlayerMoneyCommand()
    {
    }

    override public function execute():void
    {
        var vo:UpdatePlayerMoneyVO = _payload as UpdatePlayerMoneyVO;
        playersModel.getPlayer().money = vo.newMoney;
        playersModel.getPlayer().hc = vo.newPremium;

        coreEventDispatcher.broadcastMessage(
                PlayerMoneyUpdatedNotification.PLAYER_MONEY_UPDATED,
                new PlayerMoneyUpdatedNotification(vo.newMoney, vo.newPremium)
        );
    }
}
}
