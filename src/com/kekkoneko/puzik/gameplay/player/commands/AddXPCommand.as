/**
 * Created by SeeD on 08/02/2015.
 */
package com.kekkoneko.puzik.gameplay.player.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.controller.notifications.PlayerLevelUpdatedNotification;
import com.kekkoneko.puzik.controller.notifications.PlayerXPUpdatedNotification;
import com.kekkoneko.puzik.gameplay.player.helpers.PlayerXPHelper;
import com.kekkoneko.puzik.gameplay.player.model.PlayersModel;
import com.kekkoneko.puzik.gameplay.player.vos.AddXPVO;
import com.kekkoneko.puzik.gameplay.player.vos.LevelUpVO;

public class AddXPCommand extends Command
{
    [Inject]
    public var playersModel:PlayersModel;

    public function AddXPCommand()
    {
    }

    override public function execute():void
    {
        var vo:AddXPVO = _payload as AddXPVO;
        var xpCap:int = PlayerXPHelper.getMaxXPByLevel(playersModel.getPlayer().level);

        if (playersModel.getPlayer().xp + vo.xp < xpCap)
        {
            playersModel.getPlayer().xp += vo.xp;
        }
        else
        {
            playersModel.getPlayer().xp = (playersModel.getPlayer().xp + vo.xp) - xpCap;
            coreEventDispatcher.broadcastMessage(PuzikCommands.LEVEL_UP, new LevelUpVO());
            coreEventDispatcher.broadcastMessage(PlayerLevelUpdatedNotification.PLAYER_LEVEL_UPDATED, new PlayerLevelUpdatedNotification(playersModel.getPlayer().level));
        }

        coreEventDispatcher.broadcastMessage(PlayerXPUpdatedNotification.PLAYER_XP_UPDATED, new PlayerXPUpdatedNotification(playersModel.getPlayer().xp));
    }

}
}
