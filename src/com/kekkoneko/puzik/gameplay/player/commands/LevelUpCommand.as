/**
 * Created by SeeD on 08/02/2015.
 */
package com.kekkoneko.puzik.gameplay.player.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.puzik.controller.notifications.PlayerLevelUpdatedNotification;
import com.kekkoneko.puzik.gameplay.player.model.PlayersModel;
import com.kekkoneko.puzik.gameplay.player.vos.LevelUpVO;

public class LevelUpCommand extends Command
{
    [Inject]
    public var playersModel:PlayersModel;

    public function LevelUpCommand()
    {
    }

    override public function execute():void
    {
        var vo:LevelUpVO = _payload as LevelUpVO;
        playersModel.getPlayer().level++;
        coreEventDispatcher.broadcastMessage(PlayerLevelUpdatedNotification.PLAYER_LEVEL_UPDATED, new PlayerLevelUpdatedNotification(playersModel.getPlayer().level));
    }

}
}
