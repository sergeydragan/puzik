/**
 * Created by sdragan on 27.01.2015.
 */
package com.kekkoneko.puzik.gameplay.player.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.puzik.gameplay.cycles.SingleBabyNeedsModel;
import com.kekkoneko.puzik.gameplay.cycles.vos.NeedsVO;
import com.kekkoneko.puzik.gameplay.player.model.PlayersModel;
import com.kekkoneko.puzik.gameplay.player.vos.ParsePlayersJSONVO;
import com.kekkoneko.puzik.gameplay.player.vos.PlayerVO;
import com.kekkoneko.puzik.gameplay.room.model.RoomsModel;
import com.kekkoneko.puzik.gameplay.room.vos.RoomVO;

public class ParsePlayersJSONCommand extends Command
{
    [Inject]
    public var playersModel:PlayersModel;

    [Inject]
    public var singleBabyNeedsModel:SingleBabyNeedsModel;

    [Inject]
    public var roomsModel:RoomsModel;

    public function ParsePlayersJSONCommand()
    {
    }

    override public function execute():void
    {
        var vo:ParsePlayersJSONVO = _payload as ParsePlayersJSONVO;
        var json:String = vo.json;

        var playersArray:Array = JSON.parse(json) as Array;
        var playerVO:PlayerVO;

        for (var i:int = 0; i < playersArray.length; i++)
        {
            playerVO = new PlayerVO();
            playerVO.id = playersArray[i].id;
            playerVO.name = playersArray[i].name;
            playerVO.level = playersArray[i].level;
            playerVO.xp = playersArray[i].xp;
            playerVO.money = playersArray[i].money;
            playerVO.hc = playersArray[i].premium;
            playerVO.energy = playersArray[i].energy;
            playerVO.avatar = playersArray[i].avatar;
            playerVO.roomsCount = playersArray[i].roomsCount;
            playerVO.dailyVisitBonus = playersArray[i].dailyVisitBonus;
            playerVO.dailyVisitDay = playersArray[i].dailyVisitDay;
            playerVO.tutorialPhase = playersArray[i].tutorialPhase;
            createRooms(playerVO);
            playersModel.addPlayer(playerVO);
        }

        // для теста
        var needsVO:NeedsVO = new NeedsVO();
        needsVO.foodMax = 200;
        needsVO.foodCurrent = 150;
        needsVO.sleepMax = 3000;
        needsVO.sleepCurrent = 2800;
        needsVO.washMax = 300;
        needsVO.washCurrent = 100;
        needsVO.toiletMax = 250;
        needsVO.toiletCurrent = 20;
        needsVO.happinessMax = 150;
        needsVO.happinessCurrent = 150;
        singleBabyNeedsModel.setNeedsVO(needsVO);

        singleBabyNeedsModel.init();

        if (vo.callback)
        {
            vo.callback();
        }
    }

    private function createRooms(playerVO:PlayerVO):void
    {
        for (var i:int = 1; i <= playerVO.roomsCount; i++)
        {
            var roomVO:RoomVO = new RoomVO();
            roomVO.id = i;
            roomsModel.addRoom(roomVO, playerVO.id)
        }
    }
}
}
