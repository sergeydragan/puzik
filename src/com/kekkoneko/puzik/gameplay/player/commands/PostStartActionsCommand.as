/**
 * Created by sdragan on 27.01.2015.
 */
package com.kekkoneko.puzik.gameplay.player.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.dialog.vos.ShowDialogPropertiesVO;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.dialog.constants.PuzikDialogNames;
import com.kekkoneko.puzik.dialog.dialogs.dailyReward.DailyRewardDialogPropertiesVO;
import com.kekkoneko.puzik.gameplay.player.model.PlayersModel;
import com.kekkoneko.puzik.gameplay.player.vos.PlayerVO;
import com.kekkoneko.puzik.gameplay.player.vos.UpdatePlayerMoneyVO;

public class PostStartActionsCommand extends Command
{

    [Inject]
    public var playersModel:PlayersModel;

    public function PostStartActionsCommand()
    {

    }

    override public function execute():void
    {
        var playerVO:PlayerVO = playersModel.getPlayer();

        coreEventDispatcher.broadcastMessage(PuzikCommands.UPDATE_PLAYER_MONEY, new UpdatePlayerMoneyVO(playerVO.money, playerVO.hc));

        if (playerVO.dailyVisitBonus)
        {
            coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_DIALOG, new ShowDialogPropertiesVO(PuzikDialogNames.DAILY_REWARD, new DailyRewardDialogPropertiesVO()));
        }
    }
}
}
