/**
 * Created by sdragan on 27.01.2015.
 */
package com.kekkoneko.puzik.gameplay.player.model
{
import com.kekkoneko.puzik.gameplay.player.vos.PlayerVO;

import flash.utils.Dictionary;

public class PlayersModel
{
    private var _playerVOs:Dictionary;

    public static const OWN_PLAYER_ID:int = -1;

    public function PlayersModel()
    {
        _playerVOs = new Dictionary();
    }

    public function addPlayer(playerVO:PlayerVO):void
    {
        _playerVOs[playerVO.id] = playerVO;
    }

    public function getPlayer(playerId:int = OWN_PLAYER_ID):PlayerVO
    {
        return _playerVOs[playerId];
    }
}
}
