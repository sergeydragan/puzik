/**
 * Created by SeeD on 13/01/2015.
 */
package com.kekkoneko.puzik.gameplay.player.vos
{
public class UpdatePlayerMoneyVO
{
    public var newMoney:int = -1;
    public var newPremium:int = -1;

    public function UpdatePlayerMoneyVO(newMoney:int, newPremium:int)
    {
        this.newMoney = newMoney;
        this.newPremium = newPremium;
    }
}
}
