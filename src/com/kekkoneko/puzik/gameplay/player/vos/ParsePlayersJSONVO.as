/**
 * Created by sdragan on 27.01.2015.
 */
package com.kekkoneko.puzik.gameplay.player.vos
{
public class ParsePlayersJSONVO
{
    public var json:String;
    public var callback:Function;

    public function ParsePlayersJSONVO(json:String, callback:Function = null)
    {
        this.json = json;
        this.callback = callback;
    }
}
}
