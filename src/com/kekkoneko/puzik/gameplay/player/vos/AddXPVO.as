/**
 * Created by SeeD on 11/03/2015.
 */
package com.kekkoneko.puzik.gameplay.player.vos
{
public class AddXPVO
{
    public var xp:int;

    public function AddXPVO(xp:int)
    {
        this.xp = xp;
    }
}
}
