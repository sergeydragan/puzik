/**
 * Created by SeeD on 11/01/2015.
 */
package com.kekkoneko.puzik.gameplay.player.vos
{
public class PlayerVO
{
    public var id:int;
    public var name:String;
    public var money:int;
    public var hc:int;
    public var energy:int;
    public var level:int;
    public var xp:int;
    public var avatar:String;

    /** Вот на этом остановимся подробнее. Комнаты мне видится правильными хранить так же, как и инвентарь,
     * но в RoomVO практически нЕчего хранить (по большому счёту - ВООБЩЕ нечего). Но быть может, в будущем
     * нужно будет там хранить какие-нибудь дополнительные параметры, или ещё что-то в таком духе.
     * Потому когда-нибудь лучше хранить комнаты (наверное) в отдельном JSON и парсить так же, как инвентарь.
     * Но пока что будем просто использовать переменную с количеством комнат у игрока - которая пока что будет
     * храниться вместе с остальными параметрами игрока в PlayerVO.
     */
    public var roomsCount:int;

    public var tutorialPhase:int;

    public var dailyVisitBonus:Boolean;
    public var dailyVisitDay:int;

    public var characterVOs:Vector.<CharacterVO>;

    public function PlayerVO()
    {
        characterVOs = new <CharacterVO>[];
    }


}
}
