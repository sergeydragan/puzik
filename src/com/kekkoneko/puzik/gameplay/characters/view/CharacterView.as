/**
 * Created by SeeD on 30/11/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.view
{
import com.kekkoneko.puzik.gameplay.characters.constants.EmotionTypes;
import com.kekkoneko.puzik.gameplay.characters.helpers.AppearanceHelper;
import com.kekkoneko.puzik.gameplay.characters.helpers.EmotionsHelper;
import com.kekkoneko.puzik.gameplay.characters.model.CharacterModel;
import com.kekkoneko.puzik.gameplay.characters.model.CharactersModel;

import flash.display.MovieClip;
import flash.display.Sprite;

public class CharacterView extends Sprite
{
    [Inject]
    public var charactersModel:CharactersModel;

    public var characterModel:CharacterModel;
    public var appearance:MovieClip;

    public var appearanceHelper:AppearanceHelper;
    public var emotionsHelper:EmotionsHelper;

    public static const SCALE:Number = 0.84;

    public function CharacterView(model:CharacterModel)
    {
        characterModel = model;
        characterModel.view = this;
    }

    public function initHelpers():void
    {
        appearanceHelper = CharactersModel.getAppearanceHelper(characterModel);
        emotionsHelper = CharactersModel.getEmotionsHelper(characterModel);
    }

    public function setAnimation(animationMC:MovieClip):void
    {
        if (appearance && appearance.parent)
        {
            removeChild(appearance);
        }
        appearance = animationMC;
        addChild(appearance);

        updateAppearance();
        setEmotion(EmotionTypes.NORMAL);
        setEmotion(EmotionTypes.NORMAL, true);

        appearance.scaleX = appearance.scaleY = SCALE;
    }

    public function setEmotion(emotionType:String, isForBaby:Boolean = false):void
    {
        if (isForBaby)
        {
            if (characterModel.babyInHands)
            {
                var babyModel:CharacterModel = characterModel.babyInHands;
                var babyMC:MovieClip;
                if (appearance.container && appearance.container.baby)
                {
                    babyMC = appearance.container.baby;
                    babyModel.view.emotionsHelper.setEmotion(emotionType, babyMC);
                }
            }
        }
        else
        {
            emotionsHelper.setEmotion(emotionType);
        }

    }

    public function updateAppearance():void
    {
        appearanceHelper.updateAppearance(characterModel.appearanceConfig);

        if (characterModel.babyInHands)
        {
            var babyModel:CharacterModel = characterModel.babyInHands;
            var babyMC:MovieClip;
            if (appearance.container && appearance.container.baby)
            {
                babyMC = appearance.container.baby;
                babyModel.view.appearanceHelper.updateAppearance(babyModel.appearanceConfig, babyMC);

                babyModel.view.emotionsHelper.setEmotion(EmotionTypes.NORMAL, babyMC);
            }
        }
    }

    public function cleanUp():void
    {

    }
}
}
