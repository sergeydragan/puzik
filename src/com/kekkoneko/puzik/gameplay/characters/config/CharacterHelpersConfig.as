/**
 * Created by SeeD on 25/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.config
{
import com.kekkoneko.puzik.gameplay.characters.appearance.BabyAppearanceConfig;
import com.kekkoneko.puzik.gameplay.characters.appearance.DadAppearanceConfig;
import com.kekkoneko.puzik.gameplay.characters.appearance.MomAppearanceConfig;
import com.kekkoneko.puzik.gameplay.characters.constants.CharacterTypes;
import com.kekkoneko.puzik.gameplay.characters.helpers.BabyAppearanceHelper;
import com.kekkoneko.puzik.gameplay.characters.helpers.BabyEmotionsHelper;
import com.kekkoneko.puzik.gameplay.characters.helpers.DadAppearanceHelper;
import com.kekkoneko.puzik.gameplay.characters.helpers.DadEmotionsHelper;
import com.kekkoneko.puzik.gameplay.characters.helpers.MomAppearanceHelper;
import com.kekkoneko.puzik.gameplay.characters.helpers.MomEmotionsHelper;
import com.kekkoneko.puzik.gameplay.characters.model.CharactersModel;

public class CharacterHelpersConfig
{
    [Inject]
    public var charactersModel:CharactersModel;

    public function CharacterHelpersConfig()
    {
    }

    public function init():void
    {
        configAppearanceHelpers();
        configEmotionsHelpers();
        configAppearanceConfigs();
    }

    private function configAppearanceHelpers():void
    {
        CharactersModel.registerAppearanceHelper(CharacterTypes.MOM, MomAppearanceHelper);
        CharactersModel.registerAppearanceHelper(CharacterTypes.BABY, BabyAppearanceHelper);
        CharactersModel.registerAppearanceHelper(CharacterTypes.DAD, DadAppearanceHelper);
    }

    private function configEmotionsHelpers():void
    {
        CharactersModel.registerEmotionsHelper(CharacterTypes.MOM, MomEmotionsHelper);
        CharactersModel.registerEmotionsHelper(CharacterTypes.BABY, BabyEmotionsHelper);
        CharactersModel.registerEmotionsHelper(CharacterTypes.DAD, DadEmotionsHelper);
    }

    private function configAppearanceConfigs():void
    {
        CharactersModel.registerAppearanceConfig(CharacterTypes.MOM, MomAppearanceConfig);
        CharactersModel.registerAppearanceConfig(CharacterTypes.DAD, DadAppearanceConfig);
        CharactersModel.registerAppearanceConfig(CharacterTypes.BABY, BabyAppearanceConfig);
    }
}
}
