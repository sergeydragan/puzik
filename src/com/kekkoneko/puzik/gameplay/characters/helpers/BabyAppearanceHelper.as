/**
 * Created by SeeD on 25/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.helpers
{
import com.kekkoneko.puzik.gameplay.characters.appearance.BabyAppearanceConfig;
import com.kekkoneko.puzik.gameplay.characters.appearance.CharacterAppearanceConfig;
import com.kekkoneko.puzik.gameplay.characters.view.CharacterView;

import flash.display.MovieClip;

public class BabyAppearanceHelper extends AppearanceHelper
{
    public function BabyAppearanceHelper(characterView:CharacterView)
    {
        super(characterView);
    }

    override public function updateAppearance(appearanceConfig:CharacterAppearanceConfig, customContainer:MovieClip = null):void
    {
        if (!_characterView.appearance || !_characterView.appearance.container)
        {
            return;
        }

        var appearanceInfo:BabyAppearanceConfig = appearanceConfig as BabyAppearanceConfig;

        var container:MovieClip;
        if (!customContainer)
        {
            container = _characterView.appearance.container;
        }
        else
        {
            container = customContainer;
        }

        if (container)
        {
            if (container.head)
            {
                if (container.head.mouth) container.head.mouth.gotoAndStop(appearanceInfo.babyLips);
                if (container.head.faceForm) container.head.faceForm.gotoAndStop(appearanceInfo.babyFaceForm);
                if (container.head.hair) container.head.hair.gotoAndStop(appearanceInfo.babyHairStyle);
                if (container.head.eyes) container.head.eyes.gotoAndStop(appearanceInfo.babyEyes);
                if (container.head.nose) container.head.nose.gotoAndStop(appearanceInfo.babyNose);
                if (container.head.pupil1) container.head.pupil1.gotoAndStop(appearanceInfo.babyEyesColor);
                if (container.head.pupil2) container.head.pupil2.gotoAndStop(appearanceInfo.babyEyesColor);
                if (container.head.hat) gotoFrameName(container.head.hat, appearanceInfo.babyHat);
            }

            if (container.clothTop) gotoFrameName(container.clothTop, appearanceInfo.babyClothTop);
            if (container.hand11) gotoFrameName(container.hand11, appearanceInfo.babyClothTop);
            if (container.hand12) gotoFrameName(container.hand12, appearanceInfo.babyClothTop);
            if (container.hand21) gotoFrameName(container.hand21, appearanceInfo.babyClothTop);
            if (container.hand22) gotoFrameName(container.hand22, appearanceInfo.babyClothTop);
            if (container.clothBottom) gotoFrameName(container.clothBottom, appearanceInfo.babyClothBottom);
            if (container.leg1) gotoFrameName(container.leg1, appearanceInfo.babyClothBottom);
            if (container.leg2) gotoFrameName(container.leg2, appearanceInfo.babyClothBottom);
            if (container.foot1) gotoFrameName(container.foot1, appearanceInfo.babyClothBottom);
            if (container.foot2) gotoFrameName(container.foot2, appearanceInfo.babyClothBottom);

            /*
             if (_mudIsVisible)
             {
             if (_appearance.container.hand12 && _appearance.container.hand12.mud) _appearance.container.hand12.mud.visible = true;
             if (_appearance.container.hand22 && _appearance.container.hand22.mud) _appearance.container.hand22.mud.visible = true;
             if (_appearance.container.hand12_1 && _appearance.container.hand12_1.mud) _appearance.container.hand12_1.mud.visible = true;
             if (_appearance.container.head.faceForm.mud) _appearance.container.head.faceForm.mud.visible = true;
             }
             else
             {
             if (_appearance.container.hand12 && _appearance.container.hand12.mud) _appearance.container.hand12.mud.visible = false;
             if (_appearance.container.hand22 && _appearance.container.hand22.mud) _appearance.container.hand22.mud.visible = false;
             if (_appearance.container.hand12_1 && _appearance.container.hand12_1.mud) _appearance.container.hand12_1.mud.visible = false;
             if (_appearance.container.head.faceForm.mud) _appearance.container.head.faceForm.mud.visible = false;
             }
             */
        }

    }
}
}
