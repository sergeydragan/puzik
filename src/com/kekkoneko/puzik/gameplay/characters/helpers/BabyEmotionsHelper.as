/**
 * Created by SeeD on 23/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.helpers
{
import com.kekkoneko.puzik.gameplay.characters.appearance.BabyAppearanceConfig;
import com.kekkoneko.puzik.gameplay.characters.constants.EmotionTypes;
import com.kekkoneko.puzik.gameplay.characters.view.CharacterView;

import flash.display.MovieClip;

public class BabyEmotionsHelper extends EmotionsHelper
{
    public function BabyEmotionsHelper(characterView:CharacterView)
    {
        super(characterView);
    }

    override public function setEmotion(emotionType:String, customContainer:MovieClip = null):void
    {
        if (!_characterView.appearance.container || !_characterView.appearance.container.head || !_characterView.appearance.container.head.emotion)
        {
            return;
        }

        var appearanceInfo:BabyAppearanceConfig = _characterView.characterModel.appearanceConfig as BabyAppearanceConfig;

        var container:MovieClip;
        if (!customContainer)
        {
            container = _characterView.appearance.container;
        }
        else
        {
            container = customContainer;
        }
        var head:MovieClip = container.head;

        if (emotionType == EmotionTypes.NORMAL)		// для этой эмоции нам нужны элементы лица
        {
            head.eyes.visible = true;
            head.pupil1.visible = true;
            head.pupil2.visible = true;
            head.mouth.visible = true;
            head.winking.visible = true;
        }
        else if (emotionType == EmotionTypes.HAPPY || emotionType == EmotionTypes.EATING || emotionType == EmotionTypes.OPEN_MOUTH || emotionType == EmotionTypes.WITH_PACIFIER)	// для этой - нужны только глаза
        {
            head.eyes.visible = true;
            head.pupil1.visible = true;
            head.pupil2.visible = true;
            head.winking.visible = true;
            head.mouth.visible = false;
        }
        else if (emotionType == EmotionTypes.CRAPPING)	// для этой - нужен только рот
        {
            head.eyes.visible = false;
            head.pupil1.visible = false;
            head.pupil2.visible = false;
            head.winking.visible = false;
            head.mouth.visible = true;
        }
        else										// для этой - не нужно ни глаз, ни рта
        {
            head.eyes.visible = false;
            head.pupil1.visible = false;
            head.pupil2.visible = false;
            head.winking.visible = false;
            head.mouth.visible = false;
        }

        // ну и покажем нужную эмоцию
        if (container.emotion)
        {
//            container.emotion.gotoAndStop(emotionType);
            container.emotion.gotoAndStop("blank");
            container.emotion.visible = false;

            // и ещё остаточно кастомизацию навесим
            if (container.emotion.emotionContainer && container.emotion.emotionContainer.nose) container.emotion.emotionContainer.nose.gotoAndStop(appearanceInfo.babyNose);
            if (container.emotion.emotionContainer && container.emotion.emotionContainer.pupil1) container.emotion.emotionContainer.pupil1.gotoAndStop(appearanceInfo.babyEyesColor);
            if (container.emotion.emotionContainer && container.emotion.emotionContainer.pupil2) container.emotion.emotionContainer.pupil2.gotoAndStop(appearanceInfo.babyEyesColor);
            if (container.emotion.emotionContainer && container.emotion.emotionContainer.eyes) container.emotion.emotionContainer.eyes.gotoAndStop(appearanceInfo.babyEyes);

        }

        if (head.emotion)
        {
            head.emotion.gotoAndStop(emotionType);

            // и ещё остаточно кастомизацию навесим
            if (head.emotion.emotionContainer && head.emotion.emotionContainer.nose) head.emotion.emotionContainer.nose.gotoAndStop(appearanceInfo.babyNose);
            if (head.emotion.emotionContainer && head.emotion.emotionContainer.pupil1) head.emotion.emotionContainer.pupil1.gotoAndStop(appearanceInfo.babyEyesColor);
            if (head.emotion.emotionContainer && head.emotion.emotionContainer.pupil2) head.emotion.emotionContainer.pupil2.gotoAndStop(appearanceInfo.babyEyesColor);
            if (head.emotion.emotionContainer && head.emotion.emotionContainer.eyes) head.emotion.emotionContainer.eyes.gotoAndStop(appearanceInfo.babyEyes);
        }
    }
}
}
