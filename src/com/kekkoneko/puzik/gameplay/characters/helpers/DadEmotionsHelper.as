/**
 * Created by SeeD on 23/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.helpers
{
import com.kekkoneko.puzik.gameplay.characters.appearance.DadAppearanceConfig;
import com.kekkoneko.puzik.gameplay.characters.constants.EmotionTypes;
import com.kekkoneko.puzik.gameplay.characters.view.CharacterView;

import flash.display.MovieClip;

public class DadEmotionsHelper extends EmotionsHelper
{
    public function DadEmotionsHelper(characterView:CharacterView)
    {
        super(characterView);
    }

    override public function setEmotion(emotionType:String, customContainer:MovieClip = null):void
    {
        if (!_characterView.appearance.container || !_characterView.appearance.container.head || !_characterView.appearance.container.head.emotion)
        {
            return;
        }

        var appearanceInfo:DadAppearanceConfig = _characterView.characterModel.appearanceConfig as DadAppearanceConfig;
        var head:MovieClip = _characterView.appearance.container.head;

        if (emotionType == EmotionTypes.NORMAL)
        {
            head.winking.visible = true;
            head.lips.visible = true;
            head.brows.visible = true;
            head.eyes.visible = true;
            head.pupil1.visible = true;
            head.pupil2.visible = true;

            head.emotion.pupil1.visible = false;
            head.emotion.pupil2.visible = false;
        }
        else if (emotionType == EmotionTypes.TENDER || emotionType == EmotionTypes.SAD)
        {
            head.winking.visible = false;
            head.lips.visible = false;
            head.brows.visible = false;
            head.eyes.visible = false;
            head.pupil1.visible = false;
            head.pupil2.visible = false;

            head.emotion.pupil1.visible = true;
            head.emotion.pupil2.visible = true;
        }
        else if (emotionType == EmotionTypes.HAPPY)
        {
            head.winking.visible = false;
            head.lips.visible = false;
            head.brows.visible = true;
            head.eyes.visible = false;
            head.pupil1.visible = false;
            head.pupil2.visible = false;

            head.emotion.pupil1.visible = false;
            head.emotion.pupil2.visible = false;
        }

        if (head.emotion)
        {
            head.emotion.gotoAndStop(emotionType);
        }
    }
}
}
