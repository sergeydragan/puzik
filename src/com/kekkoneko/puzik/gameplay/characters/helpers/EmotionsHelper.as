/**
 * Created by SeeD on 23/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.helpers
{
import com.kekkoneko.puzik.gameplay.characters.view.CharacterView;

import flash.display.MovieClip;

public class EmotionsHelper
{
    protected var _characterView:CharacterView;

    public function EmotionsHelper(characterView:CharacterView)
    {
        _characterView = characterView;
    }

    public function setEmotion(emotionType:String, customContainer:MovieClip = null):void
    {
        throw new Error("To be overriden by subclass");
    }
}
}
