/**
 * Created by SeeD on 25/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.helpers
{
import com.kekkoneko.puzik.gameplay.characters.appearance.CharacterAppearanceConfig;
import com.kekkoneko.puzik.gameplay.characters.view.CharacterView;

import flash.display.FrameLabel;
import flash.display.MovieClip;

public class AppearanceHelper
{
    protected var _characterView:CharacterView;

    public function AppearanceHelper(characterView:CharacterView)
    {
        _characterView = characterView;
    }

    public function updateAppearance(appearanceConfig:CharacterAppearanceConfig, customContainer:MovieClip = null):void
    {
        throw new Error("To be overriden by subclass");
    }

    protected function gotoFrameName(mc:MovieClip, frameName:String):void
    {
        if (!mc) return;

        for each (var currentFrameLabel:FrameLabel in mc.currentScene.labels)
        {
            if (currentFrameLabel.name == frameName)
            {
                mc.gotoAndStop(currentFrameLabel.frame);
                return;
            }
        }
        mc.gotoAndStop(1);
//        Debug.warning("Error in gotoFrameName(), " + this + ", mc: " + mc.name + ", frameName: " + frameName);
    }
}
}
