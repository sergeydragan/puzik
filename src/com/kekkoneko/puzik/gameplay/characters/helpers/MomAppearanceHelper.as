/**
 * Created by SeeD on 25/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.helpers
{
import com.kekkoneko.puzik.gameplay.characters.appearance.CharacterAppearanceConfig;
import com.kekkoneko.puzik.gameplay.characters.appearance.MomAppearanceConfig;
import com.kekkoneko.puzik.gameplay.characters.view.CharacterView;

import flash.display.MovieClip;

public class MomAppearanceHelper extends AppearanceHelper
{
    public function MomAppearanceHelper(characterView:CharacterView)
    {
        super(characterView);
    }

    override public function updateAppearance(appearanceConfig:CharacterAppearanceConfig, customContainer:MovieClip = null):void
    {
        if (!_characterView.appearance || !_characterView.appearance.container)
        {
            return;
        }

        var appearanceInfo:MomAppearanceConfig = appearanceConfig as MomAppearanceConfig;
        var container:MovieClip = _characterView.appearance.container;

        if (container.head)
        {
            if (container.head.lips)
            {
                container.head.lips.gotoAndStop(appearanceInfo.momLips);
                if (container.head.lips.lipsVisage) container.head.lips.lipsVisage.gotoAndStop(appearanceInfo.momLipsVisage);
            }
            if (container.head.faceForm) container.head.faceForm.gotoAndStop(appearanceInfo.momFaceForm);
            if (container.head.hair) container.head.hair.gotoAndStop(appearanceInfo.momHairStyle);
            if (container.head.eyes)
            {
                container.head.eyes.gotoAndStop(appearanceInfo.momEyes);
                if (container.head.eyes.eyesVisage) container.head.eyes.eyesVisage.gotoAndStop(appearanceInfo.momEyesVisage);
                if (container.head.emotion)
                {
                    container.head.emotion.pupil1.gotoAndStop(appearanceInfo.momEyesColor);
                    container.head.emotion.pupil2.gotoAndStop(appearanceInfo.momEyesColor);
                }
            }
            if (container.head.winking) container.head.winking.gotoAndStop(appearanceInfo.momEyesVisage);
            if (container.head.brows) container.head.brows.gotoAndStop(appearanceInfo.momEyebrows);
            if (container.head.nose) container.head.nose.gotoAndStop(appearanceInfo.momNose);
            if (container.head.pupil1) container.head.pupil1.gotoAndStop(appearanceInfo.momEyesColor);
            if (container.head.pupil2) container.head.pupil2.gotoAndStop(appearanceInfo.momEyesColor);

            gotoFrameName(container.head.glasses, appearanceInfo.momGlasses);
        }
        if (container.clothTop && container.neck && container.hand11 && container.hand12 && container.hand21 && container.hand22)
        {
            gotoFrameName(container.clothTop, appearanceInfo.momClothTop);
            gotoFrameName(container.neck, appearanceInfo.momClothTop);
            gotoFrameName(container.hand11, appearanceInfo.momClothTop);
            gotoFrameName(container.hand12, appearanceInfo.momClothTop);
            gotoFrameName(container.hand21, appearanceInfo.momClothTop);
            gotoFrameName(container.hand22, appearanceInfo.momClothTop);
            if (container.hand21_1) gotoFrameName(container.hand21_1, appearanceInfo.momClothTop);
            if (container.hand22_1) gotoFrameName(container.hand22_1, appearanceInfo.momClothTop);
            if (container.leg22_1) gotoFrameName(container.leg22_1, appearanceInfo.momClothBottom);
            if (container.foot2_1 && container.foot2_1.shoes) gotoFrameName(container.foot2_1.shoes, appearanceInfo.momClothShoes);
            if (container.foot2_1 && container.foot2_1.socks) gotoFrameName(container.foot2_1.socks, appearanceInfo.momClothSocks);
        }
        if (container.leg11 && container.leg12 && container.leg21 && container.leg22 && container.clothBottom)
        {
            if (appearanceInfo.momClothBottom == "0")
            {
                gotoFrameName(container.clothBottom, "none");
                gotoFrameName(container.leg11, "none");
                gotoFrameName(container.leg12, "none");
                gotoFrameName(container.leg21, "none");
                gotoFrameName(container.leg22, "none");
                if (container.leg22_1) gotoFrameName(container.leg22_1, "none");
            }
            else
            {
                gotoFrameName(container.clothBottom, appearanceInfo.momClothBottom);
                gotoFrameName(container.leg11, appearanceInfo.momClothBottom);
                gotoFrameName(container.leg12, appearanceInfo.momClothBottom);
                gotoFrameName(container.leg21, appearanceInfo.momClothBottom);
                gotoFrameName(container.leg22, appearanceInfo.momClothBottom);
            }
        }
        if (_characterView.appearance.container.skirt)
        {
            if (appearanceInfo.momClothSkirt == "0")
            {
                gotoFrameName(container.skirt, "none");
            }
            else
            {
                gotoFrameName(container.skirt, appearanceInfo.momClothSkirt);
            }
        }
        if (container.foot1 && container.foot1.shoes && container.foot2 && container.foot2.shoes)
        {
            gotoFrameName(container.foot1.shoes, appearanceInfo.momClothShoes);
            gotoFrameName(container.foot2.shoes, appearanceInfo.momClothShoes);
            gotoFrameName(container.foot1.socks, appearanceInfo.momClothSocks);
            gotoFrameName(container.foot2.socks, appearanceInfo.momClothSocks);
        }
    }
}
}
