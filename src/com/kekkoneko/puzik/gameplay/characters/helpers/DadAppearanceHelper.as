/**
 * Created by SeeD on 25/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.helpers
{
import com.kekkoneko.puzik.gameplay.characters.appearance.CharacterAppearanceConfig;
import com.kekkoneko.puzik.gameplay.characters.appearance.DadAppearanceConfig;
import com.kekkoneko.puzik.gameplay.characters.view.CharacterView;

import flash.display.MovieClip;

public class DadAppearanceHelper extends AppearanceHelper
{
    public function DadAppearanceHelper(characterView:CharacterView)
    {
        super(characterView);
    }

    override public function updateAppearance(appearanceConfig:CharacterAppearanceConfig, customContainer:MovieClip = null):void
    {
        if (!_characterView.appearance || !_characterView.appearance.container)
        {
            return;
        }

        var appearanceInfo:DadAppearanceConfig = appearanceConfig as DadAppearanceConfig;
        var container:MovieClip = _characterView.appearance.container;

        if (container)
        {
            if (container.head)
            {
                if (container.head.lips)
                {
                    container.head.lips.gotoAndStop(appearanceInfo.dadLips);
                }
                if (container.head.faceForm) container.head.faceForm.gotoAndStop(appearanceInfo.dadFaceForm);
                if (container.head.hair) container.head.hair.gotoAndStop(appearanceInfo.dadHairStyle);
                if (container.head.eyes)
                {
                    container.head.eyes.gotoAndStop(appearanceInfo.dadEyes);
                }
                if (container.head.brows) container.head.brows.gotoAndStop(appearanceInfo.dadEyebrows);
                if (container.head.nose) container.head.nose.gotoAndStop(appearanceInfo.dadNose);
                if (container.head.pupil1) container.head.pupil1.gotoAndStop(appearanceInfo.dadEyesColor);
                if (container.head.pupil2) container.head.pupil2.gotoAndStop(appearanceInfo.dadEyesColor);
            }
            if (container.clothTop && container.neck && container.hand11 && container.hand12 && container.hand21 && container.hand22)
            {
                gotoFrameName(container.clothTop, appearanceInfo.dadClothTop);
                gotoFrameName(container.neck, appearanceInfo.dadClothTop);
                gotoFrameName(container.hand11, appearanceInfo.dadClothTop);
                gotoFrameName(container.hand12, appearanceInfo.dadClothTop);
                gotoFrameName(container.hand21, appearanceInfo.dadClothTop);
                gotoFrameName(container.hand22, appearanceInfo.dadClothTop);
                if (container.hand21_1) gotoFrameName(container.hand21_1, appearanceInfo.dadClothTop);
                if (container.hand22_1) gotoFrameName(container.hand22_1, appearanceInfo.dadClothTop);
                if (container.leg22_1) gotoFrameName(container.leg22_1, appearanceInfo.dadClothBottom);
                if (container.foot2_1 && container.foot2_1.shoes) gotoFrameName(container.foot2_1.shoes, appearanceInfo.dadClothShoes);
                //if (container.foot2_1 && container.foot2_1.socks) gotoFrameName(container.foot2_1.socks, appearanceInfo.dadClothSocks);
            }
            if (container.leg11 && container.leg12 && container.leg21 && container.leg22 && container.clothBottom)
            {
                if (appearanceInfo.dadClothBottom == "0")
                {
                    gotoFrameName(container.clothBottom, "none");
                    gotoFrameName(container.leg11, "none");
                    gotoFrameName(container.leg12, "none");
                    gotoFrameName(container.leg21, "none");
                    gotoFrameName(container.leg22, "none");
                    if (container.leg22_1) gotoFrameName(container.leg22_1, "none");
                }
                else
                {
                    gotoFrameName(container.clothBottom, appearanceInfo.dadClothBottom);
                    gotoFrameName(container.leg11, appearanceInfo.dadClothBottom);
                    gotoFrameName(container.leg12, appearanceInfo.dadClothBottom);
                    gotoFrameName(container.leg21, appearanceInfo.dadClothBottom);
                    gotoFrameName(container.leg22, appearanceInfo.dadClothBottom);
                }
            }
            if (container.foot1 && container.foot1.shoes && container.foot2 && container.foot2.shoes)
            {
                gotoFrameName(container.foot1.shoes, appearanceInfo.dadClothShoes);
                gotoFrameName(container.foot2.shoes, appearanceInfo.dadClothShoes);
                //gotoFrameName(container.foot1.socks, appearanceInfo.dadClothSocks);
                //gotoFrameName(container.foot2.socks, appearanceInfo.dadClothSocks);
            }
        }

    }
}
}
