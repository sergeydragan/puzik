/**
 * Created by SeeD on 23/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.helpers
{
import com.kekkoneko.puzik.gameplay.characters.appearance.MomAppearanceConfig;
import com.kekkoneko.puzik.gameplay.characters.constants.EmotionTypes;
import com.kekkoneko.puzik.gameplay.characters.view.CharacterView;

import flash.display.MovieClip;

public class MomEmotionsHelper extends EmotionsHelper
{
    public function MomEmotionsHelper(characterView:CharacterView)
    {
        super(characterView);
    }

    override public function setEmotion(emotionType:String, customContainer:MovieClip = null):void
    {
        if (!_characterView.appearance.container || !_characterView.appearance.container.head || !_characterView.appearance.container.head.emotion)
        {
            return;
        }

        var appearanceInfo:MomAppearanceConfig = _characterView.characterModel.appearanceConfig as MomAppearanceConfig;
        var head:MovieClip = _characterView.appearance.container.head;

        if (emotionType == EmotionTypes.NORMAL)
        {
            head.winking.visible = true;
            head.lips.visible = true;
            head.brows.visible = true;
            head.eyes.visible = true;
            head.pupil1.visible = true;
            head.pupil2.visible = true;

            head.emotion.pupil1.visible = false;
            head.emotion.pupil2.visible = false;
        }
        else if (emotionType == EmotionTypes.TENDER || emotionType == EmotionTypes.SCARED || emotionType == EmotionTypes.SAD)
        {
            head.winking.visible = false;
            head.lips.visible = false;
            head.brows.visible = false;
            head.eyes.visible = false;
            head.pupil1.visible = false;
            head.pupil2.visible = false;

            head.emotion.pupil1.visible = true;
            head.emotion.pupil2.visible = true;
            head.emotion.pupil1.gotoAndStop(appearanceInfo.momEyesColor);
            head.emotion.pupil2.gotoAndStop(appearanceInfo.momEyesColor);
        }
        else if (emotionType == EmotionTypes.HAPPY)
        {
            head.winking.visible = false;
            head.lips.visible = false;
            head.brows.visible = true;
            head.eyes.visible = false;
            head.pupil1.visible = false;
            head.pupil2.visible = false;

            head.emotion.pupil1.visible = false;
            head.emotion.pupil2.visible = false;
            head.emotion.pupil1.gotoAndStop(appearanceInfo.momEyesColor);
            head.emotion.pupil2.gotoAndStop(appearanceInfo.momEyesColor);
        }

        if (head.emotion)
        {
            head.emotion.gotoAndStop(emotionType);
            // а здесь засинкаем визаж на эмоции)
            if (head.emotion.lipsVisage) head.emotion.lipsVisage.gotoAndStop(appearanceInfo.momLipsVisage);
            if (head.emotion.eyesVisage) head.emotion.eyesVisage.gotoAndStop(appearanceInfo.momEyesVisage);
        }
    }
}
}
