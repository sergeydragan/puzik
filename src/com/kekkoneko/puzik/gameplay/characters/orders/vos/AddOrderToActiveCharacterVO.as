/**
 * Created by SeeD on 30/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.vos
{
public class AddOrderToActiveCharacterVO
{
    public var orderVO:OrderVO;

    public function AddOrderToActiveCharacterVO(orderVO:OrderVO)
    {
        this.orderVO = orderVO;
    }
}
}
