/**
 * Created by SeeD on 30/11/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.vos
{
import com.kekkoneko.core.debug.Debug;
import com.kekkoneko.puzik.gameplay.characters.model.CharacterModel;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;

public class OrderVO
{
    public var owner:CharacterModel;
    public var subject:CharacterModel;
    public var targetInventoryObjectVO:InventoryObjectVO;
    public var ingredients:Vector.<InventoryObjectVO>;
    public var interactionType:String;
    public var coordsX:Number;
    public var isIdle:Boolean;

    public function OrderVO()
    {
    }

    public function addIngredients(...args):void
    {
        if (!ingredients)
        {
            ingredients = new <InventoryObjectVO>[];
        }

        for (var i:int = 0; i < args.length; i++)
        {
            if (!(args[i] is InventoryObjectVO))
            {
                Debug.warning("Trying to add not an InventoryObjectVO to the list of ingredients: " + args[i]);
                continue;
            }
            ingredients.push(args[i]);
        }
    }
}
}
