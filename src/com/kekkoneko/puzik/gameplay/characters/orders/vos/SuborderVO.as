/**
 * Created by SeeD on 30/11/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.vos
{
import com.kekkoneko.puzik.gameplay.characters.model.CharacterModel;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;

public class SuborderVO
{
    public var owner:CharacterModel;
    public var subject:CharacterModel;
    public var targetInventoryObjectVO:InventoryObjectVO;
    public var ingredients:Vector.<InventoryObjectVO>;
    public var interactionType:String;
    public var coordsX:Number;
    // на всякий случай дадим возможность хранить здесь ссылку на сам ордер
    public var orderVO:OrderVO;

    public function SuborderVO()
    {
    }
}
}
