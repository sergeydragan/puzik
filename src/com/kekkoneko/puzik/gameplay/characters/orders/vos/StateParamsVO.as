/**
 * Created by SeeD on 17/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.vos
{
public class StateParamsVO
{
    public var stateType:String;
    /*
    public var subject:CharacterModel;
    public var target:InventoryObjectVO;
    public var coordsX:Number;
    public var ingredients:Vector.<InventoryObjectVO>;
     */
    public var ticksTotal:int;
    public var suborderVO:SuborderVO;

//    public function StateParamsVO(stateType:String, subject:CharacterModel = null, target:InventoryObjectVO = null, coordsX:Number = -1, ingredients:Vector.<InventoryObjectVO> = null, ticksTotal:int = -1)
    public function StateParamsVO()
    {
        /*
        this.stateType = stateType;
        this.subject = subject;
        this.target = target;
        this.coordsX = coordsX;
        this.ingredients = ingredients;
        this.ticksTotal = ticksTotal;
         */
    }
}
}
