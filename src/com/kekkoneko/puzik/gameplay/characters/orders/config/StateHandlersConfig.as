/**
 * Created by SeeD on 21/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.config
{
import com.kekkoneko.puzik.gameplay.characters.orders.constants.StateTypes;
import com.kekkoneko.puzik.gameplay.characters.orders.model.OrdersModel;
import com.kekkoneko.puzik.gameplay.characters.states.StateBabySittingIdle;
import com.kekkoneko.puzik.gameplay.characters.states.StateDadFeedingBaby;
import com.kekkoneko.puzik.gameplay.characters.states.StateDadFromSitToStand;
import com.kekkoneko.puzik.gameplay.characters.states.StateDadFromStandToSit;
import com.kekkoneko.puzik.gameplay.characters.states.StateDadPutBabyToFloor;
import com.kekkoneko.puzik.gameplay.characters.states.StateDadSittingIdle;
import com.kekkoneko.puzik.gameplay.characters.states.StateDadStandingIdle;
import com.kekkoneko.puzik.gameplay.characters.states.StateDadTakeBaby;
import com.kekkoneko.puzik.gameplay.characters.states.StateDadWalking;
import com.kekkoneko.puzik.gameplay.characters.states.StateMomFeedingBaby;
import com.kekkoneko.puzik.gameplay.characters.states.StateMomFromSitToStand;
import com.kekkoneko.puzik.gameplay.characters.states.StateMomFromStandToSit;
import com.kekkoneko.puzik.gameplay.characters.states.StateMomPlayingWithBaby;
import com.kekkoneko.puzik.gameplay.characters.states.StateMomPreSittingIdle;
import com.kekkoneko.puzik.gameplay.characters.states.StateMomPutBabyToFloor;
import com.kekkoneko.puzik.gameplay.characters.states.StateMomSittingIdle;
import com.kekkoneko.puzik.gameplay.characters.states.StateMomStandingIdle;
import com.kekkoneko.puzik.gameplay.characters.states.StateMomTakeBaby;
import com.kekkoneko.puzik.gameplay.characters.states.StateMomWalking;

public class StateHandlersConfig
{
    [Inject]
    public var ordersModel:OrdersModel;

    public function StateHandlersConfig()
    {
    }

    public function init():void
    {
        ordersModel.registerState(StateTypes.MOM_IDLE, StateMomStandingIdle);
        ordersModel.registerState(StateTypes.DAD_IDLE, StateDadStandingIdle);

        ordersModel.registerState(StateTypes.MOM_WALKING, StateMomWalking);
        ordersModel.registerState(StateTypes.DAD_WALKING, StateDadWalking);

        ordersModel.registerState(StateTypes.MOM_FROM_STAND_TO_SIT, StateMomFromStandToSit);
        ordersModel.registerState(StateTypes.DAD_FROM_STAND_TO_SIT, StateDadFromStandToSit);

        ordersModel.registerState(StateTypes.MOM_IDLE_PRE_SITTING, StateMomPreSittingIdle);

        ordersModel.registerState(StateTypes.MOM_FROM_SIT_TO_STAND, StateMomFromSitToStand);
        ordersModel.registerState(StateTypes.DAD_FROM_SIT_TO_STAND, StateDadFromSitToStand);

        ordersModel.registerState(StateTypes.MOM_TAKE_BABY, StateMomTakeBaby);
        ordersModel.registerState(StateTypes.DAD_TAKE_BABY, StateDadTakeBaby);

        ordersModel.registerState(StateTypes.MOM_PUT_BABY_TO_FLOOR, StateMomPutBabyToFloor);
        ordersModel.registerState(StateTypes.DAD_PUT_BABY_TO_FLOOR, StateDadPutBabyToFloor);

        ordersModel.registerState(StateTypes.MOM_IDLE_SITTING, StateMomSittingIdle);
        ordersModel.registerState(StateTypes.DAD_IDLE_SITTING, StateDadSittingIdle);
        ordersModel.registerState(StateTypes.BABY_IDLE_SITTING, StateBabySittingIdle);

        ordersModel.registerState(StateTypes.MOM_FEED_BABY, StateMomFeedingBaby);
        ordersModel.registerState(StateTypes.DAD_FEED_BABY, StateDadFeedingBaby);

        ordersModel.registerState(StateTypes.MOM_PLAYING_WITH_BABY, StateMomPlayingWithBaby);
        ordersModel.registerState(StateTypes.DAD_PLAYING_WITH_BABY, StateDadFeedingBaby);
    }
}
}
