/**
 * Created by SeeD on 04/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.config
{
import com.kekkoneko.puzik.gameplay.characters.constants.CharacterTypes;
import com.kekkoneko.puzik.gameplay.characters.orders.constants.InteractionTypes;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.suborders.FeedBabyDadSuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.suborders.FeedBabyMomSuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.suborders.GetUpFromDadSuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.suborders.GetUpFromMomSuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.suborders.IdleBabySuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.suborders.IdleDadSuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.suborders.IdleMomSuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.suborders.MoveToXDadSuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.suborders.MoveToXMomSuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.suborders.PlayWithBabyDadSuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.suborders.PlayWithBabyMomSuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.suborders.PutBabyDadSuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.suborders.PutBabyMomSuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.suborders.SitOnDadSuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.suborders.SitOnMomSuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.suborders.TakeBabyDadSuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.suborders.TakeBabyMomSuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.model.OrdersModel;

public class SuborderHandlersConfig
{
    [Inject]
    public var ordersModel:OrdersModel;

    public function SuborderHandlersConfig()
    {
    }

    public function init():void
    {
        ordersModel.registerSuborderHandler(InteractionTypes.IDLE, CharacterTypes.MOM, IdleMomSuborderHandler);
        ordersModel.registerSuborderHandler(InteractionTypes.IDLE, CharacterTypes.DAD, IdleDadSuborderHandler);
        ordersModel.registerSuborderHandler(InteractionTypes.IDLE, CharacterTypes.BABY, IdleBabySuborderHandler);

        ordersModel.registerSuborderHandler(InteractionTypes.MOVE_TO_X, CharacterTypes.MOM, MoveToXMomSuborderHandler);
        ordersModel.registerSuborderHandler(InteractionTypes.MOVE_TO_X, CharacterTypes.DAD, MoveToXDadSuborderHandler);

        ordersModel.registerSuborderHandler(InteractionTypes.SIT_ON, CharacterTypes.MOM, SitOnMomSuborderHandler);
        ordersModel.registerSuborderHandler(InteractionTypes.SIT_ON, CharacterTypes.DAD, SitOnDadSuborderHandler);
        ordersModel.registerSuborderHandler(InteractionTypes.GET_UP_FROM, CharacterTypes.MOM, GetUpFromMomSuborderHandler);
        ordersModel.registerSuborderHandler(InteractionTypes.GET_UP_FROM, CharacterTypes.DAD, GetUpFromDadSuborderHandler);

        ordersModel.registerSuborderHandler(InteractionTypes.TAKE_BABY, CharacterTypes.MOM, TakeBabyMomSuborderHandler);
        ordersModel.registerSuborderHandler(InteractionTypes.TAKE_BABY, CharacterTypes.DAD, TakeBabyDadSuborderHandler);

        ordersModel.registerSuborderHandler(InteractionTypes.PUT_BABY, CharacterTypes.MOM, PutBabyMomSuborderHandler);
        ordersModel.registerSuborderHandler(InteractionTypes.PUT_BABY, CharacterTypes.DAD, PutBabyDadSuborderHandler);

        ordersModel.registerSuborderHandler(InteractionTypes.FEED_BABY, CharacterTypes.MOM, FeedBabyMomSuborderHandler);
        ordersModel.registerSuborderHandler(InteractionTypes.FEED_BABY, CharacterTypes.DAD, FeedBabyDadSuborderHandler);

        ordersModel.registerSuborderHandler(InteractionTypes.PLAY_WITH_BABY, CharacterTypes.MOM, PlayWithBabyMomSuborderHandler);
        ordersModel.registerSuborderHandler(InteractionTypes.PLAY_WITH_BABY, CharacterTypes.DAD, PlayWithBabyDadSuborderHandler);
    }
}
}
