/**
 * Created by SeeD on 30/11/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.config
{
import com.kekkoneko.puzik.gameplay.characters.orders.constants.InteractionTypes;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.orders.FeedBabyOrderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.orders.IdleOrderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.orders.MoveToXOrderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.orders.PlayWithBabyOrderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.orders.PutBabyOrderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.orders.SitOnOrderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.orders.TakeBabyOrderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.orders.UseObjectOrderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.model.OrdersModel;

public class OrderHandlersConfig
{
    [Inject]
    public var ordersModel:OrdersModel;

    public function OrderHandlersConfig()
    {
    }

    public function init():void
    {
        ordersModel.registerOrderHandler(InteractionTypes.IDLE, IdleOrderHandler);
        ordersModel.registerOrderHandler(InteractionTypes.USE_OBJECT, UseObjectOrderHandler);
        ordersModel.registerOrderHandler(InteractionTypes.MOVE_TO_X, MoveToXOrderHandler);
        ordersModel.registerOrderHandler(InteractionTypes.SIT_ON, SitOnOrderHandler);
        ordersModel.registerOrderHandler(InteractionTypes.TAKE_BABY, TakeBabyOrderHandler);
        ordersModel.registerOrderHandler(InteractionTypes.PUT_BABY, PutBabyOrderHandler);
        ordersModel.registerOrderHandler(InteractionTypes.FEED_BABY, FeedBabyOrderHandler);
        ordersModel.registerOrderHandler(InteractionTypes.PLAY_WITH_BABY, PlayWithBabyOrderHandler);
    }
}
}
