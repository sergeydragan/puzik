/**
 * Created by SeeD on 30/11/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.model
{
import com.kekkoneko.core.debug.Debug;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.OrderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.SuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.OrderVO;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.SuborderVO;
import com.kekkoneko.puzik.gameplay.characters.states.CharacterState;

import flash.utils.Dictionary;

public class OrdersModel
{
    private var _orderHandlers:Dictionary;
    private var _suborderHandlers:Dictionary;
    private var _states:Dictionary;

    public function OrdersModel()
    {
        _orderHandlers = new Dictionary();
        _suborderHandlers = new Dictionary();
        _states = new Dictionary();
    }

    public function registerOrderHandler(interactionType:String, orderHandler:Class):void
    {
        if (_orderHandlers[interactionType])
        {
            Debug.warning("Order handler for " + interactionType + " is already registered")
        }
        _orderHandlers[interactionType] = orderHandler;
        trace();
    }

    public function registerSuborderHandler(interactionType:String, character:String, suborderHandler:Class):void
    {
        if (!_suborderHandlers[interactionType])
        {
            _suborderHandlers[interactionType] = new Dictionary();
        }
        if (_suborderHandlers[interactionType][character])
        {
            Debug.warning("Suborder handler for " + interactionType + " (character " + character + ") is already registered")
        }
        _suborderHandlers[interactionType][character] = suborderHandler;
    }

    public function registerState(stateType:String, state:Class):void
    {
        if (_states[stateType])
        {
            Debug.warning("State for " + stateType + " is already registered")
        }
        _states[stateType] = state;
    }

    public function getOrderHandler(orderVO:OrderVO):OrderHandler
    {
        if (!_orderHandlers[orderVO.interactionType])
        {
            Debug.warning("Requested order handler for " + orderVO.interactionType + " not found")
        }
        var orderHandlerClass:Class = _orderHandlers[orderVO.interactionType] as Class;
        return new orderHandlerClass(orderVO);
    }

    public function getSuborderHandler(suborderVO:SuborderVO, characterType:String):SuborderHandler
    {
        if (!_suborderHandlers[suborderVO.interactionType][characterType])
        {
            Debug.warning("Requested suborder handler for " + suborderVO.interactionType + " (character " + characterType + ") not found")
        }
        var suborderHandlerClass:Class = _suborderHandlers[suborderVO.interactionType][characterType] as Class;
        return new suborderHandlerClass(suborderVO);
    }

    public function getState(stateType:String):CharacterState
    {
        if (!_states[stateType])
        {
            Debug.warning("Requested state for " + stateType + " not found")
        }
        var stateClass:Class = _states[stateType] as Class;
        return new stateClass();
    }
}
}
