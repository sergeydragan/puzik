/**
 * Created by SeeD on 29/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.debug.Debug;
import com.kekkoneko.core.dialog.vos.ShowDialogPropertiesVO;
import com.kekkoneko.puzik.controller.notifications.OrderAddedNotification;
import com.kekkoneko.puzik.controller.notifications.PlayerEnergyUpdatedNotification;
import com.kekkoneko.puzik.dialog.constants.PuzikDialogNames;
import com.kekkoneko.puzik.gameplay.characters.model.CharactersModel;
import com.kekkoneko.puzik.gameplay.characters.orders.helpers.OrderEnergyHelper;
import com.kekkoneko.puzik.gameplay.characters.orders.model.OrdersModel;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.AddOrderToActiveCharacterVO;
import com.kekkoneko.puzik.gameplay.player.model.PlayersModel;

public class AddOrderToActiveCharacterCommand extends Command
{
    [Inject]
    public var charactersModel:CharactersModel;

    [Inject]
    public var ordersModel:OrdersModel;

    [Inject]
    public var playersModel:PlayersModel;

    public function AddOrderToActiveCharacterCommand()
    {
    }

    override public function execute():void
    {
        var addOrderToActiveCharacterVO:AddOrderToActiveCharacterVO = _payload as AddOrderToActiveCharacterVO;

        if (OrderEnergyHelper.orderCostsEnergy(addOrderToActiveCharacterVO.orderVO.interactionType))
        {
            if (playersModel.getPlayer().energy > 0)
            {
                playersModel.getPlayer().energy--;
                coreEventDispatcher.broadcastMessage(PlayerEnergyUpdatedNotification.PLAYER_ENERGY_UPDATED);
            }
            else
            {
                Debug.info("Energy is zero, will not add order");
                coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_DIALOG, new ShowDialogPropertiesVO(PuzikDialogNames.NO_ENERGY));
                return;
            }
        }

        if (charactersModel.getActiveCharacter())
        {
            addOrderToActiveCharacterVO.orderVO.owner = charactersModel.getActiveCharacter();
            charactersModel.getActiveCharacter().addOrder(addOrderToActiveCharacterVO.orderVO);
            coreEventDispatcher.broadcastMessage(OrderAddedNotification.ORDER_ADDED, new OrderAddedNotification(addOrderToActiveCharacterVO.orderVO));
        }
    }
}
}