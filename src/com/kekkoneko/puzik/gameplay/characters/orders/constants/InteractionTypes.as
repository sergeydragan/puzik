/**
 * Created by SeeD on 30/11/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.constants
{
public class InteractionTypes
{
    public static const IDLE:String = "Idle";
    public static const USE_OBJECT:String = "UseObject";
    public static const PUT_BABY:String = "PutBaby";
    public static const MOVE_TO_X:String = "MoveToX";
    public static const SIT_ON:String = "SitOn";
    public static const GET_UP_FROM:String = "GetUpFrom";
    public static const TAKE_BABY:String = "TakeBaby";
    public static const FEED_BABY:String = "FeedBaby";
    public static const PLAY_WITH_BABY:String = "PlayWithBaby";

    public function InteractionTypes()
    {
    }
}
}
