/**
 * Created by SeeD on 18/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.constants
{
public class StateTypes
{
    public static const MOM_IDLE:String = "MomIdle";
    public static const DAD_IDLE:String = "DadIdle";
    public static const MOM_FROM_STAND_TO_SIT:String = "MomFromStandToSit";
    public static const DAD_FROM_STAND_TO_SIT:String = "DadFromStandToSit";
    public static const MOM_IDLE_SITTING:String = "MomIdleSitting";
    public static const MOM_FROM_SIT_TO_STAND:String = "MomFromSitToStand";
    public static const MOM_IDLE_PRE_SITTING:String = "MomIdlePreSitting";
    public static const MOM_WALKING:String = "MomWalking";
    public static const DAD_WALKING:String = "DadWalking";
    public static const DAD_IDLE_SITTING:String = "DadIdleSitting";
    public static const BABY_IDLE_SITTING:String = "BabyIdleSitting";
    public static const MOM_TAKE_BABY:String = "MomTakeBaby";
    public static const DAD_TAKE_BABY:String = "DadTakeBaby";
    public static const DAD_FROM_SIT_TO_STAND:String = "DadFromSitToStand";
    public static const MOM_FEED_BABY:String = "MomFeedBaby";
    public static const DAD_FEED_BABY:String = "DadFeedBaby";
    public static const MOM_PLAYING_WITH_BABY:String = "MomPlayingWithBaby";
    public static const DAD_PLAYING_WITH_BABY:String = "DadPlayingWithBaby";
    public static const DAD_PUT_BABY_TO_FLOOR:String = "DadPutBaby";
    public static const MOM_PUT_BABY_TO_FLOOR:String = "MomPutBaby";

    public function StateTypes()
    {
    }
}
}
