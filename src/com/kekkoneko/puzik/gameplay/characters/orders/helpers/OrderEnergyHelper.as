/**
 * Created by SeeD on 08/03/2015.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.helpers
{
import com.kekkoneko.puzik.gameplay.characters.orders.constants.InteractionTypes;

public class OrderEnergyHelper
{
    private static const ORDERS_FOR_ENERGY:Vector.<String> = new <String>[
        InteractionTypes.FEED_BABY,
        InteractionTypes.PLAY_WITH_BABY
    ];

    public function OrderEnergyHelper()
    {
    }

    public static function orderCostsEnergy(interactionType:String):Boolean
    {
        return (ORDERS_FOR_ENERGY.indexOf(interactionType) >= 0);
    }
}
}
