/**
 * Created by SeeD on 14/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.handlers.suborders
{
import com.kekkoneko.puzik.gameplay.characters.orders.constants.StateTypes;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.SuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.SuborderVO;

public class PutBabyDadSuborderHandler extends SuborderHandler
{
    public function PutBabyDadSuborderHandler(suborder:SuborderVO)
    {
        super(suborder);
    }

    override public function formStatesQueue():void
    {
        addState(StateTypes.DAD_PUT_BABY_TO_FLOOR);
    }

}
}
