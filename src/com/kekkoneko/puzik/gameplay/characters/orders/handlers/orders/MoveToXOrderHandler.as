/**
 * Created by SeeD on 21/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.handlers.orders
{
import com.kekkoneko.puzik.gameplay.characters.orders.constants.InteractionTypes;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.*;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.OrderVO;

public class MoveToXOrderHandler extends OrderHandler
{
    public function MoveToXOrderHandler(order:OrderVO)
    {
        super(order);
    }

    override public function formSubordersQueue():void
    {
        var coordsX:Number = orderVO.targetInventoryObjectVO ? orderVO.targetInventoryObjectVO.x : orderVO.coordsX;
        if (orderVO.owner.sittingOnObject)
        {
            addSuborder(InteractionTypes.GET_UP_FROM);
        }
        addSuborder(InteractionTypes.MOVE_TO_X, null, null, coordsX);
    }
}
}
