/**
 * Для того, чтобы использовать предмет, нужно построить следующую очередь действий:
 * 1. Положить ребёнка, если он уже в руках;
 * 2. Подойти к предмету;
 * 3. Использовать предмет.
 *
 * Created by SeeD on 30/11/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.handlers.orders
{
import com.kekkoneko.puzik.gameplay.characters.orders.constants.InteractionTypes;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.OrderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.OrderVO;

public class UseObjectOrderHandler extends OrderHandler
{
    public function UseObjectOrderHandler(order:OrderVO)
    {
        super(order);
    }

    override public function formSubordersQueue():void
    {
        addSuborder(InteractionTypes.PUT_BABY);
        addSuborder(InteractionTypes.MOVE_TO_X, null, null, orderVO.targetInventoryObjectVO.x);
        addSuborder(InteractionTypes.USE_OBJECT, null, orderVO.targetInventoryObjectVO);
    }
}
}
