/**
 * Created by SeeD on 14/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.handlers.suborders
{
import com.kekkoneko.puzik.gameplay.characters.orders.constants.StateTypes;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.SuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.SuborderVO;
import com.kekkoneko.puzik.gameplay.characters.states.CharacterState;

public class IdleDadSuborderHandler extends SuborderHandler
{
    public function IdleDadSuborderHandler(suborder:SuborderVO)
    {
        super(suborder);
    }

    override public function formStatesQueue():void
    {
        if (suborderVO.owner.sittingOnObject)
        {
            addState(StateTypes.DAD_IDLE_SITTING, CharacterState.TICKS_INFINITE_STATE);
        }
        else
        {
            addState(StateTypes.DAD_IDLE, CharacterState.TICKS_INFINITE_STATE);
        }
    }

}
}
