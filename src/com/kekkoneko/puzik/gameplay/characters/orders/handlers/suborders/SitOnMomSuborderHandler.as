/**
 * Created by SeeD on 14/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.handlers.suborders
{
import com.kekkoneko.puzik.gameplay.characters.orders.constants.StateTypes;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.SuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.SuborderVO;

public class SitOnMomSuborderHandler extends SuborderHandler
{
    public function SitOnMomSuborderHandler(suborder:SuborderVO)
    {
        super(suborder);
    }

    override public function formStatesQueue():void
    {
        addState(StateTypes.MOM_FROM_STAND_TO_SIT);
        if (!suborderVO.owner.babyInHands)
        {
            addState(StateTypes.MOM_IDLE_PRE_SITTING);
        }
    }

}
}
