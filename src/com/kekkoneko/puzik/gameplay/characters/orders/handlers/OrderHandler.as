/**
 * Created by SeeD on 30/11/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.handlers
{
import com.kekkoneko.core.debug.Debug;
import com.kekkoneko.puzik.gameplay.characters.model.CharacterModel;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.OrderVO;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.SuborderVO;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;

public class OrderHandler
{
    public var orderVO:OrderVO;
    public var suborders:Vector.<SuborderVO>;

    public function OrderHandler(order:OrderVO)
    {
        orderVO = order;
        suborders = new <SuborderVO>[];
    }

    public function formSubordersQueue():void
    {
        Debug.error("formSubordersQueue() has to be overriden by subclass!")
    }

    protected function addSuborder(interactionType:String, subject:CharacterModel = null, targetInventoryObjectVO:InventoryObjectVO = null, coordsX:Number = -1, ingredients:Vector.<InventoryObjectVO> = null):void
    {
        var suborderVO:SuborderVO = new SuborderVO();
        suborderVO.owner = orderVO.owner;
        suborderVO.interactionType = interactionType;
        suborderVO.subject = subject;
        suborderVO.targetInventoryObjectVO = targetInventoryObjectVO;
        suborderVO.coordsX = coordsX;
        suborderVO.ingredients = ingredients;
        suborderVO.orderVO = orderVO;
        suborders.push(suborderVO);
    }

}
}
