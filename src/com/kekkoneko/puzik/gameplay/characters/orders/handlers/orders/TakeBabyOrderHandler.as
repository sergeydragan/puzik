/**
 * Created by SeeD on 21/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.handlers.orders
{
import com.kekkoneko.puzik.gameplay.characters.orders.constants.InteractionTypes;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.*;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.OrderVO;

public class TakeBabyOrderHandler extends OrderHandler
{
    public function TakeBabyOrderHandler(order:OrderVO)
    {
        super(order);
    }

    override public function formSubordersQueue():void
    {
        var coordsX:Number = orderVO.subject ? orderVO.subject.cellX : orderVO.coordsX;
        addSuborder(InteractionTypes.MOVE_TO_X, null, null, coordsX);
        addSuborder(InteractionTypes.TAKE_BABY, orderVO.subject);
    }
}
}
