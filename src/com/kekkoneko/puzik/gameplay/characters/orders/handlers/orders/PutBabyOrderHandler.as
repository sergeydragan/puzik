/**
 * Created by SeeD on 21/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.handlers.orders
{
import com.kekkoneko.puzik.gameplay.characters.orders.constants.InteractionTypes;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.*;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.OrderVO;

public class PutBabyOrderHandler extends OrderHandler
{
    public function PutBabyOrderHandler(order:OrderVO)
    {
        super(order);
    }

    override public function formSubordersQueue():void
    {
        addSuborder(InteractionTypes.PUT_BABY, orderVO.subject);
    }
}
}
