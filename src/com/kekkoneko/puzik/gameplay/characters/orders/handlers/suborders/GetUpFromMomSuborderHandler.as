/**
 * Created by SeeD on 01/03/2015.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.handlers.suborders
{
import com.kekkoneko.puzik.gameplay.characters.orders.constants.StateTypes;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.SuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.SuborderVO;

public class GetUpFromMomSuborderHandler extends SuborderHandler
{
    public function GetUpFromMomSuborderHandler(suborder:SuborderVO)
    {
        super(suborder);
    }

    override public function formStatesQueue():void
    {
        addState(StateTypes.MOM_FROM_SIT_TO_STAND);
    }
}
}
