/**
 * Created by SeeD on 14/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.handlers.suborders
{
import com.kekkoneko.puzik.gameplay.characters.constants.CharacterSpeed;
import com.kekkoneko.puzik.gameplay.characters.orders.constants.StateTypes;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.SuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.SuborderVO;
import com.kekkoneko.puzik.gameplay.room.view.RoomModeView;

public class MoveToXDadSuborderHandler extends SuborderHandler
{
    public function MoveToXDadSuborderHandler(suborder:SuborderVO)
    {
        super(suborder);
    }

    override public function formStatesQueue():void
    {
        var ticksTotal:int = Math.abs(Math.floor(((suborderVO.owner.cellX - suborderVO.coordsX) * RoomModeView.CELL_WIDTH) / CharacterSpeed.DAD_WALK_SPEED));
        addState(StateTypes.DAD_WALKING, ticksTotal);
    }

}
}
