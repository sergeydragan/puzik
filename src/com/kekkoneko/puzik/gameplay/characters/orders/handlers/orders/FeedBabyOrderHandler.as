/**
 * Created by SeeD on 21/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.handlers.orders
{
import com.kekkoneko.puzik.gameplay.characters.orders.constants.InteractionTypes;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.*;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.OrderVO;

public class FeedBabyOrderHandler extends OrderHandler
{
    public function FeedBabyOrderHandler(order:OrderVO)
    {
        super(order);
    }

    override public function formSubordersQueue():void
    {
        var coordsX:Number = orderVO.subject ? orderVO.subject.cellX : orderVO.coordsX;
        if (orderVO.owner.babyInHands)
        {
            if (orderVO.owner.babyInHands == orderVO.subject)
            {
                addSuborder(InteractionTypes.FEED_BABY, orderVO.subject, null, -1, orderVO.ingredients);
                return;
            }
            else
            {
                addSuborder(InteractionTypes.PUT_BABY, orderVO.owner.babyInHands);
            }
        }

        addSuborder(InteractionTypes.MOVE_TO_X, null, null, coordsX);
        addSuborder(InteractionTypes.TAKE_BABY, orderVO.subject);
        addSuborder(InteractionTypes.FEED_BABY, orderVO.subject, null, -1, orderVO.ingredients);
    }
}
}
