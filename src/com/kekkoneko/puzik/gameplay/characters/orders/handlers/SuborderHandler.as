/**
 * Created by SeeD on 30/11/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.handlers
{
import com.kekkoneko.core.debug.Debug;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.StateParamsVO;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.SuborderVO;
import com.kekkoneko.puzik.gameplay.characters.states.CharacterState;

public class SuborderHandler
{
    public var suborderVO:SuborderVO;
    public var stateParams:Vector.<StateParamsVO>;

    public function SuborderHandler(suborder:SuborderVO)
    {
        suborderVO = suborder;
        stateParams = new <StateParamsVO>[];
    }

    public function formStatesQueue():void
    {
        Debug.error("formStatesQueue() has to be overriden by subclass!")
    }

    /*
    protected function addStateParamsVO(stateType:String, subject:CharacterModel = null, target:InventoryObjectVO = null, coordsX:Number = -1, ingredients:Vector.<InventoryObjectVO> = null, ticksTotal:int = -1):void
    {
        // клик отдает приказ
        // на приказ отрабатывает соответствующий ордер хендлер
        // строится очередь субордеров, каждый из которых выполняется одним стейтом
        // стейты - разные, потому один субордер может выполняться по-разному
        // соответственно, у каждого персонажа должен быть свой субордер хандлер
        var stateParamsVO:StateParamsVO = new StateParamsVO(stateType, subject, target, coordsX, ingredients, ticksTotal);
        stateParams.push(stateParamsVO);

     stateParamsVO.suborderVO = suborderVO;
    }
     */

    protected function addState(stateType:String, ticksTotal:int = CharacterState.TICKS_UNTIL_ANIMATION_END):void
    {
        var stateParamsVO:StateParamsVO = new StateParamsVO();
        stateParamsVO.stateType = stateType;
        stateParamsVO.ticksTotal = ticksTotal;
        stateParamsVO.suborderVO = suborderVO;

        stateParams.push(stateParamsVO);
    }
}
}
