/**
 * Created by SeeD on 21/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.handlers.orders
{
import com.kekkoneko.puzik.gameplay.characters.orders.constants.InteractionTypes;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.*;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.OrderVO;

public class SitOnOrderHandler extends OrderHandler
{
    public function SitOnOrderHandler(order:OrderVO)
    {
        super(order);
    }

    override public function formSubordersQueue():void
    {
        var coordsX:Number = orderVO.targetInventoryObjectVO ? orderVO.targetInventoryObjectVO.x : orderVO.coordsX;
        addSuborder(InteractionTypes.MOVE_TO_X, null, null, coordsX);
        addSuborder(InteractionTypes.SIT_ON, null, orderVO.targetInventoryObjectVO);
    }
}
}
