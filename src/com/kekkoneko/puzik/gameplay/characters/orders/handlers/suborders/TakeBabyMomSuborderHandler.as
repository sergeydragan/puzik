/**
 * Created by SeeD on 14/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.orders.handlers.suborders
{
import com.kekkoneko.puzik.gameplay.characters.orders.constants.StateTypes;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.SuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.SuborderVO;

public class TakeBabyMomSuborderHandler extends SuborderHandler
{
    public function TakeBabyMomSuborderHandler(suborder:SuborderVO)
    {
        super(suborder);
    }

    override public function formStatesQueue():void
    {
        addState(StateTypes.MOM_TAKE_BABY);
    }

}
}
