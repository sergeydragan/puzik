/**
 * Created by SeeD on 21/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.model
{
import com.kekkoneko.core.debug.Debug;
import com.kekkoneko.puzik.gameplay.characters.appearance.CharacterAppearanceConfig;
import com.kekkoneko.puzik.gameplay.characters.helpers.AppearanceHelper;
import com.kekkoneko.puzik.gameplay.characters.helpers.EmotionsHelper;

import flash.utils.Dictionary;

public class CharactersModel
{
    private var _characters:Vector.<CharacterModel>;
    private var _activeCharacter:CharacterModel;

    private static var _emotionsHelpers:Dictionary;
    private static var _appearanceHelpers:Dictionary;
    private static var _appearanceConfigs:Dictionary;

    public function CharactersModel()
    {
        _characters = new <CharacterModel>[];
        _emotionsHelpers = new Dictionary();
        _appearanceHelpers = new Dictionary();
        _appearanceConfigs = new Dictionary();
    }

    public function addCharacter(character:CharacterModel):void
    {
        _characters.push(character);
    }

    public static function registerEmotionsHelper(characterType:String, emotionsHelperClass:Class):void
    {
        if (_emotionsHelpers[characterType])
        {
            Debug.warning("Emotions helper for " + characterType + " is already registered")
        }
        _emotionsHelpers[characterType] = emotionsHelperClass;
    }

    public static function registerAppearanceHelper(characterType:String, appearanceHelperClass:Class):void
    {
        if (_appearanceHelpers[characterType])
        {
            Debug.warning("Appearance helper for " + characterType + " is already registered")
        }
        _appearanceHelpers[characterType] = appearanceHelperClass;
    }

    public static function registerAppearanceConfig(characterType:String, appearanceConfigClass:Class):void
    {
        if (_appearanceConfigs[characterType])
        {
            Debug.warning("Appearance config for " + characterType + " is already registered")
        }
        _appearanceConfigs[characterType] = appearanceConfigClass;
    }

    public static function getEmotionsHelper(characterModel:CharacterModel):EmotionsHelper
    {
        if (!_emotionsHelpers[characterModel.type])
        {
            Debug.warning("Requested emotions helper for " + characterModel.type + " not found")
        }
        var emotionsHelperClass:Class = _emotionsHelpers[characterModel.type] as Class;
        return new emotionsHelperClass(characterModel.view);
    }

    public static function getAppearanceHelper(characterModel:CharacterModel):AppearanceHelper
    {
        if (!_appearanceHelpers[characterModel.type])
        {
            Debug.warning("Requested appearance helper for " + characterModel.type + " not found")
        }
        var appearanceHelperClass:Class = _appearanceHelpers[characterModel.type] as Class;
        return new appearanceHelperClass(characterModel.view);
    }

    public static function getNewAppearanceConfig(characterType:String):CharacterAppearanceConfig
    {
        if (!_appearanceHelpers[characterType])
        {
            Debug.warning("Requested appearance config for " + characterType + " not found")
        }
        var appearanceConfigClass:Class = _appearanceConfigs[characterType] as Class;
        return new appearanceConfigClass();
    }

    public function getActiveCharacter():CharacterModel
    {
        return _activeCharacter;
    }

    public function setActiveCharacter(character:CharacterModel):void
    {
        _activeCharacter = character;
    }

    public function getAllCharacters():Vector.<CharacterModel>
    {
        return _characters;
    }

    public function clearCharacters():void
    {
        _characters = new <CharacterModel>[];
        _activeCharacter = null;
    }
}
}
