/**
 * Created by SeeD on 30/11/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.model
{
import com.kekkoneko.core.eventdispatcher.CoreEventDispatcher;
import com.kekkoneko.puzik.controller.notifications.OrderFinishedNotification;
import com.kekkoneko.puzik.gameplay.characters.appearance.CharacterAppearanceConfig;
import com.kekkoneko.puzik.gameplay.characters.orders.constants.InteractionTypes;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.OrderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.handlers.SuborderHandler;
import com.kekkoneko.puzik.gameplay.characters.orders.model.OrdersModel;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.OrderVO;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.SuborderVO;
import com.kekkoneko.puzik.gameplay.characters.states.CharacterState;
import com.kekkoneko.puzik.gameplay.characters.view.CharacterView;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;
import com.kekkoneko.puzik.gameplay.room.constants.Directions;

public class CharacterModel
{
    [Inject]
    public var ordersModel:OrdersModel;

    [Inject]
    public var coreEventDispatcher:CoreEventDispatcher;

    public var appearanceConfig:CharacterAppearanceConfig;
    public var view:CharacterView;
    public var type:String;
    public var cellX:Number;
    public var direction:int = Directions.RIGHT;
    public var sittingOnObject:InventoryObjectVO;   // TODO: дать этой переменной более подходящее название
    public var interactingWithObject:InventoryObjectVO; // TODO: этой тоже
    public var babyInHands:CharacterModel;

    private var _ordersVector:Vector.<OrderVO>;
    private var _subordersVector:Vector.<SuborderVO>;
    private var _statesVector:Vector.<CharacterState>;

    public function CharacterModel()
    {
        _ordersVector = new <OrderVO>[];
        _subordersVector = new <SuborderVO>[];
        _statesVector = new <CharacterState>[];
    }

    public function update():void
    {
        updateCurrentState();
    }

    private function updateCurrentState():void
    {
        if (_statesVector.length > 0)
        {
            _statesVector[0].update();

            if (_statesVector[0].isFinished)
            {
                proceedToNextState();
            }
        }
    }


    private function proceedToNextState():void
    {
        _statesVector[0].cleanUp();
        _statesVector.splice(0, 1);
        if (_statesVector.length > 0)
        {
            _statesVector[0].init(this, view);
        }
        else
        {
            proceedToNextSuborder();
        }
    }

    private function proceedToNextSuborder():void
    {
        _subordersVector.splice(0, 1);
        if (_subordersVector.length > 0)
        {
            startSuborder(_subordersVector[0]);
            _statesVector[0].init(this, view);
        }
        else
        {
            proceedToNextOrder();
        }
    }

    private function proceedToNextOrder():void
    {
        _ordersVector.splice(0, 1);
        if (_ordersVector.length > 0)
        {
            startOrder(_ordersVector[0]);
            startSuborder(_subordersVector[0]);
            _statesVector[0].init(this, view);
        }
        else
        {
            trace("All orders were finished, going to idle state");
            var idleOrder:OrderVO = new OrderVO();
            idleOrder.interactionType = InteractionTypes.IDLE;
            idleOrder.isIdle = true;
            idleOrder.owner = this;
            addOrder(idleOrder);
        }
        coreEventDispatcher.broadcastMessage(OrderFinishedNotification.ORDER_FINISHED, new OrderFinishedNotification(_ordersVector[0]));
    }

    public function addOrder(order:OrderVO):void
    {
        _ordersVector.push(order);
        if (_ordersVector.length == 1)
        {
            startOrder(_ordersVector[0]);
            startSuborder(_subordersVector[0]);
            _statesVector[0].init(this, view);
        }
        else if (_ordersVector[0].isIdle)
        {
            clearSuborders();
            clearStates();
            proceedToNextOrder();
        }
    }

    private function startOrder(orderVO:OrderVO):void
    {
        var orderHandler:OrderHandler = ordersModel.getOrderHandler(orderVO);
        orderHandler.formSubordersQueue();
        for (var i:int = 0; i < orderHandler.suborders.length; i++)
        {
            _subordersVector.push(orderHandler.suborders[i]);
        }
    }

    private function startSuborder(suborderVO:SuborderVO):void
    {
        var suborderHandler:SuborderHandler = ordersModel.getSuborderHandler(suborderVO, type);
        suborderHandler.formStatesQueue();
        for (var i:int = 0; i < suborderHandler.stateParams.length; i++)
        {
            var state:CharacterState = ordersModel.getState(suborderHandler.stateParams[i].stateType);
            Main.injector.injectInto(state);
            state.setParams(suborderHandler.stateParams[i]);
            _statesVector.push(state);
        }
    }

    private function clearStates():void
    {
        if (_statesVector.length > 0)
        {
            _statesVector[0].cleanUp();
            _statesVector = new <CharacterState>[];
        }
    }

    private function clearSuborders():void
    {
        _subordersVector = new <SuborderVO>[];
    }

    public function cleanUp():void
    {

    }

    public function get ordersVector():Vector.<OrderVO>
    {
        return _ordersVector;
    }
}
}
