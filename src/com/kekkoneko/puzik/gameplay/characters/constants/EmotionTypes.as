/**
 * Created by SeeD on 25/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.constants
{
public class EmotionTypes
{
    public static const NORMAL:String = "Normal";
    public static const TENDER:String = "Tender";
    public static const HAPPY:String = "Happy";
    public static const SCARED:String = "Scared";
    public static const SAD:String = "Sad";
    public static const NO:String = "No";
    public static const SLEEPING:String = "Sleeping";
    public static const EATING:String = "Eating";
    public static const OPEN_MOUTH:String = "OpenMouth";
    public static const CHEWING:String = "Chewing";
    public static const WITH_PACIFIER:String = "WithPacifier";
    public static const CRAPPING:String = "Crap";
    public static const ILL:String = "Ill";

    public function EmotionTypes()
    {
    }
}
}
