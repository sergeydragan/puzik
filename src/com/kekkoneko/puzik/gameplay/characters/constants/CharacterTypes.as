/**
 * Created by SeeD on 13/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.constants
{
public class CharacterTypes
{
    public static const MOM:String = "Mom";
    public static const DAD:String = "Dad";
    public static const BABY:String = "Baby";
    public static const DOG:String = "Dog";

    public function CharacterTypes()
    {
    }
}
}
