/**
 * Created by SeeD on 14/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.states
{
public class StateDadFromStandToSit extends CharacterState
{
    public function StateDadFromStandToSit()
    {
        super();
    }

    override protected function preInitActions():void
    {
        if (_characterModel.babyInHands)
        {
            _animationClass = DadStateFromStandToSitWithBabyAppearance;
        }
        else
        {
            _animationClass = DadStateFromStandToSitAppearance;
        }
    }

    override protected function preFinishedActions():void
    {
        _characterModel.sittingOnObject = _stateParamsVO.suborderVO.targetInventoryObjectVO;
    }

}
}
