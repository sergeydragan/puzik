/**
 * Created by SeeD on 14/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.states
{
import com.kekkoneko.puzik.gameplay.characters.constants.CharacterSpeed;
import com.kekkoneko.puzik.gameplay.room.constants.Directions;
import com.kekkoneko.puzik.gameplay.room.view.RoomModeView;

public class StateDadWalking extends CharacterState
{
    public function StateDadWalking()
    {
        _animationClass = DadStateWalkLeftAppearance;
        super();
    }

    override protected function preInitActions():void
    {
        if (_characterModel.cellX < _stateParamsVO.suborderVO.coordsX)
        {
            _characterModel.direction = Directions.RIGHT;
            if (_stateParamsVO.suborderVO.owner.babyInHands)
            {
                _animationClass = DadStateWalkRightWithBabyAppearance;
            }
            else
            {
                _animationClass = _animationClass = DadStateWalkRightAppearance;
            }
        }
        else
        {
            _characterModel.direction = Directions.LEFT;
            if (_stateParamsVO.suborderVO.owner.babyInHands)
            {
                _animationClass = DadStateWalkLeftWithBabyAppearance;
            }
            else
            {
                _animationClass = _animationClass = DadStateWalkLeftAppearance;
            }
        }
    }

    override public function update():void
    {
        if (_characterModel.direction == Directions.LEFT)
        {
            _characterModel.cellX -= CharacterSpeed.DAD_WALK_SPEED / RoomModeView.CELL_WIDTH;
        }
        else
        {
            _characterModel.cellX += CharacterSpeed.DAD_WALK_SPEED / RoomModeView.CELL_WIDTH;
        }

        super.update();
    }

}
}
