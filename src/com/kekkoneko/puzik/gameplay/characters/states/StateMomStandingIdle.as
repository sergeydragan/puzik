/**
 * Created by SeeD on 14/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.states
{
public class StateMomStandingIdle extends CharacterState
{
    public function StateMomStandingIdle()
    {
        super();
    }

    override protected function preInitActions():void
    {
        if (_stateParamsVO.suborderVO.owner.babyInHands)
        {
            _animationClass = MomStateIdleStandingWithBabyAppearance;
        }
        else
        {
            _animationClass = MomStateIdleStandingAppearance;
        }
    }

    override protected function postInitActions():void
    {
        applyAppearanceMirroring();
    }

}
}
