/**
 * Created by SeeD on 30/11/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.states
{
import com.kekkoneko.core.eventdispatcher.CoreEventDispatcher;
import com.kekkoneko.puzik.gameplay.characters.model.CharacterModel;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.StateParamsVO;
import com.kekkoneko.puzik.gameplay.characters.view.CharacterView;
import com.kekkoneko.puzik.gameplay.room.constants.Directions;

import flash.display.MovieClip;

public class CharacterState
{
    [Inject]
    public var coreEventDispatcher:CoreEventDispatcher;

    public var isFinished:Boolean = false;

    protected var _animationClass:Class;
    protected var _characterModel:CharacterModel;
    protected var _characterView:CharacterView;
    protected var _ticksMade:int;
    protected var _stateParamsVO:StateParamsVO;

    public static const TICKS_INFINITE_STATE:int = -2;
    public static const TICKS_UNTIL_ANIMATION_END:int = -1;

    public function CharacterState()
    {
        _ticksMade = 0;
    }

    public function setParams(stateParamsVO:StateParamsVO):void
    {
        _stateParamsVO = stateParamsVO;
    }

    /** Наилучшее место, чтобы выбрать нужную анимацию, если для этого стейта их доступно несколько */
    protected function preInitActions():void
    {
    }

    public function init(characterModel:CharacterModel, characterView:CharacterView):void
    {
        _characterModel = characterModel;
        _characterView = characterView;

        preInitActions();

        if (_characterView)
        {
            _characterView.setAnimation(new _animationClass() as MovieClip);
        }

        postInitActions();
    }

    /** Подходящее место для того, чтобы вложить персонажам в руки ингредиенты или скрыть/показать что-то в анимации */
    protected function postInitActions():void
    {
    }

    public function update():void
    {
        if (_stateParamsVO.ticksTotal >= 0)
        {
            if (_ticksMade >= _stateParamsVO.ticksTotal)
            {
                preFinishedActions();
                isFinished = true;
            }
        }
        else if (_stateParamsVO.ticksTotal == TICKS_UNTIL_ANIMATION_END)
        {
            if (_characterView.appearance.container.currentFrame == _characterView.appearance.container.totalFrames)
            {
                preFinishedActions();
                isFinished = true;
            }
        }

        _ticksMade++;
    }

    protected function preFinishedActions():void
    {

    }

    protected function applyAppearanceMirroring():void
    {
        if (_characterModel.direction == Directions.LEFT)
        {
            _characterView.appearance.scaleX *= -1;
            _characterView.appearance.x += _characterView.appearance.width;
        }
    }

    public function cleanUp():void
    {

    }
}
}
