/**
 * Created by SeeD on 14/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.states
{
public class StateMomTakeBaby extends CharacterState
{
    public function StateMomTakeBaby()
    {
        super();
    }

    override protected function preInitActions():void
    {
        if (_stateParamsVO.suborderVO.subject.sittingOnObject)
        {
            _animationClass = MomStateTakeBabyFromObjectAppearance;
        }
        else
        {
            _animationClass = MomStateTakeBabyFromFloorAppearance;
        }
    }

    override public function update():void
    {
        super.update();

        if (_characterView.appearance.container.currentFrameLabel == "childInHands")
        {
            _stateParamsVO.suborderVO.subject.view.visible = false;
            _characterModel.babyInHands = _stateParamsVO.suborderVO.subject;
            _characterView.updateAppearance();

            _stateParamsVO.suborderVO.subject.sittingOnObject = null;
        }
    }

}
}
