/**
 * Created by SeeD on 14/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.states
{
public class StateMomFromSitToStand extends CharacterState
{
    public function StateMomFromSitToStand()
    {
        super();
    }

    override protected function preInitActions():void
    {
        if (_characterModel.babyInHands)
        {
            _animationClass = MomStateFromSitToStandWithBabyAppearance;
        }
        else
        {
            _animationClass = MomStateFromSitToStandAppearance;
        }
    }

    override protected function preFinishedActions():void
    {
        _characterModel.sittingOnObject = null;
    }

}
}
