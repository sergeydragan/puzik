/**
 * Created by SeeD on 14/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.states
{
public class StateDadFromSitToStand extends CharacterState
{
    public function StateDadFromSitToStand()
    {
        super();
    }

    override protected function preInitActions():void
    {
        if (_characterModel.babyInHands)
        {
            _animationClass = DadStateFromSitToStandWithBabyAppearance;
        }
        else
        {
            _animationClass = DadStateFromSitToStandAppearance;
        }
    }

    override protected function preFinishedActions():void
    {
        _characterModel.sittingOnObject = null;
    }

}
}
