/**
 * Created by SeeD on 14/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.states
{
public class StateMomPutBabyToFloor extends CharacterState
{
    public function StateMomPutBabyToFloor()
    {
        super();
        _animationClass = MomStatePutBabyToFloorAppearance;
    }

    override public function update():void
    {
        super.update();

        if (_characterView.appearance.container.currentFrameLabel == "childOutOfHands")
        {
            _stateParamsVO.suborderVO.subject.view.visible = true;
            _characterModel.babyInHands = null;
            _characterView.updateAppearance();
            _stateParamsVO.suborderVO.subject.view.updateAppearance();
        }
    }

}
}
