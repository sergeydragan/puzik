/**
 * Created by SeeD on 14/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.states
{
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.gameplay.characters.constants.EmotionTypes;
import com.kekkoneko.puzik.gameplay.objects.helpers.ObjectAppearanceHelper;
import com.kekkoneko.puzik.gameplay.objects.vo.IngredientsUsedVO;

public class StateMomPlayingWithBaby extends CharacterState
{
    private var _repeatCount:int = 0;

    public function StateMomPlayingWithBaby()
    {
        super();
    }

    override protected function preInitActions():void
    {
        if (_stateParamsVO.suborderVO.owner.sittingOnObject)
        {
            _animationClass = MomStatePlayingWithBabySittingAppearance;
        }
        else
        {
            _animationClass = MomStatePlayingWithBabyStandingAppearance;
            applyAppearanceMirroring();
        }
    }

    override protected function postInitActions():void
    {
        if (_stateParamsVO.suborderVO.ingredients.length > 0)
        {
            _characterView.appearance.container.toy.addChild(ObjectAppearanceHelper.getObjectSprite(_stateParamsVO.suborderVO.ingredients[0].gameObjectVO));
        }
    }

    override public function update():void
    {
        super.update();

        if (_stateParamsVO.suborderVO.owner.sittingOnObject)
        {
            if (_characterView.appearance.container.currentFrame == 25)
            {
                _characterView.appearance.container.toy.visible = true;
                _characterView.setEmotion(EmotionTypes.HAPPY);
                _characterView.setEmotion(EmotionTypes.HAPPY, true);
            }
            else if (_characterView.appearance.container.currentFrame == 103)
            {
                if (_repeatCount >= 1)
                {
                    _characterView.appearance.container.toy.visible = false;
                }
                else
                {
                    _repeatCount++;
                    _characterView.appearance.container.gotoAndPlay(32);
                }
            }
        }
        else
        {
            if (_ticksMade == 6)
            {
                _characterView.appearance.container.toy.visible = true;
                _characterView.setEmotion(EmotionTypes.HAPPY);
                _characterView.setEmotion(EmotionTypes.HAPPY, true);
            }
        }
    }

    override protected function preFinishedActions():void
    {
        coreEventDispatcher.broadcastMessage(PuzikCommands.INGREDIENTS_USED, new IngredientsUsedVO(_stateParamsVO.suborderVO.ingredients));
    }

}
}
