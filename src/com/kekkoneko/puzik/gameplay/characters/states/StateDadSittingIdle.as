/**
 * Created by SeeD on 14/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.states
{
public class StateDadSittingIdle extends CharacterState
{
    public function StateDadSittingIdle()
    {
        super();
    }

    override protected function preInitActions():void
    {
        if (_characterModel.babyInHands)
        {
            _animationClass = DadStateIdleSittingWithBabyAppearance;
        }
        else
        {
            _animationClass = DadStateIdleSittingAppearance;
        }
    }
}
}
