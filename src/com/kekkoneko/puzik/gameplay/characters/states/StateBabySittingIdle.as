/**
 * Created by SeeD on 14/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.states
{
public class StateBabySittingIdle extends CharacterState
{
    public function StateBabySittingIdle()
    {
        _animationClass = ChildStateIdleSittingAppearance;
        super();
    }

}
}
