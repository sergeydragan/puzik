/**
 * Created by SeeD on 14/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.states
{
public class StateMomSittingIdle extends CharacterState
{
    public function StateMomSittingIdle()
    {
        super();
    }

    override protected function preInitActions():void
    {
        if (_characterModel.babyInHands)
        {
            _animationClass = MomStateIdleSittingWithBabyAppearance;
        }
        else
        {
            _animationClass = MomStateIdleSittingAppearance;
        }
    }

}
}
