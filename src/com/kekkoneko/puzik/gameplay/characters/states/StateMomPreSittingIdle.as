/**
 * Created by SeeD on 14/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.states
{
public class StateMomPreSittingIdle extends CharacterState
{
    public function StateMomPreSittingIdle()
    {
        _animationClass = MomStateIdlePreSittingAppearance;
        super();
    }

    override protected function postInitActions():void
    {
        _characterView.appearance.container.leg22_1.visible = false;
        _characterView.appearance.container.foot2_1.visible = false;
    }

    override public function update():void
    {
        if (_characterView.appearance.container.currentFrame == 26)
        {
            _characterView.appearance.container.leg22_1.visible = true;
            _characterView.appearance.container.foot2_1.visible = true;
            _characterView.appearance.container.leg12.visible = false;
            _characterView.appearance.container.foot1.visible = false;
        }

        super.update()
    }
}
}
