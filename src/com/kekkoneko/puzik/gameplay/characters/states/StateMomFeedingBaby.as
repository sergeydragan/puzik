/**
 * Created by SeeD on 14/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.states
{
import com.kekkoneko.puzik.controller.notifications.IngredientsUsedNotification;

public class StateMomFeedingBaby extends CharacterState
{
    public function StateMomFeedingBaby()
    {
        super();
    }

    override protected function preInitActions():void
    {
        if (_stateParamsVO.suborderVO.owner.sittingOnObject)
        {
            _animationClass = MomStateFeedingBabySittingAppearance;
        }
        else
        {
            _animationClass = MomStateFeedingBabyStandingAppearance;
            applyAppearanceMirroring();
        }
    }

    override protected function preFinishedActions():void
    {
        coreEventDispatcher.broadcastMessage(IngredientsUsedNotification.INGREDIENTS_USED, new IngredientsUsedNotification(_stateParamsVO.suborderVO.ingredients));
    }

}
}
