/**
 * Created by SeeD on 14/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.states
{
public class StateDadStandingIdle extends CharacterState
{
    public function StateDadStandingIdle()
    {
        super();
    }

    override protected function preInitActions():void
    {
        if (_stateParamsVO.suborderVO.owner.babyInHands)
        {
            _animationClass = DadStateIdleStandingWithBabyAppearance;
        }
        else
        {
            _animationClass = DadStateIdleStandingAppearance;
        }
    }

    override protected function postInitActions():void
    {
        applyAppearanceMirroring();
    }

}
}
