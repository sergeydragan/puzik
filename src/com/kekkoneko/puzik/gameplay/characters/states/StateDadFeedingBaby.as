/**
 * Created by SeeD on 14/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.states
{
import com.kekkoneko.puzik.controller.notifications.IngredientsUsedNotification;

public class StateDadFeedingBaby extends CharacterState
{
    public function StateDadFeedingBaby()
    {
        super();
    }

    override protected function preInitActions():void
    {
        if (_stateParamsVO.suborderVO.owner.sittingOnObject)
        {
            _animationClass = DadStateFeedingBabySittingAppearance;
        }
        else
        {
            _animationClass = DadStateFeedingBabyStandingAppearance;
            applyAppearanceMirroring();
        }
    }

    override protected function preFinishedActions():void
    {
        coreEventDispatcher.broadcastMessage(IngredientsUsedNotification.INGREDIENTS_USED, new IngredientsUsedNotification(_stateParamsVO.suborderVO.ingredients, _stateParamsVO.suborderVO.subject));
    }

}
}
