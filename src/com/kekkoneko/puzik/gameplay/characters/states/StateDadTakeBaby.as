/**
 * Created by SeeD on 14/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.states
{
public class StateDadTakeBaby extends CharacterState
{
    public function StateDadTakeBaby()
    {
        super();
    }

    override protected function preInitActions():void
    {
        if (_stateParamsVO.suborderVO.subject.sittingOnObject)
        {
            _animationClass = DadStateTakeBabyFromFloorAppearance;  // todo: должна быть анимация "взять ребенка из кроватки"
        }
        else
        {
            _animationClass = DadStateTakeBabyFromFloorAppearance;
        }
    }

    override public function update():void
    {
        super.update();

        if (_characterView.appearance.container.currentFrameLabel == "childInHands")
        {
            _stateParamsVO.suborderVO.subject.view.visible = false;
            _characterModel.babyInHands = _stateParamsVO.suborderVO.subject;
            _characterView.updateAppearance();

            _stateParamsVO.suborderVO.subject.sittingOnObject = null;
        }
    }
}
}
