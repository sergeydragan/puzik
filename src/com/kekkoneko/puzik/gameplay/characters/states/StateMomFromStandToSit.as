/**
 * Created by SeeD on 14/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.states
{
public class StateMomFromStandToSit extends CharacterState
{
    public function StateMomFromStandToSit()
    {
        super();
    }

    override protected function preInitActions():void
    {
        if (_characterModel.babyInHands)
        {
            _animationClass = MomStateFromStandToSitWithBabyAppearance;
        }
        else
        {
            _animationClass = MomStateFromStandToSitAppearance;
        }
    }

    override protected function preFinishedActions():void
    {
        _characterModel.sittingOnObject = _stateParamsVO.suborderVO.targetInventoryObjectVO;

    }

}
}
