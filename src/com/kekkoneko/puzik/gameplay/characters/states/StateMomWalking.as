/**
 * Created by SeeD on 14/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.states
{
import com.kekkoneko.puzik.gameplay.characters.constants.CharacterSpeed;
import com.kekkoneko.puzik.gameplay.room.constants.Directions;
import com.kekkoneko.puzik.gameplay.room.view.RoomModeView;

public class StateMomWalking extends CharacterState
{

    public function StateMomWalking()
    {
        _animationClass = MomStateWalkLeftAppearance;
        super();
    }

    override protected function preInitActions():void
    {
        if (_characterModel.cellX < _stateParamsVO.suborderVO.coordsX)
        {
            _characterModel.direction = Directions.RIGHT;
            if (_stateParamsVO.suborderVO.owner.babyInHands)
            {
                _animationClass = MomStateWalkRightWithBabyAppearance;
            }
            else
            {
                _animationClass = _animationClass = MomStateWalkRightAppearance;
            }
        }
        else
        {
            _characterModel.direction = Directions.LEFT;
            if (_stateParamsVO.suborderVO.owner.babyInHands)
            {
                _animationClass = MomStateWalkLeftWithBabyAppearance;
            }
            else
            {
                _animationClass = _animationClass = MomStateWalkLeftAppearance;
            }
        }
    }

    override public function update():void
    {
        if (_characterModel.direction == Directions.LEFT)
        {
            _characterModel.cellX -= CharacterSpeed.MOM_WALK_SPEED / RoomModeView.CELL_WIDTH;
        }
        else
        {
            _characterModel.cellX += CharacterSpeed.MOM_WALK_SPEED / RoomModeView.CELL_WIDTH;
        }

        super.update();
    }

}
}
