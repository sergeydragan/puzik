/**
 * Created by SeeD on 30/11/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.appearance
{
import com.kekkoneko.core.debug.Debug;
import com.kekkoneko.puzik.gameplay.objects.model.InventoryModel;
import com.kekkoneko.puzik.gameplay.objects.vos.GameObjectVO;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;

public class CharacterAppearanceConfig
{
    public function CharacterAppearanceConfig()
    {
    }

    public function clone():CharacterAppearanceConfig
    {
        return null;
    }

    public function saveFrom(appearance:CharacterAppearanceConfig):void
    {

    }

    public function getFromInventoryModel(inventoryModel:InventoryModel):void
    {

    }

    protected function getGameObjectId(classId:int, inventoryModel:InventoryModel):String
    {
        /*
         Normally we don't need this algorithm with two vectors below, because there can be no more than one
         inventoryObject which has inWorld == true (for example, only one noseForm or only one clothBottom).
         But just in case I want to have some kind of fool-proofing here, because this "customization as
         InventoryObjects"-way is not yet tested.
         */
        var inventoryObjects:Vector.<InventoryObjectVO> = inventoryModel.getInventoryObjectsByClassId(classId);
        var resultedInventoryObjects:Vector.<InventoryObjectVO> = new <InventoryObjectVO>[];
        for (var i:int = 0; i < inventoryObjects.length; i++)
        {
            if (inventoryObjects[i].isInWorld)
            {
                resultedInventoryObjects.push(inventoryObjects[i]);
            }
        }

        if (resultedInventoryObjects.length == 0)
        {
            return "none";
        }
        else if (resultedInventoryObjects.length == 1)
        {
            return resultedInventoryObjects[0].gameObjectVO.id.toString();
        }
        else
        {
            Debug.error("More than one appearance inventoryObject of classId " + classId + " with inWorld == true");
            return resultedInventoryObjects[0].gameObjectVO.id.toString();
        }
    }

    public function replaceElementByGameObject(objVO:GameObjectVO):void
    {

    }
}
}
