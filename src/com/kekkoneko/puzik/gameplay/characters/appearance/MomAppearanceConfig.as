/**
 * Created by SeeD on 30/11/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.appearance
{
import com.kekkoneko.core.debug.Debug;
import com.kekkoneko.puzik.gameplay.objects.constants.GameObjectClasses;
import com.kekkoneko.puzik.gameplay.objects.model.InventoryModel;
import com.kekkoneko.puzik.gameplay.objects.vos.GameObjectVO;

public class MomAppearanceConfig extends CharacterAppearanceConfig
{
    public var momEyes:String = "1";
    public var momLips:String = "1";
    public var momNose:String = "1";
    public var momEyebrows:String = "1";
    public var momFaceForm:String = "1";
    public var momHairStyle:String = "1";
    public var momEyesColor:String = "1";
    public var momEyesVisage:String = "1";
    public var momLipsVisage:String = "1";
    public var momGlasses:String = "1";
    public var momHat:String = "1";
    public var momClothTop:String = "20101";
    public var momClothBottom:String = NONE;
    public var momClothSkirt:String = "20301";
    public var momClothShoes:String = "20401";
    public var momClothSocks:String = "20501";

    public static const NONE:String = "none";

    public function MomAppearanceConfig()
    {
        super();
    }

    override public function clone():CharacterAppearanceConfig
    {
        var momAppearanceConfig:MomAppearanceConfig = new MomAppearanceConfig();
        momAppearanceConfig.momEyes = momEyes;
        momAppearanceConfig.momLips = momLips;
        momAppearanceConfig.momNose = momNose;
        momAppearanceConfig.momEyebrows = momEyebrows;
        momAppearanceConfig.momFaceForm = momFaceForm;
        momAppearanceConfig.momHairStyle = momHairStyle;
        momAppearanceConfig.momEyesColor = momEyesColor;
        momAppearanceConfig.momEyesVisage = momEyesVisage;
        momAppearanceConfig.momLipsVisage = momLipsVisage;
        momAppearanceConfig.momGlasses = momGlasses;
        momAppearanceConfig.momHat = momHat;
        momAppearanceConfig.momClothTop = momClothTop;
        momAppearanceConfig.momClothBottom = momClothBottom;
        momAppearanceConfig.momClothSkirt = momClothSkirt;
        momAppearanceConfig.momClothShoes = momClothShoes;
        momAppearanceConfig.momClothSocks = momClothSocks;
        return momAppearanceConfig;
    }

    override public function saveFrom(appearanceConfig:CharacterAppearanceConfig):void
    {
        if (!(appearanceConfig is MomAppearanceConfig))
        {
            Debug.info("trying to perform MomAppearanceConfig.saveFrom not from instance of MomAppearanceConfig");
            return;
        }

        var momAppearance:MomAppearanceConfig = appearanceConfig as MomAppearanceConfig;
        momEyes = momAppearance.momEyes;
        momLips = momAppearance.momLips;
        momNose = momAppearance.momNose;
        momEyebrows = momAppearance.momEyebrows;
        momFaceForm = momAppearance.momFaceForm;
        momHairStyle = momAppearance.momHairStyle;
        momEyesColor = momAppearance.momEyesColor;
        momEyesVisage = momAppearance.momEyesVisage;
        momLipsVisage = momAppearance.momLipsVisage;
        momGlasses = momAppearance.momGlasses;
        momHat = momAppearance.momHat;
        momClothTop = momAppearance.momClothTop;
        momClothBottom = momAppearance.momClothBottom;
        momClothSkirt = momAppearance.momClothSkirt;
        momClothShoes = momAppearance.momClothShoes;
        momClothSocks = momAppearance.momClothSocks;
    }

    override public function getFromInventoryModel(inventoryModel:InventoryModel):void
    {
        momClothTop = getGameObjectId(GameObjectClasses.MOM_CLOTH_TOP, inventoryModel);
        momClothBottom = getGameObjectId(GameObjectClasses.MOM_CLOTH_BOTTOM, inventoryModel);
        momClothSkirt = getGameObjectId(GameObjectClasses.MOM_CLOTH_SKIRT, inventoryModel);
        momClothShoes = getGameObjectId(GameObjectClasses.MOM_CLOTH_SHOES, inventoryModel);
        momClothSocks = getGameObjectId(GameObjectClasses.MOM_CLOTH_SOCKS, inventoryModel);
        momGlasses = getGameObjectId(GameObjectClasses.MOM_CLOTH_GLASSES, inventoryModel);
        momHat = getGameObjectId(GameObjectClasses.MOM_CLOTH_HAT, inventoryModel);

        momEyes = getGameObjectId(GameObjectClasses.MOM_CLOTH_EYES, inventoryModel);
        momLips = getGameObjectId(GameObjectClasses.MOM_CLOTH_LIPS, inventoryModel);
        momNose = getGameObjectId(GameObjectClasses.MOM_CLOTH_NOSE, inventoryModel);
        momEyebrows = getGameObjectId(GameObjectClasses.MOM_CLOTH_EYEBROWS, inventoryModel);
        momFaceForm = getGameObjectId(GameObjectClasses.MOM_CLOTH_FACE_FORM, inventoryModel);
        momHairStyle = getGameObjectId(GameObjectClasses.MOM_CLOTH_HAIR_STYLE, inventoryModel);
        momEyesColor = getGameObjectId(GameObjectClasses.MOM_CLOTH_EYES_COLOR, inventoryModel);
        momEyesVisage = getGameObjectId(GameObjectClasses.MOM_CLOTH_EYES_VISAGE, inventoryModel);
        momLipsVisage = getGameObjectId(GameObjectClasses.MOM_CLOTH_LIPS_VISAGE, inventoryModel);

    }

    override public function replaceElementByGameObject(objVO:GameObjectVO):void
    {
        switch (objVO.classId)
        {
            case GameObjectClasses.MOM_CLOTH_BOTTOM:
                momClothBottom = objVO.id.toString();
                momClothSkirt = NONE;
                break;

            case GameObjectClasses.MOM_CLOTH_EYEBROWS:
                momEyebrows = objVO.id.toString();
                break;

            case GameObjectClasses.MOM_CLOTH_EYES:
                momEyes = objVO.id.toString();
                break;

            case GameObjectClasses.MOM_CLOTH_EYES_COLOR:
                momEyesColor = objVO.id.toString();
                break;

            case GameObjectClasses.MOM_CLOTH_EYES_VISAGE:
                momEyesVisage = objVO.id.toString();
                break;

            case GameObjectClasses.MOM_CLOTH_FACE_FORM:
                momFaceForm = objVO.id.toString();
                break;

            case GameObjectClasses.MOM_CLOTH_GLASSES:
                momGlasses = objVO.id.toString();
                break;

            case GameObjectClasses.MOM_CLOTH_HAIR_STYLE:
                momHairStyle = objVO.id.toString();
                break;

            case GameObjectClasses.MOM_CLOTH_HAT:
                momHat = objVO.id.toString();
                break;

            case GameObjectClasses.MOM_CLOTH_LIPS:
                momLips = objVO.id.toString();
                break;

            case GameObjectClasses.MOM_CLOTH_LIPS_VISAGE:
                momLipsVisage = objVO.id.toString();
                break;

            case GameObjectClasses.MOM_CLOTH_NOSE:
                momNose = objVO.id.toString();
                break;

            case GameObjectClasses.MOM_CLOTH_SHOES:
                momClothShoes = objVO.id.toString();
                break;

            case GameObjectClasses.MOM_CLOTH_SKIRT:
                momClothSkirt = objVO.id.toString();
                momClothBottom = NONE;
                break;

            case GameObjectClasses.MOM_CLOTH_SOCKS:
                momClothSocks = objVO.id.toString();
                break;

            case GameObjectClasses.MOM_CLOTH_TOP:
                momClothTop = objVO.id.toString();
                break;

            default:
                Debug.warning("GameObject of unknown type was provided to MomAppearanceConfig");
        }
    }
}
}
