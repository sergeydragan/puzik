/**
 * Created by SeeD on 30/11/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.appearance
{
import com.kekkoneko.core.debug.Debug;
import com.kekkoneko.puzik.gameplay.objects.constants.GameObjectClasses;
import com.kekkoneko.puzik.gameplay.objects.model.InventoryModel;
import com.kekkoneko.puzik.gameplay.objects.vos.GameObjectVO;

public class BabyAppearanceConfig extends CharacterAppearanceConfig
{
    public var babyEyes:String = "1";
    public var babyEyesColor:String = "1";
    public var babyLips:String = "1";
    public var babyNose:String = "1";
    public var babyFaceForm:String = "1";
    public var babyHairStyle:String = "1";
    public var babyClothTop:String = "1";
    public var babyClothBottom:String = "1";
    public var babyClothShoes:String = "1";
    public var babyHat:String = "22400";

    public function BabyAppearanceConfig()
    {
        super();
    }

    override public function clone():CharacterAppearanceConfig
    {
        var babyAppearanceConfig:BabyAppearanceConfig = new BabyAppearanceConfig;
        babyAppearanceConfig.babyEyes = babyEyes;
        babyAppearanceConfig.babyEyesColor = babyEyesColor;
        babyAppearanceConfig.babyLips = babyLips;
        babyAppearanceConfig.babyNose = babyNose;
        babyAppearanceConfig.babyFaceForm = babyFaceForm;
        babyAppearanceConfig.babyHairStyle = babyHairStyle;
        babyAppearanceConfig.babyClothTop = babyClothTop;
        babyAppearanceConfig.babyClothBottom = babyClothBottom;
        babyAppearanceConfig.babyClothShoes = babyClothShoes;
        babyAppearanceConfig.babyHat = babyHat;
        return babyAppearanceConfig;
    }

    override public function saveFrom(appearanceConfig:CharacterAppearanceConfig):void
    {
        if (!(appearanceConfig is BabyAppearanceConfig))
        {
            Debug.info("trying to perform BabyAppearanceConfig.saveFrom not from instance of BabyAppearanceConfig");
            return;
        }

        var babyAppearance:BabyAppearanceConfig = appearanceConfig as BabyAppearanceConfig;
        babyEyes = babyAppearance.babyEyes;
        babyEyesColor = babyAppearance.babyEyesColor;
        babyLips = babyAppearance.babyLips;
        babyNose = babyAppearance.babyNose;
        babyFaceForm = babyAppearance.babyFaceForm;
        babyHairStyle = babyAppearance.babyHairStyle;
        babyClothTop = babyAppearance.babyClothTop;
        babyClothBottom = babyAppearance.babyClothBottom;
        babyClothShoes = babyAppearance.babyClothShoes;
        babyHat = babyAppearance.babyHat;
    }

    override public function getFromInventoryModel(inventoryModel:InventoryModel):void
    {
        babyClothTop = getGameObjectId(GameObjectClasses.BABY_CLOTH_TOP, inventoryModel);
        babyClothBottom = getGameObjectId(GameObjectClasses.BABY_CLOTH_BOTTOM, inventoryModel);
        babyClothShoes = getGameObjectId(GameObjectClasses.BABY_CLOTH_SHOES, inventoryModel);
        babyHat = getGameObjectId(GameObjectClasses.BABY_CLOTH_HAT, inventoryModel);

        babyEyes = getGameObjectId(GameObjectClasses.BABY_CLOTH_EYES, inventoryModel);
        babyLips = getGameObjectId(GameObjectClasses.BABY_CLOTH_LIPS, inventoryModel);
        babyNose = getGameObjectId(GameObjectClasses.BABY_CLOTH_NOSE, inventoryModel);
        babyFaceForm = getGameObjectId(GameObjectClasses.BABY_CLOTH_FACE_FORM, inventoryModel);
        babyHairStyle = getGameObjectId(GameObjectClasses.BABY_CLOTH_HAIR_STYLE, inventoryModel);
        babyEyesColor = getGameObjectId(GameObjectClasses.BABY_CLOTH_EYES_COLOR, inventoryModel);
    }

    override public function replaceElementByGameObject(objVO:GameObjectVO):void
    {
        switch (objVO.classId)
        {
            case GameObjectClasses.BABY_CLOTH_BOTTOM:
                babyClothBottom = objVO.id.toString();
                break;

            case GameObjectClasses.BABY_CLOTH_EYES:
                babyEyes = objVO.id.toString();
                break;

            case GameObjectClasses.BABY_CLOTH_EYES_COLOR:
                babyEyesColor = objVO.id.toString();
                break;

            case GameObjectClasses.BABY_CLOTH_FACE_FORM:
                babyFaceForm = objVO.id.toString();
                break;

            case GameObjectClasses.BABY_CLOTH_HAIR_STYLE:
                babyHairStyle = objVO.id.toString();
                break;

            case GameObjectClasses.BABY_CLOTH_HAT:
                babyHat = objVO.id.toString();
                break;

            case GameObjectClasses.BABY_CLOTH_LIPS:
                babyLips = objVO.id.toString();
                break;

            case GameObjectClasses.BABY_CLOTH_NOSE:
                babyNose = objVO.id.toString();
                break;

            case GameObjectClasses.BABY_CLOTH_SHOES:
                babyClothShoes = objVO.id.toString();
                break;

            case GameObjectClasses.BABY_CLOTH_TOP:
                babyClothTop = objVO.id.toString();
                break;

            default:
                Debug.warning("GameObject of unknown type was provided to BabyAppearanceConfig");
        }
    }
}
}
