/**
 * Created by SeeD on 30/11/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.appearance
{
import com.kekkoneko.core.debug.Debug;
import com.kekkoneko.puzik.gameplay.objects.constants.GameObjectClasses;
import com.kekkoneko.puzik.gameplay.objects.model.InventoryModel;
import com.kekkoneko.puzik.gameplay.objects.vos.GameObjectVO;

public class DadAppearanceConfig extends CharacterAppearanceConfig
{
    public var dadEyes:String = "1";
    public var dadLips:String = "1";
    public var dadNose:String = "1";
    public var dadEyebrows:String = "1";
    public var dadFaceForm:String = "1";
    public var dadHairStyle:String = "1";
    public var dadEyesColor:String = "1";
    public var dadClothTop:String = "21101";
    public var dadClothBottom:String = "21201";
    public var dadClothShoes:String = "21301";

    public function DadAppearanceConfig()
    {
    }

    override public function clone():CharacterAppearanceConfig
    {
        var dadAppearanceConfig:DadAppearanceConfig = new DadAppearanceConfig();
        dadAppearanceConfig.dadEyes = dadEyes;
        dadAppearanceConfig.dadLips = dadLips;
        dadAppearanceConfig.dadNose = dadNose;
        dadAppearanceConfig.dadEyebrows = dadEyebrows;
        dadAppearanceConfig.dadFaceForm = dadFaceForm;
        dadAppearanceConfig.dadHairStyle = dadHairStyle;
        dadAppearanceConfig.dadEyesColor = dadEyesColor;
        dadAppearanceConfig.dadClothTop = dadClothTop;
        dadAppearanceConfig.dadClothBottom = dadClothBottom;
        dadAppearanceConfig.dadClothShoes = dadClothShoes;
        return dadAppearanceConfig;
    }

    override public function saveFrom(appearanceConfig:CharacterAppearanceConfig):void
    {
        if (!(appearanceConfig is DadAppearanceConfig))
        {
            Debug.info("trying to perform BabyAppearanceConfig.saveFrom not from instance of DadAppearanceConfig");
            return;
        }

        var dadAppearance:DadAppearanceConfig = appearanceConfig as DadAppearanceConfig;
        dadEyes = dadAppearance.dadEyes;
        dadLips = dadAppearance.dadLips;
        dadNose = dadAppearance.dadNose;
        dadEyebrows = dadAppearance.dadEyebrows;
        dadFaceForm = dadAppearance.dadFaceForm;
        dadHairStyle = dadAppearance.dadHairStyle;
        dadEyesColor = dadAppearance.dadEyesColor;
        dadClothTop = dadAppearance.dadClothTop;
        dadClothBottom = dadAppearance.dadClothBottom;
        dadClothShoes = dadAppearance.dadClothShoes;
    }

    override public function getFromInventoryModel(inventoryModel:InventoryModel):void
    {
        dadClothTop = getGameObjectId(GameObjectClasses.DAD_CLOTH_TOP, inventoryModel);
        dadClothBottom = getGameObjectId(GameObjectClasses.DAD_CLOTH_BOTTOM, inventoryModel);
        dadClothShoes = getGameObjectId(GameObjectClasses.DAD_CLOTH_SHOES, inventoryModel);

        dadEyes = getGameObjectId(GameObjectClasses.DAD_CLOTH_EYES, inventoryModel);
        dadLips = getGameObjectId(GameObjectClasses.DAD_CLOTH_LIPS, inventoryModel);
        dadNose = getGameObjectId(GameObjectClasses.DAD_CLOTH_NOSE, inventoryModel);
        dadEyebrows = getGameObjectId(GameObjectClasses.DAD_CLOTH_EYEBROWS, inventoryModel);
        dadFaceForm = getGameObjectId(GameObjectClasses.DAD_CLOTH_FACE_FORM, inventoryModel);
        dadHairStyle = getGameObjectId(GameObjectClasses.DAD_CLOTH_HAIR_STYLE, inventoryModel);
        dadEyesColor = getGameObjectId(GameObjectClasses.DAD_CLOTH_EYES_COLOR, inventoryModel);
    }

    override public function replaceElementByGameObject(objVO:GameObjectVO):void
    {
        switch (objVO.classId)
        {
            case GameObjectClasses.DAD_CLOTH_BOTTOM:
                dadClothBottom = objVO.id.toString();
                break;

            case GameObjectClasses.DAD_CLOTH_EYEBROWS:
                dadEyebrows = objVO.id.toString();
                break;

            case GameObjectClasses.DAD_CLOTH_EYES:
                dadEyes = objVO.id.toString();
                break;

            case GameObjectClasses.DAD_CLOTH_EYES_COLOR:
                dadEyesColor = objVO.id.toString();
                break;

            case GameObjectClasses.DAD_CLOTH_FACE_FORM:
                dadFaceForm = objVO.id.toString();
                break;

            case GameObjectClasses.DAD_CLOTH_HAIR_STYLE:
                dadHairStyle = objVO.id.toString();
                break;

            case GameObjectClasses.DAD_CLOTH_LIPS:
                dadLips = objVO.id.toString();
                break;

            case GameObjectClasses.DAD_CLOTH_NOSE:
                dadNose = objVO.id.toString();
                break;

            case GameObjectClasses.DAD_CLOTH_SHOES:
                dadClothShoes = objVO.id.toString();
                break;

            case GameObjectClasses.DAD_CLOTH_TOP:
                dadClothTop = objVO.id.toString();
                break;

            default:
                Debug.warning("GameObject of unknown type was provided to DadAppearanceConfig");
        }
    }
}
}
