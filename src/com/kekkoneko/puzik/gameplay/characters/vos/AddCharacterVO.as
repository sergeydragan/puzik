/**
 * Created by SeeD on 25/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.vos
{
public class AddCharacterVO
{
    public var type:String;
    public var makeActive:Boolean;
    public var initialPositionX:int;

    public function AddCharacterVO(type:String, initialPositionX:int = 1, makeActive:Boolean = false):void
    {
        this.type = type;
        this.initialPositionX = initialPositionX;
        this.makeActive = makeActive;
    }
}
}
