/**
 * Created by SeeD on 15/03/2015.
 */
package com.kekkoneko.puzik.gameplay.characters.vos
{
public class SwitchActiveCharacterVO
{
    public var character:String;

    public function SwitchActiveCharacterVO(characterType:String)
    {
        this.character = characterType;
    }
}
}
