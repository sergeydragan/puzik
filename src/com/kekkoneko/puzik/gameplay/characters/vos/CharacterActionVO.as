/**
 * Created by SeeD on 28/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.vos
{
import com.kekkoneko.puzik.gameplay.characters.model.CharacterModel;
import com.kekkoneko.puzik.gameplay.objects.vos.*;

public class CharacterActionVO
{
    public var interactionType:String;
    public var subject:CharacterModel;
    public var inventoryObjectVO:InventoryObjectVO;
    public var ingredientsNeeded:Boolean = false;
    public var ingredientsType:int;
    public var ingredients:Vector.<InventoryObjectVO>;

    public function CharacterActionVO(interactionType:String, subject:CharacterModel, inventoryObjectVO:InventoryObjectVO = null, ingredientsType:int = -1)
    {
        this.subject = subject;
        this.interactionType = interactionType;
        this.ingredientsType = ingredientsType;
        if (ingredientsType >= 0)
        {
            ingredientsNeeded = true;
        }
    }
}
}
