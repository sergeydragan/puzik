/**
 * Created by SeeD on 07/02/2015.
 */
package com.kekkoneko.puzik.gameplay.characters.vos
{
import com.kekkoneko.puzik.gameplay.characters.view.CharacterView;

public class ProcessCharacterSelectionVO
{
    public var characterView:CharacterView;

    public function ProcessCharacterSelectionVO(characterView:CharacterView)
    {
        this.characterView = characterView;
    }
}
}
