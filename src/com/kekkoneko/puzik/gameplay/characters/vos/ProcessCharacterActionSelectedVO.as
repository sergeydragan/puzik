/**
 * Created by SeeD on 02/01/2015.
 */
package com.kekkoneko.puzik.gameplay.characters.vos
{
import com.kekkoneko.puzik.gameplay.characters.model.CharacterModel;
import com.kekkoneko.puzik.gameplay.characters.view.CharacterView;

public class ProcessCharacterActionSelectedVO
{
    public var characterModel:CharacterModel;
    public var characterView:CharacterView;
    public var characterActionVO:CharacterActionVO;

    public function ProcessCharacterActionSelectedVO()
    {
    }

}
}
