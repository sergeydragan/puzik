/**
 * Created by SeeD on 25/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.puzik.gameplay.characters.appearance.BabyAppearanceConfig;
import com.kekkoneko.puzik.gameplay.characters.appearance.DadAppearanceConfig;
import com.kekkoneko.puzik.gameplay.characters.appearance.MomAppearanceConfig;
import com.kekkoneko.puzik.gameplay.characters.constants.CharacterTypes;
import com.kekkoneko.puzik.gameplay.characters.model.CharacterModel;
import com.kekkoneko.puzik.gameplay.characters.model.CharactersModel;
import com.kekkoneko.puzik.gameplay.characters.orders.constants.InteractionTypes;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.OrderVO;
import com.kekkoneko.puzik.gameplay.characters.vos.AddCharacterVO;
import com.kekkoneko.puzik.gameplay.locations.models.LocationsModel;

public class AddCharacterCommand extends Command
{
    [Inject]
    public var locationsModel:LocationsModel;

    [Inject]
    public var charactersModel:CharactersModel;

    public function AddCharacterCommand()
    {
        super();
    }

    override public function execute():void
    {
        var addCharacterVO:AddCharacterVO = _payload as AddCharacterVO;
        var charModel:CharacterModel = new CharacterModel();
        injector.injectInto(charModel);
        charModel.cellX = addCharacterVO.initialPositionX;
        charModel.type = addCharacterVO.type;
        charactersModel.addCharacter(charModel);

        if (addCharacterVO.type == CharacterTypes.MOM)
        {
            charModel.appearanceConfig = new MomAppearanceConfig();
        }
        else if (addCharacterVO.type == CharacterTypes.DAD)
        {
            charModel.appearanceConfig = new DadAppearanceConfig();
        }
        if (addCharacterVO.type == CharacterTypes.BABY)
        {
            charModel.appearanceConfig = new BabyAppearanceConfig();
        }

        if (addCharacterVO.makeActive)
        {
            charactersModel.setActiveCharacter(charModel);
        }

        locationsModel.getCurrentRoomModel().addCharacterModel(charModel);

        var orderVO:OrderVO = new OrderVO();
        orderVO.owner = charModel;
        orderVO.interactionType = InteractionTypes.IDLE;
        orderVO.isIdle = true;
        charModel.addOrder(orderVO);
    }
}
}
