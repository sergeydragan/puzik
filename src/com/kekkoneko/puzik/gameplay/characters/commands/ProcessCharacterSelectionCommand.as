/**
 * Created by SeeD on 28/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.panel.vos.ShowPanelPropertiesVO;
import com.kekkoneko.core.screen.ScreenModel;
import com.kekkoneko.puzik.gameplay.characters.constants.CharacterTypes;
import com.kekkoneko.puzik.gameplay.characters.model.CharacterModel;
import com.kekkoneko.puzik.gameplay.characters.model.CharactersModel;
import com.kekkoneko.puzik.gameplay.characters.orders.constants.InteractionTypes;
import com.kekkoneko.puzik.gameplay.characters.view.CharacterView;
import com.kekkoneko.puzik.gameplay.characters.vos.CharacterActionVO;
import com.kekkoneko.puzik.gameplay.characters.vos.ProcessCharacterSelectionVO;
import com.kekkoneko.puzik.gameplay.objects.constants.GameObjectClasses;
import com.kekkoneko.puzik.panel.constants.PuzikPanelNames;
import com.kekkoneko.puzik.panel.panels.characterActionSelection.vos.CharacterActionSelectionPanelPropertiesVO;

public class ProcessCharacterSelectionCommand extends Command
{
    [Inject]
    public var charactersModel:CharactersModel;

    [Inject]
    public var screenModel:ScreenModel;

    public function ProcessCharacterSelectionCommand()
    {
    }

    override public function execute():void
    {
        var payload:ProcessCharacterSelectionVO = _payload as ProcessCharacterSelectionVO;
        var characterView:CharacterView = payload.characterView;
        var characterModel:CharacterModel = characterView.characterModel;

        var actions:Vector.<CharacterActionVO> = getPossibleActionsForCharacter(characterModel);

//        trace("Here are possible actions for " + characterModel.type + ":");
        if (actions && actions.length > 0)
        {
//            for (var i:int = 0; i < actions.length; i++)
//            {
//                trace(actions[i].interactionType);
//            }

            // показать панельку про выбор действия
            var characterActionSelectionPanelPropertiesVO:CharacterActionSelectionPanelPropertiesVO = new CharacterActionSelectionPanelPropertiesVO();
            characterActionSelectionPanelPropertiesVO.actions = actions;
            characterActionSelectionPanelPropertiesVO.characterModel = characterModel;

            var showActionsPanelPropertiesVO:ShowPanelPropertiesVO = new ShowPanelPropertiesVO(PuzikPanelNames.CHARACTER_ACTION_SELECTION_PANEL, "", screenModel.getMouseCoords(), characterActionSelectionPanelPropertiesVO);
            coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_PANEL, showActionsPanelPropertiesVO);
        }
    }

    private function getPossibleActionsForCharacter(characterModel:CharacterModel):Vector.<CharacterActionVO>
    {
        var actionsVector:Vector.<CharacterActionVO> = new <CharacterActionVO>[];

        switch (characterModel.type)
        {
            case CharacterTypes.BABY:
                actionsVector.push(new CharacterActionVO(InteractionTypes.PLAY_WITH_BABY, characterModel, null, GameObjectClasses.TOYS_SMALL));
                actionsVector.push(new CharacterActionVO(InteractionTypes.FEED_BABY, characterModel, null, GameObjectClasses.FOOD_0_12_READY));
                actionsVector.push(new CharacterActionVO(InteractionTypes.TAKE_BABY, characterModel));
                break;

            case CharacterTypes.MOM:
                if (characterModel == charactersModel.getActiveCharacter() && characterModel.babyInHands)
                {
                    actionsVector.push(new CharacterActionVO(InteractionTypes.PLAY_WITH_BABY, characterModel.babyInHands, null, GameObjectClasses.TOYS_SMALL));
                    actionsVector.push(new CharacterActionVO(InteractionTypes.FEED_BABY, characterModel.babyInHands, null, GameObjectClasses.FOOD_0_12_READY));
                    actionsVector.push(new CharacterActionVO(InteractionTypes.PUT_BABY, characterModel.babyInHands));
                }
                break;

            case CharacterTypes.DAD:
                if (characterModel == charactersModel.getActiveCharacter() && characterModel.babyInHands)
                {
                    actionsVector.push(new CharacterActionVO(InteractionTypes.PLAY_WITH_BABY, characterModel.babyInHands, null, GameObjectClasses.TOYS_SMALL));
                    actionsVector.push(new CharacterActionVO(InteractionTypes.FEED_BABY, characterModel.babyInHands, null, GameObjectClasses.FOOD_0_12_READY));
                    actionsVector.push(new CharacterActionVO(InteractionTypes.PUT_BABY, characterModel.babyInHands));
                }
                break;
        }

        return actionsVector;
    }
}
}
