/**
 * Created by SeeD on 28/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.dialog.vos.ShowDialogPropertiesVO;
import com.kekkoneko.core.panel.vos.ShowPanelPropertiesVO;
import com.kekkoneko.core.screen.ScreenModel;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.dialog.constants.PuzikDialogNames;
import com.kekkoneko.puzik.dialog.dialogs.noIngredients.NoIngredientsDialogPropertiesVO;
import com.kekkoneko.puzik.gameplay.characters.model.CharactersModel;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.AddOrderToActiveCharacterVO;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.OrderVO;
import com.kekkoneko.puzik.gameplay.characters.vos.ProcessCharacterActionSelectedVO;
import com.kekkoneko.puzik.gameplay.objects.model.InventoryModel;
import com.kekkoneko.puzik.gameplay.objects.model.InventoryObjectsModel;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;
import com.kekkoneko.puzik.panel.constants.PuzikPanelNames;
import com.kekkoneko.puzik.panel.panels.ingredientSelection.vos.IngredientSelectionPanelPropertiesVO;

public class ProcessCharacterActionSelectedCommand extends Command
{
    [Inject]
    public var inventoryObjectsModel:InventoryObjectsModel;

    [Inject]
    public var charactersModel:CharactersModel;

    [Inject]
    public var screenModel:ScreenModel;

    [Inject]
    public var inventoryModel:InventoryModel;

    public function ProcessCharacterActionSelectedCommand()
    {
    }

    override public function execute():void
    {
        var vo:ProcessCharacterActionSelectedVO = _payload as ProcessCharacterActionSelectedVO;

        var orderVO:OrderVO = new OrderVO();
        orderVO.coordsX = vo.characterModel.cellX;
        orderVO.interactionType = vo.characterActionVO.interactionType;
        orderVO.subject = vo.characterActionVO.subject;

        if (vo.characterActionVO.ingredientsNeeded)
        {
            var usableInventoryObjectsVO:Vector.<InventoryObjectVO> = inventoryModel.getInventoryObjectsByClassId(vo.characterActionVO.ingredientsType);
            if (usableInventoryObjectsVO.length > 0)
            {
                // если нужны ингредиенты - показать панельку про выбор ингредиентов
                var ingredientSelectionPanelPropertiesVO:IngredientSelectionPanelPropertiesVO = new IngredientSelectionPanelPropertiesVO();
                // ей скармливается класс ингредиентов и сам ордер, чтобы она в него запихнула выбранные ингредиенты и отправила дальше
                ingredientSelectionPanelPropertiesVO.ingredientsClass = vo.characterActionVO.ingredientsType;
                ingredientSelectionPanelPropertiesVO.orderVO = orderVO;

                var showIngredientsPanelPropertiesVO:ShowPanelPropertiesVO = new ShowPanelPropertiesVO(PuzikPanelNames.INGREDIENT_SELECTION_PANEL, "", screenModel.getMouseCoords(), ingredientSelectionPanelPropertiesVO);
                coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_PANEL, showIngredientsPanelPropertiesVO);
            }
            else
            {
                trace(":(");
                var noIngredientsDialogPropertiesVO:NoIngredientsDialogPropertiesVO = new NoIngredientsDialogPropertiesVO(vo.characterActionVO.ingredientsType);
                coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_DIALOG, new ShowDialogPropertiesVO(PuzikDialogNames.NO_INGREDIENTS, noIngredientsDialogPropertiesVO));
            }
        }
        else
        {
            // если не нужны ингредиенты - выполнить сразу это действие
            coreEventDispatcher.broadcastMessage(PuzikCommands.ADD_ORDER_TO_ACTIVE_CHARACTER, new AddOrderToActiveCharacterVO(orderVO));
        }
    }
}
}
