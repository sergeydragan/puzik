/**
 * Created by SeeD on 25/12/2014.
 */
package com.kekkoneko.puzik.gameplay.characters.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.panel.vos.ClosePanelVO;
import com.kekkoneko.puzik.controller.notifications.CharacterSwitchedNotification;
import com.kekkoneko.puzik.gameplay.characters.model.CharacterModel;
import com.kekkoneko.puzik.gameplay.characters.model.CharactersModel;
import com.kekkoneko.puzik.gameplay.characters.vos.SwitchActiveCharacterVO;
import com.kekkoneko.puzik.gameplay.locations.models.LocationsModel;
import com.kekkoneko.puzik.panel.constants.PuzikPanelNames;

public class SwitchActiveCharacterCommand extends Command
{
    [Inject]
    public var locationsModel:LocationsModel;

    [Inject]
    public var charactersModel:CharactersModel;

    public function SwitchActiveCharacterCommand()
    {
        super();
    }

    override public function execute():void
    {
        var switchCharacterVO:SwitchActiveCharacterVO = _payload as SwitchActiveCharacterVO;
        var characters:Vector.<CharacterModel> = charactersModel.getAllCharacters();
        for (var i:int = 0; i < characters.length; i++)
        {
            if (characters[i].type == switchCharacterVO.character)
            {
                charactersModel.setActiveCharacter(characters[i]);
                coreEventDispatcher.broadcastMessage(CharacterSwitchedNotification.CHARACTER_SWITCHED, new CharacterSwitchedNotification(characters[i]));
                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.ACTION_SELECTION_PANEL));
                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.CHARACTER_ACTION_SELECTION_PANEL));
                break;
            }
        }
    }
}
}
