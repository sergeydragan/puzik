/**
 * Created by SeeD on 31/12/2014.
 */
package com.kekkoneko.puzik.mediator
{
import com.kekkoneko.core.eventdispatcher.CoreEventDispatcher;
import com.kekkoneko.core.loader.CommonDataLoader;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.gameplay.locations.constants.LocationTypes;
import com.kekkoneko.puzik.gameplay.locations.models.LocationVO;
import com.kekkoneko.puzik.gameplay.locations.vos.SwitchLocationVO;
import com.kekkoneko.puzik.gameplay.objects.helpers.DummyInventoryCreator;
import com.kekkoneko.puzik.gameplay.objects.model.GameObjectsModel;
import com.kekkoneko.puzik.gameplay.objects.vos.ParseGameObjectsJSONVO;
import com.kekkoneko.puzik.gameplay.objects.vos.ParseInventoryJSONVO;
import com.kekkoneko.puzik.gameplay.player.helpers.DummyPlayersCreator;
import com.kekkoneko.puzik.gameplay.player.vos.ParsePlayersJSONVO;
import com.kekkoneko.puzik.gameplay.player.vos.PostStartActionsVO;
import com.kekkoneko.puzik.gameplay.quests.vos.ParseQuestSubtasksJSONVO;
import com.kekkoneko.puzik.gameplay.quests.vos.ParseQuestsJSONVO;

public class GameMediator
{
    [Inject]
    public var coreEventDispatcher:CoreEventDispatcher;

    [Inject]
    public var gameObjectsModel:GameObjectsModel;

    public function GameMediator()
    {

    }

    public function init():void
    {
        new CommonDataLoader("objects.json", onObjectsLoaded);
    }

    private function onObjectsLoaded(data:Object):void
    {
        coreEventDispatcher.broadcastMessage(PuzikCommands.PARSE_GAME_OBJECTS_JSON, new ParseGameObjectsJSONVO(data as String));
        new CommonDataLoader("quests.json", onQuestsLoaded);
    }

    private function onQuestsLoaded(data:Object):void
    {
        coreEventDispatcher.broadcastMessage(PuzikCommands.PARSE_QUESTS_JSON, new ParseQuestsJSONVO(data as String));
        new CommonDataLoader("subtasks.json", onSubtasksLoaded);
    }

    private function onSubtasksLoaded(data:Object):void
    {
        coreEventDispatcher.broadcastMessage(PuzikCommands.PARSE_QUEST_SUBTASKS_JSON, new ParseQuestSubtasksJSONVO(data as String));
        startGame();
    }

    private function startGame():void
    {
        coreEventDispatcher.broadcastMessage(PuzikCommands.PARSE_PLAYERS_JSON, new ParsePlayersJSONVO(DummyPlayersCreator.generateDummyPlayersJSON()));
        coreEventDispatcher.broadcastMessage(PuzikCommands.PARSE_INVENTORY_JSON, new ParseInventoryJSONVO(DummyInventoryCreator.generateDummyInventoryJSON(gameObjectsModel)));

        coreEventDispatcher.broadcastMessage(PuzikCommands.SWITCH_LOCATION, new SwitchLocationVO(new LocationVO(LocationTypes.HOME)));
        coreEventDispatcher.broadcastMessage(PuzikCommands.POSTSTART_ACTIONS, new PostStartActionsVO());
//        coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_DIALOG, new ShowDialogPropertiesVO(PuzikDialogNames.QUEST_LIST));
    }

}
}
