/**
 * Created by SeeD on 25/01/2015.
 */
package com.kekkoneko.puzik.social.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.dialog.vos.ShowDialogPropertiesVO;
import com.kekkoneko.puzik.dialog.constants.PuzikDialogNames;
import com.kekkoneko.puzik.dialog.dialogs.buyHC.BuyHCDialogPropertiesVO;
import com.kekkoneko.puzik.social.vos.SuggestBuyHCVO;

public class SuggestBuyHCCommand extends Command
{
    public function SuggestBuyHCCommand()
    {
    }

    override public function execute():void
    {
        var payload:SuggestBuyHCVO = _payload as SuggestBuyHCVO;

        coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_DIALOG, new ShowDialogPropertiesVO(PuzikDialogNames.BUY_HC, new BuyHCDialogPropertiesVO(payload.hcVO)));
    }
}
}
