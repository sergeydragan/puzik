/**
 * Created by SeeD on 25/01/2015.
 */
package com.kekkoneko.puzik.social.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.core.debug.Debug;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.display.vos.ShowOverlayVO;
import com.kekkoneko.puzik.gameplay.objects.model.GameObjectsModel;
import com.kekkoneko.puzik.social.vos.InitiateBuyHCVO;

public class InitiateBuyHCCommand extends Command
{
    [Inject]
    public var gameObjectsModel:GameObjectsModel;

    public function InitiateBuyHCCommand()
    {
    }

    override public function execute():void
    {
        var payload:InitiateBuyHCVO = _payload as InitiateBuyHCVO;

        coreEventDispatcher.broadcastMessage(PuzikCommands.SHOW_OVERLAY, new ShowOverlayVO());

        // todo: send server command here
        Debug.info("Sending HC buy command");
    }
}
}
