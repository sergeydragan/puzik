/**
 * Created by sdragan on 28.01.2015.
 */
package com.kekkoneko.puzik.social.model
{
import com.kekkoneko.puzik.social.vos.HCVO;

public class HCModel
{
    private var _hcVOs:Vector.<HCVO>;

    public function HCModel()
    {
    }

    public function addHCVO(hcVO:HCVO):void
    {
        for (var i:int = 0; i < _hcVOs.length; i++)
        {
            if (_hcVOs[i].id == hcVO.id)
            {
                return;
            }
        }
        _hcVOs.push(hcVO);
    }

    public function getHCVOs():Vector.<HCVO>
    {
        return _hcVOs;
    }
}
}
