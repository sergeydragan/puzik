/**
 * Created by SeeD on 29/12/2014.
 */
package com.kekkoneko.puzik.panel.panels.ingredientSelection
{
import com.kekkoneko.core.composition.events.CompositionListEvent;
import com.kekkoneko.core.composition.models.CompositionPropertiesModel;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.panel.PanelMediator;
import com.kekkoneko.core.panel.vos.ClosePanelVO;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.dialog.renderers.vos.IngredientSelectionItemRendererDataVO;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.AddOrderToActiveCharacterVO;
import com.kekkoneko.puzik.gameplay.objects.model.InventoryModel;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;
import com.kekkoneko.puzik.panel.constants.PuzikPanelNames;
import com.kekkoneko.puzik.panel.panels.ingredientSelection.vos.IngredientSelectionPanelPropertiesVO;

public class IngredientSelectionPanelMediator extends PanelMediator
{
    [Inject]
    public var compositionPropertiesModel:CompositionPropertiesModel;

    [Inject]
    public var inventoryModel:InventoryModel;

    private var _properties:IngredientSelectionPanelPropertiesVO;

    public function IngredientSelectionPanelMediator()
    {
        super();
    }

    override public function init():void
    {
        _properties = compositionPropertiesModel.getPropertiesByCompositionName(PuzikPanelNames.INGREDIENT_SELECTION_PANEL) as IngredientSelectionPanelPropertiesVO;
        var usableInventoryObjectsVO:Vector.<InventoryObjectVO> = inventoryModel.getInventoryObjectsByClassId(_properties.ingredientsClass);
        if (usableInventoryObjectsVO.length > 0)
        {
            if (usableInventoryObjectsVO.length > 1)
            {
                var listItems:Array = [];
                for (var i:int = 0; i < usableInventoryObjectsVO.length; i++)
                {
                    var ingredientSelectionListItemRendererVO:IngredientSelectionItemRendererDataVO = new IngredientSelectionItemRendererDataVO(usableInventoryObjectsVO[i]);
                    listItems.push(ingredientSelectionListItemRendererVO);
                }
                (view as IngredientSelectionPanelView).setList(listItems);
                view.addEventListener(CompositionListEvent.LIST_EVENT, onViewListEvent);
            }
            else
            {
                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.INGREDIENT_SELECTION_PANEL));

                _properties.orderVO.addIngredients(usableInventoryObjectsVO[0]);
                coreEventDispatcher.broadcastMessage(PuzikCommands.ADD_ORDER_TO_ACTIVE_CHARACTER, new AddOrderToActiveCharacterVO(_properties.orderVO));
            }
        }
        else
        {
            trace("No ingredients of needed type :c");
            coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.INGREDIENT_SELECTION_PANEL));
        }

        super.init();
    }

    private function onViewListEvent(event:CompositionListEvent):void
    {
        _properties.orderVO.addIngredients((event.listItemRendererDataVO as IngredientSelectionItemRendererDataVO).inventoryObjectVO);
        coreEventDispatcher.broadcastMessage(PuzikCommands.ADD_ORDER_TO_ACTIVE_CHARACTER, new AddOrderToActiveCharacterVO(_properties.orderVO));
        coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.INGREDIENT_SELECTION_PANEL));
    }

    override public function destroy():void
    {
        view.removeEventListener(CompositionListEvent.LIST_EVENT, onViewListEvent);
        super.destroy();
    }
}
}
