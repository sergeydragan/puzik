/**
 * Created by SeeD on 29/12/2014.
 */
package com.kekkoneko.puzik.panel.panels.ingredientSelection
{
import com.kekkoneko.core.composition.elements.list.List;
import com.kekkoneko.core.composition.elements.list.layouts.ListLayoutTypes;
import com.kekkoneko.core.panel.PanelView;

public class IngredientSelectionPanelView extends PanelView
{
    private var _ingredientsList:List;

    public function IngredientSelectionPanelView()
    {
        super();
    }

    override public function init():void
    {
        _ingredientsList = getList("list_ingredientSelection");
        _ingredientsList.setLayoutType(ListLayoutTypes.HORIZONTAL);
        _ingredientsList.layout.horizontalSpacing = 5;
    }

    public function setList(listItems:Array):void
    {
        _ingredientsList.setData(listItems);
    }
}
}
