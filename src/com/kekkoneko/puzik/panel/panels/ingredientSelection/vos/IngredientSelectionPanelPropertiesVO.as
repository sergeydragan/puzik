/**
 * Created by SeeD on 29/12/2014.
 */
package com.kekkoneko.puzik.panel.panels.ingredientSelection.vos
{
import com.kekkoneko.core.composition.vos.CompositionPropertiesVO;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.OrderVO;

public class IngredientSelectionPanelPropertiesVO extends CompositionPropertiesVO
{
    // наша панелька возьмёт этот ордер и по окончанию выбора отправит его дальше с засунутыми в него ингредиентами
    public var orderVO:OrderVO;
    public var ingredientsClass:int;

    public function IngredientSelectionPanelPropertiesVO()
    {
        super();
    }
}
}
