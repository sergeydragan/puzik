/**
 * Created by SeeD on 29/12/2014.
 */
package com.kekkoneko.puzik.panel.panels.activeCharacter
{
import com.kekkoneko.core.panel.PanelView;
import com.kekkoneko.puzik.gameplay.characters.constants.CharacterTypes;

public class ActiveCharacterPanelView extends PanelView
{
    public function ActiveCharacterPanelView()
    {
        super();
    }

    override public function init():void
    {

    }

    public function setCharacterButtonsStatus(characterType:String):void
    {
        if (characterType == CharacterTypes.MOM)
        {
            getButton("btn_mom").disable();
            getButton("btn_dad").enable();
        }
        else if (characterType == CharacterTypes.DAD)
        {
            getButton("btn_mom").enable();
            getButton("btn_dad").disable();
        }
    }

}
}
