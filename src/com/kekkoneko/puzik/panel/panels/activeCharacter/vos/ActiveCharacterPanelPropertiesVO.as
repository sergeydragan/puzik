/**
 * Created by SeeD on 29/12/2014.
 */
package com.kekkoneko.puzik.panel.panels.activeCharacter.vos
{
import com.kekkoneko.core.composition.vos.CompositionPropertiesVO;

public class ActiveCharacterPanelPropertiesVO extends CompositionPropertiesVO
{

    public function ActiveCharacterPanelPropertiesVO()
    {
        super();
    }
}
}
