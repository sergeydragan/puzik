/**
 * Created by SeeD on 29/12/2014.
 */
package com.kekkoneko.puzik.panel.panels.activeCharacter
{
import com.kekkoneko.core.composition.events.CompositionButtonPressedEvent;
import com.kekkoneko.core.composition.models.CompositionPropertiesModel;
import com.kekkoneko.core.panel.PanelMediator;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.gameplay.characters.constants.CharacterTypes;
import com.kekkoneko.puzik.gameplay.characters.model.CharactersModel;
import com.kekkoneko.puzik.gameplay.characters.vos.SwitchActiveCharacterVO;
import com.kekkoneko.puzik.panel.panels.activeCharacter.vos.ActiveCharacterPanelPropertiesVO;

public class ActiveCharacterPanelMediator extends PanelMediator
{
    [Inject]
    public var compositionPropertiesModel:CompositionPropertiesModel;

    [Inject]
    public var charactersModel:CharactersModel;

    private var _properties:ActiveCharacterPanelPropertiesVO;

    public function ActiveCharacterPanelMediator()
    {
        super();
    }

    override public function init():void
    {
//        _properties = compositionPropertiesModel.getPropertiesByCompositionName(PuzikPanelNames.ACTIVE_CHARACTER) as ActiveCharacterPanelPropertiesVO;

        view.addEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onButtonPressed);
        (view as ActiveCharacterPanelView).setCharacterButtonsStatus(charactersModel.getActiveCharacter().type);
        super.init();
    }

    private function onButtonPressed(event:CompositionButtonPressedEvent):void
    {
        var characterType:String;
        if (event.buttonName == "btn_mom")
        {
            if (charactersModel.getActiveCharacter().type == CharacterTypes.MOM)
            {
                return;
            }
            characterType = CharacterTypes.MOM;
        }
        else if (event.buttonName == "btn_dad")
        {
            if (charactersModel.getActiveCharacter().type == CharacterTypes.DAD)
            {
                return;
            }
            characterType = CharacterTypes.DAD;
        }

        coreEventDispatcher.broadcastMessage(PuzikCommands.SWITCH_ACTIVE_CHARACTER, new SwitchActiveCharacterVO(characterType));
        (view as ActiveCharacterPanelView).setCharacterButtonsStatus(characterType);
    }

    override public function destroy():void
    {
        view.removeEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onButtonPressed);
        super.destroy();
    }
}
}
