/**
 * Created by SeeD on 29/12/2014.
 */
package com.kekkoneko.puzik.panel.panels.inventoryObjectEdit.vos
{
import com.kekkoneko.core.composition.vos.CompositionPropertiesVO;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;

public class InventoryObjectEditPanelPropertiesVO extends CompositionPropertiesVO
{
    public var inventoryObjectVO:InventoryObjectVO;

    public function InventoryObjectEditPanelPropertiesVO(inventoryObjectVO:InventoryObjectVO)
    {
        this.inventoryObjectVO = inventoryObjectVO;
        super();
    }
}
}
