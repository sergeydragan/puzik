/**
 * Created by SeeD on 29/12/2014.
 */
package com.kekkoneko.puzik.panel.panels.inventoryObjectEdit
{
import com.kekkoneko.core.composition.events.CompositionButtonPressedEvent;
import com.kekkoneko.core.composition.models.CompositionPropertiesModel;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.panel.PanelMediator;
import com.kekkoneko.core.panel.vos.ClosePanelVO;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.controller.notifications.EndInventoryObjectEditNotification;
import com.kekkoneko.puzik.gameplay.objects.vos.SetInventoryObjectVO;
import com.kekkoneko.puzik.gameplay.shop.vos.SuggestSellObjectVO;
import com.kekkoneko.puzik.panel.constants.PuzikPanelNames;
import com.kekkoneko.puzik.panel.panels.inventoryObjectEdit.vos.InventoryObjectEditPanelPropertiesVO;

public class InventoryObjectEditPanelMediator extends PanelMediator
{
    [Inject]
    public var compositionPropertiesModel:CompositionPropertiesModel;

    private var _properties:InventoryObjectEditPanelPropertiesVO;

    public function InventoryObjectEditPanelMediator()
    {
        super();
    }

    override public function init():void
    {
        _properties = compositionPropertiesModel.getPropertiesByCompositionName(PuzikPanelNames.INVENTORY_OBJECT_EDIT_PANEL) as InventoryObjectEditPanelPropertiesVO;

        view.addEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);

        super.init();
    }

    private function onViewButtonPressed(e:CompositionButtonPressedEvent):void
    {
        if (e.buttonName == "btn_sell")
        {
            coreEventDispatcher.broadcastMessage(PuzikCommands.SUGGEST_SELL_OBJECT, new SuggestSellObjectVO(_properties.inventoryObjectVO.id));
            coreEventDispatcher.broadcastMessage(EndInventoryObjectEditNotification.END_INVENTORY_OBJECT_EDIT);
        }
        else if (e.buttonName == "btn_toInventory")
        {
            coreEventDispatcher.broadcastMessage(PuzikCommands.SET_INVENTORY_OBJECT, new SetInventoryObjectVO(_properties.inventoryObjectVO, _properties.inventoryObjectVO.x, _properties.inventoryObjectVO.y, false, _properties.inventoryObjectVO.roomId));
        }
        else if (e.buttonName == "btn_confirm")
        {
            coreEventDispatcher.broadcastMessage(EndInventoryObjectEditNotification.END_INVENTORY_OBJECT_EDIT);
        }

        var hidePanelVO:ClosePanelVO = new ClosePanelVO(PuzikPanelNames.INVENTORY_OBJECT_EDIT_PANEL);
        coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, hidePanelVO);
    }

    override public function destroy():void
    {
        view.removeEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
        super.destroy();
    }
}
}
