/**
 * Created by SeeD on 29/12/2014.
 */
package com.kekkoneko.puzik.panel.panels.actionSelection
{
import com.kekkoneko.core.composition.events.CompositionButtonPressedEvent;
import com.kekkoneko.core.composition.models.CompositionPropertiesModel;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.panel.PanelMediator;
import com.kekkoneko.core.panel.vos.ClosePanelVO;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.controller.notifications.EditInventoryObjectNotification;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectActionVO;
import com.kekkoneko.puzik.gameplay.objects.vos.ProcessInventoryObjectActionSelectedVO;
import com.kekkoneko.puzik.panel.constants.PuzikPanelNames;
import com.kekkoneko.puzik.panel.panels.actionSelection.vos.ActionSelectionPanelPropertiesVO;

public class ActionSelectionPanelMediator extends PanelMediator
{
    [Inject]
    public var compositionPropertiesModel:CompositionPropertiesModel;

    private var _properties:ActionSelectionPanelPropertiesVO;

    public function ActionSelectionPanelMediator()
    {
        super();
    }

    override public function init():void
    {
        _properties = compositionPropertiesModel.getPropertiesByCompositionName(PuzikPanelNames.ACTION_SELECTION_PANEL) as ActionSelectionPanelPropertiesVO;

        var actionNamesVector:Vector.<String> = new <String>[];
        for (var i:int = 0; i < _properties.actions.length; i++)
        {
            actionNamesVector.push(_properties.actions[i].interactionType);
        }
        (view as ActionSelectionPanelView).setActionButtonsFrames(actionNamesVector);
        view.addEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);

        super.init();
    }

    private function onViewButtonPressed(e:CompositionButtonPressedEvent):void
    {
        if (e.buttonName == "btn_move")
        {
            coreEventDispatcher.broadcastMessage(EditInventoryObjectNotification.EDIT_INVENTORY_OBJECT, new EditInventoryObjectNotification(_properties.inventoryObjectVO));
        }
        else
        {
            var actionVO:InventoryObjectActionVO;
            for (var i:int = 0; i < _properties.actions.length; i++)
            {
                if (_properties.actions[i].interactionType == e.buttonName)
                {
                    actionVO = _properties.actions[i];
                    break;
                }
            }
            trace("Action selected: " + actionVO.interactionType);
            var processInventoryObjectActionSelectedVO:ProcessInventoryObjectActionSelectedVO = new ProcessInventoryObjectActionSelectedVO();
            processInventoryObjectActionSelectedVO.inventoryObjectVO = _properties.inventoryObjectVO;
            processInventoryObjectActionSelectedVO.inventoryObjectActionVO = actionVO;
            coreEventDispatcher.broadcastMessage(PuzikCommands.PROCESS_INVENTORY_OBJECT_ACTION_SELECTED, processInventoryObjectActionSelectedVO);
        }

        var hidePanelVO:ClosePanelVO = new ClosePanelVO(PuzikPanelNames.ACTION_SELECTION_PANEL);
        coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, hidePanelVO);
    }

    override public function destroy():void
    {
        view.removeEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
        super.destroy();
    }
}
}
