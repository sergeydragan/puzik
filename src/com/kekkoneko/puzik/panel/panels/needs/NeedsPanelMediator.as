/**
 * Created by SeeD on 29/12/2014.
 */
package com.kekkoneko.puzik.panel.panels.needs
{
import com.kekkoneko.core.composition.models.CompositionPropertiesModel;
import com.kekkoneko.core.eventdispatcher.CoreEvent;
import com.kekkoneko.core.panel.PanelMediator;
import com.kekkoneko.puzik.controller.notifications.NeedsChangedNotification;
import com.kekkoneko.puzik.gameplay.cycles.SingleBabyNeedsModel;
import com.kekkoneko.puzik.panel.panels.needs.vos.NeedsPanelPropertiesVO;

public class NeedsPanelMediator extends PanelMediator
{
    [Inject]
    public var compositionPropertiesModel:CompositionPropertiesModel;

    [Inject]
    public var singleBabyNeedsModel:SingleBabyNeedsModel;

    private var _properties:NeedsPanelPropertiesVO;

    public function NeedsPanelMediator()
    {
        super();
    }

    override public function init():void
    {
//        _properties = compositionPropertiesModel.getPropertiesByCompositionName(PuzikPanelNames.NEEDS_PANEL) as NeedsPanelPropertiesVO;

        coreEventDispatcher.addEventListener(NeedsChangedNotification.NEEDS_CHANGED, onNeedsChanged);

        super.init();
    }

    private function onNeedsChanged(event:CoreEvent):void
    {
        updateNeedsOnView();
    }

    private function updateNeedsOnView():void
    {
        (view as NeedsPanelView).setFood(singleBabyNeedsModel.getNeedsVO().foodCurrent, singleBabyNeedsModel.getNeedsVO().foodMax);
        (view as NeedsPanelView).setSleep(singleBabyNeedsModel.getNeedsVO().sleepCurrent, singleBabyNeedsModel.getNeedsVO().sleepMax);
        (view as NeedsPanelView).setToilet(singleBabyNeedsModel.getNeedsVO().toiletCurrent, singleBabyNeedsModel.getNeedsVO().toiletMax);
        (view as NeedsPanelView).setWash(singleBabyNeedsModel.getNeedsVO().washCurrent, singleBabyNeedsModel.getNeedsVO().washMax);
        (view as NeedsPanelView).setHappiness(singleBabyNeedsModel.getNeedsVO().happinessCurrent, singleBabyNeedsModel.getNeedsVO().happinessMax);
    }

    override public function destroy():void
    {
        coreEventDispatcher.removeEventListener(NeedsChangedNotification.NEEDS_CHANGED, onNeedsChanged);

        super.destroy();
    }
}
}
