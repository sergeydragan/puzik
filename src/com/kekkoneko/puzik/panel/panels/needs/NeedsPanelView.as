/**
 * Created by SeeD on 29/12/2014.
 */
package com.kekkoneko.puzik.panel.panels.needs
{
import com.kekkoneko.core.panel.PanelView;

public class NeedsPanelView extends PanelView
{
    public function NeedsPanelView()
    {
        super();
    }

    override public function init():void
    {

    }

    public function setFood(foodCurrent:int, foodMax:int):void
    {
        getProgressBar("progressbar_food").setMaxValue(foodMax);
        getProgressBar("progressbar_food").setCurrentValue(foodCurrent);
    }

    public function setSleep(sleepCurrent:int, sleepMax:int):void
    {
        getProgressBar("progressbar_sleep").setMaxValue(sleepMax);
        getProgressBar("progressbar_sleep").setCurrentValue(sleepCurrent);
    }

    public function setToilet(toiletCurrent:int, toiletMax:int):void
    {
        getProgressBar("progressbar_toilet").setMaxValue(toiletMax);
        getProgressBar("progressbar_toilet").setCurrentValue(toiletCurrent);
    }

    public function setWash(washCurrent:int, washMax:int):void
    {
        getProgressBar("progressbar_wash").setMaxValue(washMax);
        getProgressBar("progressbar_wash").setCurrentValue(washCurrent);
    }

    public function setHappiness(happinessCurrent:int, happinessMax:int):void
    {
        getProgressBar("progressbar_happiness").setMaxValue(happinessMax);
        getProgressBar("progressbar_happiness").setCurrentValue(happinessCurrent);
    }
}
}
