/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.panel.panels.inventoryBig
{
import com.kekkoneko.core.composition.events.CompositionButtonPressedEvent;
import com.kekkoneko.core.composition.events.CompositionListEvent;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.eventdispatcher.CoreEvent;
import com.kekkoneko.core.panel.PanelMediator;
import com.kekkoneko.core.panel.vos.ClosePanelVO;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.controller.notifications.InventoryObjectSoldNotification;
import com.kekkoneko.puzik.controller.notifications.InventoryObjectUpdatedNotification;
import com.kekkoneko.puzik.dialog.renderers.InventoryBigObjectItemRenderer;
import com.kekkoneko.puzik.dialog.renderers.vos.InventoryBigObjectItemRendererDataVO;
import com.kekkoneko.puzik.gameplay.locations.models.LocationsModel;
import com.kekkoneko.puzik.gameplay.objects.constants.GameObjectClasses;
import com.kekkoneko.puzik.gameplay.objects.constants.GameObjectCollections;
import com.kekkoneko.puzik.gameplay.objects.model.GameObjectsModel;
import com.kekkoneko.puzik.gameplay.objects.model.InventoryModel;
import com.kekkoneko.puzik.gameplay.objects.vos.GameObjectVO;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;
import com.kekkoneko.puzik.gameplay.objects.vos.SetInventoryObjectVO;
import com.kekkoneko.puzik.gameplay.objects.vos.SetRoomAppearanceVO;
import com.kekkoneko.puzik.gameplay.player.model.PlayersModel;
import com.kekkoneko.puzik.gameplay.shop.vos.SuggestSellObjectVO;
import com.kekkoneko.puzik.panel.constants.PuzikPanelNames;

public class InventoryBigPanelMediator extends PanelMediator
{
    private var _collection:String;

    [Inject]
    public var playersModel:PlayersModel;

    [Inject]
    public var inventoryModel:InventoryModel;

    [Inject]
    public var gameObjectsModel:GameObjectsModel;

    [Inject]
    public var locationsModel:LocationsModel;

    public function InventoryBigPanelMediator()
    {
    }

    override public function init():void
    {
        super.init();

        view.addEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
        view.addEventListener(CompositionListEvent.LIST_EVENT, onViewListEvent);

        coreEventDispatcher.addEventListener(InventoryObjectSoldNotification.INVENTORY_OBJECT_SOLD, onInventoryObjectSoldNotification);
        coreEventDispatcher.addEventListener(InventoryObjectUpdatedNotification.INVENTORY_OBJECT_UPDATED, onInventoryObjectUpdatedNotification);

        selectCollection(GameObjectCollections.FURNITURE);
        (view as InventoryBigPanelView).highlightSelectedButton("btn_furniture");
    }

    private function selectCollection(categoryName:String):void
    {
        _collection = categoryName;
        var itemClasses:Vector.<int> = gameObjectsModel.getObjectsCollection(categoryName);
        var inventoryObjects:Vector.<InventoryObjectVO> = new <InventoryObjectVO>[];
        var itemsList:Array = [];
        var i:int;

        for (i = 0; i < itemClasses.length; i++)
        {
            inventoryObjects = inventoryObjects.concat(inventoryModel.getInventoryObjectsByClassId(itemClasses[i]));
        }

        // todo: now we have an array of inventoryObjects - it's a perfect moment to sort them

        for (i = 0; i < inventoryObjects.length; i++)
        {
            if (!inventoryObjects[i].isInWorld)
            {
                itemsList.push(new InventoryBigObjectItemRendererDataVO(inventoryObjects[i]));
            }
        }

        (view as InventoryBigPanelView).displayItemsList(itemsList);
    }

    private function onViewListEvent(event:CompositionListEvent):void
    {
        const rendererVO:InventoryBigObjectItemRendererDataVO = event.listItemRendererDataVO as InventoryBigObjectItemRendererDataVO;
        const inventoryObjectVO:InventoryObjectVO = rendererVO.inventoryObjectVO;

        switch (event.eventType)
        {
            case InventoryBigObjectItemRenderer.SELL_INVENTORY_OBJECT:
                coreEventDispatcher.broadcastMessage(PuzikCommands.SUGGEST_SELL_OBJECT, new SuggestSellObjectVO(inventoryObjectVO.id));
                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.INVENTORY_BIG_PANEL));
                break;
            case InventoryBigObjectItemRenderer.TO_WORLD:
                if (inventoryObjectVO && inventoryObjectVO.gameObjectVO)
                {
                    const gameObjectVO:GameObjectVO = inventoryObjectVO.gameObjectVO;
                    const classId : int = gameObjectVO.classId; 
                    if (classId == GameObjectClasses.FLOORS || classId == GameObjectClasses.WALLPAPERS)
                    {
                        coreEventDispatcher.broadcastMessage(PuzikCommands.SET_ROOM_APPEARANCE, new SetRoomAppearanceVO(inventoryObjectVO));
                    }
                    else
                    {
                        coreEventDispatcher.broadcastMessage(PuzikCommands.SET_INVENTORY_OBJECT, new SetInventoryObjectVO(inventoryObjectVO, inventoryObjectVO.x, inventoryObjectVO.y, true, locationsModel.getCurrentRoomModel().roomVO.id));
                    }
                }
                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.INVENTORY_BIG_PANEL));
                break;
        }
    }

    private function onViewButtonPressed(event:CompositionButtonPressedEvent):void
    {
        if (event.buttonName == "btn_close")
        {
            coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, new ClosePanelVO(PuzikPanelNames.INVENTORY_BIG_PANEL));
        }
        else
        {
            switch (event.buttonName)
            {
                case "btn_furniture":
                    selectCollection(GameObjectCollections.FURNITURE);
                    break;
                case "btn_electronics":
                    selectCollection(GameObjectCollections.ELECTRONICS);
                    break;
                case "btn_food":
                    selectCollection(GameObjectCollections.FOOD);
                    break;
                case "btn_wash":
                    selectCollection(GameObjectCollections.HYGIENE);
                    break;
                case "btn_toys":
                    selectCollection(GameObjectCollections.TOYS);
                    break;

                case "btn_decoration":
                    selectCollection(GameObjectCollections.DECO);
                    break;
                case "btn_floor":
                    selectCollection(GameObjectCollections.FLOORS);
                    break;
                case "btn_wallpapers":
                    selectCollection(GameObjectCollections.WALLPAPERS);
                    break;
            }

            (view as InventoryBigPanelView).highlightSelectedButton(event.buttonName);
        }
    }

    override public function destroy():void
    {
        view.removeEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
        super.destroy();
    }

    private function onInventoryObjectSoldNotification(event:CoreEvent):void
    {
        selectCollection(_collection);
    }

    private function onInventoryObjectUpdatedNotification(event:CoreEvent):void
    {
        selectCollection(_collection);
    }

}
}
