/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.panel.panels.inventoryBig
{
import com.kekkoneko.core.composition.elements.SimpleBtn;
import com.kekkoneko.core.composition.elements.list.List;
import com.kekkoneko.core.composition.elements.list.layouts.ListLayoutTypes;
import com.kekkoneko.core.panel.PanelView;

public class InventoryBigPanelView extends PanelView
{
    private static const BUTTON_NAMES:Vector.<String> = new <String>["btn_furniture", "btn_electronics", "btn_food", "btn_wash", "btn_toys", "btn_decoration", "btn_wallpapers", "btn_floor"];

    private var _objectsList:List;
    private var _buttons:Vector.<SimpleBtn>;

    public function InventoryBigPanelView()
    {
    }

    override public function init():void
    {
        _objectsList = getList("list_inventoryBigObjects");
        _objectsList.setLayoutType(ListLayoutTypes.TILED_ROWS);
        _objectsList.layout.paddingLeft = 7;
        _objectsList.layout.paddingTop = 8;
        _objectsList.layout.horizontalSpacing = 8;
        _objectsList.layout.verticalSpacing = 16;

        _buttons = new Vector.<SimpleBtn>();
        for each (var buttonName:String in BUTTON_NAMES)
        {
            _buttons.push(getButton(buttonName));
        }
    }

    public function displayItemsList(itemsList:Array):void
    {
        _objectsList.setData(itemsList);
    }

    public function highlightSelectedButton(buttonName:String):void
    {
        for each (var button:SimpleBtn in _buttons)
        {
            if (button.name == buttonName)
            {
                button.disable();
            }
            else
            {
                button.enable();
            }
        }
    }

}
}
