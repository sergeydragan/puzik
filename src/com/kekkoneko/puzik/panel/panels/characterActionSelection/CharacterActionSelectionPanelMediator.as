/**
 * Created by SeeD on 29/12/2014.
 */
package com.kekkoneko.puzik.panel.panels.characterActionSelection
{
import com.kekkoneko.core.composition.events.CompositionButtonPressedEvent;
import com.kekkoneko.core.composition.models.CompositionPropertiesModel;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.panel.PanelMediator;
import com.kekkoneko.core.panel.vos.ClosePanelVO;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.gameplay.characters.vos.CharacterActionVO;
import com.kekkoneko.puzik.gameplay.characters.vos.ProcessCharacterActionSelectedVO;
import com.kekkoneko.puzik.panel.constants.PuzikPanelNames;
import com.kekkoneko.puzik.panel.panels.characterActionSelection.vos.CharacterActionSelectionPanelPropertiesVO;

public class CharacterActionSelectionPanelMediator extends PanelMediator
{
    [Inject]
    public var compositionPropertiesModel:CompositionPropertiesModel;

    private var _properties:CharacterActionSelectionPanelPropertiesVO;

    public function CharacterActionSelectionPanelMediator()
    {
        super();
    }

    override public function init():void
    {
        _properties = compositionPropertiesModel.getPropertiesByCompositionName(PuzikPanelNames.CHARACTER_ACTION_SELECTION_PANEL) as CharacterActionSelectionPanelPropertiesVO;

        var actionNamesVector:Vector.<String> = new <String>[];
        for (var i:int = 0; i < _properties.actions.length; i++)
        {
            actionNamesVector.push(_properties.actions[i].interactionType);
        }
        (view as CharacterActionSelectionPanelView).setActionButtonsFrames(actionNamesVector);
        view.addEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);

        super.init();
    }

    private function onViewButtonPressed(e:CompositionButtonPressedEvent):void
    {
        var hidePanelVO:ClosePanelVO = new ClosePanelVO(PuzikPanelNames.CHARACTER_ACTION_SELECTION_PANEL);
        coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_PANEL, hidePanelVO);

        var actionVO:CharacterActionVO;
        for (var i:int = 0; i < _properties.actions.length; i++)
        {
            if (_properties.actions[i].interactionType == e.buttonName)
            {
                actionVO = _properties.actions[i];
                break;
            }
        }
        trace("Action selected: " + actionVO.interactionType);
        var processCharacterActionSelectedVO:ProcessCharacterActionSelectedVO = new ProcessCharacterActionSelectedVO();
        processCharacterActionSelectedVO.characterModel = _properties.characterModel;
        processCharacterActionSelectedVO.characterView = _properties.characterModel.view;
        processCharacterActionSelectedVO.characterActionVO = actionVO;
        coreEventDispatcher.broadcastMessage(PuzikCommands.PROCESS_CHARACTER_ACTION_SELECTED, processCharacterActionSelectedVO);
    }

    override public function destroy():void
    {
        view.removeEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
        super.destroy();
    }
}
}
