/**
 * Created by SeeD on 29/12/2014.
 */
package com.kekkoneko.puzik.panel.panels.characterActionSelection
{
import com.greensock.TweenLite;
import com.kekkoneko.core.panel.PanelView;

import flash.display.MovieClip;
import flash.events.MouseEvent;

public class CharacterActionSelectionPanelView extends PanelView
{
    private static const BUTTONS_AMOUNT:int = 4;
    private static const BUTTON_NAME_BASE:String = "actionIcon_";
    private static const APPEARANCE_DURATION:Number = 0.2;
    private static const APPEARANCE_DELAY:Number = 0.1;

    public function CharacterActionSelectionPanelView()
    {
        super();
    }

    override public function init():void
    {

    }

    public function setActionButtonsFrames(frameNames:Vector.<String>):void
    {
        var i:int;
        for (i = 0; i < frameNames.length; i++)
        {
            if (i > (BUTTONS_AMOUNT - 1)) return;  // todo: пока что так из-за того, что кнопки не генерятся автоматически

            (getChildByName(BUTTON_NAME_BASE + i.toString()) as MovieClip).gotoAndStop(frameNames[i]);
        }

        for (i = 0; i < BUTTONS_AMOUNT; i++)
        {
            var currentButton:MovieClip = getChildByName(BUTTON_NAME_BASE + i.toString()) as MovieClip;
            currentButton.alpha = 0;
            TweenLite.to(currentButton, APPEARANCE_DURATION, {alpha:1, delay:APPEARANCE_DELAY * i});
            if (i > frameNames.length - 1)
            {
                currentButton.stop();
                currentButton.visible = false;
            }
            else
            {
                currentButton.addEventListener(MouseEvent.CLICK, onActionButtonClick);
                currentButton.buttonMode = true;
            }
        }
    }

    private function onActionButtonClick(e:MouseEvent):void
    {
        dispatchButtonPressedEvent(MovieClip(e.currentTarget).currentFrameLabel);
    }
}
}
