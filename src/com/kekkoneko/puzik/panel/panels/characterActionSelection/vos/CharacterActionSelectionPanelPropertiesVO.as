/**
 * Created by SeeD on 29/12/2014.
 */
package com.kekkoneko.puzik.panel.panels.characterActionSelection.vos
{
import com.kekkoneko.core.composition.vos.CompositionPropertiesVO;
import com.kekkoneko.puzik.gameplay.characters.model.CharacterModel;
import com.kekkoneko.puzik.gameplay.characters.vos.CharacterActionVO;

public class CharacterActionSelectionPanelPropertiesVO extends CompositionPropertiesVO
{
    public var actions:Vector.<CharacterActionVO>;
    public var characterModel:CharacterModel;

    public function CharacterActionSelectionPanelPropertiesVO()
    {
        super();
    }
}
}
