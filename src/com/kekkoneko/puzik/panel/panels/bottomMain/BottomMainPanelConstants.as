/**
 * Created by SeeD on 07/09/2014.
 */
package com.kekkoneko.puzik.panel.panels.bottomMain
{
public class BottomMainPanelConstants
{
    public static const BTN_MAP:String = "btn_map";
    public static const BTN_INVENTORY:String = "btn_inventory";
    public static const BTN_FRIENDS:String = "btn_friends";
    public static const BTN_QUESTS:String = "btn_quests";

    public static const CMP_MONEY_INDICATOR:String = "cmp_moneyIndicator";
    public static const CMP_HC_INDICATOR:String = "cmp_donateIndicator";

    public function BottomMainPanelConstants()
    {
    }
}
}
