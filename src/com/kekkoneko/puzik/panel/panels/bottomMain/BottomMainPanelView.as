/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.panel.panels.bottomMain
{
import com.kekkoneko.core.panel.PanelView;

public class BottomMainPanelView extends PanelView
{
    public function BottomMainPanelView()
    {
    }

    override public function init():void
    {
        getButton(BottomMainPanelConstants.BTN_FRIENDS).disable();
    }

    public function setMoney(money:int, hc:int):void
    {
        getComposition(BottomMainPanelConstants.CMP_MONEY_INDICATOR).getLabel("txt_amount").setText(money.toString())
        getComposition(BottomMainPanelConstants.CMP_HC_INDICATOR).getLabel("txt_amount").setText(hc.toString())
    }

    public function setEnergy(energy:int):void
    {
        getProgressBar("progressbar_energy").setCurrentValue(energy);
    }

    public function setMaxEnergy(maxEnergy:int):void
    {
        getProgressBar("progressbar_energy").setMaxValue(maxEnergy);
    }

    public function setXP(xp:int):void
    {
        getProgressBar("progressbar_xp").setCurrentValue(xp);
    }

    public function setMaxXP(maxXP:int):void
    {
        getProgressBar("progressbar_xp").setMaxValue(maxXP);
    }
}
}
