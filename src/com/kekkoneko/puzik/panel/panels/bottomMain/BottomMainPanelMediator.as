/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.panel.panels.bottomMain
{
import com.kekkoneko.core.composition.events.CompositionButtonPressedEvent;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.dialog.vos.ShowDialogPropertiesVO;
import com.kekkoneko.core.eventdispatcher.CoreEvent;
import com.kekkoneko.core.panel.PanelMediator;
import com.kekkoneko.core.panel.constants.PanelPositions;
import com.kekkoneko.core.panel.vos.ShowPanelPropertiesVO;
import com.kekkoneko.puzik.controller.notifications.PlayerEnergyUpdatedNotification;
import com.kekkoneko.puzik.controller.notifications.PlayerMoneyUpdatedNotification;
import com.kekkoneko.puzik.controller.notifications.PlayerXPUpdatedNotification;
import com.kekkoneko.puzik.dialog.constants.PuzikDialogNames;
import com.kekkoneko.puzik.gameplay.player.helpers.PlayerEnergyHelper;
import com.kekkoneko.puzik.gameplay.player.helpers.PlayerXPHelper;
import com.kekkoneko.puzik.gameplay.player.model.PlayersModel;
import com.kekkoneko.puzik.panel.constants.PuzikPanelNames;

public class BottomMainPanelMediator extends PanelMediator
{
    [Inject]
    public var playersModel:PlayersModel;

    public function BottomMainPanelMediator()
    {
    }

    override public function init():void
    {
        super.init();

        coreEventDispatcher.addEventListener(PlayerMoneyUpdatedNotification.PLAYER_MONEY_UPDATED, onPlayerMoneyUpdated);
        coreEventDispatcher.addEventListener(PlayerEnergyUpdatedNotification.PLAYER_ENERGY_UPDATED, onPlayerEnergyUpdated);
        coreEventDispatcher.addEventListener(PlayerXPUpdatedNotification.PLAYER_XP_UPDATED, onPlayerXPUpdated);

        view.addEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
        (view as BottomMainPanelView).setMoney(playersModel.getPlayer().money, playersModel.getPlayer().hc);
        (view as BottomMainPanelView).setMaxEnergy(PlayerEnergyHelper.getMaxEnergyByLevel(playersModel.getPlayer().level));
        (view as BottomMainPanelView).setEnergy(playersModel.getPlayer().energy);
        (view as BottomMainPanelView).setMaxXP(PlayerXPHelper.getMaxXPByLevel(playersModel.getPlayer().level));
        (view as BottomMainPanelView).setXP(playersModel.getPlayer().xp);
    }

    private function onViewButtonPressed(e:CompositionButtonPressedEvent):void
    {
        switch (e.buttonName)
        {
            case BottomMainPanelConstants.BTN_MAP:
                coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_DIALOG, new ShowDialogPropertiesVO(PuzikDialogNames.WORLD_MAP));
                break;
            case BottomMainPanelConstants.BTN_INVENTORY:
                coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_PANEL, new ShowPanelPropertiesVO(PuzikPanelNames.INVENTORY_BIG_PANEL, PanelPositions.CENTER));
                break;
            case BottomMainPanelConstants.BTN_FRIENDS:
                trace("Friends selected");
                break;
            case BottomMainPanelConstants.BTN_QUESTS:
                coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_DIALOG, new ShowDialogPropertiesVO(PuzikDialogNames.QUEST_LIST));
                break;
            default:
                trace("Unknown button pressed on view: " + e.buttonName);
        }
    }

    private function onPlayerMoneyUpdated(event:CoreEvent):void
    {
        (view as BottomMainPanelView).setMoney(playersModel.getPlayer().money, playersModel.getPlayer().hc);
    }

    private function onPlayerXPUpdated(event:CoreEvent):void
    {
        (view as BottomMainPanelView).setMaxXP(PlayerXPHelper.getMaxXPByLevel(playersModel.getPlayer().level));
        (view as BottomMainPanelView).setXP(playersModel.getPlayer().xp);
    }

    private function onPlayerEnergyUpdated(event:CoreEvent):void
    {
        (view as BottomMainPanelView).setMaxEnergy(PlayerEnergyHelper.getMaxEnergyByLevel(playersModel.getPlayer().level));
        (view as BottomMainPanelView).setEnergy(playersModel.getPlayer().energy);
    }

    override public function destroy():void
    {
        coreEventDispatcher.removeEventListener(PlayerMoneyUpdatedNotification.PLAYER_MONEY_UPDATED, onPlayerMoneyUpdated);
        coreEventDispatcher.removeEventListener(PlayerXPUpdatedNotification.PLAYER_XP_UPDATED, onPlayerXPUpdated);
        coreEventDispatcher.removeEventListener(PlayerEnergyUpdatedNotification.PLAYER_ENERGY_UPDATED, onPlayerEnergyUpdated);

        view.removeEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
        super.destroy();
    }
}
}
