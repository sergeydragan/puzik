/**
 * Created by SeeD on 29/12/2014.
 */
package com.kekkoneko.puzik.panel.panels.ordersQueue.vos
{
import com.kekkoneko.core.composition.vos.CompositionPropertiesVO;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectActionVO;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;

public class OrdersQueuePanelPropertiesVO extends CompositionPropertiesVO
{
    public var actions:Vector.<InventoryObjectActionVO>;
    public var inventoryObjectVO:InventoryObjectVO;

    public function OrdersQueuePanelPropertiesVO()
    {
        super();
    }
}
}
