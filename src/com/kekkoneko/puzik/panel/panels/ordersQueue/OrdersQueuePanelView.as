/**
 * Created by SeeD on 29/12/2014.
 */
package com.kekkoneko.puzik.panel.panels.ordersQueue
{
import com.kekkoneko.core.composition.elements.list.List;
import com.kekkoneko.core.composition.elements.list.layouts.ListLayoutTypes;
import com.kekkoneko.core.panel.PanelView;

public class OrdersQueuePanelView extends PanelView
{
    private var _list:List;

    public function OrdersQueuePanelView()
    {
        super();
    }

    override public function init():void
    {
        _list = getList("list_ordersQueue");
        _list.setLayoutType(ListLayoutTypes.HORIZONTAL);
        _list.layout.horizontalSpacing = 10;
        _list.layout.paddingLeft = 10;
    }

    public function setList(ordersList:Array):void
    {
        _list.setData(ordersList);
    }
}
}
