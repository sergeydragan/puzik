/**
 * Created by SeeD on 29/12/2014.
 */
package com.kekkoneko.puzik.panel.panels.ordersQueue
{
import com.kekkoneko.core.composition.models.CompositionPropertiesModel;
import com.kekkoneko.core.eventdispatcher.CoreEvent;
import com.kekkoneko.core.panel.PanelMediator;
import com.kekkoneko.puzik.controller.notifications.CharacterSwitchedNotification;
import com.kekkoneko.puzik.controller.notifications.OrderAddedNotification;
import com.kekkoneko.puzik.controller.notifications.OrderFinishedNotification;
import com.kekkoneko.puzik.dialog.renderers.vos.OrdersQueueListItemRendererDataVO;
import com.kekkoneko.puzik.gameplay.characters.model.CharactersModel;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.OrderVO;
import com.kekkoneko.puzik.panel.panels.ordersQueue.vos.OrdersQueuePanelPropertiesVO;

public class OrdersQueuePanelMediator extends PanelMediator
{
    [Inject]
    public var compositionPropertiesModel:CompositionPropertiesModel;

    [Inject]
    public var charactersModel:CharactersModel;

    private var _properties:OrdersQueuePanelPropertiesVO;
    private var _ordersList:Array;

    public function OrdersQueuePanelMediator()
    {
        super();
    }

    override public function init():void
    {
//        _properties = compositionPropertiesModel.getPropertiesByCompositionName(PuzikPanelNames.ORDERS_QUEUE_PANEL) as OrdersQueuePanelPropertiesVO;
        _ordersList = [];
        coreEventDispatcher.addEventListener(OrderAddedNotification.ORDER_ADDED, onOrderAddedNotification);
        coreEventDispatcher.addEventListener(OrderFinishedNotification.ORDER_FINISHED, onOrderFinishedNotification);
        coreEventDispatcher.addEventListener(CharacterSwitchedNotification.CHARACTER_SWITCHED, onCharacterSwitchedNotification);
        super.init();
    }

    private function onCharacterSwitchedNotification(event:CoreEvent):void
    {
        showOrdersList();
    }

    private function onOrderAddedNotification(event:CoreEvent):void
    {
        var orderAddedNotification:OrderAddedNotification = event.body as OrderAddedNotification;
        if (charactersModel.getActiveCharacter() == orderAddedNotification.orderVO.owner)
        {
            showOrdersList();
        }
    }

    private function showOrdersList():void
    {
        var ordersList:Array = [];
        for (var i:int = 0; i < charactersModel.getActiveCharacter().ordersVector.length; i++)
        {
            var orderVO:OrderVO = charactersModel.getActiveCharacter().ordersVector[i];
            if (!orderVO.isIdle)
            {
                ordersList.push(new OrdersQueueListItemRendererDataVO(orderVO));
            }
        }
        (view as OrdersQueuePanelView).setList(ordersList);
    }

    private function onOrderFinishedNotification(event:CoreEvent):void
    {
        var orderFinishedNotification:OrderFinishedNotification = event.body as OrderFinishedNotification;
        var orderVO:OrderVO = orderFinishedNotification.orderVO;
        if (charactersModel.getActiveCharacter() == orderVO.owner)
        {
            showOrdersList();
        }
    }

    override public function destroy():void
    {
        coreEventDispatcher.removeEventListener(OrderAddedNotification.ORDER_ADDED, onOrderAddedNotification);
        coreEventDispatcher.removeEventListener(OrderFinishedNotification.ORDER_FINISHED, onOrderFinishedNotification);
        coreEventDispatcher.removeEventListener(CharacterSwitchedNotification.CHARACTER_SWITCHED, onCharacterSwitchedNotification);
        super.destroy();
    }
}
}
