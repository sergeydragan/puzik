/**
 * Created by SeeD on 29/12/2014.
 */
package com.kekkoneko.puzik.panel.panels.debugInfo
{
import com.kekkoneko.core.panel.PanelView;

public class DebugInfoPanelView extends PanelView
{
    public function DebugInfoPanelView()
    {
        super();
    }

    override public function init():void
    {

    }

    public function setInfoText(text:String):void
    {
        getLabel("txt_info").setText(text);
    }
}
}
