/**
 * Created by SeeD on 29/12/2014.
 */
package com.kekkoneko.puzik.panel.panels.debugInfo
{
import com.kekkoneko.core.debug.DebugVariables;
import com.kekkoneko.core.panel.PanelMediator;

public class DebugInfoPanelMediator extends PanelMediator
{
    public function DebugInfoPanelMediator()
    {
        super();
    }

    override public function init():void
    {
        var text:String = "Puzik rework\nBuild " + DebugVariables.buildNumber.toString();
        (view as DebugInfoPanelView).setInfoText(text);

        super.init();
    }

    override public function destroy():void
    {
        super.destroy();
    }
}
}
