/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.panel.constants
{
public class PuzikPanelNames
{
    public static const BOTTOM_MAIN_PANEL:String = "BottomMainPanel";
    public static const ORDERS_QUEUE_PANEL:String = "OrdersQueuePanel";
    public static const ACTION_SELECTION_PANEL:String = "ActionSelectionPanel";
    public static const INGREDIENT_SELECTION_PANEL:String = "IngredientSelectionPanel";
    public static const DEBUG_INFO:String = "DebugInfoPanel";
    public static const INVENTORY_OBJECT_EDIT_PANEL:String = "InventoryObjectEditPanel";
    public static const INVENTORY_BIG_PANEL:String = "InventoryPanel";
    public static const NEEDS_PANEL:String = "NeedsPanel";
    public static const CHARACTER_ACTION_SELECTION_PANEL:String = "CharacterActionSelectionPanel";
    public static const ACTIVE_CHARACTER_PANEL:String = "ActiveCharacterPanel";

    public function PuzikPanelNames()
    {
    }
}
}
