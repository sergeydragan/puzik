/**
 * Created by SeeD on 03/09/2014.
 */
package com.kekkoneko.puzik.panel.configs
{
import com.kekkoneko.core.eventdispatcher.CoreEventDispatcher;
import com.kekkoneko.core.panel.PanelDisplayModel;
import com.kekkoneko.core.panel.PanelModel;
import com.kekkoneko.core.screen.DisplayLayersModel;
import com.kekkoneko.puzik.display.constants.PuzikLayers;
import com.kekkoneko.puzik.panel.constants.PuzikPanelNames;
import com.kekkoneko.puzik.panel.panels.actionSelection.ActionSelectionPanelMediator;
import com.kekkoneko.puzik.panel.panels.actionSelection.ActionSelectionPanelView;
import com.kekkoneko.puzik.panel.panels.activeCharacter.ActiveCharacterPanelMediator;
import com.kekkoneko.puzik.panel.panels.activeCharacter.ActiveCharacterPanelView;
import com.kekkoneko.puzik.panel.panels.bottomMain.BottomMainPanelMediator;
import com.kekkoneko.puzik.panel.panels.bottomMain.BottomMainPanelView;
import com.kekkoneko.puzik.panel.panels.characterActionSelection.CharacterActionSelectionPanelMediator;
import com.kekkoneko.puzik.panel.panels.characterActionSelection.CharacterActionSelectionPanelView;
import com.kekkoneko.puzik.panel.panels.debugInfo.DebugInfoPanelMediator;
import com.kekkoneko.puzik.panel.panels.debugInfo.DebugInfoPanelView;
import com.kekkoneko.puzik.panel.panels.ingredientSelection.IngredientSelectionPanelMediator;
import com.kekkoneko.puzik.panel.panels.ingredientSelection.IngredientSelectionPanelView;
import com.kekkoneko.puzik.panel.panels.inventoryBig.InventoryBigPanelMediator;
import com.kekkoneko.puzik.panel.panels.inventoryBig.InventoryBigPanelView;
import com.kekkoneko.puzik.panel.panels.inventoryObjectEdit.InventoryObjectEditPanelMediator;
import com.kekkoneko.puzik.panel.panels.inventoryObjectEdit.InventoryObjectEditPanelView;
import com.kekkoneko.puzik.panel.panels.needs.NeedsPanelMediator;
import com.kekkoneko.puzik.panel.panels.needs.NeedsPanelView;
import com.kekkoneko.puzik.panel.panels.ordersQueue.OrdersQueuePanelMediator;
import com.kekkoneko.puzik.panel.panels.ordersQueue.OrdersQueuePanelView;

import flash.display.DisplayObjectContainer;

public class PuzikPanelConfig
{
    [Inject]
    public var panelModel:PanelModel;

    [Inject]
    public var coreEventDispatcher:CoreEventDispatcher;

    [Inject]
    public var panelDisplayModel:PanelDisplayModel;

    [Inject]
    public var layersDisplayModel:DisplayLayersModel;

    public function PuzikPanelConfig()
    {

    }

    public function init():void
    {
        var container:DisplayObjectContainer = layersDisplayModel.getLayer(PuzikLayers.PANELS);
        panelDisplayModel.setContainer(container);

        panelModel.registerPanel(PuzikPanelNames.BOTTOM_MAIN_PANEL, BottomMainPanelView, BottomMainPanelMediator, Panel_BottomMain);
        panelModel.registerPanel(PuzikPanelNames.ACTION_SELECTION_PANEL, ActionSelectionPanelView, ActionSelectionPanelMediator, Panel_ActionSelection);
        panelModel.registerPanel(PuzikPanelNames.CHARACTER_ACTION_SELECTION_PANEL, CharacterActionSelectionPanelView, CharacterActionSelectionPanelMediator, Panel_CharacterActionSelection);
        panelModel.registerPanel(PuzikPanelNames.INGREDIENT_SELECTION_PANEL, IngredientSelectionPanelView, IngredientSelectionPanelMediator, Panel_IngredientSelection);
        panelModel.registerPanel(PuzikPanelNames.ORDERS_QUEUE_PANEL, OrdersQueuePanelView, OrdersQueuePanelMediator, Panel_OrdersQueue);
        panelModel.registerPanel(PuzikPanelNames.DEBUG_INFO, DebugInfoPanelView, DebugInfoPanelMediator, Panel_DebugInfo);
        panelModel.registerPanel(PuzikPanelNames.INVENTORY_OBJECT_EDIT_PANEL, InventoryObjectEditPanelView, InventoryObjectEditPanelMediator, Panel_InventoryObjectEdit);
        panelModel.registerPanel(PuzikPanelNames.INVENTORY_BIG_PANEL, InventoryBigPanelView, InventoryBigPanelMediator, Panel_InventoryBig);
        panelModel.registerPanel(PuzikPanelNames.NEEDS_PANEL, NeedsPanelView, NeedsPanelMediator, Panel_Needs);
        panelModel.registerPanel(PuzikPanelNames.ACTIVE_CHARACTER_PANEL, ActiveCharacterPanelView, ActiveCharacterPanelMediator, Panel_ActiveCharacter);
    }
}
}

