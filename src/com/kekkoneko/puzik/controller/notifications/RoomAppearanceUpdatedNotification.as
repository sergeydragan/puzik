package com.kekkoneko.puzik.controller.notifications
{
/**
 * @author Ilya Malanin (flashdeveloper[at]pastila.org)
 */
public class RoomAppearanceUpdatedNotification
{
    public static const ROOM_APPEARANCE_UPDATED:String = "RoomAppearanceUpdated";

    public function RoomAppearanceUpdatedNotification()
    {
        super();
    }

}
}
