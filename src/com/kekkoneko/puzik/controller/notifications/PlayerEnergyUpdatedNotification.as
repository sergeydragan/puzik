/**
 * Created by SeeD on 13/01/2015.
 */
package com.kekkoneko.puzik.controller.notifications
{
public class PlayerEnergyUpdatedNotification
{
    public var newEnergy:int = 0;

    public static const PLAYER_ENERGY_UPDATED:String = "PlayerEnergyUpdated";

    public function PlayerEnergyUpdatedNotification(newEnergy:int)
    {
        this.newEnergy = newEnergy;
    }
}
}
