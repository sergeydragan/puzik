/**
 * Created by SeeD on 07/02/2015.
 */
package com.kekkoneko.puzik.controller.notifications
{
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;

public class EditInventoryObjectNotification
{
    public var inventoryObjectVO:InventoryObjectVO;

    public static const EDIT_INVENTORY_OBJECT:String = "EditInventoryObject";

    public function EditInventoryObjectNotification(inventoryObjectVO:InventoryObjectVO)
    {
        this.inventoryObjectVO = inventoryObjectVO;
    }
}
}
