/**
 * Created by SeeD on 13/01/2015.
 */
package com.kekkoneko.puzik.controller.notifications
{
public class PlayerMoneyUpdatedNotification
{
    public var newMoney:int = 0;
    public var newPremium:int = 0;

    public static const PLAYER_MONEY_UPDATED:String = "PlayerMoneyUpdated";

    public function PlayerMoneyUpdatedNotification(newMoney:int, newPremium:int)
    {
        this.newMoney = newMoney;
        this.newPremium = newPremium;
    }
}
}
