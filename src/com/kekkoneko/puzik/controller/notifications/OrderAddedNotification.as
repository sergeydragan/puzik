/**
 * Created by SeeD on 08/02/2015.
 */
package com.kekkoneko.puzik.controller.notifications
{
import com.kekkoneko.puzik.gameplay.characters.orders.vos.OrderVO;

public class OrderAddedNotification
{
    public var orderVO:OrderVO;

    public static const ORDER_ADDED:String = "OrderAddedNotification";

    public function OrderAddedNotification(orderVO:OrderVO)
    {
        this.orderVO = orderVO;
    }
}
}
