/**
 * Created by SeeD on 08/02/2015.
 */
package com.kekkoneko.puzik.controller.notifications
{
import com.kekkoneko.puzik.gameplay.characters.model.CharacterModel;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;

public class IngredientsUsedNotification
{
    public var ingredients:Vector.<InventoryObjectVO>;
    public var subject:CharacterModel;

    public static const INGREDIENTS_USED:String = "IngredientsUsedNotification";

    public function IngredientsUsedNotification(ingredients:Vector.<InventoryObjectVO>, subject:CharacterModel = null)
    {
        this.ingredients = ingredients;
        this.subject = subject;
    }
}
}
