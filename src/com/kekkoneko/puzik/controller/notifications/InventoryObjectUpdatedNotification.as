/**
 * Created by SeeD on 13/01/2015.
 */
package com.kekkoneko.puzik.controller.notifications
{
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;

public class InventoryObjectUpdatedNotification
{
    public var inventoryObjectVO:InventoryObjectVO;
    public var positionHasChanged:Boolean;
    public var inWorldHasChanged:Boolean;
    public var stateHasChanged:Boolean;

    public static const INVENTORY_OBJECT_UPDATED:String = "InventoryObjectUpdated";

    public function InventoryObjectUpdatedNotification(inventoryObjectVO:InventoryObjectVO, positionHasChanged:Boolean, inWorldHasChanged:Boolean, stateHasChanged:Boolean)
    {
        this.inventoryObjectVO = inventoryObjectVO;
        this.positionHasChanged = positionHasChanged;
        this.inWorldHasChanged = inWorldHasChanged;
        this.stateHasChanged = stateHasChanged;
    }
}
}
