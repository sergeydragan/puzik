/**
 * Created by SeeD on 08/02/2015.
 */
package com.kekkoneko.puzik.controller.notifications
{
public class InventoryObjectSoldNotification
{
    public var price:int;

    public static const INVENTORY_OBJECT_SOLD:String = "InventoryObjectSold";

    public function InventoryObjectSoldNotification(price:int = -1)
    {
        this.price = price;
    }
}
}
