/**
 * Created by SeeD on 08/02/2015.
 */
package com.kekkoneko.puzik.controller.notifications
{
import com.kekkoneko.puzik.gameplay.characters.model.CharacterModel;

public class CharacterSwitchedNotification
{
    public var characterModel:CharacterModel;

    public static const CHARACTER_SWITCHED:String = "CharacterSwitchedNotification";

    public function CharacterSwitchedNotification(characterModel:CharacterModel)
    {
        this.characterModel = characterModel;
    }
}
}
