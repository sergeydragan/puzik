/**
 * Created by SeeD on 13/01/2015.
 */
package com.kekkoneko.puzik.controller.notifications
{
public class PlayerXPUpdatedNotification
{
    public var newXP:int = 0;

    public static const PLAYER_XP_UPDATED:String = "PlayerXPUpdated";

    public function PlayerXPUpdatedNotification(newXP:int)
    {
        this.newXP = newXP;
    }
}
}
