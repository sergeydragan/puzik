/**
 * Created by SeeD on 13/01/2015.
 */
package com.kekkoneko.puzik.controller.notifications
{
public class PlayerLevelUpdatedNotification
{
    public var newLevel:int = 0;

    public static const PLAYER_LEVEL_UPDATED:String = "PlayerLevelUpdated";

    public function PlayerLevelUpdatedNotification(newLevel:int)
    {
        this.newLevel = newLevel;
    }
}
}
