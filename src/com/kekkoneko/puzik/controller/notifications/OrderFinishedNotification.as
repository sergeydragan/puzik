/**
 * Created by SeeD on 08/02/2015.
 */
package com.kekkoneko.puzik.controller.notifications
{
import com.kekkoneko.puzik.gameplay.characters.orders.vos.OrderVO;

public class OrderFinishedNotification
{
    public var orderVO:OrderVO;

    public static const ORDER_FINISHED:String = "OrderFinishedNotification";

    public function OrderFinishedNotification(orderVO:OrderVO)
    {
        this.orderVO = orderVO;
    }
}
}
