/**
 * Created by SeeD on 28/12/2014.
 */
package com.kekkoneko.puzik.controller.constants
{
public class PuzikCommands
{
    public static const PARSE_PLAYERS_JSON:String = "ParsePlayersJSON";
    public static const PARSE_GAME_OBJECTS_JSON:String = "ParseGameObjectsJSON";
    public static const PARSE_INVENTORY_JSON:String = "ParseInventoryJSON";
    public static const PARSE_QUESTS_JSON:String = "ParseQuestsJSON";
    public static const PARSE_QUEST_SUBTASKS_JSON:String = "ParseQuestSubtasksJSON";

    public static const PROCESS_INVENTORY_OBJECT_SELECTION:String = "ProcessInventoryObjectSelection";
    public static const PROCESS_INVENTORY_OBJECT_ACTION_SELECTED:String = "ProcessInventoryObjectActionSelected";
    public static const PROCESS_CHARACTER_SELECTION:String = "ProcessCharacterSelection";
    public static const PROCESS_CHARACTER_ACTION_SELECTED:String = "ProcessCharacterActionSelected";
    public static const ADD_ORDER_TO_ACTIVE_CHARACTER:String = "AddOrderToActiveCharacter";
    public static const ADD_CHARACTER:String = "AddCharacter";
    public static const SWITCH_ACTIVE_CHARACTER:String = "SwitchActiveCharacter";
    public static const UPDATE_PLAYER_MONEY:String = "UpdatePlayerMoney";
    public static const SWITCH_LOCATION:String = "SwitchLocation";
    public static const SUGGEST_BUY_OBJECT:String = "SuggestBuyObject";
    public static const SUGGEST_SELL_OBJECT:String = "SuggestSellObject";
    public static const INITIATE_BUY_OBJECT:String = "InitiateBuyObject";
    public static const INITIATE_SELL_OBJECT:String = "InitiateSellObject";
    public static const SHOW_OVERLAY:String = "ShowOverlay";
    public static const REMOVE_OVERLAY:String = "RemoveOverlay";
    public static const POSTSTART_ACTIONS:String = "PostStartActions";
    public static const ADD_XP:String = "AddXP";
    public static const LEVEL_UP:String = "LevelUp";
    public static const SET_INVENTORY_OBJECT:String = "SetInventoryObject";
    public static const SET_ROOM_APPEARANCE:String = "SetRoomAppearance";
    public static const ADD_QUEST_PROGRESS:String = "AddQuestProgress";
    public static const INGREDIENTS_USED:String = "IngredientsUsed";
    public static const SWITCH_CURRENT_ROOM:String = "SwitchCurrentRoom";


    public function PuzikCommands()
    {
    }
}
}
