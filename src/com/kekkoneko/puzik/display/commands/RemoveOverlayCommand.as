/**
 * Created by SeeD on 27/01/2015.
 */
package com.kekkoneko.puzik.display.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.core.screen.DisplayLayersModel;
import com.kekkoneko.puzik.display.constants.OverlayConstants;
import com.kekkoneko.puzik.display.constants.PuzikLayers;
import com.kekkoneko.puzik.display.vos.RemoveOverlayVO;

import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;

public class RemoveOverlayCommand extends Command
{
    [Inject]
    public var displayLayersModel:DisplayLayersModel;

    public function RemoveOverlayCommand()
    {
    }

    override public function execute():void
    {
        var removeOverlayVO:RemoveOverlayVO = _payload as RemoveOverlayVO;

        if (removeOverlayVO.removeEverythingFromOverlayLayer)
        {
            var overlayLayer:DisplayObjectContainer = displayLayersModel.getLayer(PuzikLayers.OVERLAY);
            overlayLayer.removeChildren(0, overlayLayer.numChildren - 1);
        }
        else
        {
            var overlay:DisplayObject = displayLayersModel.getLayer(PuzikLayers.OVERLAY).getChildByName(OverlayConstants.OVERLAY_SPRITE_NAME);
            if (overlay)
            {
                displayLayersModel.getLayer(PuzikLayers.OVERLAY).removeChild(overlay);
            }
        }
    }
}
}
