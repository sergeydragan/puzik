/**
 * Created by SeeD on 27/01/2015.
 */
package com.kekkoneko.puzik.display.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.core.screen.DisplayLayersModel;
import com.kekkoneko.core.screen.Screen;
import com.kekkoneko.puzik.display.constants.OverlayConstants;
import com.kekkoneko.puzik.display.constants.PuzikLayers;
import com.kekkoneko.puzik.display.vos.ShowOverlayVO;

import flash.display.Sprite;

public class ShowOverlayCommand extends Command
{
    [Inject]
    public var displayLayersModel:DisplayLayersModel;

    public function ShowOverlayCommand()
    {
    }

    override public function execute():void
    {
        var showOverlayVO:ShowOverlayVO = _payload as ShowOverlayVO;

        if (displayLayersModel.getLayer(PuzikLayers.OVERLAY).getChildByName(OverlayConstants.OVERLAY_SPRITE_NAME))
        {
            // there is overlay in place already, do nothing
            return;
        }

        // todo: add some kind of "loading" animation
        var overlay:Sprite = new Sprite();
        overlay.graphics.beginFill(0x000000, 0.8);
        overlay.graphics.drawRect(0, 0, Screen.width, Screen.height);
        overlay.graphics.endFill();
        overlay.name = OverlayConstants.OVERLAY_SPRITE_NAME;

        displayLayersModel.getLayer(PuzikLayers.OVERLAY).addChild(overlay);
    }
}
}
