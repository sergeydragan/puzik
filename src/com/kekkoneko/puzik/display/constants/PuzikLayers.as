/**
 * Created by SeeD on 31/12/2014.
 */
package com.kekkoneko.puzik.display.constants
{
public class PuzikLayers
{
    public static const GAMEPLAY:String = "Gameplay";
    public static const PANELS:String = "Panels";
    public static const DIALOGS:String = "Dialogs";
    public static const OVERLAY:String = "Overlay";

    public function PuzikLayers()
    {
    }
}
}
