/**
 * Created by SeeD on 27/01/2015.
 */
package com.kekkoneko.puzik.display.vos
{
public class RemoveOverlayVO
{
    public var removeEverythingFromOverlayLayer:Boolean;

    public function RemoveOverlayVO(removeEverythingFromOverlayLayer:Boolean = false)
    {
        this.removeEverythingFromOverlayLayer = removeEverythingFromOverlayLayer;
    }
}
}
