/**
 * Created by SeeD on 31/12/2014.
 */
package com.kekkoneko.puzik.display.config
{
import com.kekkoneko.core.screen.DisplayLayersModel;
import com.kekkoneko.puzik.display.constants.PuzikLayers;

import flash.display.DisplayObjectContainer;

public class PuzikDisplayLayersConfig
{
    [Inject]
    public var displayLayersModel:DisplayLayersModel;

    public function PuzikDisplayLayersConfig()
    {
    }

    public function init(viewContainer:DisplayObjectContainer):void
    {
        displayLayersModel.setViewContainer(viewContainer);
        displayLayersModel.addLayer(PuzikLayers.GAMEPLAY);
        displayLayersModel.addLayer(PuzikLayers.PANELS);
        displayLayersModel.addLayer(PuzikLayers.DIALOGS);
        displayLayersModel.addLayer(PuzikLayers.OVERLAY);
    }
}
}
