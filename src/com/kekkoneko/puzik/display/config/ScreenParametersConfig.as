/**
 * Created by SeeD on 31/12/2014.
 */
package com.kekkoneko.puzik.display.config
{
import com.kekkoneko.core.screen.Screen;

public class ScreenParametersConfig
{
    public function ScreenParametersConfig()
    {
    }

    public function init():void
    {
        Screen.width = 800;
        Screen.height = 600;
    }
}
}
