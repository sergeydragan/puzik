/**
 * Created by SeeD on 08/09/2014.
 */
package com.kekkoneko.puzik.config
{
import com.kekkoneko.core.controller.CommandExecutor;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.display.commands.RemoveOverlayCommand;
import com.kekkoneko.puzik.display.commands.ShowOverlayCommand;
import com.kekkoneko.puzik.gameplay.characters.commands.AddCharacterCommand;
import com.kekkoneko.puzik.gameplay.characters.commands.ProcessCharacterActionSelectedCommand;
import com.kekkoneko.puzik.gameplay.characters.commands.ProcessCharacterSelectionCommand;
import com.kekkoneko.puzik.gameplay.characters.commands.SwitchActiveCharacterCommand;
import com.kekkoneko.puzik.gameplay.characters.orders.commands.AddOrderToActiveCharacterCommand;
import com.kekkoneko.puzik.gameplay.locations.commands.SwitchLocationCommand;
import com.kekkoneko.puzik.gameplay.objects.commands.IngredientsUsedCommand;
import com.kekkoneko.puzik.gameplay.objects.commands.ParseGameObjectsJSONCommand;
import com.kekkoneko.puzik.gameplay.objects.commands.ParseInventoryJSONCommand;
import com.kekkoneko.puzik.gameplay.objects.commands.ProcessInventoryObjectActionSelectedCommand;
import com.kekkoneko.puzik.gameplay.objects.commands.ProcessInventoryObjectSelectionCommand;
import com.kekkoneko.puzik.gameplay.objects.commands.SetInventoryObjectCommand;
import com.kekkoneko.puzik.gameplay.objects.commands.SetRoomAppearanceCommand;
import com.kekkoneko.puzik.gameplay.player.commands.AddXPCommand;
import com.kekkoneko.puzik.gameplay.player.commands.LevelUpCommand;
import com.kekkoneko.puzik.gameplay.player.commands.ParsePlayersJSONCommand;
import com.kekkoneko.puzik.gameplay.player.commands.PostStartActionsCommand;
import com.kekkoneko.puzik.gameplay.player.commands.UpdatePlayerMoneyCommand;
import com.kekkoneko.puzik.gameplay.quests.commands.AddQuestProgressCommand;
import com.kekkoneko.puzik.gameplay.quests.commands.ParseQuestSubtasksJSONCommand;
import com.kekkoneko.puzik.gameplay.quests.commands.ParseQuestsJSONCommand;
import com.kekkoneko.puzik.gameplay.room.commands.SwitchCurrentRoomCommand;
import com.kekkoneko.puzik.gameplay.shop.commands.InitiateBuyObjectCommand;
import com.kekkoneko.puzik.gameplay.shop.commands.InitiateSellObjectCommand;
import com.kekkoneko.puzik.gameplay.shop.commands.SuggestBuyObjectCommand;
import com.kekkoneko.puzik.gameplay.shop.commands.SuggestSellObjectCommand;

public class PuzikCommandConfig
{
    [Inject]
    public var commandExecutor:CommandExecutor;

    public function PuzikCommandConfig()
    {
    }

    public function init():void
    {
        commandExecutor.registerCommand(PuzikCommands.PROCESS_INVENTORY_OBJECT_SELECTION, ProcessInventoryObjectSelectionCommand);
        commandExecutor.registerCommand(PuzikCommands.PROCESS_INVENTORY_OBJECT_ACTION_SELECTED, ProcessInventoryObjectActionSelectedCommand);
        commandExecutor.registerCommand(PuzikCommands.PROCESS_CHARACTER_SELECTION, ProcessCharacterSelectionCommand);
        commandExecutor.registerCommand(PuzikCommands.PROCESS_CHARACTER_ACTION_SELECTED, ProcessCharacterActionSelectedCommand);
        commandExecutor.registerCommand(PuzikCommands.ADD_ORDER_TO_ACTIVE_CHARACTER, AddOrderToActiveCharacterCommand);
        commandExecutor.registerCommand(PuzikCommands.ADD_CHARACTER, AddCharacterCommand);
        commandExecutor.registerCommand(PuzikCommands.UPDATE_PLAYER_MONEY, UpdatePlayerMoneyCommand);
        commandExecutor.registerCommand(PuzikCommands.SWITCH_LOCATION, SwitchLocationCommand);
        commandExecutor.registerCommand(PuzikCommands.SUGGEST_BUY_OBJECT, SuggestBuyObjectCommand);
        commandExecutor.registerCommand(PuzikCommands.SUGGEST_SELL_OBJECT, SuggestSellObjectCommand);
        commandExecutor.registerCommand(PuzikCommands.INITIATE_BUY_OBJECT, InitiateBuyObjectCommand);
        commandExecutor.registerCommand(PuzikCommands.INITIATE_SELL_OBJECT, InitiateSellObjectCommand);
        commandExecutor.registerCommand(PuzikCommands.POSTSTART_ACTIONS, PostStartActionsCommand);
        commandExecutor.registerCommand(PuzikCommands.SHOW_OVERLAY, ShowOverlayCommand);
        commandExecutor.registerCommand(PuzikCommands.REMOVE_OVERLAY, RemoveOverlayCommand);
        commandExecutor.registerCommand(PuzikCommands.ADD_QUEST_PROGRESS, AddQuestProgressCommand);
        commandExecutor.registerCommand(PuzikCommands.SET_INVENTORY_OBJECT, SetInventoryObjectCommand);
        commandExecutor.registerCommand(PuzikCommands.SET_ROOM_APPEARANCE, SetRoomAppearanceCommand);
        commandExecutor.registerCommand(PuzikCommands.ADD_XP, AddXPCommand);
        commandExecutor.registerCommand(PuzikCommands.LEVEL_UP, LevelUpCommand);
        commandExecutor.registerCommand(PuzikCommands.SWITCH_ACTIVE_CHARACTER, SwitchActiveCharacterCommand);
        commandExecutor.registerCommand(PuzikCommands.SWITCH_CURRENT_ROOM, SwitchCurrentRoomCommand);

        commandExecutor.registerCommand(PuzikCommands.INGREDIENTS_USED, IngredientsUsedCommand);

        commandExecutor.registerCommand(PuzikCommands.PARSE_PLAYERS_JSON, ParsePlayersJSONCommand);
        commandExecutor.registerCommand(PuzikCommands.PARSE_QUESTS_JSON, ParseQuestsJSONCommand);
        commandExecutor.registerCommand(PuzikCommands.PARSE_QUEST_SUBTASKS_JSON, ParseQuestSubtasksJSONCommand);
        commandExecutor.registerCommand(PuzikCommands.PARSE_GAME_OBJECTS_JSON, ParseGameObjectsJSONCommand);
        commandExecutor.registerCommand(PuzikCommands.PARSE_INVENTORY_JSON, ParseInventoryJSONCommand);
    }

}
}
