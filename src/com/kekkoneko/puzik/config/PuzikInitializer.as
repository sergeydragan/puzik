/**
 * Created by SeeD on 28/09/2014.
 */
package com.kekkoneko.puzik.config
{
import com.kekkoneko.puzik.dialog.configs.PuzikDialogConfig;
import com.kekkoneko.puzik.dialog.renderers.configs.PuzikListItemRenderersConfig;
import com.kekkoneko.puzik.display.config.PuzikDisplayLayersConfig;
import com.kekkoneko.puzik.display.config.ScreenParametersConfig;
import com.kekkoneko.puzik.gameplay.characters.config.CharacterHelpersConfig;
import com.kekkoneko.puzik.gameplay.characters.model.CharactersModel;
import com.kekkoneko.puzik.gameplay.characters.orders.config.OrderHandlersConfig;
import com.kekkoneko.puzik.gameplay.characters.orders.config.StateHandlersConfig;
import com.kekkoneko.puzik.gameplay.characters.orders.config.SuborderHandlersConfig;
import com.kekkoneko.puzik.gameplay.characters.orders.model.OrdersModel;
import com.kekkoneko.puzik.gameplay.cycles.NeedsModel;
import com.kekkoneko.puzik.gameplay.cycles.SingleBabyNeedsModel;
import com.kekkoneko.puzik.gameplay.locations.models.LocationsModel;
import com.kekkoneko.puzik.gameplay.objects.config.GameObjectsConfig;
import com.kekkoneko.puzik.gameplay.objects.config.InventoryObjectMediatorsConfig;
import com.kekkoneko.puzik.gameplay.objects.model.GameObjectsModel;
import com.kekkoneko.puzik.gameplay.objects.model.InventoryModel;
import com.kekkoneko.puzik.gameplay.objects.model.InventoryObjectsModel;
import com.kekkoneko.puzik.gameplay.player.model.PlayersModel;
import com.kekkoneko.puzik.gameplay.quests.model.QuestsModel;
import com.kekkoneko.puzik.gameplay.room.model.RoomsModel;
import com.kekkoneko.puzik.mediator.GameMediator;
import com.kekkoneko.puzik.panel.configs.PuzikPanelConfig;
import com.kekkoneko.puzik.social.model.HCModel;

import flash.display.DisplayObjectContainer;

import org.swiftsuspenders.Injector;

public class PuzikInitializer
{
    private var _injector:Injector;
    private var _viewContainer:DisplayObjectContainer;

    public function PuzikInitializer(injector:Injector, viewContainer:DisplayObjectContainer)
    {
        _injector = injector;
        _viewContainer = viewContainer;
    }

    public function initPuzik():void
    {
        initScreen();
        initModels();
        initConfigs();
        initGameMediator();
    }

    private function initScreen():void
    {
        var screenParametersConfig:ScreenParametersConfig = new ScreenParametersConfig();
        _injector.injectInto(screenParametersConfig);
        screenParametersConfig.init();

        var displayLayersConfig:PuzikDisplayLayersConfig = new PuzikDisplayLayersConfig();
        _injector.injectInto(displayLayersConfig);
        displayLayersConfig.init(_viewContainer);
    }

    private function initModels():void
    {
        _injector.map(GameObjectsModel).asSingleton();
        _injector.map(InventoryModel).asSingleton();
        _injector.map(NeedsModel).asSingleton();
        _injector.map(SingleBabyNeedsModel).asSingleton();
        _injector.map(CharactersModel).asSingleton();
        _injector.map(QuestsModel).asSingleton();
        _injector.map(OrdersModel).asSingleton();
        _injector.map(InventoryObjectsModel).asSingleton();
        _injector.map(LocationsModel).asSingleton();
        _injector.map(PlayersModel).asSingleton();
        _injector.map(HCModel).asSingleton();
        _injector.map(RoomsModel).asSingleton();
    }

    private function initConfigs():void
    {
        var commandsConfig:PuzikCommandConfig = new PuzikCommandConfig();
        _injector.injectInto(commandsConfig);
        commandsConfig.init();

        var dialogConfig:PuzikDialogConfig = new PuzikDialogConfig();
        _injector.injectInto(dialogConfig);
        dialogConfig.init();

        var panelConfig:PuzikPanelConfig = new PuzikPanelConfig();
        _injector.injectInto(panelConfig);
        panelConfig.init();

        var listItemRenderersConfig:PuzikListItemRenderersConfig = new PuzikListItemRenderersConfig();
        _injector.injectInto(listItemRenderersConfig);
        listItemRenderersConfig.init();

        var orderHandlersConfig:OrderHandlersConfig = new OrderHandlersConfig();
        _injector.injectInto(orderHandlersConfig);
        orderHandlersConfig.init();

        var gameObjectsConfig:GameObjectsConfig = new GameObjectsConfig();
        _injector.injectInto(gameObjectsConfig);
        gameObjectsConfig.init();

        var suborderHandlersConfig:SuborderHandlersConfig = new SuborderHandlersConfig();
        _injector.injectInto(suborderHandlersConfig);
        suborderHandlersConfig.init();

        var statesConfig:StateHandlersConfig = new StateHandlersConfig();
        _injector.injectInto(statesConfig);
        statesConfig.init();

        var characterHelpersConfig:CharacterHelpersConfig = new CharacterHelpersConfig();
        _injector.injectInto(characterHelpersConfig);
        characterHelpersConfig.init();

        var inventoryObjectMediatorsConfig:InventoryObjectMediatorsConfig = new InventoryObjectMediatorsConfig();
        _injector.injectInto(inventoryObjectMediatorsConfig);
        inventoryObjectMediatorsConfig.init();
    }

    private function initGameMediator():void
    {
        var gameMediator:GameMediator = new GameMediator();
        _injector.injectInto(gameMediator);
        gameMediator.init();
    }
}
}
