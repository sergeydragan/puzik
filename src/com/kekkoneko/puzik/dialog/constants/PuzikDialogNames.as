/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.dialog.constants
{
public class PuzikDialogNames
{
    public static const TEST_DIALOG:String = "TestDialog";
    public static const SHOP:String = "Shop";
    public static const WORLD_MAP:String = "WorldMap";
    public static const DAILY_REWARD:String = "DailyReward";
    public static const CLOTH_SHOP:String = "ClothShop";
    public static const HOSPITAL:String = "Hospital";
    public static const BUY_OBJECT:String = "BuyObject";
    public static const BUY_HC:String = "BuyHC";
    public static const HC_SHOP:String = "HCShop";
    public static const SELL_OBJECT:String = "SellObject";
    public static const QUEST_LIST:String = "QuestList";
    public static const NO_ENERGY:String = "NoEnergy";
    public static const NO_INGREDIENTS:String = "NoIngredients";

    public function PuzikDialogNames()
    {
    }
}
}
