/**
 * Created by SeeD on 03/01/2015.
 */
package com.kekkoneko.puzik.dialog.renderers
{
import com.kekkoneko.core.composition.elements.list.ListItemRenderer;
import com.kekkoneko.core.localization.Localize;
import com.kekkoneko.puzik.dialog.renderers.vos.DailyRewardListItemRendererDataVO;

public class DailyRewardListItemRenderer extends ListItemRenderer
{
    public function DailyRewardListItemRenderer()
    {
        super();
    }

    override public function commitData():void
    {
        var data:DailyRewardListItemRendererDataVO = this.data as DailyRewardListItemRendererDataVO;

        getImage("bg_unlocked").visible = !data.isLocked;
        getImage("bg_locked").visible = data.isLocked;

        getImage("deco_shine").visible = !data.isLocked && data.premium > 0;
        getImage("icon_coinBig").visible = !data.isLocked && data.money > 0 && data.premium == 0;
        getImage("icon_diamondBig").visible = !data.isLocked && data.money == 0 && data.premium > 0;
        getImage("icon_coinSmall").visible = !data.isLocked && data.money > 0 && data.premium > 0;
        getImage("icon_diamondSmall").visible = !data.isLocked && data.money > 0 && data.premium > 0;

        getLabel("txt_day").setText(Localize.text("daily_reward_day") + " " + (data.day + 1).toString());
    }

}
}
