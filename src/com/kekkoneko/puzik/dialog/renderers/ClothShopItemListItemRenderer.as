/**
 * Created by SeeD on 27/01/2015.
 */
package com.kekkoneko.puzik.dialog.renderers
{
import com.kekkoneko.core.composition.elements.list.ListItemRenderer;
import com.kekkoneko.core.composition.events.CompositionButtonPressedEvent;
import com.kekkoneko.puzik.dialog.renderers.vos.ClothShopItemListItemRendererVO;
import com.kekkoneko.puzik.gameplay.objects.helpers.ObjectAppearanceHelper;

import flash.events.MouseEvent;

public class ClothShopItemListItemRenderer extends ListItemRenderer
{
    public static const BUY_ITEM:String = "BuyItem";
    public static const TRY_ITEM:String = "TryItem";

    public function ClothShopItemListItemRenderer()
    {
    }

    override public function commitData():void
    {
        var vo:ClothShopItemListItemRendererVO = data as ClothShopItemListItemRendererVO;

        replacePlaceholderWithImage("placeholder_thumbnail", ObjectAppearanceHelper.getObjectSprite(vo.gameObjectVO, true));
        if (vo.isOwned)
        {
            getImage("icon_owned").visible = true;
            _appearance.buttonMode = false;
            _appearance.removeEventListener(MouseEvent.CLICK, onMouseDown);
            getButton("btn_buy").visible = false;
            getButton("btn_buyPremium").visible = false;
        }
        else
        {
            getImage("icon_owned").visible = false;

            // todo: предусмотреть распродаажи, когда цена берется из priceSale/pricePremiumSale
            getButton("btn_buy").setLabel(vo.gameObjectVO.price.toString());
            getButton("btn_buyPremium").setLabel(vo.gameObjectVO.pricePremium.toString());

            _appearance.buttonMode = true;
            _appearance.addEventListener(MouseEvent.CLICK, onMouseDown);

            if (vo.gameObjectVO.pricePremium > 0)
            {
                getButton("btn_buy").visible = false;
                getButton("btn_buyPremium").visible = true;
            }
            else
            {
                getButton("btn_buy").visible = true;
                getButton("btn_buyPremium").visible = false;
            }
        }

        addEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onButtonPressed);
    }

    private function onButtonPressed(event:CompositionButtonPressedEvent):void
    {
        if (event.buttonName == "btn_buy" || event.buttonName == "btn_buyPremium")
        {
            owner.dispatchListEvent(data, BUY_ITEM);
        }
    }

    private function onMouseDown(event:MouseEvent):void
    {
//        owner.dispatchListEvent(data, BUY_ITEM);
        owner.dispatchListEvent(data, TRY_ITEM);
    }

    override public function destroy():void
    {
        _appearance.removeEventListener(MouseEvent.CLICK, onMouseDown);
    }

    override public function get width():int
    {
        return getPlaceholderDimensions("placeholder_thumbnail").width;
    }

    override public function get height():int
    {
        return getPlaceholderDimensions("placeholder_thumbnail").height;
    }
}
}
