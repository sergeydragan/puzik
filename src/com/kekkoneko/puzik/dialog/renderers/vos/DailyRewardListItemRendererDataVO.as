/**
 * Created by SeeD on 03/01/2015.
 */
package com.kekkoneko.puzik.dialog.renderers.vos
{
import com.kekkoneko.core.composition.vos.ListItemRendererDataVO;

public class DailyRewardListItemRendererDataVO extends ListItemRendererDataVO
{
    public var isLocked:Boolean;
    public var money:int;
    public var premium:int;
    public var day:int;

    public function DailyRewardListItemRendererDataVO(day:int = 0, isLocked:Boolean = true, money:int = 0, premium:int = 0)
    {
        this.isLocked = isLocked;
        this.day = day;
        this.money = money;
        this.premium = premium;
    }
}
}
