/**
 * Created by SeeD on 03/01/2015.
 */
package com.kekkoneko.puzik.dialog.renderers.vos
{
import com.kekkoneko.core.composition.vos.ListItemRendererDataVO;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.OrderVO;

public class OrdersQueueListItemRendererDataVO extends ListItemRendererDataVO
{
    public var orderVO:OrderVO;

    public function OrdersQueueListItemRendererDataVO(orderVO:OrderVO)
    {
        this.orderVO = orderVO;
    }
}
}
