/**
 * Created by SeeD on 27/01/2015.
 */
package com.kekkoneko.puzik.dialog.renderers.vos
{
import com.kekkoneko.core.composition.vos.ListItemRendererDataVO;

import flash.display.DisplayObject;

public class ClothShopClassListItemRendererVO extends ListItemRendererDataVO
{
    public var icon:DisplayObject;
    public var name:String;
    public var isSelected:Boolean = false;

    public function ClothShopClassListItemRendererVO(name:String, icon:DisplayObject)
    {
        super();

        this.name = name;
        this.icon = icon;
    }
}
}
