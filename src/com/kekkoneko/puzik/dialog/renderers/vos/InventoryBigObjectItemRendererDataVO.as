/**
 * Created by SeeD on 03/01/2015.
 */
package com.kekkoneko.puzik.dialog.renderers.vos
{
import com.kekkoneko.core.composition.vos.ListItemRendererDataVO;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;

public class InventoryBigObjectItemRendererDataVO extends ListItemRendererDataVO
{
    public var inventoryObjectVO:InventoryObjectVO;

    public function InventoryBigObjectItemRendererDataVO(inventoryObjectVO:InventoryObjectVO)
    {
        this.inventoryObjectVO = inventoryObjectVO;
    }
}
}
