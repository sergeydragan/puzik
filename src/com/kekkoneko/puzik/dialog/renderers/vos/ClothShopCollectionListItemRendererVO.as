/**
 * Created by SeeD on 27/01/2015.
 */
package com.kekkoneko.puzik.dialog.renderers.vos
{
import com.kekkoneko.core.composition.vos.ListItemRendererDataVO;

public class ClothShopCollectionListItemRendererVO extends ListItemRendererDataVO
{
    public var name:String;
    public var isSelected:Boolean = false;

    public function ClothShopCollectionListItemRendererVO(name:String)
    {
        super();

        this.name = name;
    }
}
}
