/**
 * Created by sdragan on 29.01.2015.
 */
package com.kekkoneko.puzik.dialog.renderers.vos
{
import com.kekkoneko.core.composition.vos.ListItemRendererDataVO;
import com.kekkoneko.puzik.gameplay.quests.vos.QuestSubtaskVO;

public class QuestListSubtaskItemRendererDataVO extends ListItemRendererDataVO
{
    public var subtaskVO:QuestSubtaskVO;

    public function QuestListSubtaskItemRendererDataVO(subtaskVO:QuestSubtaskVO)
    {
        super();
        this.subtaskVO = subtaskVO;
    }
}
}
