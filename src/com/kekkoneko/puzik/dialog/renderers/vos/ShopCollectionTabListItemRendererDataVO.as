/**
 * Created by SeeD on 03/01/2015.
 */
package com.kekkoneko.puzik.dialog.renderers.vos
{
import com.kekkoneko.core.composition.vos.ListItemRendererDataVO;

public class ShopCollectionTabListItemRendererDataVO extends ListItemRendererDataVO
{
    public var collectionName:String;
    public var isSelected:Boolean = false;

    public function ShopCollectionTabListItemRendererDataVO(collectionName:String)
    {
        this.collectionName = collectionName;
    }
}
}
