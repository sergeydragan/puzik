/**
 * Created by SeeD on 03/01/2015.
 */
package com.kekkoneko.puzik.dialog.renderers.vos
{
import com.kekkoneko.core.composition.vos.ListItemRendererDataVO;
import com.kekkoneko.puzik.gameplay.objects.vos.GameObjectVO;

public class ShopItemListItemRendererDataVO extends ListItemRendererDataVO
{
    public var gameObjectVO:GameObjectVO;

    public function ShopItemListItemRendererDataVO(gameObjectVO:GameObjectVO)
    {
        this.gameObjectVO = gameObjectVO;
    }
}
}
