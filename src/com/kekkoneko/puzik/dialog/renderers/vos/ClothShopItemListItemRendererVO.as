/**
 * Created by SeeD on 27/01/2015.
 */
package com.kekkoneko.puzik.dialog.renderers.vos
{
import com.kekkoneko.core.composition.vos.ListItemRendererDataVO;
import com.kekkoneko.puzik.gameplay.objects.vos.GameObjectVO;

public class ClothShopItemListItemRendererVO extends ListItemRendererDataVO
{
    public var gameObjectVO:GameObjectVO;
    public var isOwned:Boolean = false;

    public function ClothShopItemListItemRendererVO(gameObjectVO:GameObjectVO)
    {
        super();

        this.gameObjectVO = gameObjectVO;
    }
}
}
