/**
 * Created by sdragan on 29.01.2015.
 */
package com.kekkoneko.puzik.dialog.renderers.vos
{
import com.kekkoneko.core.composition.vos.ListItemRendererDataVO;
import com.kekkoneko.puzik.gameplay.quests.vos.QuestVO;

public class QuestListItemRendererDataVO extends ListItemRendererDataVO
{
    public var questVO:QuestVO;
    public var isSelected:Boolean = false;

    public function QuestListItemRendererDataVO(questVO:QuestVO)
    {
        super();
        this.questVO = questVO;
    }
}
}
