/**
 * Created by SeeD on 08/02/2015.
 */
package com.kekkoneko.puzik.dialog.renderers
{
import com.kekkoneko.core.composition.elements.list.ListItemRenderer;
import com.kekkoneko.core.composition.events.CompositionButtonPressedEvent;
import com.kekkoneko.puzik.dialog.renderers.vos.InventoryBigObjectItemRendererDataVO;
import com.kekkoneko.puzik.gameplay.objects.helpers.ObjectAppearanceHelper;
import com.kekkoneko.puzik.gameplay.objects.helpers.ObjectsPriceHelper;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;

import flash.display.DisplayObject;

public class InventoryBigObjectItemRenderer extends ListItemRenderer
{
    public static const SELL_INVENTORY_OBJECT:String = "SellInventorObject";
    public static const TO_WORLD:String = "ToWorld";

    public function InventoryBigObjectItemRenderer()
    {
        super();
    }

    override public function commitData():void
    {
        var data:InventoryBigObjectItemRendererDataVO = this.data as InventoryBigObjectItemRendererDataVO;
        var inventoryObjectVO:InventoryObjectVO = data.inventoryObjectVO;
        var objectIcon:DisplayObject = ObjectAppearanceHelper.getObjectSprite(inventoryObjectVO.gameObjectVO, true);
        replacePlaceholderWithImage("placeholder_preview", objectIcon, true);
        getImage("deco_shine").visible = inventoryObjectVO.gameObjectVO.pricePremium > 0;
        getLabel("txt_price").setText(ObjectsPriceHelper.getObjectSellPrice(data.inventoryObjectVO.gameObjectVO).toString());

//        _appearance.buttonMode = true;
        addEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onButtonPressed);
    }

    private function onButtonPressed(event:CompositionButtonPressedEvent):void
    {
        if (event.buttonName == "btn_sell")
        {
            owner.dispatchListEvent(data, SELL_INVENTORY_OBJECT);
        }
        else if (event.buttonName == "btn_toWorld")
        {
            owner.dispatchListEvent(data, TO_WORLD);

        }
    }

    override public function get width():int
    {
        return _appearance.getChildByName("bg").width;
    }

    override public function get height():int
    {
        return _appearance.getChildByName("bg").height;
    }

    override public function destroy():void
    {
    }
}
}
