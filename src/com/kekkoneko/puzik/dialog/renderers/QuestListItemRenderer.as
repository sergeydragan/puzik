/**
 * Created by SeeD on 03/01/2015.
 */
package com.kekkoneko.puzik.dialog.renderers
{
import com.kekkoneko.core.composition.elements.list.ListItemRenderer;
import com.kekkoneko.core.composition.events.CompositionListEvent;
import com.kekkoneko.puzik.dialog.renderers.vos.QuestListItemRendererDataVO;

import flash.events.MouseEvent;

public class QuestListItemRenderer extends ListItemRenderer
{
    public function QuestListItemRenderer()
    {
        super();
    }


    override public function init():void
    {
        super.init();
    }

    override public function commitData():void
    {
        var data:QuestListItemRendererDataVO = this.data as QuestListItemRendererDataVO;

        getLabel("txt_questName").setText(data.questVO.name);

        getImage("bg_deselected").visible = !data.isSelected;
        getImage("bg_selected").visible = data.isSelected;

        _appearance.addEventListener(MouseEvent.CLICK, onMouseClick);
        _appearance.buttonMode = true;
    }

    private function onMouseClick(event:MouseEvent):void
    {
        owner.dispatchListEvent(data, CompositionListEvent.ITEM_SELECTED);
    }

    override public function destroy():void
    {
        _appearance.removeEventListener(MouseEvent.CLICK, onMouseClick);
    }
}
}
