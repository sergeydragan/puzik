/**
 * Created by SeeD on 27/01/2015.
 */
package com.kekkoneko.puzik.dialog.renderers
{
import com.kekkoneko.core.composition.elements.list.ListItemRenderer;
import com.kekkoneko.core.composition.events.CompositionListEvent;
import com.kekkoneko.puzik.dialog.renderers.vos.ClothShopCollectionListItemRendererVO;
import com.kekkoneko.puzik.gameplay.objects.helpers.ClothCollectionsHelper;

import flash.display.MovieClip;
import flash.events.MouseEvent;

public class ClothShopCollectionListItemRenderer extends ListItemRenderer
{
    public function ClothShopCollectionListItemRenderer()
    {
    }

    override public function commitData():void
    {
        var vo:ClothShopCollectionListItemRendererVO = data as ClothShopCollectionListItemRendererVO;

        if (vo.isSelected)
        {
            getImage("bg_deselected").visible = false;
            getImage("bg_selected").visible = true;
            _appearance.buttonMode = false;
            _appearance.removeEventListener(MouseEvent.CLICK, onMouseDown);
        }
        else
        {
            getImage("bg_deselected").visible = true;
            getImage("bg_selected").visible = false;
            _appearance.buttonMode = true;
            _appearance.addEventListener(MouseEvent.CLICK, onMouseDown);
        }

        (getImage("icon_type") as MovieClip).gotoAndStop(ClothCollectionsHelper.getIconNameForClothCollection(vo.id));
    }

    private function onMouseDown(event:MouseEvent):void
    {
        owner.dispatchListEvent(data, CompositionListEvent.ITEM_SELECTED);
    }

    override public function destroy():void
    {
        _appearance.removeEventListener(MouseEvent.CLICK, onMouseDown);
    }

}
}
