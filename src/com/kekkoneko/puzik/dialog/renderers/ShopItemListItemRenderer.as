/**
 * Created by SeeD on 03/01/2015.
 */
package com.kekkoneko.puzik.dialog.renderers
{
import com.kekkoneko.core.composition.elements.list.ListItemRenderer;
import com.kekkoneko.core.composition.events.CompositionListEvent;
import com.kekkoneko.puzik.dialog.renderers.vos.ShopItemListItemRendererDataVO;
import com.kekkoneko.puzik.gameplay.objects.helpers.ObjectAppearanceHelper;
import com.kekkoneko.puzik.gameplay.objects.vos.GameObjectVO;

import flash.display.DisplayObject;
import flash.events.MouseEvent;

public class ShopItemListItemRenderer extends ListItemRenderer
{
    public function ShopItemListItemRenderer()
    {
        super();
    }

    override public function commitData():void
    {
        var data:ShopItemListItemRendererDataVO = this.data as ShopItemListItemRendererDataVO;
        var objectVO:GameObjectVO = data.gameObjectVO;
        var objectIcon:DisplayObject = ObjectAppearanceHelper.getObjectSprite(objectVO, true);
        replacePlaceholderWithImage("placeholder_preview", objectIcon, true);

        if (objectVO.pricePremium > 0)
        {
            getLabel("txt_price").setText(objectVO.pricePremium.toString());
            getImage("icon_diamond").visible = true;
            getImage("icon_coin").visible = false;
            getImage("deco_shine").visible = true;
        }
        else
        {
            getLabel("txt_price").setText(objectVO.price.toString());
            getImage("icon_diamond").visible = false;
            getImage("icon_coin").visible = true;
            getImage("deco_shine").visible = false;
        }

        getImage("icon_new").visible = false;

        _appearance.buttonMode = true;
        _appearance.addEventListener(MouseEvent.CLICK, onMouseClick);
    }

    private function onMouseClick(e:MouseEvent):void
    {
        owner.dispatchListEvent(data, CompositionListEvent.ITEM_SELECTED);
    }

    override public function get width():int
    {
        return _appearance.getChildByName("bg").width;
    }

    override public function get height():int
    {
        return _appearance.getChildByName("bg").height;
    }

    override public function destroy():void
    {
        removeEventListener(MouseEvent.CLICK, onMouseClick);
    }
}
}
