/**
 * Created by SeeD on 03/01/2015.
 */
package com.kekkoneko.puzik.dialog.renderers
{
import com.kekkoneko.core.composition.elements.list.ListItemRenderer;
import com.kekkoneko.core.composition.events.CompositionListEvent;
import com.kekkoneko.core.localization.Localize;
import com.kekkoneko.puzik.dialog.renderers.vos.ShopCollectionTabListItemRendererDataVO;

import flash.events.MouseEvent;

public class ShopCollectionTabListItemRenderer extends ListItemRenderer
{
    public function ShopCollectionTabListItemRenderer()
    {
        super();
    }

    override public function commitData():void
    {
        var data:ShopCollectionTabListItemRendererDataVO = this.data as ShopCollectionTabListItemRendererDataVO;

        if (data.isSelected)
        {
            getImage("bg_inactive").visible = false;
            getImage("bg_active").visible = true;
            _appearance.buttonMode = false;
            _appearance.removeEventListener(MouseEvent.CLICK, onMouseClick);
        }
        else
        {
            getImage("bg_inactive").visible = true;
            getImage("bg_active").visible = false;
            _appearance.buttonMode = true;
            _appearance.addEventListener(MouseEvent.CLICK, onMouseClick);
        }

        getLabel("txt_name").setText(Localize.text(data.collectionName));
    }

    private function onMouseClick(event:MouseEvent):void
    {
        owner.dispatchListEvent(data, CompositionListEvent.ITEM_SELECTED);
    }

    override public function destroy():void
    {
        removeEventListener(MouseEvent.CLICK, onMouseClick);
    }
}
}
