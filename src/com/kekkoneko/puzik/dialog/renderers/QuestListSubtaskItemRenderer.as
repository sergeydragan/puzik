/**
 * Created by SeeD on 03/01/2015.
 */
package com.kekkoneko.puzik.dialog.renderers
{
import com.kekkoneko.core.composition.elements.list.ListItemRenderer;
import com.kekkoneko.puzik.dialog.renderers.vos.QuestListSubtaskItemRendererDataVO;
import com.kekkoneko.puzik.gameplay.quests.vos.QuestSubtaskVO;

import flash.events.MouseEvent;

public class QuestListSubtaskItemRenderer extends ListItemRenderer
{
    public function QuestListSubtaskItemRenderer()
    {
        super();
    }

    override public function commitData():void
    {
        var data:QuestListSubtaskItemRendererDataVO = this.data as QuestListSubtaskItemRendererDataVO;

        getLabel("txt_description").setText(data.subtaskVO.description);

        const subtask:QuestSubtaskVO = data.subtaskVO;
        if (subtask.isComplete)
        {
            getLabel("txt_count").visible = false;
        }
        else
        {
            getLabel("txt_count").setText(subtask.countCompleted + " / " + subtask.countTotal);
        }

        getImage("icon_checkmark").visible = subtask.isComplete;


        addEventListener(MouseEvent.CLICK, onMouseClick);
    }

    private function onMouseClick(event:MouseEvent):void
    {
        //owner.dispatchListEvent(data, CompositionListEvent.ITEM_SELECTED);
    }

    override public function destroy():void
    {
        removeEventListener(MouseEvent.CLICK, onMouseClick);
    }
}
}
