/**
 * Created by SeeD on 03/01/2015.
 */
package com.kekkoneko.puzik.dialog.renderers.configs
{
import com.kekkoneko.core.composition.models.ListItemRenderersModel;
import com.kekkoneko.puzik.dialog.renderers.ClothShopClassListItemRenderer;
import com.kekkoneko.puzik.dialog.renderers.ClothShopCollectionListItemRenderer;
import com.kekkoneko.puzik.dialog.renderers.ClothShopItemListItemRenderer;
import com.kekkoneko.puzik.dialog.renderers.DailyRewardListItemRenderer;
import com.kekkoneko.puzik.dialog.renderers.IngredientSelectionItemRenderer;
import com.kekkoneko.puzik.dialog.renderers.InventoryBigObjectItemRenderer;
import com.kekkoneko.puzik.dialog.renderers.OrdersQueueListItemRenderer;
import com.kekkoneko.puzik.dialog.renderers.QuestListItemRenderer;
import com.kekkoneko.puzik.dialog.renderers.QuestListSubtaskItemRenderer;
import com.kekkoneko.puzik.dialog.renderers.ShopCollectionTabListItemRenderer;
import com.kekkoneko.puzik.dialog.renderers.ShopItemListItemRenderer;

public class PuzikListItemRenderersConfig
{
    public function PuzikListItemRenderersConfig()
    {
    }

    public function init():void
    {
        ListItemRenderersModel.registerListItemRendererByListName("list_ordersQueue", ListItem_OrdersQueue, OrdersQueueListItemRenderer);
        ListItemRenderersModel.registerListItemRendererByListName("list_shopItems", ListItem_ShopItem, ShopItemListItemRenderer);
        ListItemRenderersModel.registerListItemRendererByListName("list_shopCollections", ListItem_ShopCollection, ShopCollectionTabListItemRenderer);
        ListItemRenderersModel.registerListItemRendererByListName("list_dailyRewards", ListItem_DailyReward, DailyRewardListItemRenderer);
        ListItemRenderersModel.registerListItemRendererByListName("list_questListQuests", ListItem_QuestListItem, QuestListItemRenderer);
        ListItemRenderersModel.registerListItemRendererByListName("list_questListSubtasks", ListItem_QuestListSubtask, QuestListSubtaskItemRenderer);
        ListItemRenderersModel.registerListItemRendererByListName("list_inventoryBigObjects", ListItem_InventoryBig, InventoryBigObjectItemRenderer);
        ListItemRenderersModel.registerListItemRendererByListName("list_clothShopItems", ListItem_ClothShopItem, ClothShopItemListItemRenderer);
        ListItemRenderersModel.registerListItemRendererByListName("list_clothShopClasses", ListItem_ClothShopClass, ClothShopClassListItemRenderer);
        ListItemRenderersModel.registerListItemRendererByListName("list_clothShopCollections", ListItem_ClothShopCollection, ClothShopCollectionListItemRenderer);
        ListItemRenderersModel.registerListItemRendererByListName("list_ingredientSelection", ListItem_IngredientSelection, IngredientSelectionItemRenderer);
    }
}
}
