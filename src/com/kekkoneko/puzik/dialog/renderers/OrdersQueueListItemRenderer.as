/**
 * Created by SeeD on 03/01/2015.
 */
package com.kekkoneko.puzik.dialog.renderers
{
import com.kekkoneko.core.composition.elements.list.ListItemRenderer;
import com.kekkoneko.puzik.dialog.renderers.vos.OrdersQueueListItemRendererDataVO;

import flash.display.MovieClip;

public class OrdersQueueListItemRenderer extends ListItemRenderer
{
    public function OrdersQueueListItemRenderer()
    {
        super();
    }

    override public function commitData():void
    {
        var data:OrdersQueueListItemRendererDataVO = this.data as OrdersQueueListItemRendererDataVO;
        (_appearance.getChildByName("actionIcon") as MovieClip).gotoAndStop(data.orderVO.interactionType);
    }
}
}
