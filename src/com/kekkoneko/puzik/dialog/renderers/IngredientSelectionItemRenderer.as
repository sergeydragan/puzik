/**
 * Created by SeeD on 08/02/2015.
 */
package com.kekkoneko.puzik.dialog.renderers
{
import com.kekkoneko.core.composition.elements.list.ListItemRenderer;
import com.kekkoneko.puzik.dialog.renderers.vos.IngredientSelectionItemRendererDataVO;
import com.kekkoneko.puzik.gameplay.objects.helpers.ObjectAppearanceHelper;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;

import flash.display.DisplayObject;

public class IngredientSelectionItemRenderer extends ListItemRenderer
{
    public function IngredientSelectionItemRenderer()
    {
        super();
    }

    override public function commitData():void
    {
        var data:IngredientSelectionItemRendererDataVO = this.data as IngredientSelectionItemRendererDataVO;
        var inventoryObjectVO:InventoryObjectVO = data.inventoryObjectVO;
        var objectIcon:DisplayObject = ObjectAppearanceHelper.getObjectSprite(inventoryObjectVO.gameObjectVO, true);
        replacePlaceholderWithImage("placeholder_objectPreview", objectIcon, true);
        _appearance.buttonMode = true;
    }

    override public function destroy():void
    {
    }
}
}
