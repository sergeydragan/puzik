/**
 * Created by SeeD on 07/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.noIngredients
{
public class NoIngredientsDialogConstants
{
    public static const TXT_TITLE:String = "txt_title";
    public static const TXT_DESCRIPTION:String = "txt_desc";
    public static const DECO_TYPE:String = "deco_icon";
    public static const BTN_CLOSE:String = "btn_close";

    public function NoIngredientsDialogConstants()
    {
    }
}
}
