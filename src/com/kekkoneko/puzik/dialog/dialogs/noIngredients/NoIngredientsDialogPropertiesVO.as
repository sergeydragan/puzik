/**
 * Created by SeeD on 07/01/2015.
 */
package com.kekkoneko.puzik.dialog.dialogs.noIngredients
{
import com.kekkoneko.core.composition.vos.CompositionPropertiesVO;

public class NoIngredientsDialogPropertiesVO extends CompositionPropertiesVO
{
    public var ingredientsClass:int;

    public function NoIngredientsDialogPropertiesVO(ingredientsClass:int)
    {
        this.ingredientsClass = ingredientsClass;
        super();
    }
}
}
