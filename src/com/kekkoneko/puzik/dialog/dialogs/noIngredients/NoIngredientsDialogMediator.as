/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.noIngredients
{
import com.kekkoneko.core.composition.events.CompositionButtonPressedEvent;
import com.kekkoneko.core.composition.models.CompositionPropertiesModel;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.dialog.DialogMediator;
import com.kekkoneko.core.dialog.vos.CloseDialogVO;
import com.kekkoneko.core.localization.Localize;
import com.kekkoneko.puzik.dialog.constants.PuzikDialogNames;
import com.kekkoneko.puzik.dialog.dialogs.hospital.*;
import com.kekkoneko.puzik.gameplay.objects.constants.GameObjectClasses;

public class NoIngredientsDialogMediator extends DialogMediator
{
    [Inject]
    public var compositionPropertiesModel:CompositionPropertiesModel;

    private var _properties:NoIngredientsDialogPropertiesVO;

    public function NoIngredientsDialogMediator()
    {
    }

    override public function init():void
    {
        super.init();

        _properties = compositionPropertiesModel.getPropertiesByCompositionName(PuzikDialogNames.NO_INGREDIENTS) as NoIngredientsDialogPropertiesVO;

        view.addEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);

        var text:String = "";
        var decoType:String = "";   // если он останется пустым - то просто никакой декорации не будет, это тоже нормально
        if (_properties.ingredientsClass == GameObjectClasses.SOAP)
        {
            decoType = "Wash";
            text = Localize.text("no_ingredients_dialog_desc_wash");
        }
        else if (_properties.ingredientsClass == GameObjectClasses.DIAPERS)
        {
            decoType = "Diapers";
            text = Localize.text("no_ingredients_dialog_desc_diapers");
        }
        else if (_properties.ingredientsClass == GameObjectClasses.TOYS_SMALL || _properties.ingredientsClass == GameObjectClasses.TOYS_BIG)
        {
            decoType = "Toys";
            text = Localize.text("no_ingredients_dialog_desc_toys");
        }
        else if (_properties.ingredientsClass == GameObjectClasses.FOOD_0_12_READY)
        {
            decoType = "Food";
            text = Localize.text("no_ingredients_dialog_desc_food");
        }
        else
        {
            decoType = "";
            text = Localize.text("no_ingredients_dialog_desc_common");
        }

        (view as NoIngredientsDialogView).setIngredientsTypeDecoAndText(decoType, text);
    }

    private function onViewButtonPressed(event:CompositionButtonPressedEvent):void
    {
        switch (event.buttonName)
        {
            case HospitalDialogConstants.BTN_CLOSE:
            case HospitalDialogConstants.BTN_OK:
                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_DIALOG, new CloseDialogVO(PuzikDialogNames.NO_INGREDIENTS));
        }
    }

    override public function destroy():void
    {
        view.removeEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
        super.destroy();
    }
}
}
