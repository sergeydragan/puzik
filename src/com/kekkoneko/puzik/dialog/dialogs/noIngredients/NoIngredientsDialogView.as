/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.noIngredients
{
import com.kekkoneko.core.dialog.DialogView;
import com.kekkoneko.core.localization.Localize;

import flash.display.MovieClip;

public class NoIngredientsDialogView extends DialogView
{
    public function NoIngredientsDialogView()
    {
    }

    override public function init():void
    {
        getButton("btn_ok").setLabel(Localize.text("common_ok"));
    }

    public function setIngredientsTypeDecoAndText(typeDeco:String = "", text:String = ""):void
    {
        if (typeDeco == "")
        {
            getImage(NoIngredientsDialogConstants.DECO_TYPE).visible = false;
            (getImage(NoIngredientsDialogConstants.DECO_TYPE) as MovieClip).stop();
        }
        else
        {
            getImage(NoIngredientsDialogConstants.DECO_TYPE).visible = true;
            (getImage(NoIngredientsDialogConstants.DECO_TYPE) as MovieClip).gotoAndStop(typeDeco);
        }

        getLabel(NoIngredientsDialogConstants.TXT_DESCRIPTION).setText(text);
    }
}
}
