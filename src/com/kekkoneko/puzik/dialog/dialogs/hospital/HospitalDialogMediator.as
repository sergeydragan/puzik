/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.hospital
{
import com.kekkoneko.core.composition.events.CompositionButtonPressedEvent;
import com.kekkoneko.core.composition.models.CompositionPropertiesModel;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.dialog.DialogMediator;
import com.kekkoneko.core.dialog.constants.DialogPriorities;
import com.kekkoneko.core.dialog.vos.ShowDialogPropertiesVO;
import com.kekkoneko.puzik.dialog.constants.PuzikDialogNames;

public class HospitalDialogMediator extends DialogMediator
{
    [Inject]
    public var compositionPropertiesModel:CompositionPropertiesModel;

    private var _properties:HospitalDialogPropertiesVO;

    public function HospitalDialogMediator()
    {
    }

    override public function init():void
    {
        super.init();

        _properties = compositionPropertiesModel.getPropertiesByCompositionName(PuzikDialogNames.HOSPITAL) as HospitalDialogPropertiesVO;

        view.addEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
    }

    private function onViewButtonPressed(event:CompositionButtonPressedEvent):void
    {
        switch (event.buttonName)
        {
            case HospitalDialogConstants.BTN_CLOSE:
                coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_DIALOG, new ShowDialogPropertiesVO(PuzikDialogNames.WORLD_MAP, null, DialogPriorities.IMMEDIATE));
//                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_DIALOG, new CloseDialogVO(PuzikDialogNames.HOSPITAL));
        }
    }

    override public function destroy():void
    {
        view.removeEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
        super.destroy();
    }
}
}
