/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.hospital
{
import com.kekkoneko.core.dialog.DialogView;

public class HospitalDialogView extends DialogView
{
    public function HospitalDialogView()
    {
    }

    override public function init():void
    {
        replacePlaceholderWithImage("placeholder_bg", new Part_Hospital_Bg());
    }

}
}
