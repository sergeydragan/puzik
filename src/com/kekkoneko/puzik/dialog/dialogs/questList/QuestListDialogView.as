/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.questList
{
import com.kekkoneko.core.composition.elements.list.List;
import com.kekkoneko.core.composition.elements.list.layouts.ListLayoutTypes;
import com.kekkoneko.core.dialog.DialogView;
import com.kekkoneko.core.localization.Localize;
import com.kekkoneko.puzik.dialog.renderers.vos.QuestListItemRendererDataVO;
import com.kekkoneko.puzik.gameplay.quests.vos.QuestVO;

public class QuestListDialogView extends DialogView
{
    private var _questsList:List;
    private var _subtasksList:List;

    public function QuestListDialogView()
    {
    }

    override public function init():void
    {
        _questsList = getList(QuestListDialogConstants.LIST_QUESTS);
        _subtasksList = getList(QuestListDialogConstants.LIST_SUBTASKS);

        _questsList.setLayoutType(ListLayoutTypes.VERTICAL);
        _subtasksList.setLayoutType(ListLayoutTypes.VERTICAL);

        _questsList.layout.verticalSpacing = 5;
        _subtasksList.layout.verticalSpacing = 5;

        getLabel("txt_title").setText(Localize.text("questlist_dialog_title"));
    }

    public function displayQuestsList(questsList:Array):void
    {
        _questsList.setData(questsList);
    }

    public function displaySubtasksList(subtasksList:Array):void
    {
        _subtasksList.setData(subtasksList);
    }


    public function displayQuestDescription(questVO:QuestVO):void
    {
        const hasHCReward:Boolean = questVO.rewardHC > 0;
        if (hasHCReward)
        {
            getLabel('txt_hc').setText(questVO.rewardHC.toString());
        }

        getLabel('txt_hc').visible = getImage('icon_hc').visible = hasHCReward;

        getLabel('txt_money').setText(questVO.rewardMoney.toString());
    }

    public function selectQuest(questVO:QuestVO):void
    {
        var itemIndex:int;
        itemIndex = _questsList.getListItemIndexByParameter("isSelected", true);
        if (itemIndex >= 0)
        {
            (_questsList.getListItemByIndex(itemIndex).data as QuestListItemRendererDataVO).isSelected = false;
            _questsList.update(itemIndex);
        }

        itemIndex = _questsList.getListItemIndexByParameter("questVO", questVO);
        (_questsList.getListItemByIndex(itemIndex).data as QuestListItemRendererDataVO).isSelected = true;
        _questsList.update(itemIndex);
    }
}
}
