/**
 * Created by SeeD on 07/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.questList
{
public class QuestListDialogConstants
{
    public static const TXT_TITLE:String = "txt_title";
    public static const TXT_DESCRIPTION:String = "txt_description";
    public static const BTN_OK:String = "btn_ok";
    public static const BTN_CLOSE:String = "btn_close";

    public static const LIST_QUESTS:String = "list_questListQuests";
    public static const LIST_SUBTASKS:String = "list_questListSubtasks";

    public function QuestListDialogConstants()
    {
    }
}
}
