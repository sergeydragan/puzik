/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.questList
{
import com.kekkoneko.core.composition.events.CompositionButtonPressedEvent;
import com.kekkoneko.core.composition.events.CompositionListEvent;
import com.kekkoneko.core.composition.models.CompositionPropertiesModel;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.dialog.DialogMediator;
import com.kekkoneko.core.dialog.vos.CloseDialogVO;
import com.kekkoneko.core.eventdispatcher.CoreEvent;
import com.kekkoneko.puzik.dialog.constants.PuzikDialogNames;
import com.kekkoneko.puzik.dialog.renderers.vos.QuestListItemRendererDataVO;
import com.kekkoneko.puzik.dialog.renderers.vos.QuestListSubtaskItemRendererDataVO;
import com.kekkoneko.puzik.gameplay.player.model.PlayersModel;
import com.kekkoneko.puzik.gameplay.quests.model.QuestsModel;
import com.kekkoneko.puzik.gameplay.quests.vos.QuestVO;

public class QuestListDialogMediator extends DialogMediator
{
    [Inject]
    public var compositionPropertiesModel:CompositionPropertiesModel;

    [Inject]
    public var questsModel:QuestsModel;

    [Inject]
    public var playerModel:PlayersModel;

    private var _properties:QuestListDialogPropertiesVO;
    private var _currentQuestId:int;

    public function QuestListDialogMediator()
    {
    }

    override public function init():void
    {
        _properties = compositionPropertiesModel.getPropertiesByCompositionName(PuzikDialogNames.QUEST_LIST) as QuestListDialogPropertiesVO;

        view.addEventListener(CompositionListEvent.LIST_EVENT, onViewListEvent);
        view.addEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);

        coreEventDispatcher.addEventListener("QUESTS_UPDATED", onQuestsUpdated);

        setupList();

        super.init();
    }

    private function setupList():void
    {
        var activeQuests:Vector.<QuestVO> = questsModel.getActiveQuests(playerModel.getPlayer().level);
        var renderersData:Array = [];
        for each (var questVO:QuestVO in activeQuests)
        {
            var rendererData:QuestListItemRendererDataVO = new QuestListItemRendererDataVO(questVO);
            renderersData.push(rendererData);
        }

        (view as QuestListDialogView).displayQuestsList(renderersData);
        displayQuestDetails(activeQuests[0]);
    }

    private function onQuestsUpdated(event:CoreEvent):void
    {
        // update quest list and/or current quest
    }

    private function onViewListEvent(event:CompositionListEvent):void
    {
        if (event.listItemRendererDataVO.listName == QuestListDialogConstants.LIST_QUESTS)
        {
            displayQuestDetails((event.listItemRendererDataVO as QuestListItemRendererDataVO).questVO);
        }
    }

    private function displayQuestDetails(questVO:QuestVO):void
    {
        _currentQuestId = questVO.id;

        var subtasksList:Array = [];
        for (var i:int = 0; i < questVO.subtasks.length; i++)
        {
            subtasksList.push(new QuestListSubtaskItemRendererDataVO(questVO.subtasks[i]));
        }
        (view as QuestListDialogView).displaySubtasksList(subtasksList);
        (view as QuestListDialogView).displayQuestDescription(questVO);
        (view as QuestListDialogView).selectQuest(questVO);
    }

    private function onViewButtonPressed(event:CompositionButtonPressedEvent):void
    {
        switch (event.buttonName)
        {
            case QuestListDialogConstants.BTN_CLOSE:
            case QuestListDialogConstants.BTN_OK:
                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_DIALOG, new CloseDialogVO(PuzikDialogNames.QUEST_LIST));
                break;
        }
    }

    override public function destroy():void
    {
        view.removeEventListener(CompositionListEvent.LIST_EVENT, onViewListEvent);
        view.removeEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
        super.destroy();
    }
}
}
