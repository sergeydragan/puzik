/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.buyObject
{
import com.kekkoneko.core.composition.events.CompositionButtonPressedEvent;
import com.kekkoneko.core.composition.models.CompositionPropertiesModel;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.dialog.DialogMediator;
import com.kekkoneko.core.dialog.vos.CloseDialogVO;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.dialog.constants.PuzikDialogNames;
import com.kekkoneko.puzik.gameplay.objects.model.GameObjectsModel;
import com.kekkoneko.puzik.gameplay.objects.vos.GameObjectVO;
import com.kekkoneko.puzik.gameplay.shop.vos.InitiateBuyObjectVO;

public class BuyObjectDialogMediator extends DialogMediator
{
    [Inject]
    public var compositionPropertiesModel:CompositionPropertiesModel;

    [Inject]
    public var gameObjectsModel:GameObjectsModel;

    private var _properties:BuyObjectDialogPropertiesVO;

    public function BuyObjectDialogMediator()
    {
    }

    override public function init():void
    {
        super.init();

        _properties = compositionPropertiesModel.getPropertiesByCompositionName(PuzikDialogNames.BUY_OBJECT) as BuyObjectDialogPropertiesVO;
        var gameobjectVO:GameObjectVO = _properties.gameObjectVO;

        (view as BuyObjectDialogView).showObjectInfo(gameobjectVO);
        view.addEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
    }

    private function onViewButtonPressed(event:CompositionButtonPressedEvent):void
    {
        switch (event.buttonName)
        {
            case BuyObjectDialogConstants.BTN_CLOSE:
            case BuyObjectDialogConstants.BTN_CANCEL:
                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_DIALOG, new CloseDialogVO(PuzikDialogNames.BUY_OBJECT));
                break;
            case BuyObjectDialogConstants.BTN_OK:
            case BuyObjectDialogConstants.BTN_OK_PREMIUM:
                coreEventDispatcher.broadcastMessage(PuzikCommands.INITIATE_BUY_OBJECT, new InitiateBuyObjectVO(_properties.gameObjectVO));
                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_DIALOG, new CloseDialogVO(PuzikDialogNames.BUY_OBJECT));
                break;
        }
    }

    override public function destroy():void
    {
        view.removeEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
        super.destroy();
    }
}
}
