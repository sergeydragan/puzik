/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.buyObject
{
import com.kekkoneko.core.dialog.DialogView;
import com.kekkoneko.core.localization.Localize;
import com.kekkoneko.puzik.gameplay.objects.helpers.ObjectAppearanceHelper;
import com.kekkoneko.puzik.gameplay.objects.vos.GameObjectVO;

public class BuyObjectDialogView extends DialogView
{
    public function BuyObjectDialogView()
    {
    }

    override public function init():void
    {

    }

    public function showObjectInfo(gameobjectVO:GameObjectVO):void
    {
        replacePlaceholderWithImage("placeholder_preview", ObjectAppearanceHelper.getObjectSprite(gameobjectVO, true), true);
        getLabel("txt_title").setText(gameobjectVO.name);
        getLabel("txt_description").setText(gameobjectVO.description);

        getButton(BuyObjectDialogConstants.BTN_OK).setLabel(Localize.text("buy_object_ok"));
        getButton(BuyObjectDialogConstants.BTN_CANCEL).setLabel(Localize.text("common_cancel"));

        if (gameobjectVO.pricePremium > 0)
        {
            getLabel("txt_price").setText(gameobjectVO.pricePremium.toString());
            getImage("icon_diamond").visible = true;
            getImage("icon_coin").visible = false;
            getImage("deco_shine").visible = true;

            getButton(BuyObjectDialogConstants.BTN_OK_PREMIUM).visible = true;
            getButton(BuyObjectDialogConstants.BTN_OK_PREMIUM).setLabel(gameobjectVO.pricePremium.toString());
            getButton(BuyObjectDialogConstants.BTN_OK).visible = false;
        }
        else
        {
            getLabel("txt_price").setText(gameobjectVO.price.toString());
            getImage("icon_diamond").visible = false;
            getImage("icon_coin").visible = true;
            getImage("deco_shine").visible = false;

            getButton(BuyObjectDialogConstants.BTN_OK_PREMIUM).visible = false;
            getButton(BuyObjectDialogConstants.BTN_OK).visible = true;
        }
    }
}
}
