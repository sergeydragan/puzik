/**
 * Created by SeeD on 07/01/2015.
 */
package com.kekkoneko.puzik.dialog.dialogs.buyObject
{
import com.kekkoneko.core.composition.vos.CompositionPropertiesVO;
import com.kekkoneko.puzik.gameplay.objects.vos.GameObjectVO;

public class BuyObjectDialogPropertiesVO extends CompositionPropertiesVO
{
    public var gameObjectVO:GameObjectVO;

    public function BuyObjectDialogPropertiesVO(gameObjectVO:GameObjectVO)
    {
        this.gameObjectVO = gameObjectVO;
        super();
    }
}
}
