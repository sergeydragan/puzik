/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.clothShop
{
import com.kekkoneko.core.composition.CompositionView;
import com.kekkoneko.core.composition.elements.list.List;
import com.kekkoneko.core.composition.elements.list.layouts.ListLayoutTypes;
import com.kekkoneko.core.composition.events.CompositionListEvent;
import com.kekkoneko.core.dialog.DialogView;
import com.kekkoneko.puzik.dialog.renderers.vos.ClothShopClassListItemRendererVO;
import com.kekkoneko.puzik.dialog.renderers.vos.ClothShopCollectionListItemRendererVO;
import com.kekkoneko.puzik.gameplay.characters.constants.CharacterTypes;
import com.kekkoneko.puzik.gameplay.characters.model.CharacterModel;
import com.kekkoneko.puzik.gameplay.characters.view.CharacterView;

import flash.display.Sprite;

public class ClothShopDialogView extends DialogView
{
    private var _collectionsList:List;
    private var _classesList:List;
    private var _itemsList:List;

    private var _panel:CompositionView;

    private var _characterView:CharacterView;

    public function ClothShopDialogView()
    {
    }

    override public function init():void
    {
        _panel = getComposition(ClothShopDialogConstants.CMP_ITEMS_PANEL);

        _collectionsList = _panel.getList(ClothShopDialogConstants.LIST_COLLECTIONS);
        _collectionsList.setLayoutType(ListLayoutTypes.HORIZONTAL);

        _classesList = _panel.getList(ClothShopDialogConstants.LIST_CLASSES);
        _classesList.setLayoutType(ListLayoutTypes.VERTICAL);
        _classesList.layout.verticalSpacing = 5;

        _itemsList = _panel.getList(ClothShopDialogConstants.LIST_ITEMS);
        _itemsList.setLayoutType(ListLayoutTypes.TILED_ROWS);
        _itemsList.layout.paddingLeft = 6;
        _itemsList.layout.horizontalSpacing = 5;
        _itemsList.layout.verticalSpacing = 8;

        _panel.getLabel("txt_pageNum").visible = false;

        _panel.addEventListener(CompositionListEvent.LIST_EVENT, onListEvent);
    }

    private function onListEvent(event:CompositionListEvent):void
    {
        dispatchListEvent(event.listItemRendererDataVO, event.eventType);
    }

    public function setCollections(collectionsList:Array):void
    {
        _collectionsList.setData(collectionsList);
        if (collectionsList.length > 0)
        {
            dispatchListEvent(collectionsList[0], CompositionListEvent.ITEM_SELECTED);
        }
    }

    public function setClasses(classesList:Array):void
    {
        _classesList.setData(classesList);
        if (classesList.length > 0)
        {
            dispatchListEvent(classesList[0], CompositionListEvent.ITEM_SELECTED);
        }
        else
        {
            setItems([]);
        }
    }

    public function setItems(itemsList:Array):void
    {
        _itemsList.gotoFirstPage();
        _itemsList.setData(itemsList);
    }

    public function updatePlayerStats(money:int, hc:int):void
    {
        _panel.getComposition("cmp_stats").getLabel("txt_money").setText(money.toString());
        _panel.getComposition("cmp_stats").getLabel("txt_hc").setText(hc.toString());
    }

    public function set selectedCollection(selectedCollection:String):void
    {
        var itemIndex:int;
        itemIndex = _collectionsList.getListItemIndexByParameter("isSelected", true);
        if (itemIndex >= 0) // when methiod called first time - nothing is selected yet, so we should also check this case
        {
            (_collectionsList.getListItemByIndex(itemIndex).data as ClothShopCollectionListItemRendererVO).isSelected = false;
            _collectionsList.update(itemIndex);
        }

        itemIndex = _collectionsList.getListItemIndexByParameter("id", selectedCollection);
        (_collectionsList.getListItemByIndex(itemIndex).data as ClothShopCollectionListItemRendererVO).isSelected = true;
        _collectionsList.update(itemIndex);
    }

    public function set selectedClass(selectedClass:String):void
    {
        var itemIndex:int;
        itemIndex = _classesList.getListItemIndexByParameter("isSelected", true);
        if (itemIndex >= 0) // when called first time - nothing is selected yet
        {
            (_classesList.getListItemByIndex(itemIndex).data as ClothShopClassListItemRendererVO).isSelected = false;
            _classesList.update(itemIndex);
        }

        itemIndex = _classesList.getListItemIndexByParameter("id", selectedClass);
        (_classesList.getListItemByIndex(itemIndex).data as ClothShopClassListItemRendererVO).isSelected = true;
        _classesList.update(itemIndex);
    }

    override public function destroy():void
    {
        _panel.removeEventListener(CompositionListEvent.LIST_EVENT, onListEvent);
        super.destroy();
    }

    public function setCharacterType(characterType:String):void
    {
        setBackground(characterType);
        setTabButtonsStatus(characterType);
    }

    private function setTabButtonsStatus(characterType:String):void
    {
        if (characterType == CharacterTypes.MOM)
        {
            getButton(ClothShopDialogConstants.BTN_TAB_MOM).disable();
            getButton(ClothShopDialogConstants.BTN_TAB_BABY).enable();
            getButton(ClothShopDialogConstants.BTN_TAB_DAD).enable();
        }
        else if (characterType == CharacterTypes.BABY)
        {
            getButton(ClothShopDialogConstants.BTN_TAB_MOM).enable();
            getButton(ClothShopDialogConstants.BTN_TAB_BABY).disable();
            getButton(ClothShopDialogConstants.BTN_TAB_DAD).enable();
        }
        else if (characterType == CharacterTypes.DAD)
        {
            getButton(ClothShopDialogConstants.BTN_TAB_MOM).enable();
            getButton(ClothShopDialogConstants.BTN_TAB_BABY).enable();
            getButton(ClothShopDialogConstants.BTN_TAB_DAD).disable();
        }
    }

    private function setBackground(characterType:String):void
    {
        var background:Sprite;
        if (characterType == CharacterTypes.MOM)
        {
            background = new Part_ClothShop_Mom_Bg();
        }
        else if (characterType == CharacterTypes.BABY)
        {
            background = new Part_ClothShop_Baby_Bg();
        }
        else if (characterType == CharacterTypes.DAD)
        {
            background = new Part_ClothShop_Dad_Bg();
        }
        else
        {
            background = new Sprite();
        }

        replacePlaceholderWithImage("placeholder_bg", background);
    }

    public function setCharacterModel(characterModel:CharacterModel):void
    {
        if (_characterView)
        {
            _characterView.cleanUp();
            if (_characterView.parent)
            {
                _characterView.parent.removeChild(_characterView);
            }
        }

        _characterView = new CharacterView(characterModel);
        _appearance.addChild(_characterView);
        _characterView.x = -210;
        _characterView.y = 220;
        _characterView.initHelpers();
    }

    public function updateCharacter():void
    {
        _characterView.updateAppearance();
    }
}
}
