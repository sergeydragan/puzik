/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.clothShop
{
import com.kekkoneko.core.composition.events.CompositionButtonPressedEvent;
import com.kekkoneko.core.composition.events.CompositionListEvent;
import com.kekkoneko.core.composition.models.CompositionPropertiesModel;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.debug.Debug;
import com.kekkoneko.core.dialog.DialogMediator;
import com.kekkoneko.core.dialog.constants.DialogPriorities;
import com.kekkoneko.core.dialog.vos.ShowDialogPropertiesVO;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.dialog.constants.PuzikDialogNames;
import com.kekkoneko.puzik.dialog.renderers.ClothShopItemListItemRenderer;
import com.kekkoneko.puzik.dialog.renderers.vos.ClothShopClassListItemRendererVO;
import com.kekkoneko.puzik.dialog.renderers.vos.ClothShopCollectionListItemRendererVO;
import com.kekkoneko.puzik.dialog.renderers.vos.ClothShopItemListItemRendererVO;
import com.kekkoneko.puzik.gameplay.characters.appearance.BabyAppearanceConfig;
import com.kekkoneko.puzik.gameplay.characters.appearance.CharacterAppearanceConfig;
import com.kekkoneko.puzik.gameplay.characters.appearance.DadAppearanceConfig;
import com.kekkoneko.puzik.gameplay.characters.appearance.MomAppearanceConfig;
import com.kekkoneko.puzik.gameplay.characters.constants.CharacterTypes;
import com.kekkoneko.puzik.gameplay.characters.model.CharacterModel;
import com.kekkoneko.puzik.gameplay.characters.orders.constants.InteractionTypes;
import com.kekkoneko.puzik.gameplay.characters.orders.vos.OrderVO;
import com.kekkoneko.puzik.gameplay.objects.constants.GameObjectCollections;
import com.kekkoneko.puzik.gameplay.objects.helpers.ClothCollectionsHelper;
import com.kekkoneko.puzik.gameplay.objects.model.GameObjectsModel;
import com.kekkoneko.puzik.gameplay.objects.model.InventoryModel;
import com.kekkoneko.puzik.gameplay.objects.vos.GameObjectVO;
import com.kekkoneko.puzik.gameplay.player.model.PlayersModel;
import com.kekkoneko.puzik.gameplay.shop.vos.SuggestBuyObjectVO;

public class ClothShopDialogMediator extends DialogMediator
{
    [Inject]
    public var compositionPropertiesModel:CompositionPropertiesModel;

    [Inject]
    public var gameObjectsModel:GameObjectsModel;

    [Inject]
    public var playersModel:PlayersModel;

    [Inject]
    public var inventoryModel:InventoryModel;

    private var _properties:ClothShopDialogPropertiesVO;
    private var _characterType:String;
    private var _characterModel:CharacterModel;
    private var _characterAppearanceConfig:CharacterAppearanceConfig;

    public function ClothShopDialogMediator()
    {
    }

    override public function init():void
    {
        super.init();

        _properties = compositionPropertiesModel.getPropertiesByCompositionName(PuzikDialogNames.CLOTH_SHOP) as ClothShopDialogPropertiesVO;

        view.addEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
        view.addEventListener(CompositionListEvent.LIST_EVENT, onViewListEvent);

        updatePlayerStats();
        setCharacterType(CharacterTypes.MOM);
    }

    private function updatePlayerStats():void
    {
        (view as ClothShopDialogView).updatePlayerStats(playersModel.getPlayer().money, playersModel.getPlayer().hc);
    }

    private function onViewButtonPressed(event:CompositionButtonPressedEvent):void
    {
        switch (event.buttonName)
        {
            case ClothShopDialogConstants.BTN_OK:
            case ClothShopDialogConstants.BTN_CLOSE:
                coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_DIALOG, new ShowDialogPropertiesVO(PuzikDialogNames.WORLD_MAP, null, DialogPriorities.IMMEDIATE));
                break;
            case ClothShopDialogConstants.BTN_TAB_MOM:
                setCharacterType(CharacterTypes.MOM);
                break;
            case ClothShopDialogConstants.BTN_TAB_DAD:
                setCharacterType(CharacterTypes.DAD);
                break;
            case ClothShopDialogConstants.BTN_TAB_BABY:
                setCharacterType(CharacterTypes.BABY);
                break;
        }
    }

    private function setCharacterType(characterType:String):void
    {
        _characterType = characterType;
        (view as ClothShopDialogView).setCharacterType(characterType);
        formCollectionsList();

        _characterModel = new CharacterModel();
        Main.injector.injectInto(_characterModel);

        if (characterType == CharacterTypes.MOM)
        {
            _characterAppearanceConfig = new MomAppearanceConfig();
            _characterModel.type = CharacterTypes.MOM;
        }
        else if (characterType == CharacterTypes.DAD)
        {
            _characterAppearanceConfig = new DadAppearanceConfig();
            _characterModel.type = CharacterTypes.DAD;
        }
        else if (characterType == CharacterTypes.BABY)
        {
            _characterAppearanceConfig = new BabyAppearanceConfig();
            _characterModel.type = CharacterTypes.BABY;
        }

        _characterModel.appearanceConfig = _characterAppearanceConfig;
        (view as ClothShopDialogView).setCharacterModel(_characterModel);
        (view as ClothShopDialogView).updateCharacter();

        var orderVO:OrderVO = new OrderVO();
        orderVO.interactionType = InteractionTypes.IDLE;
        orderVO.owner = _characterModel;
        _characterModel.addOrder(orderVO);
    }

    private function onViewListEvent(event:CompositionListEvent):void
    {
        switch (event.listItemRendererDataVO.listName)
        {
            case ClothShopDialogConstants.LIST_COLLECTIONS:
                (view as ClothShopDialogView).selectedCollection = event.listItemRendererDataVO.id;
                formClassesList(event.listItemRendererDataVO.id);
                break;
            case ClothShopDialogConstants.LIST_CLASSES:
                (view as ClothShopDialogView).selectedClass = event.listItemRendererDataVO.id;
                formItemsList(int(event.listItemRendererDataVO.id));
                break;
            case ClothShopDialogConstants.LIST_ITEMS:
                if (event.eventType == ClothShopItemListItemRenderer.BUY_ITEM)
                {
                    coreEventDispatcher.broadcastMessage(PuzikCommands.SUGGEST_BUY_OBJECT, new SuggestBuyObjectVO((event.listItemRendererDataVO as ClothShopItemListItemRendererVO).gameObjectVO.id));
                }
                else if (event.eventType == ClothShopItemListItemRenderer.TRY_ITEM)
                {
                    Debug.info("Try selected for objID: " + (event.listItemRendererDataVO as ClothShopItemListItemRendererVO).gameObjectVO.id);
                    _characterModel.appearanceConfig.replaceElementByGameObject((event.listItemRendererDataVO as ClothShopItemListItemRendererVO).gameObjectVO);
                    (view as ClothShopDialogView).updateCharacter();
                }
                break;
        }
    }

    private function formCollectionsList():void
    {
        var collectionsList:Array = [];

        switch (_characterType)
        {
            case CharacterTypes.MOM:
                collectionsList.push(new ClothShopCollectionListItemRendererVO(GameObjectCollections.MOM_CLOTH));
//                collectionsList.push(new ClothShopCollectionListItemRendererVO(GameObjectCollections.MOM_APPEARANCE));
                break;
            case CharacterTypes.DAD:
                collectionsList.push(new ClothShopCollectionListItemRendererVO(GameObjectCollections.DAD_CLOTH));
//                collectionsList.push(new ClothShopCollectionListItemRendererVO(GameObjectCollections.DAD_APPEARANCE));
                break;
            case CharacterTypes.BABY:
                collectionsList.push(new ClothShopCollectionListItemRendererVO(GameObjectCollections.BABY_CLOTH));
//                collectionsList.push(new ClothShopCollectionListItemRendererVO(GameObjectCollections.BABY_APPEARANCE));
                break;
        }

        for (var i:int = 0; i < collectionsList.length; i++)
        {
            (collectionsList[i] as ClothShopCollectionListItemRendererVO).id = (collectionsList[i] as ClothShopCollectionListItemRendererVO).name;
        }

        (view as ClothShopDialogView).setCollections(collectionsList);
    }

    private function formClassesList(collectionId:String):void
    {
        var classIds:Vector.<int> = gameObjectsModel.getObjectsCollection(collectionId);
        var classesList:Array = [];

        for (var i:int = 0; i < classIds.length; i++)
        {
            classesList.push(new ClothShopClassListItemRendererVO(classIds[i].toString(), ClothCollectionsHelper.getIconForClothClass(classIds[i])));
            (classesList[i] as ClothShopClassListItemRendererVO).id = classIds[i].toString();
        }

        (view as ClothShopDialogView).setClasses(classesList);
    }

    private function formItemsList(classId:int):void
    {
        var gameObjects:Vector.<GameObjectVO> = gameObjectsModel.getObjectsByClassId(classId);
        var itemsList:Array = [];

        // todo: implement sorting of gameObjects here

        for (var i:int = 0; i < gameObjects.length; i++)
        {
            itemsList.push(new ClothShopItemListItemRendererVO(gameObjects[i]));
            if (inventoryModel.getInventoryObjectById(gameObjects[i].id))
            {
                itemsList[itemsList.length - 1].isOwned = true;
            }
        }

        (view as ClothShopDialogView).setItems(itemsList);
    }

    override public function destroy():void
    {
        view.removeEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
        view.removeEventListener(CompositionListEvent.LIST_EVENT, onViewListEvent);
        super.destroy();
    }
}
}
