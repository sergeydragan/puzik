/**
 * Created by SeeD on 07/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.clothShop
{
public class ClothShopDialogConstants
{
    public static const TXT_TITLE:String = "txt_title";
    public static const TXT_DESCRIPTION:String = "txt_description";
    public static const BTN_OK:String = "btn_ok";
    public static const BTN_CANCEL:String = "btn_cancel";
    public static const BTN_CLOSE:String = "btn_close";

    public static const CMP_ITEMS_PANEL:String = "cmp_itemsPanel";

    public static const LIST_COLLECTIONS:String = "list_clothShopCollections";
    public static const LIST_CLASSES:String = "list_clothShopClasses";
    public static const LIST_ITEMS:String = "list_clothShopItems";

    public static const BTN_TAB_MOM:String = "btn_tabMom";
    public static const BTN_TAB_DAD:String = "btn_tabDad";
    public static const BTN_TAB_BABY:String = "btn_tabBaby";

    public function ClothShopDialogConstants()
    {
    }
}
}
