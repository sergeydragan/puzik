/**
 * Created by SeeD on 07/01/2015.
 */
package com.kekkoneko.puzik.dialog.dialogs.sellObject
{
import com.kekkoneko.core.composition.vos.CompositionPropertiesVO;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;

public class SellObjectDialogPropertiesVO extends CompositionPropertiesVO
{
    public var inventoryObjectVO:InventoryObjectVO;

    public function SellObjectDialogPropertiesVO(inventoryObjectVO:InventoryObjectVO)
    {
        this.inventoryObjectVO = inventoryObjectVO;
        super();
    }
}
}
