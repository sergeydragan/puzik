/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.sellObject
{
import com.kekkoneko.core.dialog.DialogView;
import com.kekkoneko.core.localization.Localize;
import com.kekkoneko.puzik.gameplay.objects.helpers.ObjectAppearanceHelper;
import com.kekkoneko.puzik.gameplay.objects.helpers.ObjectsPriceHelper;
import com.kekkoneko.puzik.gameplay.objects.vos.GameObjectVO;

public class SellObjectDialogView extends DialogView
{
    public function SellObjectDialogView()
    {
    }

    override public function init():void
    {

    }

    public function showObjectInfo(gameobjectVO:GameObjectVO):void
    {
        replacePlaceholderWithImage("placeholder_preview", ObjectAppearanceHelper.getObjectSprite(gameobjectVO, true), true);
        getLabel("txt_title").setText(gameobjectVO.name);
        getLabel("txt_description").setText(gameobjectVO.description);

        getButton(SellObjectDialogConstants.BTN_OK).setLabel(Localize.text("sell_object_ok"));
        getButton(SellObjectDialogConstants.BTN_CANCEL).setLabel(Localize.text("common_cancel"));

        getLabel("txt_price").setText(ObjectsPriceHelper.getObjectSellPrice(gameobjectVO).toString());
        getImage("icon_coin").visible = true;
    }
}
}
