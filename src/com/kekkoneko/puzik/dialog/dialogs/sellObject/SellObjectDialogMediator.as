/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.sellObject
{
import com.kekkoneko.core.composition.events.CompositionButtonPressedEvent;
import com.kekkoneko.core.composition.models.CompositionPropertiesModel;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.dialog.DialogMediator;
import com.kekkoneko.core.dialog.vos.CloseDialogVO;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.dialog.constants.PuzikDialogNames;
import com.kekkoneko.puzik.dialog.dialogs.buyObject.*;
import com.kekkoneko.puzik.gameplay.objects.vos.InventoryObjectVO;
import com.kekkoneko.puzik.gameplay.shop.vos.InitiateSellObjectVO;

public class SellObjectDialogMediator extends DialogMediator
{
    [Inject]
    public var compositionPropertiesModel:CompositionPropertiesModel;

    private var _properties:SellObjectDialogPropertiesVO;

    public function SellObjectDialogMediator()
    {
    }

    override public function init():void
    {
        super.init();

        _properties = compositionPropertiesModel.getPropertiesByCompositionName(PuzikDialogNames.SELL_OBJECT) as SellObjectDialogPropertiesVO;
        var inventoryObjectVO:InventoryObjectVO = _properties.inventoryObjectVO;

        (view as SellObjectDialogView).showObjectInfo(inventoryObjectVO.gameObjectVO);
        view.addEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
    }

    private function onViewButtonPressed(event:CompositionButtonPressedEvent):void
    {
        switch (event.buttonName)
        {
            case BuyObjectDialogConstants.BTN_CLOSE:
            case BuyObjectDialogConstants.BTN_CANCEL:
                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_DIALOG, new CloseDialogVO(PuzikDialogNames.SELL_OBJECT));
                break;
            case BuyObjectDialogConstants.BTN_OK:
            case BuyObjectDialogConstants.BTN_OK_PREMIUM:
                coreEventDispatcher.broadcastMessage(PuzikCommands.INITIATE_SELL_OBJECT, new InitiateSellObjectVO(_properties.inventoryObjectVO));
                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_DIALOG, new CloseDialogVO(PuzikDialogNames.SELL_OBJECT));
                break;
        }
    }

    override public function destroy():void
    {
        view.removeEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
        super.destroy();
    }
}
}
