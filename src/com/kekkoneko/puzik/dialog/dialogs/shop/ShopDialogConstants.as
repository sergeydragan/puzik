/**
 * Created by SeeD on 07/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.shop
{
public class ShopDialogConstants
{
    public static const TXT_TITLE:String = "txt_title";
    public static const TXT_DESCRIPTION:String = "txt_description";
    public static const BTN_OK:String = "btn_ok";
    public static const BTN_CANCEL:String = "btn_cancel";
    public static const BTN_CLOSE:String = "btn_close";

    public static const LIST_COLLECTIONS:String = "list_shopCollections";
    public static const LIST_ITEMS:String = "list_shopItems";

    public function ShopDialogConstants()
    {
    }
}
}
