/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.shop
{
import com.kekkoneko.core.composition.elements.list.List;
import com.kekkoneko.core.composition.elements.list.layouts.ListLayoutTypes;
import com.kekkoneko.core.dialog.DialogView;
import com.kekkoneko.puzik.dialog.renderers.vos.ShopCollectionTabListItemRendererDataVO;

public class ShopDialogView extends DialogView
{
    private var _collectionsList:List;
    private var _itemsList:List;

    public function ShopDialogView()
    {
    }

    override public function init():void
    {
        _collectionsList = getList(ShopDialogConstants.LIST_COLLECTIONS);
        _itemsList = getList(ShopDialogConstants.LIST_ITEMS);

        _collectionsList.setLayoutType(ListLayoutTypes.TILED_ROWS);
        _collectionsList.layout.paddingLeft = 50;
        _collectionsList.layout.paddingTop = 2;
        _collectionsList.layout.horizontalSpacing = 10;
        _collectionsList.layout.verticalSpacing = 5;

        _itemsList.setLayoutType(ListLayoutTypes.TILED_ROWS);
        _itemsList.layout.horizontalSpacing = 7;
        _itemsList.layout.verticalSpacing = 7;
        _itemsList.layout.paddingLeft = 36;

        replacePlaceholderWithImage("placeholder_bg", new Part_MapBackground_Blurred());
    }

    public function displayCollectionsList(collectionsList:Array):void
    {
        _collectionsList.setData(collectionsList);
    }

    public function displayItemsList(itemsList:Array):void
    {
        _itemsList.setData(itemsList);
        _itemsList.gotoFirstPage();
    }

    public function set selectedCollection(selectedCollection:String):void
    {
        var itemIndex:int;
        itemIndex = _collectionsList.getListItemIndexByParameter("isSelected", true);
        if (itemIndex >= 0) // when called first time - nothing is selected yet
        {
            (_collectionsList.getListItemByIndex(itemIndex).data as ShopCollectionTabListItemRendererDataVO).isSelected = false;
            _collectionsList.update(itemIndex);
        }

        itemIndex = _collectionsList.getListItemIndexByParameter("collectionName", selectedCollection);
        (_collectionsList.getListItemByIndex(itemIndex).data as ShopCollectionTabListItemRendererDataVO).isSelected = true;
        _collectionsList.update(itemIndex);
    }
}
}
