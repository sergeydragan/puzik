/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.shop
{
import com.kekkoneko.core.composition.events.CompositionButtonPressedEvent;
import com.kekkoneko.core.composition.events.CompositionListEvent;
import com.kekkoneko.core.composition.models.CompositionPropertiesModel;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.dialog.DialogMediator;
import com.kekkoneko.core.dialog.constants.DialogPriorities;
import com.kekkoneko.core.dialog.vos.ShowDialogPropertiesVO;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.dialog.constants.PuzikDialogNames;
import com.kekkoneko.puzik.dialog.renderers.vos.ShopCollectionTabListItemRendererDataVO;
import com.kekkoneko.puzik.dialog.renderers.vos.ShopItemListItemRendererDataVO;
import com.kekkoneko.puzik.gameplay.objects.constants.GameObjectCollections;
import com.kekkoneko.puzik.gameplay.objects.model.GameObjectsModel;
import com.kekkoneko.puzik.gameplay.objects.vos.GameObjectVO;
import com.kekkoneko.puzik.gameplay.shop.vos.SuggestBuyObjectVO;

public class ShopDialogMediator extends DialogMediator
{
    [Inject]
    public var compositionPropertiesModel:CompositionPropertiesModel;

    [Inject]
    public var gameObjectsModel:GameObjectsModel;

    private var _properties:ShopDialogPropertiesVO;
    private var _collections:Vector.<String>;

    public function ShopDialogMediator()
    {
    }

    override public function init():void
    {
        _properties = compositionPropertiesModel.getPropertiesByCompositionName(PuzikDialogNames.SHOP) as ShopDialogPropertiesVO;

        _collections = new <String>[
            GameObjectCollections.FURNITURE,
            GameObjectCollections.FOOD,
            GameObjectCollections.HYGIENE,
            GameObjectCollections.TOYS,
            GameObjectCollections.ELECTRONICS,
            GameObjectCollections.DECO,
            GameObjectCollections.WALLPAPERS,
            GameObjectCollections.FLOORS];
        displayCollections();

        view.addEventListener(CompositionListEvent.LIST_EVENT, onViewListEvent);
        view.addEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);

        super.init();
    }

    private function displayCollections():void
    {
        var collectionsList:Array = [];
        for (var i:int = 0; i < _collections.length; i++)
        {
            collectionsList.push(new ShopCollectionTabListItemRendererDataVO(_collections[i]));
        }

        //todo: we can sort collectionsList now

        (view as ShopDialogView).displayCollectionsList(collectionsList);
        displayCollection(GameObjectCollections.FURNITURE);
    }

    private function onViewListEvent(event:CompositionListEvent):void
    {
        if (event.listItemRendererDataVO.listName == ShopDialogConstants.LIST_COLLECTIONS)
        {
            displayCollection((event.listItemRendererDataVO as ShopCollectionTabListItemRendererDataVO).collectionName);
        }
        else if (event.listItemRendererDataVO.listName == ShopDialogConstants.LIST_ITEMS)
        {
            coreEventDispatcher.broadcastMessage(PuzikCommands.SUGGEST_BUY_OBJECT, new SuggestBuyObjectVO((event.listItemRendererDataVO as ShopItemListItemRendererDataVO).gameObjectVO.id));
        }
    }

    private function displayCollection(categoryName:String):void
    {
        var itemClasses:Vector.<int> = gameObjectsModel.getObjectsCollection(categoryName);
        var gameObjects:Vector.<GameObjectVO> = new <GameObjectVO>[];
        var itemsList:Array = [];
        var i:int;

        for (i = 0; i < itemClasses.length; i++)
        {
            gameObjects = gameObjects.concat(gameObjectsModel.getObjectsByClassId(itemClasses[i]));
        }

        //todo: now we have an array of gameObjects - it's a perfect moment to sort them

        for (i = 0; i < gameObjects.length; i++)
        {
            itemsList.push(new ShopItemListItemRendererDataVO(gameObjects[i]));
        }

        (view as ShopDialogView).displayItemsList(itemsList);
        (view as ShopDialogView).selectedCollection = categoryName;
    }

    private function onViewButtonPressed(event:CompositionButtonPressedEvent):void
    {
        if (event.buttonName == "btn_close")
        {
            coreEventDispatcher.broadcastMessage(CoreCommands.SHOW_DIALOG, new ShowDialogPropertiesVO(PuzikDialogNames.WORLD_MAP, null, DialogPriorities.IMMEDIATE));
        }
    }

    override public function destroy():void
    {
        view.removeEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
        super.destroy();
    }
}
}
