/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.noEnergy
{
import com.kekkoneko.core.composition.events.CompositionButtonPressedEvent;
import com.kekkoneko.core.composition.models.CompositionPropertiesModel;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.dialog.DialogMediator;
import com.kekkoneko.core.dialog.vos.CloseDialogVO;
import com.kekkoneko.puzik.dialog.constants.PuzikDialogNames;

public class NoEnergyDialogMediator extends DialogMediator
{
    [Inject]
    public var compositionPropertiesModel:CompositionPropertiesModel;

    private var _properties:NoEnergyDialogPropertiesVO;

    public function NoEnergyDialogMediator()
    {
    }

    override public function init():void
    {
        super.init();

        _properties = compositionPropertiesModel.getPropertiesByCompositionName(PuzikDialogNames.NO_ENERGY) as NoEnergyDialogPropertiesVO;

        view.addEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
    }

    private function onViewButtonPressed(event:CompositionButtonPressedEvent):void
    {
        switch (event.buttonName)
        {
            case NoEnergyDialogConstants.BTN_OK:
                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_DIALOG, new CloseDialogVO(PuzikDialogNames.NO_ENERGY));
                break;
        }
    }

    override public function destroy():void
    {
        view.removeEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
        super.destroy();
    }
}
}
