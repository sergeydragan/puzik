/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.noEnergy
{
import com.kekkoneko.core.dialog.DialogView;
import com.kekkoneko.core.localization.Localize;

public class NoEnergyDialogView extends DialogView
{
    public function NoEnergyDialogView()
    {
    }

    override public function init():void
    {
        getLabel(NoEnergyDialogConstants.TXT_TITLE).setText(Localize.text("no_energy_dialog_title"));
        getLabel(NoEnergyDialogConstants.TXT_DESCRIPTION).setText(Localize.text("no_energy_dialog_desc"));

        getButton("btn_ok").setLabel(Localize.text("common_ok"));
    }

}
}
