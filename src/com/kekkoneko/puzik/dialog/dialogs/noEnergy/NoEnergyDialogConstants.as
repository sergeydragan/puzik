/**
 * Created by SeeD on 07/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.noEnergy
{
public class NoEnergyDialogConstants
{
    public static const TXT_TITLE:String = "txt_title";
    public static const TXT_DESCRIPTION:String = "txt_desc";
    public static const BTN_CLOSE:String = "btn_close";
    public static const BTN_OK:String = "btn_ok";

    public function NoEnergyDialogConstants()
    {
    }
}
}
