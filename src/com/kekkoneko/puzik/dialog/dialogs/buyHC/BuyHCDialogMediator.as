/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.buyHC
{
import com.kekkoneko.core.composition.events.CompositionButtonPressedEvent;
import com.kekkoneko.core.composition.models.CompositionPropertiesModel;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.dialog.DialogMediator;
import com.kekkoneko.core.dialog.vos.CloseDialogVO;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.dialog.constants.PuzikDialogNames;
import com.kekkoneko.puzik.dialog.dialogs.buyObject.*;
import com.kekkoneko.puzik.social.vos.HCVO;
import com.kekkoneko.puzik.social.vos.InitiateBuyHCVO;

public class BuyHCDialogMediator extends DialogMediator
{
    [Inject]
    public var compositionPropertiesModel:CompositionPropertiesModel;

    private var _properties:BuyHCDialogPropertiesVO;

    public function BuyHCDialogMediator()
    {
    }

    override public function init():void
    {
        super.init();

        _properties = compositionPropertiesModel.getPropertiesByCompositionName(PuzikDialogNames.BUY_HC) as BuyHCDialogPropertiesVO;
        var hcVO:HCVO = _properties.hcVO;

        (view as BuyHCDialogView).showHCInfo(hcVO);
        view.addEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
    }

    private function onViewButtonPressed(event:CompositionButtonPressedEvent):void
    {
        switch (event.buttonName)
        {
            case BuyObjectDialogConstants.BTN_CLOSE:
            case BuyObjectDialogConstants.BTN_CANCEL:
                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_DIALOG, new CloseDialogVO(PuzikDialogNames.BUY_HC));
                break;
            case BuyObjectDialogConstants.BTN_OK:
            case BuyObjectDialogConstants.BTN_OK_PREMIUM:
                coreEventDispatcher.broadcastMessage(PuzikCommands.INITIATE_BUY_OBJECT, new InitiateBuyHCVO(_properties.hcVO));
                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_DIALOG, new CloseDialogVO(PuzikDialogNames.BUY_HC));
                break;
        }
    }

    override public function destroy():void
    {
        view.removeEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
        super.destroy();
    }
}
}
