/**
 * Created by SeeD on 07/01/2015.
 */
package com.kekkoneko.puzik.dialog.dialogs.buyHC
{
import com.kekkoneko.core.composition.vos.CompositionPropertiesVO;
import com.kekkoneko.puzik.social.vos.HCVO;

public class BuyHCDialogPropertiesVO extends CompositionPropertiesVO
{
    public var hcVO:HCVO;

    public function BuyHCDialogPropertiesVO(hcVO:HCVO)
    {
        this.hcVO = hcVO;
        super();
    }
}
}
