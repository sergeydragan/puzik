/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.dailyReward
{
import com.kekkoneko.core.composition.events.CompositionButtonPressedEvent;
import com.kekkoneko.core.composition.models.CompositionPropertiesModel;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.dialog.DialogMediator;
import com.kekkoneko.core.dialog.vos.CloseDialogVO;
import com.kekkoneko.puzik.dialog.constants.PuzikDialogNames;
import com.kekkoneko.puzik.dialog.dialogs.shop.*;
import com.kekkoneko.puzik.dialog.renderers.vos.DailyRewardListItemRendererDataVO;
import com.kekkoneko.puzik.gameplay.objects.model.GameObjectsModel;
import com.kekkoneko.puzik.gameplay.player.model.PlayersModel;
import com.kekkoneko.puzik.gameplay.player.vos.PlayerVO;

public class DailyRewardDialogMediator extends DialogMediator
{
    [Inject]
    public var compositionPropertiesModel:CompositionPropertiesModel;

    [Inject]
    public var gameObjectsModel:GameObjectsModel;

    [Inject]
    public var playersModel:PlayersModel;

    private var _properties:ShopDialogPropertiesVO;
    private var _collections:Vector.<String>;

    public function DailyRewardDialogMediator()
    {
    }

    override public function init():void
    {
        super.init();

        _properties = compositionPropertiesModel.getPropertiesByCompositionName(PuzikDialogNames.DAILY_REWARD) as ShopDialogPropertiesVO;

        var playerVO:PlayerVO = playersModel.getPlayer();

        var items:Array = [];
        items.push(new DailyRewardListItemRendererDataVO(0, false, 100));
        items.push(new DailyRewardListItemRendererDataVO(1, false, 200));
        items.push(new DailyRewardListItemRendererDataVO(2, false, 500, 5));
        items.push(new DailyRewardListItemRendererDataVO(3, true));
        items.push(new DailyRewardListItemRendererDataVO(4, true));
        (view as DailyRewardDialogView).displayRewardsList(items);

        (view as DailyRewardDialogView).displayCurrentReward(100, 2);

        view.addEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
    }

    private function onViewButtonPressed(event:CompositionButtonPressedEvent):void
    {
        switch (event.buttonName)
        {
            case DailyRewardDialogConstants.BTN_OK:
                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_DIALOG, new CloseDialogVO(PuzikDialogNames.DAILY_REWARD));
        }
    }

    override public function destroy():void
    {
        view.removeEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
        super.destroy();
    }
}
}
