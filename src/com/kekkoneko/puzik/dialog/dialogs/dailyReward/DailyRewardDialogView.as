/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.dailyReward
{
import com.kekkoneko.core.composition.CompositionView;
import com.kekkoneko.core.composition.elements.list.List;
import com.kekkoneko.core.composition.elements.list.layouts.ListLayoutTypes;
import com.kekkoneko.core.dialog.DialogView;
import com.kekkoneko.core.localization.Localize;

public class DailyRewardDialogView extends DialogView
{
    private var _rewardsList:List;

    private var _rewardLeft:CompositionView;
    private var _rewardRight:CompositionView;
    private var _rewardSingle:CompositionView;

    public function DailyRewardDialogView()
    {
    }

    override public function init():void
    {
        _rewardLeft = getComposition("cmp_reward_left");
        _rewardRight = getComposition("cmp_reward_right");
        _rewardSingle = getComposition("cmp_reward_single");

        _rewardsList = getList("list_dailyRewards");
        _rewardsList.setLayoutType(ListLayoutTypes.HORIZONTAL);
        _rewardsList.layout.horizontalSpacing = 12;
        _rewardsList.layout.paddingLeft = 16;

        getLabel("txt_title").setText(Localize.text("daily_reward_title"));
        getLabel("txt_today").setText(Localize.text("daily_reward_today"));

        getButton("btn_ok").setLabel(Localize.text("common_ok_great"));
    }

    public function displayRewardsList(itemsList:Array):void
    {
        _rewardsList.setData(itemsList);
    }

    public function displayCurrentReward(money:int, premium:int):void
    {
        if (money > 0 && premium > 0)
        {
            _rewardLeft.visible = true;
            _rewardRight.visible = true;
            _rewardSingle.visible = false;

            _rewardLeft.getImage("icon_diamond").visible = true;
            _rewardLeft.getImage("icon_coin").visible = false;
            _rewardRight.getImage("icon_diamond").visible = false;
            _rewardRight.getImage("icon_coin").visible = true;

            _rewardLeft.getLabel("txt_amount").setText(premium.toString());
            _rewardRight.getLabel("txt_amount").setText(money.toString());
        }
        else
        {
            _rewardLeft.visible = false;
            _rewardRight.visible = false;
            _rewardSingle.visible = true;

            _rewardSingle.getImage("icon_diamond").visible = premium > 0;
            _rewardSingle.getImage("icon_coin").visible = money > 0;

            if (money > 0)
            {
                _rewardSingle.getLabel("txt_amount").setText(money.toString());
            }
            else
            {
                _rewardSingle.getLabel("txt_amount").setText(premium.toString());
            }
        }
    }
}
}
