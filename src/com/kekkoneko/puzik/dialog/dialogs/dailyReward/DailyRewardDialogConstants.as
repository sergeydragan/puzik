/**
 * Created by SeeD on 07/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.dailyReward
{
public class DailyRewardDialogConstants
{
    public static const TXT_TITLE:String = "txt_title";
    public static const TXT_DESCRIPTION:String = "txt_description";
    public static const BTN_OK:String = "btn_ok";
    public static const BTN_CANCEL:String = "btn_cancel";
    public static const BTN_CLOSE:String = "btn_close";

    public function DailyRewardDialogConstants()
    {
    }
}
}
