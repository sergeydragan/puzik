/**
 * Created by SeeD on 07/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.worldMap
{
public class WorldMapDialogConstants
{
    public static const BTN_CLOSE:String = "btn_close";

    public static const BTN_HOME:String = "btn_home";
    public static const BTN_SHOP:String = "btn_shop";
    public static const BTN_CLOTH_SHOP:String = "btn_clothShop";
    public static const BTN_HOSPITAL:String = "btn_hospital";
    public static const BTN_PLAYGROUND:String = "btn_playground";

    public function WorldMapDialogConstants()
    {
    }
}
}
