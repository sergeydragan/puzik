/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.worldMap
{
import com.kekkoneko.core.composition.events.CompositionButtonPressedEvent;
import com.kekkoneko.core.composition.models.CompositionPropertiesModel;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.dialog.DialogMediator;
import com.kekkoneko.core.dialog.vos.CloseDialogVO;
import com.kekkoneko.puzik.controller.constants.PuzikCommands;
import com.kekkoneko.puzik.dialog.constants.PuzikDialogNames;
import com.kekkoneko.puzik.gameplay.locations.constants.LocationTypes;
import com.kekkoneko.puzik.gameplay.locations.models.LocationVO;
import com.kekkoneko.puzik.gameplay.locations.vos.SwitchLocationVO;

public class WorldMapDialogMediator extends DialogMediator
{
    [Inject]
    public var compositionPropertiesModel:CompositionPropertiesModel;

    public function WorldMapDialogMediator()
    {
    }

    override public function init():void
    {
        super.init();

        view.addEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
    }

    private function onViewButtonPressed(e:CompositionButtonPressedEvent):void
    {
        switch (e.buttonName)
        {
            case WorldMapDialogConstants.BTN_CLOSE:
//                coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_DIALOG, new CloseDialogVO(PuzikDialogNames.WORLD_MAP));
                break;
            case WorldMapDialogConstants.BTN_HOME:
                coreEventDispatcher.broadcastMessage(PuzikCommands.SWITCH_LOCATION, new SwitchLocationVO(new LocationVO(LocationTypes.HOME)));
                break;
            case WorldMapDialogConstants.BTN_SHOP:
                coreEventDispatcher.broadcastMessage(PuzikCommands.SWITCH_LOCATION, new SwitchLocationVO(new LocationVO(LocationTypes.SHOP)));
                break;
            case WorldMapDialogConstants.BTN_CLOTH_SHOP:
                coreEventDispatcher.broadcastMessage(PuzikCommands.SWITCH_LOCATION, new SwitchLocationVO(new LocationVO(LocationTypes.CLOTH_SHOP)));
                break;
            case WorldMapDialogConstants.BTN_HOSPITAL:
                coreEventDispatcher.broadcastMessage(PuzikCommands.SWITCH_LOCATION, new SwitchLocationVO(new LocationVO(LocationTypes.HOSPITAL)));
                break;
        }
        coreEventDispatcher.broadcastMessage(CoreCommands.CLOSE_DIALOG, new CloseDialogVO(PuzikDialogNames.WORLD_MAP));
    }

    override public function destroy():void
    {
        view.removeEventListener(CompositionButtonPressedEvent.BUTTON_PRESSED, onViewButtonPressed);
        super.destroy();
    }
}
}
