/**
 * Created by SeeD on 05/09/2014.
 */
package com.kekkoneko.puzik.dialog.dialogs.worldMap
{
import com.kekkoneko.core.dialog.DialogView;

public class WorldMapDialogView extends DialogView
{

    public function WorldMapDialogView()
    {
    }

    override public function init():void
    {
        super.init();

        getButton(WorldMapDialogConstants.BTN_PLAYGROUND).disable();
        getButton(WorldMapDialogConstants.BTN_HOSPITAL).disable();
    }

}
}
