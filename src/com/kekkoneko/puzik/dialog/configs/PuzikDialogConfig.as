/**
 * Created by SeeD on 03/09/2014.
 */
package com.kekkoneko.puzik.dialog.configs
{
import com.kekkoneko.core.dialog.model.DialogDisplayModel;
import com.kekkoneko.core.dialog.model.DialogModel;
import com.kekkoneko.core.eventdispatcher.CoreEventDispatcher;
import com.kekkoneko.core.screen.DisplayLayersModel;
import com.kekkoneko.puzik.dialog.constants.PuzikDialogNames;
import com.kekkoneko.puzik.dialog.dialogs.HCShop.HCShopDialogMediator;
import com.kekkoneko.puzik.dialog.dialogs.HCShop.HCShopDialogView;
import com.kekkoneko.puzik.dialog.dialogs.buyHC.BuyHCDialogMediator;
import com.kekkoneko.puzik.dialog.dialogs.buyHC.BuyHCDialogView;
import com.kekkoneko.puzik.dialog.dialogs.buyObject.BuyObjectDialogMediator;
import com.kekkoneko.puzik.dialog.dialogs.buyObject.BuyObjectDialogView;
import com.kekkoneko.puzik.dialog.dialogs.clothShop.ClothShopDialogMediator;
import com.kekkoneko.puzik.dialog.dialogs.clothShop.ClothShopDialogView;
import com.kekkoneko.puzik.dialog.dialogs.dailyReward.DailyRewardDialogMediator;
import com.kekkoneko.puzik.dialog.dialogs.dailyReward.DailyRewardDialogView;
import com.kekkoneko.puzik.dialog.dialogs.hospital.HospitalDialogMediator;
import com.kekkoneko.puzik.dialog.dialogs.hospital.HospitalDialogView;
import com.kekkoneko.puzik.dialog.dialogs.noEnergy.NoEnergyDialogMediator;
import com.kekkoneko.puzik.dialog.dialogs.noEnergy.NoEnergyDialogView;
import com.kekkoneko.puzik.dialog.dialogs.noIngredients.NoIngredientsDialogMediator;
import com.kekkoneko.puzik.dialog.dialogs.noIngredients.NoIngredientsDialogView;
import com.kekkoneko.puzik.dialog.dialogs.questList.QuestListDialogMediator;
import com.kekkoneko.puzik.dialog.dialogs.questList.QuestListDialogView;
import com.kekkoneko.puzik.dialog.dialogs.sellObject.SellObjectDialogMediator;
import com.kekkoneko.puzik.dialog.dialogs.sellObject.SellObjectDialogView;
import com.kekkoneko.puzik.dialog.dialogs.shop.ShopDialogMediator;
import com.kekkoneko.puzik.dialog.dialogs.shop.ShopDialogView;
import com.kekkoneko.puzik.dialog.dialogs.worldMap.WorldMapDialogMediator;
import com.kekkoneko.puzik.dialog.dialogs.worldMap.WorldMapDialogView;
import com.kekkoneko.puzik.display.constants.PuzikLayers;

import flash.display.DisplayObjectContainer;
import flash.display.Sprite;

public class PuzikDialogConfig
{
    [Inject]
    public var dialogModel:DialogModel;

    [Inject]
    public var coreEventDispatcher:CoreEventDispatcher;

    [Inject]
    public var dialogDisplayModel:DialogDisplayModel;

    [Inject]
    public var layersDisplayModel:DisplayLayersModel;

    public function PuzikDialogConfig()
    {

    }

    public function init():void
    {
        var container:DisplayObjectContainer = layersDisplayModel.getLayer(PuzikLayers.DIALOGS);
        dialogDisplayModel.setContainer(container);

        dialogModel.registerDialog(PuzikDialogNames.SHOP, ShopDialogView, ShopDialogMediator, Dialog_Shop);
        dialogModel.registerDialog(PuzikDialogNames.WORLD_MAP, WorldMapDialogView, WorldMapDialogMediator, Dialog_WorldMap);
        dialogModel.registerDialog(PuzikDialogNames.DAILY_REWARD, DailyRewardDialogView, DailyRewardDialogMediator, Dialog_DailyReward);
        dialogModel.registerDialog(PuzikDialogNames.CLOTH_SHOP, ClothShopDialogView, ClothShopDialogMediator, Dialog_ClothShop);
        dialogModel.registerDialog(PuzikDialogNames.HOSPITAL, HospitalDialogView, HospitalDialogMediator, Dialog_Hospital);
        dialogModel.registerDialog(PuzikDialogNames.BUY_OBJECT, BuyObjectDialogView, BuyObjectDialogMediator, Dialog_BuyObject);
        dialogModel.registerDialog(PuzikDialogNames.HC_SHOP, HCShopDialogView, HCShopDialogMediator, Sprite);
        dialogModel.registerDialog(PuzikDialogNames.BUY_HC, BuyHCDialogView, BuyHCDialogMediator, Sprite);
        dialogModel.registerDialog(PuzikDialogNames.SELL_OBJECT, SellObjectDialogView, SellObjectDialogMediator, Dialog_SellObject);
        dialogModel.registerDialog(PuzikDialogNames.QUEST_LIST, QuestListDialogView, QuestListDialogMediator, Dialog_QuestList);
        dialogModel.registerDialog(PuzikDialogNames.NO_ENERGY, NoEnergyDialogView, NoEnergyDialogMediator, Dialog_NoEnergy);
        dialogModel.registerDialog(PuzikDialogNames.NO_INGREDIENTS, NoIngredientsDialogView, NoIngredientsDialogMediator, Dialog_NotEnoughIngredients);
    }

}
}
