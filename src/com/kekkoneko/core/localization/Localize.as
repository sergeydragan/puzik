/**
 * Created by SeeD on 10/09/2014.
 */
package com.kekkoneko.core.localization
{
import flash.utils.Dictionary;

public class Localize
{
    private static var textIds:Dictionary;

    public function Localize()
    {
    }

    public static function setDictionary(textDictionary:Dictionary):void
    {
        textIds = textDictionary;
    }

    public static function text(textId:String, params:Object = null):String
    {
        if (textIds && textIds[textId])
        {
            return textIds[textId];
        }
        else
        {
            return "~" + textId;
        }
    }

    public static function number(num:int):String
    {
        var str:String = num.toString();
        return str;
    }
}
}
