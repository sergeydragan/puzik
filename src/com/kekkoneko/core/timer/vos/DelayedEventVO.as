/**
 * Created by SeeD on 28/09/2014.
 */
package com.kekkoneko.core.timer.vos
{
import flash.utils.Timer;

public class DelayedEventVO
{
    public var name:String;
    public var params:Object;
    public var timer:Timer;

    public function DelayedEventVO(name:String, params:Object = null)
    {
        this.name = name;
        this.params = params;
    }
}
}
