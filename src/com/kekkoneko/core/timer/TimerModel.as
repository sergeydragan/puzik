/**
 * Created by SeeD on 28/09/2014.
 */
package com.kekkoneko.core.timer
{
import com.kekkoneko.core.debug.Debug;
import com.kekkoneko.core.eventdispatcher.CoreEventDispatcher;
import com.kekkoneko.core.timer.vos.DelayedEventVO;

import flash.events.TimerEvent;
import flash.utils.Timer;

public class TimerModel
{
    [Inject]
    public var centralEventDispatcher:CoreEventDispatcher;

    private var _delayedEvents:Vector.<DelayedEventVO>;
    private var _eachSecondTimer:Timer;

    public static const ONE_SECOND_TIMER:String = "OneSecondTimer";

    public function TimerModel()
    {
        _delayedEvents = new <DelayedEventVO>[];
        _eachSecondTimer = new Timer(1000, 0);
        _eachSecondTimer.addEventListener(TimerEvent.TIMER, onEachSecondTimer);
        _eachSecondTimer.start();
    }

    private function onEachSecondTimer(e:TimerEvent):void
    {
        centralEventDispatcher.broadcastMessage(ONE_SECOND_TIMER);
    }

    public function addDelayedEvent(eventName:String, delayInMilliseconds:int, eventParams:Object = null):void
    {
        var delayedEvent:DelayedEventVO = new DelayedEventVO(eventName, eventParams);
        delayedEvent.timer = new Timer(delayInMilliseconds, 1);
        delayedEvent.timer.addEventListener(TimerEvent.TIMER, onDelayedEventTimer);
        delayedEvent.timer.start();
        _delayedEvents.push(delayedEvent);
    }

    /* use delayInMilliseconds only if you want to cancel event with specified delay.
     Otherwise every event with according name will be cancelled.
     */
    public function removeDelayedEvent(eventName:String, delayInMilliseconds:int = -1):void
    {
        var delayedEventsToRemove:Vector.<DelayedEventVO> = new <DelayedEventVO>[];
        var i:int;
        for (i = 0; i < _delayedEvents.length; i++)
        {
            if (_delayedEvents[i].name == eventName)
            {
                if (delayInMilliseconds >= 0)
                {
                    if (_delayedEvents[i].timer.delay == delayInMilliseconds)
                    {
                        delayedEventsToRemove.push(_delayedEvents[i]);
                    }
                }
                else
                {
                    delayedEventsToRemove.push(_delayedEvents[i]);
                }
            }
        }

        if (delayedEventsToRemove.length > 0)
        {
            for (i = 0; i < delayedEventsToRemove.length; i++)
                delayedEventsToRemove[i].timer.removeEventListener(TimerEvent.TIMER, onDelayedEventTimer);
            delayedEventsToRemove[i].timer.stop();
            delayedEventsToRemove[i].timer = null;

            var delayedEventIndex:int = _delayedEvents.indexOf(delayedEventsToRemove[i]);
            if (delayedEventIndex >= 0)
            {
                _delayedEvents.splice(delayedEventIndex, 1);
            }

            delayedEventsToRemove.splice(0, delayedEventsToRemove.length);
        }
    }

    private function onDelayedEventTimer(e:TimerEvent):void
    {
        var timer:Timer = e.target as Timer;
        timer.removeEventListener(TimerEvent.TIMER, onDelayedEventTimer);
        timer.stop();

        var delayedEvent:DelayedEventVO;
        for (var i:int = 0; i < _delayedEvents.length; i++)
        {
            if (_delayedEvents[i].timer == timer)
            {
                delayedEvent = _delayedEvents[i];
                break;
            }
        }

        if (!delayedEvent)
        {
            Debug.warning("Tried to fire delayed event which was added " + timer.delay + "ms ago, but couldn't find it. Maybe it was cancelled?..");
            return;
        }

        Debug.info("Firing delayed event: \"" + delayedEvent.name + "\"");
        centralEventDispatcher.broadcastMessage(delayedEvent.name, delayedEvent.params);
        var delayedEventIndex:int = _delayedEvents.indexOf(delayedEvent);
        if (delayedEventIndex >= 0)
        {
            _delayedEvents.splice(delayedEventIndex, 1);
        }
    }
}
}
