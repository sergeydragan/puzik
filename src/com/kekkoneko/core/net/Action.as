package com.kekkoneko.core.net
{

public class Action
{

    public function Action()
    {
        super();
    }

    public var name:String;
    public var isAwaitingResponse:Boolean;
    protected var _payload:Object;
    protected var _response:Object;

    public function setPayload(value:Object):void
    {
        _payload = value;
    }

    public function setResponse(value:Object):void
    {
        _response = value;
    }

    public function get response():Object
    {
        return _response;
    }

    public function get payload():Object
    {
        return _payload;
    }
}
}