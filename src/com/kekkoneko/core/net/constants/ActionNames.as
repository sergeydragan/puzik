package com.kekkoneko.core.net.constants
{

/**
 * @author SeeD
 * TODO slavara: RENAME BY ActionName
 */
public class ActionNames
{
    public static const REGISTRATION_ACTION_ERROR:String = "RegistrationActionError";
    public static const LOGIN_ACTION_ERROR:String = "LoginActionError";
    public static const SET_USER_APPEARANCE_ACTION:String = "SetUserAppearanceAction";
    public static const SET_CHILD_APPEARANCE_ACTION:String = "SetChildAppearanceAction";
    public static const FOOD_ACTION:String = "FoodAction";
    public static const SLEEP_ACTION:String = "SleepAction";
    public static const WASH_ACTION:String = "WashAction";
    public static const TOILET_ACTION:String = "ToiletAction";
    public static const HAPPINESS_ACTION:String = "HappinessAction";
    public static const CYCLE_CONTROL_ACTION:String = "CycleControlAction";
    public static const SET_USER_BUILD_INVENTORY_ACTION:String = "SetUserBuildInventoryAction";
    public static const DAY_ACTION:String = "DayAction";
    public static const NEED_PARAM_ERROR:String = "NeedParamError";
    public static const SET_MONEY_ACTION:String = "SetMoneyAction";
    public static const ADD_OBJECT_ACTION:String = "AddObjectAction";
    public static const SET_OBJECT_ACTION:String = "SetObjectAction";
    public static const SHOW_FRIEND_INFO:String = "ShowFriendInfo";
    public static const SET_USER_DROP_INVENTORY_ACTION:String = "SetUserDropInventoryAction";
    public static const CLOSE_CONNECTION_ACTION:String = "CloseConnectionAction";
    public static const SET_USER_ENERGY_ACTION:String = "SetUserEnergyAction";
    public static const SET_USER_LEVEL_ACTION:String = "SetUserLevel";
    public static const SET_XP_ACTION:String = "SetXPAction";//XXX slavara: check this value
    public static const UPDATE_XP:String = "UpdateXP";
    public static const SET_HEALTH_ACTION:String = "SetHealthAction";
    public static const SET_USER_TUTORIAL_PHASE:String = "SetUserTutorialPhase";
    public static const SET_USER_DATA:String = "SetUserData";
    public static const SET_CHILD_DATA:String = "SetChildData";
    public static const ADD_INVENTORY_OBJECT:String = "AddInventoryObject";
    public static const BUY:String = "Buy";
    public static const SELL:String = "Sell";
    public static const LOGIN:String = "Login";
    public static const REGISTRATION:String = "Registration";
    public static const SET_INVENTORY_OBJECT:String = "SetInventoryObject";
    public static const USE_OBJECT:String = "UseObject";
    public static const DELETE_INVENTORY_OBJECT:String = "DeleteInventoryObject";
    public static const SET_USER_ROOM_LIST:String = "SetUserRoomList";
    public static const SET_QUESTS_STATUS:String = "SetQuestsStatus";
    public static const CONFIRM_INVITE_ACTION:String = "ConfirmInvite";
    public static const LOAD_MESSAGES_ACTION:String = "GetConversation";
    public static const MESSAGE_SEND:String = "SendMessage";
    public static const TECHNO_MESSAGE:String = "TechnoMessage";
    public static const SEARCH_FRIEND_ACTION:String = "SearchFriend";
    public static const INVITE_FRIEND_ACTION:String = "InviteFriend";
    public static const SET_PAPA_RETURN_TIME:String = "SetPapaReturnTime";
    public static const SET_CYCLE_DURATION_ACTION:String = "SetCycleDuration";
    public static const LOGIN_VIA_VK:String = "LoginViaVk";
    public static const LOGIN_VIA_FB:String = "LoginViaFb";
    public static const LOGIN_VIA_OK:String = "LoginViaOdnoklassniki";
    public static const REGISTRATION_VIA_VK:String = "Registration";
    public static const REGISTRATION_VIA_FB:String = "RegistrationViaFb";
    public static const REGISTRATION_VIA_OK:String = "RegistrationViaOdnoklassniki";
    public static const BUY_HOUSE:String = "BuyHouse";
    public static const SET_FRIENDS:String = "SetFriends";
    public static const GET_ONLINE_USERS:String = "GetOnlineUsers";
    public static const RE_WRITE_CACHE:String = "ReWriteCache";
    public static const LOGIN_STEP_1:String = "loginStep1";
}
}