package com.kekkoneko.core.net
{
import com.kekkoneko.core.debug.Debug;
import com.kekkoneko.core.net.constants.ActionNames;

import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.SecurityErrorEvent;
import flash.net.URLLoader;
import flash.net.URLLoaderDataFormat;
import flash.net.URLRequest;
import flash.net.URLRequestHeader;
import flash.net.URLRequestMethod;

/**
 * @author SlavaRa
 */
public class HTTPServerConnect extends BaseServerConnector
{
    public static const SERVER_PORT:int = 80;
    private static const _actionName2path:Object = {};

    //NOTE slavara: cinit
    {
        _domainURL = "http://master-blue-68916963.eu-west-1.elb.amazonaws.com";
        _actionName2path[ActionNames.REGISTRATION] = "/auth/registration/";
        _actionName2path[ActionNames.LOGIN_VIA_FB] = "/auth/login/";
        _actionName2path[ActionNames.LOGIN_VIA_OK] = "/auth/login/";
        _actionName2path[ActionNames.LOGIN_VIA_VK] = "/auth/login/";
        _actionName2path[ActionNames.LOGIN] = "/auth/connect/";
        _actionName2path[ActionNames.SET_CHILD_DATA] = "/child/set/";
        _actionName2path[ActionNames.USE_OBJECT] = "/child/use/";
        _actionName2path[ActionNames.ADD_INVENTORY_OBJECT] = "/inventory/add/";
        _actionName2path[ActionNames.SET_INVENTORY_OBJECT] = "/inventory/set/";
        _actionName2path[ActionNames.DELETE_INVENTORY_OBJECT] = "/inventory/delete/";
        _actionName2path[ActionNames.BUY] = "/merchant/buy/";
        _actionName2path[ActionNames.BUY_HOUSE] = "/merchant/buy/";
        _actionName2path[ActionNames.SELL] = "/merchant/sell/";
        _actionName2path[ActionNames.SET_QUESTS_STATUS] = "/quest/set/";
        _actionName2path[ActionNames.SET_USER_ROOM_LIST] = "/room/set/";
        _actionName2path[ActionNames.SET_USER_DATA] = "/user/set/";
        _actionName2path[ActionNames.UPDATE_XP] = "/user/set/";
        _actionName2path[ActionNames.SHOW_FRIEND_INFO] = "/user/friend/";
        _actionName2path[ActionNames.SET_FRIENDS] = "/user/friend/";
        _actionName2path[ActionNames.GET_ONLINE_USERS] = "/system/users/";
        _actionName2path[ActionNames.RE_WRITE_CACHE] = "/system/cache/";
    }

    public function HTTPServerConnect()
    {
        super();
    }

    private var _loader:URLLoader;

    public override function connect(port:int = -1):void
    {
        super.connect(port);
        Debug.info('Connect to server ' + DOMAIN_URL + ":" + SERVER_PORT);
        coreEventDispatcher.broadcastMessage(CONNECTED);
    }

    public override function disconnect():void
    {
        _loader = destroyLoader(_loader);
        super.disconnect();
    }

    public override function processOfflineAction(action:String):void
    {
        if (action == null || action.length == 0) return;
        cache += action;
        var jsonPart:String = "";
        var jsonVector:Vector.<String> = new Vector.<String>();
        var bracketsCount:int = 0;
        var endOfLastJson:int = 0;
        for (var i:int = 0; i < cache.length; i++)
        {
            if (cache.charAt(i) == "{") bracketsCount += 1;
            if (bracketsCount > 0) jsonPart = jsonPart.concat(cache.charAt(i));
            if (cache.charAt(i) == "}")
            {
                bracketsCount -= 1;
                if (bracketsCount == 0)
                {
                    jsonVector.push(jsonPart);
                    jsonPart = "";
                    endOfLastJson = i;
                }
            }
        }
        if (jsonVector.length > 0) cache = cache.substr(endOfLastJson + 1);
        for each (var part:String in jsonVector)
        {
            trace("Received: " + part);
            Debug.info("RECEIVED: " + part);
        }
        for each (var currentJson:String in jsonVector)
        {
            var json:Object = JSON.parse(currentJson);
            var actionName:String = json["Action"];
            var actionContent:Object = json["Data"];
            var actionToReturn:Action;
            if (actions.length > 0 && actions[0].name == actionName)
            {
                actions[0].setResponse(actionContent);
                actionToReturn = actions.shift();
                sendFirstActionFromQueue();
            }
            else
            {
                actionToReturn = new Action();
                actionToReturn.name = actionName;
                actionToReturn.setResponse(actionContent);
            }
            coreEventDispatcher.broadcastMessage(ACTION_RECEIVED, {
                actionName:String(actionToReturn.name),
                actionBody:Action(actionToReturn)
            });
        }
    }

    public override function sendPlainStringBySocket(string:String):void
    {
        trace(this, "sendStringBySocket", "//TODO slavara: implement me");
    }

    protected override function sendAction(action:Action):void
    {
        var data:String = JSON.stringify({"Action":action.name, "Data":action.payload});
        trace("send action:", action, "->data:", data);
        action.isAwaitingResponse = true;
        var path:String = _actionName2path[action.name];
        var request:URLRequest = new URLRequest(DOMAIN_URL + path);
        request.data = data;
        request.method = URLRequestMethod.POST;
        request.requestHeaders.push(new URLRequestHeader("Content-Type", "application/json"));
        _loader = createLoader();
        _loader.load(request);
        Debug.info("SENT: " + data);
    }

    private function createLoader(dataFormat:String = URLLoaderDataFormat.TEXT):URLLoader
    {
        var result:URLLoader = new URLLoader();
        result.addEventListener(Event.COMPLETE, onComplete);
        result.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
        result.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
        return result;
    }

    private function destroyLoader(loader:URLLoader):*
    {
        if (loader != null)
        {
            loader.removeEventListener(Event.COMPLETE, onComplete);
            loader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
            loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
            loader.close();
        }
        return null;
    }

    private function onSecurityError(event:SecurityErrorEvent):void
    {
        Debug.info("CONNECTION SECURITY ERROR: " + event.toString());
//        GameAnalytics.newErrorEvent("Socket connection security error (might be socket policy)", GAErrorSeverity.ERROR);
        coreEventDispatcher.broadcastMessage(ERROR);
    }

    private function onIOError(event:IOErrorEvent):void
    {
        trace(event.errorID, event.text);
        Debug.info('ServerConnect: IOError ' + event.toString());
//        GameAnalytics.newErrorEvent("Socket connection IO error", GAErrorSeverity.ERROR);
        coreEventDispatcher.broadcastMessage(ERROR);
    }

    private function onComplete(event:Event):void
    {
        trace(this, "onLoaderComplete");
        var receivedToString:String = _loader.data || "";
        if (receivedToString.length == 0) return;
        cache += receivedToString;
        var jsonPart:String = "";
        var jsonVector:Vector.<String> = new Vector.<String>();
        var bracketsCount:int = 0;
        var endOfLastJson:int = 0;
        for (var i:int = 0; i < cache.length; i++)
        {
            if (cache.charAt(i) == "{") bracketsCount += 1;
            if (bracketsCount > 0) jsonPart = jsonPart.concat(cache.charAt(i));
            if (cache.charAt(i) == "}")
            {
                bracketsCount -= 1;
                if (bracketsCount == 0)
                {
                    jsonVector.push(jsonPart);
                    jsonPart = "";
                    endOfLastJson = i;
                }
            }
        }
        if (jsonVector.length > 0) cache = cache.substr(endOfLastJson + 1);
        for each (var part:String in jsonVector)
        {
            Debug.info("RECEIVED: " + part);
        }
        for each (var currentJson:String in jsonVector)
        {
            var json:Object = JSON.parse(currentJson);
            var actionName:String = json["Action"];
            var actionContent:Object = json["Data"];
            var actionToReturn:Action;
            if (actions.length > 0 && actions[0].name == actionName)
            {
                actions[0].setResponse(actionContent);
                actionToReturn = actions.shift();
                sendFirstActionFromQueue();
            }
            else
            {
                actionToReturn = new Action();
                actionToReturn.name = actionName;
                actionToReturn.setResponse(actionContent);
            }
            if (actionName == ActionNames.LOGIN_VIA_FB || actionName == ActionNames.LOGIN_VIA_OK || actionName == ActionNames.LOGIN_VIA_VK)
            {
                coreEventDispatcher.broadcastMessage(LOGIN_STEP_1, {
                    actionName:actionToReturn.name,
                    actionBody:actionToReturn
                });
            }
            else
            {
                coreEventDispatcher.broadcastMessage(ACTION_RECEIVED, {
                    actionName:actionToReturn.name,
                    actionBody:actionToReturn
                });
            }
        }
    }
}
}