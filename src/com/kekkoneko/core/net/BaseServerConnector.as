package com.kekkoneko.core.net
{
import com.kekkoneko.core.debug.Debug;
import com.kekkoneko.core.eventdispatcher.CoreEventDispatcher;

import flash.events.TimerEvent;
import flash.utils.setTimeout;

/**
 * @author SlavaRa
 */
public class BaseServerConnector
{
    [Inject]
    public var coreEventDispatcher:CoreEventDispatcher;

    public static const QUERY_SEPARATOR:String = "\n\r";
    public static const NAME:String = "ServerConnect";
    public static const LOGIN_STEP_1:String = "loginStep1";
    public static const ACTION_RECEIVED:String = NAME + "ActionReceived";
    public static const CONNECTED:String = NAME + "Connected";
    public static const DISCONNECT:String = "Disconnect";
    public static const DISCONNECTED:String = NAME + "Disconnected";
    public static const ERROR:String = NAME + "Error";
    public static const SEND_ACTION:String = NAME + "SendAction";
    public static const SEND_STRING:String = NAME + "SendString";
    public static const ACTION_RESPONSE_WAIT_TIMEOUT:int = 25000;

    protected static var _domainURL:String;

    public static function get DOMAIN_URL():String
    {
        return _domainURL;
    }

    public function broadcastAction(action:Action):void
    {
        coreEventDispatcher.broadcastMessage(SEND_ACTION, {"action":action});
    }

    public function broadcastSettingPort(port:int):void
    {
        coreEventDispatcher.broadcastMessage("SettingPort", {"port":port});
    }

    public function BaseServerConnector()
    {
        super();
    }

    protected var actions:Vector.<Action> = new Vector.<Action>();
    protected var cache:String = "";

    public function connect(port:int = -1):void
    {
        cleanActionsQueue();
    }

    public function disconnect():void
    {
        cleanActionsQueue();
    }

    public function processOfflineAction(action:String):void
    {
    }

    public function sendPlainStringBySocket(string:String):void
    {
    }

    public function actionQueueIsEmpty():Boolean
    {
        return actions.length == 0;
    }

    public function addAction(action:Action):void
    {
        actions[actions.length] = action;
        setTimeout(sendFirstActionFromQueue, Math.random() * 1500);
    }

    public function getFirstAction():Action
    {
        return actions.length > 0 ? actions[0] : null;
    }

    public function cleanActionsQueue():void
    {
        actions = new Vector.<Action>();
    }

    protected function sendFirstActionFromQueue():void
    {
        if (actions.length != 0) sendAction(actions[0]);
    }

    protected function sendAction(action:Action):void
    {
    }

    private function onActionResponseWaitTimer(event:TimerEvent):void
    {
        Debug.warning("Waited for response too long. Disconnecting.");
        coreEventDispatcher.broadcastMessage(DISCONNECT);
    }
}
}