/**
 * Created by SeeD on 08/09/2014.
 */
package com.kekkoneko.core.eventdispatcher
{
import flash.events.EventDispatcher;

public class CoreEventDispatcher extends EventDispatcher
{
    public function CoreEventDispatcher()
    {
    }

    public function broadcastMessage(name:String, body:Object = null):void
    {
        dispatchEvent(new CoreEvent(name, body));
    }
}
}
