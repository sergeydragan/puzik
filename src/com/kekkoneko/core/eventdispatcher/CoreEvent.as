package com.kekkoneko.core.eventdispatcher
{
import flash.events.Event;

/**
 * ...
 * @author SeeD
 */
public class CoreEvent extends Event
{
    public var name:String;
    public var body:Object;

    public function CoreEvent(type:String, body:Object = null, bubbles:Boolean = false, cancelable:Boolean = false)
    {
        super(type, bubbles, cancelable);
        this.name = type;
        this.body = body;
    }

    public override function clone():Event
    {
        return new CoreEvent(type, bubbles, cancelable);
    }

    public override function toString():String
    {
        return formatToString("CoreEvent", "type", "body", "bubbles", "cancelable", "eventPhase");
    }

}

}