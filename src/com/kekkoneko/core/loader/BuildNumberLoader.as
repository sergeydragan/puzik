/**
 * Created by SeeD on 04/12/2014.
 */
package com.kekkoneko.core.loader
{
import com.kekkoneko.core.debug.DebugVariables;

import flash.events.Event;
import flash.net.URLLoader;

public class BuildNumberLoader extends CommonDataLoader
{
    public function BuildNumberLoader(url:String, onLoadedcallback:Function = null)
    {
        super(url, onLoadedcallback);
    }

    override protected function onLoaded(e:Event):void
    {
        DebugVariables.buildNumber = int((e.target as URLLoader).data);
        if (onLoadedCallback)
        {
            onLoadedCallback();
        }
    }
}
}
