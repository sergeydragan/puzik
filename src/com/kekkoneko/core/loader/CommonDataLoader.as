/**
 * Created by SeeD on 05/10/2014.
 */
package com.kekkoneko.core.loader
{
import flash.events.Event;
import flash.net.URLLoader;
import flash.net.URLRequest;

public class CommonDataLoader
{
    protected var onLoadedCallback:Function;

    public function CommonDataLoader(url:String, onLoadedcallback:Function = null)
    {
        onLoadedCallback = onLoadedcallback;
        var urlLoader:URLLoader = new URLLoader();
        urlLoader.addEventListener(Event.COMPLETE, onLoaded);
        urlLoader.load(new URLRequest(url));
    }

    protected function onLoaded(e:Event):void
    {
        if (onLoadedCallback)
        {
            onLoadedCallback((e.target as URLLoader).data);
        }
    }
}
}
