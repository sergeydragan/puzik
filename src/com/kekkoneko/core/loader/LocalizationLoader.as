/**
 * Created by SeeD on 10/09/2014.
 */
package com.kekkoneko.core.loader
{
import com.kekkoneko.core.localization.Localize;

import flash.events.Event;
import flash.net.URLLoader;
import flash.utils.Dictionary;

public class LocalizationLoader extends CommonDataLoader
{
    public function LocalizationLoader(language:String, onLoadedcallback:Function)
    {
        super("localization_" + language + ".xml", onLoadedcallback);
    }

    override protected function onLoaded(e:Event):void
    {
        Localize.setDictionary(parseLoadedXml(new XML((e.target as URLLoader).data)));
        if (onLoadedCallback)
        {
            onLoadedCallback();
        }
    }

    private function parseLoadedXml(xml:XML):Dictionary
    {
        var dictionary:Dictionary = new Dictionary();
        for each (var node:XML in xml.*)
        {
            dictionary[String(node.localName())] = node;
        }
        return dictionary;
    }

}
}
