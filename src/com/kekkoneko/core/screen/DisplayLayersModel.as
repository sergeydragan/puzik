/**
 * Created by SeeD on 31/12/2014.
 */
package com.kekkoneko.core.screen
{
import com.kekkoneko.core.debug.Debug;

import flash.display.DisplayObjectContainer;
import flash.display.Sprite;
import flash.utils.Dictionary;

public class DisplayLayersModel
{
    private var _layers:Dictionary;
    private var _viewContainer:DisplayObjectContainer;

    public function DisplayLayersModel()
    {
        _layers = new Dictionary();
    }

    public function setViewContainer(viewContainer:DisplayObjectContainer):void
    {
        if (_viewContainer)
        {
            Debug.warning("Trying to set new view container in LayerDisplayModel, but there's already one.");
        }
        _viewContainer = viewContainer;

        /*
         We can use this to add to container all layers which were registered, but weren't added to screen
         for each (var layer:Sprite in _layers)
         {
         if (!layer.parent)
         {
         _viewContainer.addChild(layer);
         }
         }
         */
    }

    public function addLayer(layerName:String):void
    {
        _layers[layerName] = new Sprite();
        _viewContainer.addChild(_layers[layerName]);
    }

    public function getLayer(layerName:String):Sprite
    {
        if (!_layers[layerName])
        {
            Debug.warning("Layer " + layerName + " wasn't found.")
        }
        return _layers[layerName];
    }
}
}
