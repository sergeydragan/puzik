/**
 * Created by SeeD on 29/12/2014.
 */
package com.kekkoneko.core.screen
{
import flash.display.Stage;
import flash.geom.Point;
import flash.geom.Rectangle;

public class ScreenModel
{
    private var _stage:Stage;

    public function ScreenModel()
    {
    }

    public function setStage(stage:Stage):void
    {
        _stage = stage;
    }

    public function getMouseCoords():Point
    {
        return new Point(_stage.mouseX, _stage.mouseY);
    }

    public function getScreenSize():Rectangle
    {
        return new Rectangle(0, 0, _stage.stageWidth, _stage.stageHeight);
    }

}
}
