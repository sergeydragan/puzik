/**
 * Created by SeeD on 08/09/2014.
 */
package com.kekkoneko.core.controller
{
import com.kekkoneko.core.eventdispatcher.CoreEvent;
import com.kekkoneko.core.eventdispatcher.CoreEventDispatcher;

import flash.utils.Dictionary;

import org.swiftsuspenders.Injector;

public class CommandExecutor
{
    [Inject]
    public var injector:Injector;

    [Inject]
    public var centralEventDispatcher:CoreEventDispatcher;

    private var _commandsDictionary:Dictionary;

    public function CommandExecutor()
    {
        _commandsDictionary = new Dictionary();
    }

    public function init():void
    {

    }

    public function registerCommand(commandName:String, command:Class):void
    {
        _commandsDictionary[commandName] = command;
        centralEventDispatcher.addEventListener(commandName, onNotificatiion);
    }

    private function onNotificatiion(e:CoreEvent):void
    {
        executeCommand(e.name, e.body);
    }

    private function executeCommand(commandName:String, payload:Object):void
    {
        var command:Command = new (_commandsDictionary[commandName] as Class);
        injector.injectInto(command);
        command.setPayload(payload);
        command.execute();
    }
}
}
