/**
 * Created by SeeD on 07/09/2014.
 */
package com.kekkoneko.core.controller
{
import com.kekkoneko.core.eventdispatcher.CoreEvent;
import com.kekkoneko.core.eventdispatcher.CoreEventDispatcher;

import org.swiftsuspenders.Injector;

public class Command
{
    [Inject]
    public var coreEventDispatcher:CoreEventDispatcher;

    [Inject]
    public var injector:Injector;

    protected var _payload:Object;

    public function Command()
    {
    }

    public function setPayload(payload:Object):void
    {
        _payload = payload;
    }

    public function execute():void
    {

    }

    protected function subscribe(eventName:String):void
    {
        coreEventDispatcher.addEventListener(eventName, onCoreEvent);
    }

    protected function unsubscribe(eventName:String):void
    {
        coreEventDispatcher.removeEventListener(eventName, onCoreEvent);
    }

    private function onCoreEvent(e:CoreEvent):void
    {
        handleEvent(e.name, e.body);
    }

    protected function handleEvent(name:String, body:Object):void
    {

    }


}
}
