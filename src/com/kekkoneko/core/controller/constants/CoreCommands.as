/**
 * Created by SeeD on 08/09/2014.
 */
package com.kekkoneko.core.controller.constants
{
public class CoreCommands
{
    public static const SHOW_DIALOG:String = "ShowDialog";
    public static const CLOSE_DIALOG:String = "CloseDialog";
    public static const SHOW_PANEL:String = "ShowPanel";
    public static const CLOSE_PANEL:String = "ClosePanel";

    public function CoreCommands()
    {
    }
}
}
