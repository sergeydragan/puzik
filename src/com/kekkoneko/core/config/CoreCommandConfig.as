/**
 * Created by SeeD on 08/09/2014.
 */
package com.kekkoneko.core.config
{
import com.kekkoneko.core.controller.CommandExecutor;
import com.kekkoneko.core.controller.constants.CoreCommands;
import com.kekkoneko.core.dialog.commands.CloseDialogCommand;
import com.kekkoneko.core.dialog.commands.ShowDialogCommand;
import com.kekkoneko.core.panel.commands.ClosePanelCommand;
import com.kekkoneko.core.panel.commands.ShowPanelCommand;

public class CoreCommandConfig
{
    [Inject]
    public var commandExecutor:CommandExecutor;

    public function CoreCommandConfig()
    {
    }

    public function init():void
    {
        commandExecutor.registerCommand(CoreCommands.SHOW_DIALOG, ShowDialogCommand);
        commandExecutor.registerCommand(CoreCommands.CLOSE_DIALOG, CloseDialogCommand);
        commandExecutor.registerCommand(CoreCommands.SHOW_PANEL, ShowPanelCommand);
        commandExecutor.registerCommand(CoreCommands.CLOSE_PANEL, ClosePanelCommand);
    }

}
}
