/**
 * Created by SeeD on 28/09/2014.
 */
package com.kekkoneko.core.config
{
import flash.display.Stage;

import org.swiftsuspenders.Injector;

public class CoreInitializer
{
    private var _injector:Injector;
    private var _stage:Stage;

    public function CoreInitializer(injector:Injector, viewContainer:Stage)
    {
        _injector = injector;
        _stage = viewContainer;
    }

    public function initCore():void
    {
        initModels();
        initConfigs();
    }

    private function initModels():void
    {
        CoreModelConfig.initModels(_injector, _stage);
    }

    private function initConfigs():void
    {
        var commandConfig:CoreCommandConfig = new CoreCommandConfig();
        _injector.injectInto(commandConfig);
        commandConfig.init();
    }
}
}
