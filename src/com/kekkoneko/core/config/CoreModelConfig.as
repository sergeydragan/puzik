/**
 * Created by SeeD on 28/09/2014.
 */
package com.kekkoneko.core.config
{
import com.kekkoneko.core.composition.models.CompositionPropertiesModel;
import com.kekkoneko.core.composition.models.ListItemRenderersModel;
import com.kekkoneko.core.controller.CommandExecutor;
import com.kekkoneko.core.dialog.model.DialogDisplayModel;
import com.kekkoneko.core.dialog.model.DialogModel;
import com.kekkoneko.core.eventdispatcher.CoreEventDispatcher;
import com.kekkoneko.core.panel.PanelDisplayModel;
import com.kekkoneko.core.panel.PanelModel;
import com.kekkoneko.core.screen.DisplayLayersModel;
import com.kekkoneko.core.screen.ScreenModel;
import com.kekkoneko.core.timer.TimerModel;

import flash.display.Stage;

import org.swiftsuspenders.Injector;

public class CoreModelConfig
{
    public function CoreModelConfig()
    {
    }

    public static function initModels(injector:Injector, stage:Stage):void
    {
        injector.map(Injector).toValue(injector);
        injector.map(CompositionPropertiesModel).asSingleton();
        injector.map(DialogDisplayModel).asSingleton();
        injector.map(DialogModel).asSingleton();
        injector.map(PanelDisplayModel).asSingleton();
        injector.map(PanelModel).asSingleton();
        injector.map(CommandExecutor).asSingleton();
        injector.map(CoreEventDispatcher).asSingleton();
        injector.map(DisplayLayersModel).asSingleton();
        injector.map(ListItemRenderersModel).asSingleton();

        // модели, требующие injectInto
        var timerModel:TimerModel = new TimerModel();
        injector.map(TimerModel).toValue(timerModel);
        injector.injectInto(timerModel);

        var screenModel:ScreenModel = new ScreenModel();
        injector.map(ScreenModel).toValue(screenModel);
        screenModel.setStage(stage);
    }
}
}
