/**
 * Created by SeeD on 08/10/2014.
 */
package com.kekkoneko.core.utils
{
public class MathUtils
{
    public function MathUtils()
    {
    }

    public static function radToDeg(rad:Number):Number
    {
        var deg:Number = rad / (Math.PI / 180);
        return deg;
    }

    public static function degToRad(deg:Number):Number
    {
        var rad:Number = deg * (Math.PI / 180);
        return rad;
    }

    /** Возвращает min <= x <= max */
    public static function randomInt(min:int, max:int):int
    {
        // защита от дурака
        if (min > max)
        {
            var minTemp:int = max;
            max = min;
            min = minTemp;
        }

        var result:int = min + Math.floor(Math.random() * (max + 1 - min));
        return result;
    }
}
}
