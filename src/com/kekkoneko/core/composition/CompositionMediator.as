/**
 * Created by SeeD on 23/11/2014.
 */
package com.kekkoneko.core.composition
{
import com.kekkoneko.core.eventdispatcher.CoreEventDispatcher;

public class CompositionMediator
{
    [Inject]
    public var coreEventDispatcher:CoreEventDispatcher;

    protected var view:CompositionView;

    public function CompositionMediator()
    {
    }

    public function init():void
    {
    }

    public function destroy():void
    {
    }

    public function setView(view:CompositionView):void
    {
        this.view = view;
    }
}
}
