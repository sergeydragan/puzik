/**
 * Created by SeeD on 23/11/2014.
 */
package com.kekkoneko.core.composition
{
import com.kekkoneko.core.composition.elements.ProgressBar;
import com.kekkoneko.core.composition.elements.SimpleBtn;
import com.kekkoneko.core.composition.elements.SimpleTextField;
import com.kekkoneko.core.composition.elements.list.List;
import com.kekkoneko.core.composition.events.CompositionButtonPressedEvent;
import com.kekkoneko.core.composition.events.CompositionListEvent;
import com.kekkoneko.core.composition.vos.ListItemRendererDataVO;
import com.kekkoneko.core.debug.Debug;

import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.display.Sprite;
import flash.events.EventDispatcher;
import flash.geom.Rectangle;
import flash.utils.Dictionary;

public class CompositionView extends EventDispatcher
{
    protected var _appearance:Sprite;
    private var _owner:CompositionView;

    private var _labelsDictionary:Dictionary;
    private var _buttonsDictionary:Dictionary;
    private var _placeholdersDictionary:Dictionary;
    private var _compositionsDictionary:Dictionary;
    private var _listsDictionary:Dictionary;
    private var _imagesDictionary:Dictionary;
    private var _progressBarsDictionary:Dictionary;

    public function CompositionView()
    {
    }

    public function setAppearance(appearance:Sprite):void
    {
        _appearance = appearance;
        parseInterfaceElements();
        init();
    }

    public function setOwner(owner:CompositionView):void
    {
        _owner = owner;
    }

    public function init():void
    {

    }

    public function destroy():void
    {
        for each (var label:SimpleTextField in _labelsDictionary)
        {
            label.destroy();
        }
        for each (var button:SimpleBtn in _buttonsDictionary)
        {
            button.destroy();
        }
        for each (var list:List in _listsDictionary)
        {
            list.destroy();
        }
        for each (var composition:CompositionView in _compositionsDictionary)
        {
            composition.destroy();
        }
        for each (var progressbar:ProgressBar in _progressBarsDictionary)
        {
            progressbar.destroy();
        }
    }

    private function parseInterfaceElements():void
    {
        var childName:String;

        _labelsDictionary = new Dictionary();
        _buttonsDictionary = new Dictionary();
        _placeholdersDictionary = new Dictionary();
        _compositionsDictionary = new Dictionary();
        _listsDictionary = new Dictionary();
        _imagesDictionary = new Dictionary();
        _progressBarsDictionary = new Dictionary();

        for (var i:int = 0; i < _appearance.numChildren; i++)
        {
            childName = _appearance.getChildAt(i).name;
            if (childName.indexOf("txt_") == 0)
            {
                _labelsDictionary[childName] = new SimpleTextField(_appearance.getChildAt(i), this);
            }
            else if (childName.indexOf("btn_") == 0)
            {
                _buttonsDictionary[childName] = new SimpleBtn(_appearance.getChildAt(i), this);
            }
            else if (childName.indexOf("deco_") == 0 || childName.indexOf("icon_") == 0 || childName.indexOf("bg") == 0)
            {
                _imagesDictionary[childName] = _appearance.getChildAt(i);
            }
            else if (childName.indexOf("placeholder_") == 0)
            {
                _placeholdersDictionary[childName] = _appearance.getChildAt(i);
                _appearance.getChildAt(i).visible = false;
            }
            else if (childName.indexOf("list_") == 0)
            {
                _listsDictionary[childName] = new List(_appearance.getChildAt(i), this);
            }
            else if (childName.indexOf("progressbar_") == 0)
            {
                _progressBarsDictionary[childName] = new ProgressBar(_appearance.getChildAt(i), this);
            }
            else if (childName.indexOf("cmp_") == 0)
            {
                _compositionsDictionary[childName] = new CompositionView();
                CompositionView(_compositionsDictionary[childName]).setAppearance(_appearance.getChildAt(i) as Sprite);
                CompositionView(_compositionsDictionary[childName]).setOwner(this);
            }
        }
    }

    public function getButton(buttonName:String):SimpleBtn
    {
        return _buttonsDictionary[buttonName];
    }

    public function getLabel(buttonName:String):SimpleTextField
    {
        return _labelsDictionary[buttonName];
    }

    public function getList(listName:String):List
    {
        return _listsDictionary[listName];
    }

    public function getImage(imageName:String):DisplayObject
    {
        return _imagesDictionary[imageName];
    }

    public function getComposition(compositionName:String):CompositionView
    {
        return _compositionsDictionary[compositionName];
    }

    public function getProgressBar(progressBarName:String):ProgressBar
    {
        return _progressBarsDictionary[progressBarName];
    }

    public function getPlaceholderDimensions(placeholderName:String):Rectangle
    {
        var placeholder:DisplayObject = _placeholdersDictionary[placeholderName];
        return new Rectangle(placeholder.x, placeholder.y, placeholder.width, placeholder.height);
    }

    public function replacePlaceholderWithImage(placeholderName:String, image:DisplayObject, scaleToFit:Boolean = false, scaleOnlyDown:Boolean = true):void
    {
        var placeholder:DisplayObject = _placeholdersDictionary[placeholderName];
        if (!placeholder)
        {
            Debug.warning("Placeholder " + placeholderName + " not found");
            return;
        }

        // first remove old image from placeholder; might be performance-consuming
        for (var i:int = 0; i < _appearance.numChildren; i++)
        {
            if (_appearance.getChildAt(i).name.indexOf("|" + placeholderName) >= 0)
            {
                _appearance.getChildAt(i).name = _appearance.getChildAt(i).name.split("|")[0];
                _appearance.removeChildAt(i);
                break;
            }
        }

        if (scaleToFit)
        {
            var scaleFactor:Number;
            var dX:Number = placeholder.width / image.width;
            var dY:Number = placeholder.height / image.height;

            if (!(scaleOnlyDown && dX > 1 && dY > 1))
            {
                if (dX < dY)
                {
                    scaleFactor = dX;
                }
                else
                {
                    scaleFactor = dY;
                }
                image.scaleX = image.scaleY = scaleFactor;
            }
        }

        _appearance.addChildAt(image, _appearance.getChildIndex(placeholder));
        image.x = placeholder.x;
        image.y = placeholder.y;
        image.name = image.name + "|" + placeholderName;
    }

    public function addToContainer(container:DisplayObjectContainer):void
    {
        container.addChild(_appearance);
    }

    public function removeFromContainer():void
    {
        _appearance.parent.removeChild(_appearance);
    }

    public function get parent():DisplayObjectContainer
    {
        return _appearance.parent;
    }

    public function get x():Number
    {
        return _appearance.x;
    }

    public function set x(value:Number):void
    {
        _appearance.x = value;
    }

    public function get y():Number
    {
        return _appearance.y;
    }

    public function set y(value:Number):void
    {
        _appearance.y = value;
    }

    public function get width():int
    {
        return _appearance.width;
    }

    public function get height():int
    {
        return _appearance.height;
    }

    public function get visible():Boolean
    {
        return _appearance.visible;
    }

    public function set visible(value:Boolean):void
    {
        _appearance.visible = value;
    }

    protected function getChildByName(childName:String):DisplayObject
    {
        return _appearance.getChildByName(childName);
    }

    public function dispatchButtonPressedEvent(buttonName:String):void
    {
        dispatchEvent(new CompositionButtonPressedEvent(CompositionButtonPressedEvent.BUTTON_PRESSED, buttonName));
    }

    public function dispatchListEvent(listItemRendererDataVO:ListItemRendererDataVO, eventType:String):void
    {
        dispatchEvent(new CompositionListEvent(CompositionListEvent.LIST_EVENT, eventType, listItemRendererDataVO));
    }

    public function get owner():CompositionView
    {
        return _owner;
    }
}
}
