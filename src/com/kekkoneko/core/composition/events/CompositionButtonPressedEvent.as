/**
 * Created by SeeD on 01/01/2015.
 */
package com.kekkoneko.core.composition.events
{
import flash.events.Event;

public class CompositionButtonPressedEvent extends Event
{
    public var buttonName:String = "";

    public static const BUTTON_PRESSED:String = "ButtonPressed";

    public function CompositionButtonPressedEvent(type:String, buttonName:String = "", bubbles:Boolean = false, cancelable:Boolean = false)
    {
        this.buttonName = buttonName;
        super(type, bubbles, cancelable);
    }

    override public function clone():Event
    {
        return new CompositionButtonPressedEvent(type, buttonName, bubbles, cancelable);
    }

    override public function toString():String
    {
        return formatToString("CompositionButtonPressedEvent", "type", "buttonName", "bubbles", "cancelable", "eventPhase");
    }
}
}
