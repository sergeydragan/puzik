/**
 * Created by SeeD on 01/01/2015.
 */
package com.kekkoneko.core.composition.events
{
import com.kekkoneko.core.composition.vos.ListItemRendererDataVO;

import flash.events.Event;

public class CompositionListEvent extends Event
{
    public var eventType:String;
    public var listItemRendererDataVO:ListItemRendererDataVO;

    public static const LIST_EVENT:String = "ListEvent";

    public static const ITEM_SELECTED:String = "ItemSelected";

    public function CompositionListEvent(type:String, eventType:String, listItemRendererDataVO:ListItemRendererDataVO, bubbles:Boolean = false, cancelable:Boolean = false)
    {
        this.listItemRendererDataVO = listItemRendererDataVO;
        this.eventType = eventType;
        super(type, bubbles, cancelable);
    }

    override public function clone():Event
    {
        return new CompositionListEvent(type, eventType, listItemRendererDataVO, bubbles, cancelable);
    }

    override public function toString():String
    {
        return formatToString("CompositionListEvent", "type", "eventType", "listItemRendererDataVO", "bubbles", "cancelable", "eventPhase");
    }
}
}
