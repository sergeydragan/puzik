/**
 * Created by SeeD on 07/09/2014.
 */
package com.kekkoneko.core.composition.elements
{
import com.greensock.TweenMax;
import com.kekkoneko.core.composition.CompositionView;
import com.kekkoneko.core.debug.Debug;

import flash.display.DisplayObject;
import flash.display.FrameLabel;
import flash.display.MovieClip;
import flash.events.MouseEvent;
import flash.text.TextField;

public class SimpleBtn extends InterfaceElement
{
    private var _hasDisabledSkin:Boolean = false;
    private var _hasOverState:Boolean = false;

    public static const TXT_LABEL:String = "txt_label";
    public static const TXT_BADGE:String = "txt_badge";
    public static const PART_BADGE:String = "badge";

    public static const OUT:String = "out";
    public static const OVER:String = "over";
    public static const DISABLED:String = "disabled";

    public function SimpleBtn(appearance:DisplayObject, owner:CompositionView)
    {
        super(appearance, owner);
    }

    override public function init():void
    {
        var frames:Vector.<String> = new Vector.<String>();

        for (var i:int = 0; i < (_appearance as MovieClip).currentLabels.length; i++)
        {
            frames.push(((_appearance as MovieClip).currentLabels[i] as FrameLabel).name);
        }

        if (frames.indexOf(OUT) < 0)
        {
            Debug.error("Button " + (_appearance as MovieClip).name + " has no OUT state")
        }

        (_appearance as MovieClip).gotoAndStop(OUT);

        if (frames.indexOf(OVER) >= 0)
        {
            _hasOverState = true;
        }

        if (frames.indexOf(DISABLED) >= 0)
        {
            _hasDisabledSkin = true;
        }

        if ((_appearance as MovieClip).getChildByName(TXT_LABEL))
        {
            ((_appearance as MovieClip).getChildByName(TXT_LABEL) as TextField).mouseEnabled = false;
        }

        if ((_appearance as MovieClip).getChildByName(TXT_BADGE))
        {
            ((_appearance as MovieClip).getChildByName(TXT_BADGE) as TextField).mouseEnabled = false;
        }

        hideBadge();
        addButtonFeel();
        addOverListeners();
    }

    private function addButtonFeel():void
    {
        (_appearance as MovieClip).buttonMode = true;
        (_appearance as MovieClip).mouseEnabled = true;
        (_appearance as MovieClip).addEventListener(MouseEvent.CLICK, onMouseClick);
    }

    private function removeButtonFeel():void
    {
        (_appearance as MovieClip).buttonMode = false;
        (_appearance as MovieClip).mouseEnabled = false;
        (_appearance as MovieClip).removeEventListener(MouseEvent.CLICK, onMouseClick);
    }

    private function addOverListeners():void
    {
        if (!_hasOverState)
        {
            return;
        }

        (_appearance as MovieClip).addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
        (_appearance as MovieClip).addEventListener(MouseEvent.ROLL_OVER, onMouseOver);
        (_appearance as MovieClip).addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
        (_appearance as MovieClip).addEventListener(MouseEvent.ROLL_OUT, onMouseOut);
    }

    private function removeOverListeners():void
    {
        if (!_hasOverState)
        {
            return;
        }

        (_appearance as MovieClip).removeEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
        (_appearance as MovieClip).removeEventListener(MouseEvent.ROLL_OVER, onMouseOver);
        (_appearance as MovieClip).removeEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
        (_appearance as MovieClip).removeEventListener(MouseEvent.ROLL_OUT, onMouseOut);
    }

    private function onMouseOut(e:MouseEvent):void
    {
        (_appearance as MovieClip).gotoAndStop(OUT);
    }

    private function onMouseOver(e:MouseEvent):void
    {
        (_appearance as MovieClip).gotoAndStop(OVER);
    }

    private function onMouseClick(event:MouseEvent):void
    {
        if (owner)
        {
            owner.dispatchButtonPressedEvent(name);
        }
    }

    public function disable():void
    {
        setDisabledState(false);
    }

    public function enable():void
    {
        setDisabledState(true);
    }

    private function setDisabledState(enabled:Boolean):void
    {
        if (enabled)
        {
            addButtonFeel();
            addOverListeners();
            (_appearance as MovieClip).gotoAndStop(OUT);

            if (!_hasDisabledSkin)
            {
                TweenMax.to(_appearance, 1, {colorMatrixFilter:{saturation:1}});
            }
        }
        else
        {
            removeButtonFeel();
            removeOverListeners();
            if (_hasDisabledSkin)
            {
                (_appearance as MovieClip).gotoAndStop(DISABLED);
            }
            else
            {
                TweenMax.to(_appearance, 0, {colorMatrixFilter:{saturation:0}});
            }
        }
    }

    public function setLabel(text:String):void
    {
        if ((_appearance as MovieClip).getChildByName(TXT_LABEL))
        {
            ((_appearance as MovieClip).getChildByName(TXT_LABEL) as TextField).text = text;
        }
    }

    public function setBadgeLabel(text:String):void
    {
        if ((_appearance as MovieClip).getChildByName(TXT_BADGE))
        {
            ((_appearance as MovieClip).getChildByName(TXT_BADGE) as TextField).text = text;
        }
    }

    public function hideBadge():void
    {
        if ((_appearance as MovieClip).getChildByName(TXT_BADGE))
        {
            (_appearance as MovieClip).getChildByName(TXT_BADGE).visible = false;
        }

        if ((_appearance as MovieClip).getChildByName(PART_BADGE))
        {
            (_appearance as MovieClip).getChildByName(PART_BADGE).visible = false;
        }
    }

    public function showBadge():void
    {
        if ((_appearance as MovieClip).getChildByName(TXT_BADGE))
        {
            (_appearance as MovieClip).getChildByName(TXT_BADGE).visible = true;
        }

        if ((_appearance as MovieClip).getChildByName(PART_BADGE))
        {
            (_appearance as MovieClip).getChildByName(PART_BADGE).visible = true;
        }
    }

    override public function destroy():void
    {
        (_appearance as MovieClip).removeEventListener(MouseEvent.CLICK, onMouseClick);
        removeOverListeners();
    }
}
}
