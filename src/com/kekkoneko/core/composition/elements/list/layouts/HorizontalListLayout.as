/**
 * Created by SeeD on 04/01/2015.
 */
package com.kekkoneko.core.composition.elements.list.layouts
{
import com.kekkoneko.core.composition.elements.list.ListItemRenderer;

import flash.geom.Rectangle;

public class HorizontalListLayout extends ListLayout
{
    public function HorizontalListLayout(items:Vector.<ListItemRenderer>, boundaries:Rectangle)
    {
        super(items, boundaries);
    }

    override public function placeItems():void
    {
        if (!_items || _items.length == 0)
        {
            return;
        }

        var itemWidth:int = _items[0].width;

        _itemVisibleCount = Math.floor((_boundaries.width - (_paddingLeft * 2)) / (itemWidth + _horizontalSpacing));
        _totalPage = Math.ceil(_items.length / _itemVisibleCount);

        var i:int;

        for (i = 0; i < _items.length; i++)
        {
            _items[i].visible = false;
        }

        var visibleItems:Vector.<ListItemRenderer> = _items.slice(_startIndex, _startIndex + _itemVisibleCount);

        for (i = 0; i < visibleItems.length; i++)
        {
            visibleItems[i].visible = true;
            visibleItems[i].x = _paddingLeft + (i * itemWidth) + (i * horizontalSpacing) + (itemWidth / 2) - (_boundaries.width / 2);
            visibleItems[i].y = 0;
        }
    }
}
}
