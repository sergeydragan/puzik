/**
 * Created by SeeD on 04/01/2015.
 */
package com.kekkoneko.core.composition.elements.list.layouts
{
public class ListLayoutTypes
{
    public static const HORIZONTAL:String = "Horizontal";
    public static const VERTICAL:String = "Vertical";
    public static const TILED_ROWS:String = "TiledRows";
    public static const TILED_COLS:String = "TiledCols";

    public function ListLayoutTypes()
    {
    }
}
}
