/**
 * Created by SeeD on 04/01/2015.
 */
package com.kekkoneko.core.composition.elements.list.layouts
{
import com.kekkoneko.core.composition.elements.list.ListItemRenderer;

import flash.geom.Rectangle;

public class ListLayout
{
    protected var _items:Vector.<ListItemRenderer>;
    protected var _boundaries:Rectangle;

    protected var _paddingLeft:Number = 0;
    protected var _paddingTop:Number = 0;
    protected var _horizontalSpacing:Number = 0;
    protected var _verticalSpacing:Number = 0;

    protected var _startIndex:int = 0;
    protected var _currentPage:int = 0;
    protected var _totalPage:int = 0;
    protected var _itemVisibleCount:int = 0;

    public function ListLayout(items:Vector.<ListItemRenderer>, boundaries:Rectangle)
    {
        _items = items;
        _boundaries = boundaries;
    }

    public function placeItems():void
    {

    }

    public function get horizontalSpacing():Number
    {
        return _horizontalSpacing;
    }

    public function set horizontalSpacing(value:Number):void
    {
        _horizontalSpacing = value;
        placeItems();
    }

    public function get paddingLeft():Number
    {
        return _paddingLeft;
    }

    public function set paddingLeft(value:Number):void
    {
        _paddingLeft = value;
        placeItems();
    }

    public function get verticalSpacing():Number
    {
        return _verticalSpacing;
    }

    public function set verticalSpacing(value:Number):void
    {
        _verticalSpacing = value;
        placeItems();
    }

    public function get paddingTop():Number
    {
        return _paddingTop;
    }

    public function set paddingTop(value:Number):void
    {
        _paddingTop = value;
        placeItems();
    }

    public function get currentPage():int
    {
        return _currentPage;
    }

    public function set currentPage(value:int):void
    {
        if (value >= _totalPage)
        {
            _currentPage = _totalPage - 1;
        }
        else if (value < 0)
        {
            _currentPage = 0;
        }
        else
        {
            _currentPage = value;
            _startIndex = _currentPage * _itemVisibleCount;

            placeItems();
        }
    }

    public function get totalPage():int
    {
        return _totalPage;
    }
}
}
