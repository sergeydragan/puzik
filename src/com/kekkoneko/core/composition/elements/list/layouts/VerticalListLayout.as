/**
 * Created by SeeD on 04/01/2015.
 */
package com.kekkoneko.core.composition.elements.list.layouts
{
import com.kekkoneko.core.composition.elements.list.ListItemRenderer;

import flash.geom.Rectangle;

public class VerticalListLayout extends ListLayout
{
    public function VerticalListLayout(items:Vector.<ListItemRenderer>, boundaries:Rectangle)
    {
        super(items, boundaries);
    }

    override public function placeItems():void
    {
        if (!_items || _items.length == 0)
        {
            return;
        }

        var itemHeight:int = _items[0].height;

        _itemVisibleCount = Math.floor((_boundaries.height - (_paddingTop * 2)) / (itemHeight + _verticalSpacing));
        _totalPage = Math.ceil(_items.length / _itemVisibleCount);

        var i:int;

        for (i = 0; i < _items.length; i++)
        {
            _items[i].visible = false;
        }

        var visibleItems:Vector.<ListItemRenderer> = _items.slice(_startIndex, _startIndex + _itemVisibleCount);

        for (i = 0; i < visibleItems.length; i++)
        {
            visibleItems[i].visible = true;
            visibleItems[i].x = 0;
            visibleItems[i].y = _paddingTop + (i * itemHeight) + (i * _verticalSpacing) + (itemHeight / 2) - (_boundaries.height / 2);
        }
    }
}
}
