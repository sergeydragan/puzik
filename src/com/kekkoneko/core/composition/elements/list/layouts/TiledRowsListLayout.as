/**
 * Created by SeeD on 04/01/2015.
 */
package com.kekkoneko.core.composition.elements.list.layouts
{
import com.kekkoneko.core.composition.elements.list.ListItemRenderer;

import flash.geom.Rectangle;

public class TiledRowsListLayout extends ListLayout
{
    public function TiledRowsListLayout(items:Vector.<ListItemRenderer>, boundaries:Rectangle)
    {
        super(items, boundaries);
    }

    override public function placeItems():void
    {
        if (!_items || _items.length == 0)
        {
            return;
        }

        var itemWidth:int = _items[0].width;
        var itemHeight:int = _items[0].height;
        var itemsInRow:int = Math.floor((_boundaries.width - (_paddingLeft * 2)) / (itemWidth + _horizontalSpacing));
        var itemsInCol:int = Math.floor((_boundaries.height - (_paddingTop * 2)) / (itemHeight + _verticalSpacing));

        _itemVisibleCount = itemsInRow * itemsInCol;
        _totalPage = Math.ceil(_items.length / _itemVisibleCount);

        var col:int;
        var row:int;

        for (var i:int = 0; i < _items.length; i++)
        {
            _items[i].visible = false;
        }

        var visibleItems:Vector.<ListItemRenderer> = _items.slice(_startIndex, _startIndex + _itemVisibleCount);
        for (row = 0; row < itemsInCol; row++)
        {
            for (col = 0; col < itemsInRow; col++)
            {
                if (row * itemsInRow + col < visibleItems.length)
                {
                    visibleItems[row * itemsInRow + col].visible = true;
                    visibleItems[row * itemsInRow + col].x = _paddingLeft + (col * itemWidth) + (col * _horizontalSpacing) + (itemWidth / 2) - (_boundaries.width / 2);
                    visibleItems[row * itemsInRow + col].y = _paddingTop + (row * itemHeight) + (row * _verticalSpacing) + (itemHeight / 2) - (_boundaries.height / 2);
                }
            }
        }
    }
}
}
