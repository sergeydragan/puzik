/**
 * Created by SeeD on 02/01/2015.
 */
package com.kekkoneko.core.composition.elements.list
{
import com.kekkoneko.core.composition.CompositionView;
import com.kekkoneko.core.composition.vos.ListItemRendererDataVO;

public class ListItemRenderer extends CompositionView
{
    public var data:ListItemRendererDataVO;

    public function ListItemRenderer()
    {
    }

    public function commitData():void
    {

    }
}
}
