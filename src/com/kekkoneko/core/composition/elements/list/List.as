/**
 * Created by SeeD on 02/01/2015.
 */
package com.kekkoneko.core.composition.elements.list
{
import com.kekkoneko.core.composition.CompositionView;
import com.kekkoneko.core.composition.elements.*;
import com.kekkoneko.core.composition.elements.list.layouts.HorizontalListLayout;
import com.kekkoneko.core.composition.elements.list.layouts.ListLayout;
import com.kekkoneko.core.composition.elements.list.layouts.ListLayoutTypes;
import com.kekkoneko.core.composition.elements.list.layouts.TiledColsListLayout;
import com.kekkoneko.core.composition.elements.list.layouts.TiledRowsListLayout;
import com.kekkoneko.core.composition.elements.list.layouts.VerticalListLayout;
import com.kekkoneko.core.composition.models.ListItemRenderersModel;
import com.kekkoneko.core.composition.vos.ListItemRendererDataVO;

import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.display.InteractiveObject;
import flash.events.MouseEvent;
import flash.geom.Rectangle;

public class List extends InterfaceElement
{
    private var _itemAppearanceClass:Class;
    private var _itemClass:Class;
    private var _items:Vector.<ListItemRenderer>;
    private var _boundaries:Rectangle;
    private var _layout:ListLayout;

    private var _prevPageButton:InteractiveObject;
    private var _nextPageButton:InteractiveObject;

    private var _hasSwitchPageButtons:Boolean;

    private static const ITEM:String = "item";
    private static const PLACEHOLDER:String = "placeholder";

    public function List(appearance:DisplayObject, owner:CompositionView)
    {
        _items = new <ListItemRenderer>[];
        var boundariesPlaceholder:DisplayObject = (appearance as DisplayObjectContainer).getChildByName(PLACEHOLDER);
        _boundaries = new Rectangle(boundariesPlaceholder.x, boundariesPlaceholder.y, boundariesPlaceholder.width, boundariesPlaceholder.height);
        boundariesPlaceholder.visible = false;

        super(appearance, owner);
    }

    override public function init():void
    {
        _itemClass = ListItemRenderersModel.getListItemRendererByListName(name);
        _itemAppearanceClass = ListItemRenderersModel.getListItemRendererAppearanceByListName(name);

        const container:DisplayObjectContainer = _appearance as DisplayObjectContainer;

        if (container.getChildByName(ITEM))
        {
            container.removeChild(container.getChildByName(ITEM));
        }

        _prevPageButton = container.getChildByName("btn_back") as InteractiveObject;
        _nextPageButton = container.getChildByName("btn_forward") as InteractiveObject;

        _hasSwitchPageButtons = _prevPageButton && _nextPageButton;

        if (_hasSwitchPageButtons)
        {
            _prevPageButton.addEventListener(MouseEvent.CLICK, onClick_prevPageButton);
            _nextPageButton.addEventListener(MouseEvent.CLICK, onClick_nextPageButton);
        }
    }

    public function setData(data:Array):void
    {
        // todo: make some kind of object pooling here
        if (_items.length > 0)
        {
            for each (var currentItem:ListItemRenderer in _items)
            {
                currentItem.removeFromContainer();
                currentItem.destroy();
            }
            _items.splice(0, _items.length);
        }

        for (var i:int = 0; i < data.length; i++)
        {
            var item:ListItemRenderer = getItemRendererInstance(data[i]);
            _items.push(item);
            item.addToContainer(_appearance as DisplayObjectContainer);
        }

        _layout.placeItems();

        if (_hasSwitchPageButtons) validateSwitchButtons();
    }

    public function update(index:int = -1):void
    {
        if (index >= 0)
        {
            _items[index].commitData();
        }
        else
        {
            for each (var currentItem:ListItemRenderer in _items)
            {
                currentItem.commitData();
            }
        }
    }

    private function getItemRendererInstance(dataVO:ListItemRendererDataVO):ListItemRenderer
    {
        var listItemRenderer:ListItemRenderer = new _itemClass as ListItemRenderer;
        listItemRenderer.data = dataVO;
        listItemRenderer.data.listName = name;
        listItemRenderer.setAppearance(new _itemAppearanceClass());
        listItemRenderer.setOwner(owner);
        listItemRenderer.commitData();
        return listItemRenderer;
    }

    public function setLayoutType(layoutType:String):void
    {
        if (layoutType == ListLayoutTypes.HORIZONTAL)
        {
            _layout = new HorizontalListLayout(_items, _boundaries);
        }
        else if (layoutType == ListLayoutTypes.VERTICAL)
        {
            _layout = new VerticalListLayout(_items, _boundaries);
        }
        else if (layoutType == ListLayoutTypes.TILED_ROWS)
        {
            _layout = new TiledRowsListLayout(_items, _boundaries);
        }
        else if (layoutType == ListLayoutTypes.TILED_COLS)
        {
            _layout = new TiledColsListLayout(_items, _boundaries);
        }

        if (_layout)
        {
            _layout.placeItems();
        }
    }

    public function getListItemIndexByParameter(parameterName:String, parameterValue:Object):int
    {
        for (var i:int = 0; i < _items.length; i++)
        {
            if (_items[i].data[parameterName] && _items[i].data[parameterName] == parameterValue)
            {
                return i;
            }
        }
        return -1;
    }

    public function getListItemByIndex(index:int):ListItemRenderer
    {
        return _items[index];
    }

    public function get layout():ListLayout
    {
        return _layout;
    }

    public function gotoFirstPage():void
    {
        _layout.currentPage = 0;
        validateSwitchButtons();
    }

    public function nextPage():void
    {
        _layout.currentPage++;
        validateSwitchButtons();
    }

    public function prevPage():void
    {
        _layout.currentPage--;
        validateSwitchButtons();
    }

    override public function destroy():void
    {
        if (_items.length > 0)
        {
            for each (var currentItem:ListItemRenderer in _items)
            {
                currentItem.removeFromContainer();
                currentItem.destroy();
            }
            _items.splice(0, _items.length);
        }

        if (_nextPageButton) _nextPageButton.removeEventListener(MouseEvent.CLICK, onClick_nextPageButton);
        if (_prevPageButton) _nextPageButton.removeEventListener(MouseEvent.CLICK, onClick_prevPageButton);

        super.destroy();
    }

    private function validateSwitchButtons():void
    {
        if (_layout.totalPage <= 1)
        {
            _prevPageButton.visible = _nextPageButton.visible = false;
        }
        else
        {
            _prevPageButton.visible = (_layout.currentPage != 0);
            _nextPageButton.visible = (_layout.currentPage < _layout.totalPage - 1);
        }
    }

    private function onClick_nextPageButton(event:MouseEvent):void
    {
        nextPage();
    }

    private function onClick_prevPageButton(event:MouseEvent):void
    {
        prevPage();
    }
}
}
