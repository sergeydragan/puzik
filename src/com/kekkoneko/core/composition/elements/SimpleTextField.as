/**
 * Created by SeeD on 07/09/2014.
 */
package com.kekkoneko.core.composition.elements
{
import com.kekkoneko.core.composition.CompositionView;

import flash.display.DisplayObject;
import flash.text.TextField;

public class SimpleTextField extends InterfaceElement
{
    public function SimpleTextField(appearance:DisplayObject, owner:CompositionView)
    {
        super(appearance, owner);
        (appearance as TextField).mouseEnabled = false;
    }

    public function setText(text:String):void
    {
        (_appearance as TextField).text = text ? text : "";
    }
}
}
