package com.kekkoneko.core.composition.elements
{
import com.greensock.TweenLite;
import com.kekkoneko.core.composition.CompositionView;

import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.display.Sprite;
import flash.text.TextField;

public class ProgressBar extends InterfaceElement
{
    public static const TYPE_PLAIN:String = "TYPE_PLAIN";
    public static const TYPE_TIME:String = "TYPE_TIME";

    private static const FULL_SCALE_TWEEN_TIME:Number = 1;

    private var _percent:Percent = new Percent();

    private var _isLabelVisible:Boolean = true;
    private var _labelType:String = TYPE_PLAIN;

    private var barMask:Sprite;
    private var label:TextField;
    private var updateTween:TweenLite;

    public function ProgressBar(appearance:DisplayObject, owner:CompositionView)
    {
        super(appearance, owner);

        extractElements();
        validate();
    }

    public function getCurrentValue():int
    {
        return _percent.currentValue;
    }

    public function setCurrentValue(currentValue:int, useTween:Boolean = true):void
    {
        if (useTween)
        {
            const valuesDifference:Number = Math.abs(_percent.decimalPerenctage - _percent.getDecimalPercentageForValue(currentValue));
            const tweenTime:Number = valuesDifference * FULL_SCALE_TWEEN_TIME;

            updateTween = TweenLite.to(_percent, tweenTime, {currentValue:currentValue, onUpdate:validate});
        }
        else
        {
            _percent.currentValue = currentValue;
            validate();
        }
    }

    public function getMaxValue():int
    {
        return _percent.maxValue;
    }

    public function setMaxValue(maxValue:int):void
    {
        _percent.maxValue = maxValue;
        validate();
    }

    public function setLabelType(labelType:String):void
    {
        _labelType = labelType;
    }

    public function get isLabelVisible():Boolean
    {
        return _isLabelVisible;
    }

    public function set isLabelVisible(isLabelVisible:Boolean):void
    {
        _isLabelVisible = isLabelVisible;
    }

    private function extractElements():void
    {
        barMask = (_appearance as DisplayObjectContainer).getChildByName("barMask") as Sprite;
        label = (_appearance as DisplayObjectContainer).getChildByName("txt_value") as TextField;
    }

    private function validate():void
    {
        barMask.scaleX = _percent.decimalPerenctage;
        if (label)
        {
            label.text = _percent.currentValue + " / " + _percent.maxValue;
        }
    }

    override public function destroy():void
    {
        if (updateTween && updateTween.isActive())
        {
            updateTween.kill();
        }

        updateTween = null;
        barMask = null;
        label = null;

        super.destroy();
    }

}
}
class Percent
{
    private var _currentValue:int = 0;
    private var _maxValue:int;

    public function Percent(maxValue:int = 100)
    {
        _maxValue = maxValue;
    }

    public function get currentValue():int
    {
        return _currentValue;
    }

    public function set currentValue(currentValue:int):void
    {
        _currentValue = currentValue;
    }

    public function get maxValue():int
    {
        return _maxValue;
    }

    public function set maxValue(maxValue:int):void
    {
        _maxValue = maxValue;
    }

    public function get decimalPerenctage():Number
    {
        return getDecimalPercentageForValue(currentValue);
    }

    public function getDecimalPercentageForValue(value:int):Number
    {
        return 1 / _maxValue * value;
    }

}