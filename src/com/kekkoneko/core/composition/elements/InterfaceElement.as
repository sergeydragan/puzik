/**
 * Created by SeeD on 07/09/2014.
 */
package com.kekkoneko.core.composition.elements
{
import com.kekkoneko.core.composition.CompositionView;

import flash.display.DisplayObject;
import flash.events.EventDispatcher;

public class InterfaceElement extends EventDispatcher
{
    protected var _appearance:DisplayObject;
    private var _owner:CompositionView;

    public function InterfaceElement(appearance:DisplayObject, owner:CompositionView)
    {
        _appearance = appearance;
        _owner = owner;
        init();
    }

    public function init():void
    {

    }

    public function destroy():void
    {

    }

    public function get name():String
    {
        return _appearance.name;
    }

    public function get alpha():Number
    {
        return _appearance.alpha;
    }

    public function set alpha(value:Number):void
    {
        _appearance.alpha = value;
    }

    public function get visible():Boolean
    {
        return _appearance.visible;
    }

    public function set visible(value:Boolean):void
    {
        _appearance.visible = value;
    }

    public function get owner():CompositionView
    {
        return _owner;
    }
}
}
