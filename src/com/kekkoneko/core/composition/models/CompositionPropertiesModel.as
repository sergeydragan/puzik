/**
 * Created by SeeD on 28/12/2014.
 */
package com.kekkoneko.core.composition.models
{
import com.kekkoneko.core.composition.vos.CompositionPropertiesVO;
import com.kekkoneko.core.debug.Debug;

import flash.utils.Dictionary;

public class CompositionPropertiesModel
{
    private var _properties:Dictionary;

    public function CompositionPropertiesModel()
    {
        _properties = new Dictionary();
    }

    public function addPropertiesByCompositionName(compositionName:String, propertiesVO:CompositionPropertiesVO):void
    {
        _properties[compositionName] = propertiesVO;
    }

    public function getPropertiesByCompositionName(compositionName:String):CompositionPropertiesVO
    {
        if (!_properties[compositionName])
        {
            Debug.info("CompositionPropertiesVO for composition " + compositionName + " is not registered");
        }
        return _properties[compositionName];
    }

    public function removePropertiesByCompositionName(compositionName:String):void
    {
        delete _properties[compositionName];
    }
}
}
