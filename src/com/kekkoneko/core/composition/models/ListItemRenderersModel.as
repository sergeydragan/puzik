/**
 * Created by SeeD on 03/01/2015.
 */
package com.kekkoneko.core.composition.models
{
import com.kekkoneko.core.debug.Debug;

import flash.utils.Dictionary;

public class ListItemRenderersModel
{
    // todo: сделать это нормальной моделью, без статических методов и переменных
    public static var itemRenderers:Dictionary = new Dictionary();
    public static var itemRendererAppearances:Dictionary = new Dictionary();

    public function ListItemRenderersModel()
    {
    }

    public static function registerListItemRenderer(itemRendererAppearanceClass:Class, itemRendererClass:Class):void
    {
        if (itemRenderers[itemRendererAppearanceClass])
        {
            Debug.warning("ListItemRenderer for appearance class " + itemRendererAppearanceClass + " is already registered");
        }
        itemRenderers[itemRendererAppearanceClass] = itemRendererClass;
    }

    public static function registerListItemRendererByListName(listName:String, itemRendererAppearanceClass:Class, itemRendererClass:Class):void
    {
        if (itemRenderers[listName])
        {
            Debug.warning("ListItemRenderer for list named " + listName + " is already registered");
        }
        if (itemRendererAppearances[listName])
        {
            Debug.warning("ListItemRendererAppearance for list named " + listName + " is already registered");
        }
        itemRenderers[listName] = itemRendererClass;
        itemRendererAppearances[listName] = itemRendererAppearanceClass;
    }

    public static function getListItemRenderer(itemRendererAppearanceClass:Class):Class
    {
        if (!itemRenderers[itemRendererAppearanceClass])
        {
            Debug.warning("ListItemRenderer for appearance class " + itemRendererAppearanceClass + " is not registered");
        }
        return itemRenderers[itemRendererAppearanceClass];
    }

    public static function getListItemRendererByListName(listName:String):Class
    {
        if (!itemRenderers[listName])
        {
            Debug.warning("ListItemRenderer for list named " + listName + " is not registered");
        }
        return itemRenderers[listName];
    }

    public static function getListItemRendererAppearanceByListName(listName:String):Class
    {
        if (!itemRendererAppearances[listName])
        {
            Debug.warning("ListItemRendererAppearance for list named " + listName + " is not registered");
        }
        return itemRendererAppearances[listName];
    }
}
}
