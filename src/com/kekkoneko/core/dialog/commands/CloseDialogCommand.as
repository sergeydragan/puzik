/**
 * Created by SeeD on 07/09/2014.
 */
package com.kekkoneko.core.dialog.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.core.dialog.model.DialogDisplayModel;
import com.kekkoneko.core.dialog.model.DialogModel;
import com.kekkoneko.core.dialog.vos.CloseDialogVO;

public class CloseDialogCommand extends Command
{
    [Inject]
    public var dialogModel:DialogModel;

    [Inject]
    public var dialogDisplayModel:DialogDisplayModel;

    public function CloseDialogCommand()
    {
        super();
    }

    override public function execute():void
    {
        var dialogProperties:CloseDialogVO = _payload as CloseDialogVO;
        // remove dialog
        dialogDisplayModel.removeDialogByName(dialogProperties.name);
    }


}
}
