/**
 * Created by SeeD on 07/09/2014.
 */
package com.kekkoneko.core.dialog.commands
{
import com.kekkoneko.core.composition.models.CompositionPropertiesModel;
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.core.dialog.*;
import com.kekkoneko.core.dialog.model.DialogDisplayModel;
import com.kekkoneko.core.dialog.model.DialogModel;
import com.kekkoneko.core.dialog.vos.DialogVO;
import com.kekkoneko.core.dialog.vos.ShowDialogPropertiesVO;

public class ShowDialogCommand extends Command
{
    [Inject]
    public var dialogModel:DialogModel;

    [Inject]
    public var dialogDisplayModel:DialogDisplayModel;

    [Inject]
    public var compositionPropertiesModel:CompositionPropertiesModel;

    public function ShowDialogCommand()
    {
        super();
    }

    override public function execute():void
    {
        var dialogProperties:ShowDialogPropertiesVO = _payload as ShowDialogPropertiesVO;
        var mediator:DialogMediator = dialogModel.getDialogMediator(dialogProperties.name);
        injector.injectInto(mediator);
        var view:DialogView = dialogModel.getDialogView(dialogProperties.name);
        view.setAppearance(dialogModel.getDialogAppearance(dialogProperties.name));
        mediator.setView(view);

        var dialogVO:DialogVO = new DialogVO(view, mediator, dialogProperties);

        // save properties to PropertiesModel
        compositionPropertiesModel.addPropertiesByCompositionName(dialogProperties.name, dialogProperties.properties);

        // add dialog (mediator + view) to vector
        dialogDisplayModel.pushDialogToQueue(dialogVO);

        /* todo: not sure if it's good to init mediator before we make sure that dialog is being
         displayed (it was pushed into queue line above, and might be displayed not now, but after
         first dialog in queue is closed)
         */
        mediator.init();
    }


}
}
