/**
 * Created by SeeD on 09/09/2014.
 */
package com.kekkoneko.core.dialog.model
{
import com.kekkoneko.core.dialog.constants.DialogPriorities;
import com.kekkoneko.core.dialog.vos.DialogVO;
import com.kekkoneko.core.screen.Screen;

import flash.display.DisplayObjectContainer;
import flash.display.Sprite;

public class DialogDisplayModel
{
    private var _container:DisplayObjectContainer;
    private var _dialogs:Vector.<DialogVO>;
    private var _background:Sprite;

    public function DialogDisplayModel()
    {
        _dialogs = new <DialogVO>[];
    }

    public function setContainer(container:DisplayObjectContainer):void
    {
        _container = container;
    }

    public function pushDialogToQueue(dialogVO:DialogVO):void
    {
        if (dialogVO.properties.priority == DialogPriorities.IMMEDIATE)
        {
            _dialogs.unshift(dialogVO);
            displayDialog(0);
        }
        else
        {
            if (_dialogs.length > 0)
            {
                for (var i:int = 0; i < _dialogs.length; i++)
                {
                    if (_dialogs[i].properties.priority <= dialogVO.properties.priority)
                    {
                        if (i == _dialogs.length - 1)
                        {
                            _dialogs.push(dialogVO);
                            break;
                        }
                    }
                    else
                    {
                        _dialogs.splice(i, 0, dialogVO)
                    }
                }

                if (_dialogs.length == 1)
                {
                    displayDialog(0);
                }
            }
            else
            {
                _dialogs.push(dialogVO);
                displayDialog(0);
            }
        }
    }

    private function displayDialog(index:int):void
    {
        displayBackgroundIfNeeded();
        _dialogs[index].view.addToContainer(_container);
        _dialogs[index].view.x = Screen.width / 2;
        _dialogs[index].view.y = Screen.height / 2;
    }

    private function displayBackgroundIfNeeded():void
    {
        if (!_background)
        {
            _background = new Sprite();
            _background.graphics.beginFill(0x000000, 0.5);
            _background.graphics.drawRect(0, 0, Screen.width, Screen.height);
            _background.graphics.endFill();
        }

        if (!_background.parent)
        {
            _container.addChildAt(_background, 0);
        }
    }

    private function removeDialogByIndex(index:int):void
    {
        hideDialogByIndex(index);
        _dialogs[index].mediator.destroy();
        _dialogs[index].view.destroy();
        _dialogs.splice(index, 1);
        removeBackgroundIfNeeded();
    }

    private function showNextDialog():void
    {
        if (_dialogs.length > 0)
        {
            displayDialog(0);
        }
    }

    public function removeDialogByName(name:String):void
    {
        var dialogsToRemove:Vector.<DialogVO> = new <DialogVO>[];
        var i:int = 0;

        for (i = 0; i < _dialogs.length; i++)
        {
            if (_dialogs[i].properties.name == name)
            {
                dialogsToRemove.push(_dialogs[i]);
            }
        }

        for (i = 0; i < dialogsToRemove.length; i++)
        {
            removeDialogByIndex(_dialogs.indexOf(dialogsToRemove[i]));
        }

        dialogsToRemove.length = 0;
        showNextDialog();
    }

    private function hideDialogByIndex(index:int):void
    {
        if (_dialogs[index].view.parent)
        {
            _dialogs[index].view.removeFromContainer();
        }
    }

    private function removeBackgroundIfNeeded():void
    {
        if (_dialogs.length == 0)
        {
            if (_background && _background.parent)
            {
                _background.parent.removeChild(_background);
            }
        }
    }

}
}
