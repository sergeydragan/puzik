/**
 * Created by SeeD on 04/09/2014.
 */
package com.kekkoneko.core.dialog.model
{
import com.kekkoneko.core.dialog.*;

import flash.display.Sprite;
import flash.utils.Dictionary;

public class DialogModel
{
    private var _viewsDictionary:Dictionary;
    private var _mediatorsDictionary:Dictionary;
    private var _appearancesDictionary:Dictionary;

    public function DialogModel()
    {
        _viewsDictionary = new Dictionary();
        _mediatorsDictionary = new Dictionary();
        _appearancesDictionary = new Dictionary();
    }

    public function registerDialog(dialogName:String, view:Class, mediator:Class, appearance:Class):void
    {
        _viewsDictionary[dialogName] = view;
        _mediatorsDictionary[dialogName] = mediator;
        _appearancesDictionary[dialogName] = appearance;
    }

    public function getDialogMediator(dialogName:String):DialogMediator
    {
        return new (_mediatorsDictionary[dialogName] as Class);
    }

    public function getDialogView(dialogName:String):DialogView
    {
        return new (_viewsDictionary[dialogName] as Class);
    }

    public function getDialogAppearance(dialogName:String):Sprite
    {
        return new (_appearancesDictionary[dialogName] as Class);
    }

}
}
