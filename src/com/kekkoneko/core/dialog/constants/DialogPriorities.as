/**
 * Created by SeeD on 07/09/2014.
 */
package com.kekkoneko.core.dialog.constants
{
public class DialogPriorities
{
    public static const IMMEDIATE:int = 0;
    public static const NORMAL:int = 1;
    public static const LOW:int = 2;

    public function DialogPriorities()
    {
    }
}
}
