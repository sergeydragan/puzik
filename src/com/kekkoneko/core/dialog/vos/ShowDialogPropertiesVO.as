/**
 * Created by SeeD on 07/09/2014.
 */
package com.kekkoneko.core.dialog.vos
{
import com.kekkoneko.core.composition.vos.CompositionPropertiesVO;
import com.kekkoneko.core.dialog.constants.DialogPriorities;

public class ShowDialogPropertiesVO
{
    public var name:String;
    public var priority:int;
    public var properties:CompositionPropertiesVO;

    public function ShowDialogPropertiesVO(name:String, dialogProperties:CompositionPropertiesVO = null, priority:int = DialogPriorities.NORMAL)
    {
        this.name = name;
        this.priority = priority;
        this.properties = dialogProperties;
    }
}
}
