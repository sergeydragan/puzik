/**
 * Created by SeeD on 07/09/2014.
 */
package com.kekkoneko.core.dialog.vos
{
public class CloseDialogVO
{
    public var name:String;

    public function CloseDialogVO(name:String)
    {
        this.name = name;
    }
}
}
