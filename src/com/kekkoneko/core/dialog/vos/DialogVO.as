/**
 * Created by SeeD on 09/09/2014.
 */
package com.kekkoneko.core.dialog.vos
{
import com.kekkoneko.core.dialog.DialogMediator;
import com.kekkoneko.core.dialog.DialogView;

public class DialogVO
{
    public var view:DialogView;
    public var mediator:DialogMediator;
    public var properties:ShowDialogPropertiesVO;

    public function DialogVO(view:DialogView, mediator:DialogMediator, properties:ShowDialogPropertiesVO)
    {
        this.view = view;
        this.mediator = mediator;
        this.properties = properties;
    }
}
}
