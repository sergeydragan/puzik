/**
 * Created by SeeD on 07/09/2014.
 */
package com.kekkoneko.core.debug
{
public class Debug
{
    private static const INFO:int = 0;
    private static const WARNING:int = 1;
    private static const ERROR:int = 2;

    public function Debug()
    {
    }

    private static function traceMessage(text:String, priority:int = INFO):void
    {
        var priorityText:String = "";
        switch (priority)
        {
            case INFO:
                priorityText = "INFO: ";
                break;
            case WARNING:
                priorityText = "WARNING: ";
                break;
            case ERROR:
                priorityText = "ERROR: ";
                throw new Error(priorityText + text);
        }

        trace(priorityText + text);
    }

    public static function info(text:String):void
    {
        traceMessage(text, INFO);
    }

    public static function warning(text:String):void
    {
        traceMessage(text, WARNING);
    }

    public static function error(text:String):void
    {
        traceMessage(text, ERROR);
        throw new Error(text);
    }
}
}
