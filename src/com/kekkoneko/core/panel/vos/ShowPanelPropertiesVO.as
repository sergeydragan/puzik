/**
 * Created by SeeD on 07/09/2014.
 */
package com.kekkoneko.core.panel.vos
{
import com.kekkoneko.core.composition.vos.CompositionPropertiesVO;
import com.kekkoneko.core.panel.constants.PanelPositions;

import flash.geom.Point;

public class ShowPanelPropertiesVO
{
    public var name:String;
    public var position:String;
    public var customPosition:Point;
    public var properties:CompositionPropertiesVO;

    public function ShowPanelPropertiesVO(name:String, position:String = PanelPositions.TOP_CENTER, customPosition:Point = null, properties:CompositionPropertiesVO = null)
    {
        this.name = name;
        this.position = position;
        this.customPosition = customPosition;
        this.properties = properties;
    }
}
}
