/**
 * Created by SeeD on 25/11/2014.
 */
package com.kekkoneko.core.panel.vos
{
import com.kekkoneko.core.panel.PanelMediator;
import com.kekkoneko.core.panel.PanelView;

public class PanelVO
{
    public var view:PanelView;
    public var mediator:PanelMediator;
    public var properties:ShowPanelPropertiesVO;

    public function PanelVO(view:PanelView, mediator:PanelMediator, properties:ShowPanelPropertiesVO)
    {
        this.view = view;
        this.mediator = mediator;
        this.properties = properties;
    }
}
}
