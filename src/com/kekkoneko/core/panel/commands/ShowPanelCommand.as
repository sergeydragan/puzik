/**
 * Created by SeeD on 07/09/2014.
 */
package com.kekkoneko.core.panel.commands
{
import com.kekkoneko.core.composition.models.CompositionPropertiesModel;
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.core.panel.PanelDisplayModel;
import com.kekkoneko.core.panel.PanelMediator;
import com.kekkoneko.core.panel.PanelModel;
import com.kekkoneko.core.panel.PanelView;
import com.kekkoneko.core.panel.vos.PanelVO;
import com.kekkoneko.core.panel.vos.ShowPanelPropertiesVO;

public class ShowPanelCommand extends Command
{
    [Inject]
    public var panelModel:PanelModel;

    [Inject]
    public var panelDisplayModel:PanelDisplayModel;

    [Inject]
    public var compositionPropertiesModel:CompositionPropertiesModel;

    public function ShowPanelCommand()
    {
        super();
    }

    override public function execute():void
    {
        var panelProperties:ShowPanelPropertiesVO = _payload as ShowPanelPropertiesVO;
        var mediator:PanelMediator = panelModel.getPanelMediator(panelProperties.name);
        injector.injectInto(mediator);
        var view:PanelView = panelModel.getPanelView(panelProperties.name);
        view.setAppearance(panelModel.getPanelAppearance(panelProperties.name));
        mediator.setView(view);

        var panelVO:PanelVO = new PanelVO(view, mediator, panelProperties);

        // save properties to PropertiesModel
        compositionPropertiesModel.addPropertiesByCompositionName(panelProperties.name, panelProperties.properties);

        // add dialog (mediator + view) to vector
        panelDisplayModel.showPanel(panelVO);

        mediator.init();
    }


}
}
