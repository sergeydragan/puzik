/**
 * Created by SeeD on 07/09/2014.
 */
package com.kekkoneko.core.panel.commands
{
import com.kekkoneko.core.controller.Command;
import com.kekkoneko.core.panel.PanelDisplayModel;
import com.kekkoneko.core.panel.vos.ClosePanelVO;

public class ClosePanelCommand extends Command
{
    [Inject]
    public var panelDisplayModel:PanelDisplayModel;

    public function ClosePanelCommand()
    {
        super();
    }

    override public function execute():void
    {
        var panelProperties:ClosePanelVO = _payload as ClosePanelVO;
        // remove dialog
        panelDisplayModel.removePanelByName(panelProperties.name);
    }


}
}
