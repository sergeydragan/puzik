/**
 * Created by SeeD on 09/09/2014.
 */
package com.kekkoneko.core.panel
{
import com.kekkoneko.core.panel.constants.PanelPositions;
import com.kekkoneko.core.panel.vos.PanelVO;
import com.kekkoneko.core.screen.Screen;

import flash.display.DisplayObjectContainer;

public class PanelDisplayModel
{
    private var _container:DisplayObjectContainer;
    private var _panels:Vector.<PanelVO>;

    public function PanelDisplayModel()
    {
        _panels = new <PanelVO>[];
    }

    public function setContainer(container:DisplayObjectContainer):void
    {
        _container = container;
    }

    public function showPanel(panelVO:PanelVO):void
    {
        _panels.push(panelVO);
        _panels[_panels.length - 1].view.addToContainer(_container);
        alignPanel(_panels[_panels.length - 1]);
    }

    private function alignPanel(panel:PanelVO):void
    {
        if (panel.properties.customPosition)
        {
            panel.view.x = panel.properties.customPosition.x;
            panel.view.y = panel.properties.customPosition.y;
            return;
        }

        var halfWidth:int = panel.view.width / 2;
        var halfHeight:int = panel.view.height / 2;
        switch (panel.properties.position)
        {
            case PanelPositions.BOTTOM_CENTER:
                panel.view.x = Screen.width / 2;
                panel.view.y = Screen.height - halfHeight;
                break;

            case PanelPositions.BOTTOM_LEFT:
                panel.view.x = halfWidth;
                panel.view.y = Screen.height - halfHeight;
                break;

            case PanelPositions.BOTTOM_RIGHT:
                panel.view.x = Screen.width - halfWidth;
                panel.view.y = Screen.height - halfHeight;
                break;

            case PanelPositions.TOP_CENTER:
                panel.view.x = Screen.width / 2;
                panel.view.y = halfHeight;
                break;

            case PanelPositions.TOP_LEFT:
                panel.view.x = halfWidth;
                panel.view.y = halfHeight;
                break;

            case PanelPositions.TOP_RIGHT:
                panel.view.x = Screen.width - halfWidth;
                panel.view.y = halfHeight;
                break;

            case PanelPositions.CENTER_LEFT:
                panel.view.x = halfWidth;
                panel.view.y = Screen.height / 2;
                break;

            case PanelPositions.CENTER:
                panel.view.x = Screen.width / 2;
                panel.view.y = Screen.height / 2;
                break;

            case PanelPositions.CENTER_RIGHT:
                panel.view.x = Screen.width - halfWidth;
                panel.view.y = Screen.height / 2;
                break;
        }
    }

    public function removePanelByName(name:String):void
    {
        var panelsToRemove:Vector.<PanelVO> = new <PanelVO>[];
        var i:int = 0;

        for (i = 0; i < _panels.length; i++)
        {
            if (_panels[i].properties.name == name)
            {
                panelsToRemove.push(_panels[i]);
                _panels[i].mediator.destroy();
                _panels[i].view.destroy();
                hidePanelByIndex(i);
            }
        }

        for (i = 0; i < panelsToRemove.length; i++)
        {
            _panels.splice(_panels.indexOf(panelsToRemove[i]), 1);
        }

        panelsToRemove.length = 0;
    }

    private function hidePanelByIndex(index:int):void
    {
        if (_panels[index].view.parent)
        {
            _panels[index].view.removeFromContainer();
        }
    }

}
}
