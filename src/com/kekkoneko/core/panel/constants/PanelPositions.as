/**
 * Created by SeeD on 25/11/2014.
 */
package com.kekkoneko.core.panel.constants
{
public class PanelPositions
{
    public static const TOP_LEFT:String = "TopLeft";
    public static const TOP_RIGHT:String = "TopRight";
    public static const BOTTOM_LEFT:String = "BottomLeft";
    public static const BOTTOM_RIGHT:String = "BottomRight";
    public static const CENTER_LEFT:String = "CenterLeft";
    public static const CENTER_RIGHT:String = "CenterRight";
    public static const TOP_CENTER:String = "TopCenter";
    public static const BOTTOM_CENTER:String = "BottomCenter";
    public static const CENTER:String = "Center";

    public function PanelPositions()
    {
    }
}
}
