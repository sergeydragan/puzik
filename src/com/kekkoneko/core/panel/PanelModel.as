/**
 * Created by SeeD on 04/09/2014.
 */
package com.kekkoneko.core.panel
{
import flash.display.Sprite;
import flash.utils.Dictionary;

public class PanelModel
{
    private var _viewsDictionary:Dictionary;
    private var _mediatorsDictionary:Dictionary;
    private var _appearancesDictionary:Dictionary;

    public function PanelModel()
    {
        _viewsDictionary = new Dictionary();
        _mediatorsDictionary = new Dictionary();
        _appearancesDictionary = new Dictionary();
    }

    public function registerPanel(panelName:String, view:Class, mediator:Class, appearance:Class):void
    {
        _viewsDictionary[panelName] = view;
        _mediatorsDictionary[panelName] = mediator;
        _appearancesDictionary[panelName] = appearance;
    }

    public function getPanelMediator(panelName:String):PanelMediator
    {
        return new (_mediatorsDictionary[panelName] as Class);
    }

    public function getPanelView(panelName:String):PanelView
    {
        return new (_viewsDictionary[panelName] as Class);
    }

    public function getPanelAppearance(panelName:String):Sprite
    {
        return new (_appearancesDictionary[panelName] as Class);
    }

}
}
