/**
 * Created by SeeD on 03/03/2015.
 */
package
{
import flash.display.DisplayObject;
import flash.display.MovieClip;
import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.MouseEvent;
import flash.events.ProgressEvent;
import flash.utils.getDefinitionByName;

[SWF(backgroundColor="#000000", width="800", height="600", frameRate="30")]
public class Preloader extends MovieClip
{
    private var _preloaderScreen:Preloader_appearance;

    private const BACKGROUND_MARGIN:int = 100;
    private const FOREGROUND_MARGIN:int = 200;

    private const BACKGROUND_MAX_OFFSET:int = 20;
    private const FOREGROUND_MAX_OFFSET:int = 60;

    public function Preloader()
    {
        super();

        _preloaderScreen = new Preloader_appearance();
        addChild(_preloaderScreen);

        addEventListener(Event.ENTER_FRAME, checkFrame);
        loaderInfo.addEventListener(ProgressEvent.PROGRESS, progress);
        loaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ioError);

        addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
    }

    private function onMouseMove(event:MouseEvent):void
    {
        var relationalMouseOffset:Number = ((stage.stageWidth / 2) - stage.mouseX) / stage.stageWidth;
        _preloaderScreen.slidingForeground.x = FOREGROUND_MAX_OFFSET * relationalMouseOffset - FOREGROUND_MARGIN;
        _preloaderScreen.slidingBackground.x = BACKGROUND_MAX_OFFSET * relationalMouseOffset - BACKGROUND_MARGIN;
    }

    private function ioError(e:IOErrorEvent):void
    {
        trace(e.text);
    }

    private function progress(e:ProgressEvent):void
    {
        var loadedPart:Number = e.bytesLoaded / e.bytesTotal;
        var frameToSet:int = _preloaderScreen.animatedLogo.totalFrames * loadedPart;
        _preloaderScreen.animatedLogo.gotoAndStop(frameToSet);
    }

    private function checkFrame(e:Event):void
    {
        if (currentFrame == totalFrames)
        {
            stop();
            loadingFinished();
        }
    }

    private function loadingFinished():void
    {
        removeEventListener(Event.ENTER_FRAME, checkFrame);
        loaderInfo.removeEventListener(ProgressEvent.PROGRESS, progress);
        loaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, ioError);
        removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);

        removeChild(_preloaderScreen);
        startup();
    }

    private function startup():void
    {
        var mainClass:Class = getDefinitionByName("Main") as Class;
        addChild(new mainClass() as DisplayObject);
    }
}
}
